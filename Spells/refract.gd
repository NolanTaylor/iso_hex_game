extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 2
const PROPERTIES : Array = [ "spell", "optics", "reaction", "refract" ]

func _ready():
	pass
	
func init(casting_unit) -> void:
	if casting_unit.has_condition("natural_refraction"):
		target_behavior = "double_target"
		
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = Globals.filled_circle( \
		casting_unit.coords, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
	if casting_unit.has_condition("natural_refraction"):
		target_behavior = "double_target"
	else:
		final_pool.remove(final_pool.find(casting_unit.coords))
		
	return final_pool
	
func get_cost(casting_unit):
	spell_cost.light = 1
	spell_cost.timing = 1
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	
	.effect(casting_unit, target_coord)
