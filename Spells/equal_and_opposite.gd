extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 5
const PROPERTIES : Array = [ "spell", "kinematics", "equal_and_opposite" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = PoolVector3Array([])
	
	for direction in Globals.CUBE_VECTORS:
		final_pool.append_array(Globals.rigid_line(casting_unit.coords, \
			direction, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES)))
		
	return final_pool
	
func get_cost(casting_unit):
	spell_cost.work = 2
	spell_cost.timing = 3
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var unit = grid.get_unit(target_coord)
	
	grid.get_parent().popup_equal_and_opposite()
	var decision : String = yield(grid.get_parent(), "push_or_pull")
	var away : Vector3 = Globals.hex_direction(casting_unit.coords, unit.coords)
	var toward : Vector3 = Globals.hex_opposite(away)
	
	if Globals.is_valid(unit):
		match decision:
			"push":
				grid.displace_unit(casting_unit, toward)
				grid.displace_unit(unit, away)
			"pull":
				grid.displace_unit(casting_unit, away)
				grid.displace_unit(unit, toward)
	
	.effect(casting_unit, target_coord)
