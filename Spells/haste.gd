extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 1
const PROPERTIES : Array = [ "spell", "wave", "haste" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return Globals.filled_circle(casting_unit.coords, \
		DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
	
func can_cast(casting_unit) -> bool:
	if casting_unit.actions >= 1:
		return true
		
	return false
	
func cast(casting_unit, target_coord : Vector3) -> void:
	var grid : Dictionary = Globals.get_hex_grid().grid
	show()
	casting_unit.actions -= 1
	var unit = grid[target_coord].get_occupying_unit()
	
	if unit.statuses.has("haste_action"):
		unit.statuses["haste_action"] += 1
	else:
		unit.statuses["haste_action"] = 1
		
	if unit.statuses.has("haste_swift"):
		unit.statuses["haste_swift"] += 1
	else:
		unit.statuses["haste_swift"] = 1
		
	unit.movement_left += 3
	
	position = Globals.grid_to_pixel(target_coord)
	casting_unit.cast(target_coord)
	casting_unit.apply_ui()
	unit.apply_ui()
	$sprite.play("cast")
