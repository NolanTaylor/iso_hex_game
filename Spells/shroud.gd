extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 1
const PROPERTIES : Array = [ "spell", "celestial", "shroud" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return Globals.filled_circle(casting_unit.coords, \
		DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
func get_cost(casting_unit):
	spell_cost.lune = 3
	spell_cost.timing = 3
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var unit = grid.get_unit(target_coord)
	
	if Globals.is_valid(unit):
		var duration : int = 1
		if unit.has_special_modification("shroud_3"):
			duration = 2
		unit.apply_condition("shroud", duration)
		
	.effect(casting_unit, target_coord)
