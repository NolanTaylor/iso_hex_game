extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 5
const DEFAULT_RADIUS : int = 2
const DEFAULT_DURATION : int = 2
const PROPERTIES : Array = [ "spell", "thermodynamics", "depressurize" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	var spell_range : int = DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES)
	
	return Globals.filled_circle(casting_unit.coords, spell_range)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var spell_range : int = DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES)
	
	return Globals.filled_circle(casting_unit.coords, spell_range + 1)
	
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	return Globals.ring(target_coord, 1)
	
func get_cost(casting_unit):
	spell_cost.entropy = 3
	spell_cost.timing = 3
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var modifiers : Array = []
	var cells : PoolVector3Array = get_flex_cells(casting_unit, target_coord)
	cells.append(target_coord)
	
	for cell in cells:
		grid.apply_terrain(cell, "depressurize", modifiers)
		
	.effect(casting_unit, target_coord)
