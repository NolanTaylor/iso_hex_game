extends "res://Spells/spell.gd"

const DEFAULT_DAMAGE : int = 2
const DEFAULT_RANGE : int = 4
const PROPERTIES : Array = [ "spell", "thermo", "absolute_zero" ]

var range_used : int = 0
var damage_used : int = 0
var direction : String = "null"
var cells : PoolVector3Array = PoolVector3Array([])

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = PoolVector3Array([])
	
	for direction in Globals.CUBE_VECTORS:
		final_pool.append_array(Globals.rigid_line(casting_unit.coords, \
			direction, range_used))
		
	return final_pool
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	range_used = DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES)
	damage_used = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	
	return Globals.filled_circle(casting_unit.coords, range_used)
	
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	for direction in Globals.DIRECTIONS:
		if target_coord in Globals.rigid_line(casting_unit.coords, direction, \
		range_used):
			cells = Globals.wedge(casting_unit.coords, direction, range_used)
			self.direction = direction
			return cells
			
	return PoolVector3Array([])
	
func get_cost(casting_unit):
	spell_cost.entropy = 4
	spell_cost.timing = 3
	spell_cost.ult = true
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	var grid = Globals.get_hex_grid()
	var dict : Dictionary = {}
	
	for cell in get_flex_cells(casting_unit, target_coord):
		if grid.has_unit(cell):
			dict[cell] = str(damage_used)
			
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	match direction:
		"north":
			$sprite.rotation_degrees = 0
		"north_east":
			$sprite.rotation_degrees = 70
		"south_east":
			$sprite.rotation_degrees = 110
		"south":
			$sprite.rotation_degrees = 180
		"south_west":
			$sprite.rotation_degrees = 250
		"north_west":
			$sprite.rotation_degrees = 290
			
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	
	for cell in cells:
		var unit = grid.get_unit(cell)
		
		if Globals.is_valid(unit):
			unit.apply_condition("stun")
			unit.deal_damage(damage_used)
			if grid.wait_for_spell and !ignore_yield:
				yield(grid, "resolve_current_spell")
				
	.effect(casting_unit, target_coord)
