extends "res://Spells/spell.gd"

const PROPERTIES : Array = [ "spell", "relativity", "wave_function_collapse" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return PoolVector3Array([casting_unit.coords])
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	if not Globals.is_valid(casting_unit.superposition):
		return PoolVector3Array([])
		
	return Globals.line(casting_unit.superposition.coords, casting_unit.coords)
	
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_cost(casting_unit):
	spell_cost.rapidity = 3
	spell_cost.timing = 3
	if is_reaction:
		spell_cost.timing = 1
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	
	if not Globals.is_valid(casting_unit.superposition):
		.effect(casting_unit, target_coord)
		return
		
	var s_pos : Vector3 = casting_unit.superposition.coords
	var u_pos : Vector3 = casting_unit.coords
	
	var units : Array = []
	
	for cell in Globals.line(s_pos, u_pos):
		var unit = grid.get_unit(cell)
		
		if !Globals.is_valid(unit):
			continue
		if unit == casting_unit:
			continue
			
		units.append(unit)
		
	casting_unit.superposition.teleport(u_pos)
		
	for unit in units:
		unit.apply_condition("stun")
		
		if grid.wait_for_spell and !ignore_yield:
			yield(grid, "resolve_current_spell")
			
	.effect(casting_unit, target_coord)
