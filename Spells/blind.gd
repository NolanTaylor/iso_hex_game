extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 3
const DEFAULT_DAMAGE : int = 0
const PROPERTIES : Array = [ "spell", "optics", "targeted", "blind" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = Globals.filled_circle( \
		casting_unit.coords, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
	final_pool.remove(final_pool.find(casting_unit.coords))
	
	return final_pool
	
func get_cost(casting_unit):
	spell_cost.light = 2
	spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	var dict : Dictionary = {}
	
	if damage_used == 0:
		return dict
	
	dict[target_coord] = str(damage_used)
	
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var unit = grid.get_unit(target_coord)
	
	if Globals.is_valid(unit):
		unit.apply_condition("blind")
		if casting_unit.has_special_modification("blind_2"):
			unit.apply_condition("lethargy", 2)
		unit.deal_damage(DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES), \
			casting_unit)
	
	.effect(casting_unit, target_coord)
