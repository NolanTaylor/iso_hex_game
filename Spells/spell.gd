extends Node2D

export var proper_name : String
export var target_type : String
export var target_behavior : String

enum SpellState {
	SPELL_CAST,
	SPELL_COUNTERED,
	SPELL_RESOLVE,
}

onready var spell_cost = preload("res://Global/DataStructures/casting_cost.tscn")

var state = SpellState.SPELL_CAST
var is_reaction : bool = false
var ignore_yield : bool = false
var alt_cast : bool = false
var targets : PoolVector3Array

signal unit_targeted(casting_unit, target_coord, spell)
signal unit_in_aoe(casting_unit, target_coord, flex_cells, spell)
signal cast_spell(casting_unit, target_coord, spell)
signal complete_cast(spell)
signal resume_cast

func _ready():
	spell_cost = spell_cost.instance()
	z_index = 2
	
func get_target_type() -> String:
	return target_type
	
func get_target_behavior() -> String:
	return target_behavior
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	# abstract function
	return PoolVector3Array([])
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	# abstract function
	return PoolVector3Array([])
	
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	# abstract function
	return PoolVector3Array([])
	
func get_cost(casting_unit):
	# abstract function
	return spell_cost
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	# abstract function
	return {}
	
func counter() -> void:
	state = SpellState.SPELL_COUNTERED
	emit_signal("resume_cast")
	
func resolve() -> void:
	state = SpellState.SPELL_RESOLVE
	emit_signal("resume_cast")
	
func hold_resolution() -> void:
	state = SpellState.SPELL_CAST
	
func ignore_yield() -> void:
	ignore_yield = true
	
func double() -> void:
	# abstract function
	return
	
func halve() -> void:
	# abstract function
	return
	
func init(casting_unit) -> void:
	# abstract function
	pass
	
func can_cast(casting_unit) -> bool:
	get_cost(casting_unit)
	
	if !Globals.is_valid(spell_cost):
		push_error("casting cost doesn't exist")
		return false
	
	if casting_unit.light < spell_cost.light:
		return false
	if casting_unit.work < spell_cost.work:
		return false
	if casting_unit.flux < spell_cost.flux:
		return false
	if casting_unit.entropy < spell_cost.entropy:
		return false
	if casting_unit.rapidity < spell_cost.rapidity:
		return false
	if casting_unit.lune < spell_cost.lune:
		return false
	if casting_unit.corrupted_health < spell_cost.corruption:
		return false
		
	match spell_cost.timing:
		1:
			if casting_unit.reactions == 0:
				return false
		2:
			if casting_unit.swift_actions == 0 and casting_unit.actions == 0:
				return false
		3:
			if casting_unit.actions == 0:
				return false
				
	if spell_cost.ult and casting_unit.has_cast_ult(proper_name):
		return false
		
	return true
	
func cast(casting_unit, target_coord : Vector3) -> void:
	get_cost(casting_unit)
	
	casting_unit.light -= spell_cost.light
	casting_unit.work -= spell_cost.work
	casting_unit.flux -= spell_cost.flux
	casting_unit.entropy -= spell_cost.entropy
	casting_unit.rapidity -= spell_cost.rapidity
	casting_unit.lune -= spell_cost.lune
	
	match spell_cost.timing:
		1:
			casting_unit.reactions -= 1
		2:
			if casting_unit.swift_actions >= 1:
				casting_unit.swift_actions -= 1
			else:
				casting_unit.actions -= 1
		3:
			casting_unit.actions -= 1
			
	if spell_cost.ult:
		casting_unit.cast_ult(proper_name)
		
	get_highlight_cells(casting_unit)
	get_flex_cells(casting_unit, target_coord)
	
	if casting_unit.has_condition("expulse") and spell_cost.work >= 3:
		casting_unit.apply_guard()
	
	casting_unit.get_node("sprite").connect("animation_finished", self, "animation", \
		[ casting_unit, target_coord ], CONNECT_ONESHOT)
	casting_unit.cast(target_coord)
	casting_unit.apply_ui()
	
func multi_cast(casting_unit, target_coords : PoolVector3Array) -> void:
	targets = target_coords
	
	cast(casting_unit, Vector3.ZERO)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	emit_signal("cast_spell", casting_unit, target_coord, self)
	
	if state == SpellState.SPELL_CAST:
		yield(self, "resume_cast")
	if state == SpellState.SPELL_COUNTERED:
		casting_unit.fizzle_toss()
		finish()
		return
	
	state = SpellState.SPELL_CAST
	
	match target_type:
		"player_unit", "enemy_unit", "any_unit", "unit":
			emit_signal("unit_targeted", casting_unit, target_coord, self)
		"non_unit", "unoccupied_space", "any":
			emit_signal("unit_in_aoe", casting_unit, target_coord, \
				get_flex_cells(casting_unit, target_coord), self)
		_:
			state = SpellState.SPELL_RESOLVE
		
	if state == SpellState.SPELL_CAST:
		yield(self, "resume_cast")
	if state == SpellState.SPELL_COUNTERED:
		casting_unit.fizzle_toss()
		finish()
		return
	if state == SpellState.SPELL_RESOLVE:
		pass
		
	casting_unit.resume_toss()
	$sprite.frame = 0
	show()
	$sprite.play("cast")
	
	if $sprite.playing:
		$sprite.connect("animation_finished", self, "effect", [ casting_unit, target_coord ], \
			CONNECT_ONESHOT)
	else:
		effect(casting_unit, target_coord)
		
func effect(casting_unit, target_coord : Vector3) -> void:
	$sprite.hide()
	$sprite.stop()
	
	if casting_unit.has_condition("momentum"):
		casting_unit.remove_condition("momentum")
		
	var grid = Globals.get_hex_grid()
	if grid.wait_for_spell() and !ignore_yield:
		yield(grid, "resolve_current_spell")
		
	casting_unit.apply_ui()
	finish()
	
func finish() -> void:
	hide()
	emit_signal("complete_cast", self)
