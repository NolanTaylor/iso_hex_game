extends "res://Spells/spell.gd"

const PROPERTIES : Array = [ "spell", "thermo", "guardian", "teleport" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var coords : PoolVector3Array = PoolVector3Array([])
	var allies = get_tree().get_nodes_in_group("PlayerUnits")
	allies.erase(casting_unit)
	
	for ally in allies:
		if is_reaction and ally.triggered == 0:
			continue
		for dir in Globals.CUBE_VECTORS:
			var new_coord : Vector3 = ally.coords + dir
			
			if not coords.has(new_coord):
				coords.append(new_coord)
				
	return coords
	
func get_flex_cells(casting_unit, target : Vector3) -> PoolVector3Array:
	var grid = Globals.get_hex_grid()
	var protected_unit
	
	for dir in Globals.CUBE_VECTORS:
		var coord : Vector3 = target + dir
		protected_unit = grid.get_unit(coord)
		
		if Globals.is_valid(protected_unit):
			if is_reaction and protected_unit.triggered == 0:
				continue
			elif protected_unit != casting_unit and protected_unit.is_in_group("PlayerUnits"):
				break
				
	if !Globals.is_valid(protected_unit):
		return PoolVector3Array([casting_unit.coords])
	else:
		return PoolVector3Array([protected_unit.coords])
	
func get_cost(casting_unit):
	spell_cost.entropy = 5
	if is_reaction:
		spell_cost.timing = 1
	else:
		spell_cost.timing = 3
	spell_cost.ult = true
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var protected_unit = grid.get_unit(get_flex_cells(casting_unit, target_coord)[0])
	var duration : int = 2
	
	if is_reaction:
		duration = 1
	
	grid.teleport_unit(casting_unit, target_coord, casting_unit)
	
	casting_unit.apply_condition("immune", duration)
	if Globals.is_valid(protected_unit):
		protected_unit.apply_condition("immune", duration)
	
	.effect(casting_unit, target_coord)
