extends "res://Spells/spell.gd"

const SMALL_RANGE : int = 2
const MED_RANGE : int = 4
const LARGE_RANGE : int = 5
const SMALL_THRESH : int = 1
const MED_THRESH : int = 3
const LARGE_THRESH : int = 6
const SMALL_DAMAGE : int = 1
const MED_DAMAGE : int = 1
const LARGE_DAMAGE : int = 2
const PROPERTIES : Array = [ "spell", "kinematics", "displace" ]

var range_used : int = 0
var damage_used : int = 0
var direction : String = "null"
var opposite_dir : String = "null"
var cells : PoolVector3Array = PoolVector3Array([])

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = PoolVector3Array([])
	
	for direction in Globals.CUBE_VECTORS:
		final_pool.append_array(Globals.rigid_line(casting_unit.coords, \
			direction, range_used + casting_unit.get_modification("range", PROPERTIES)))
		
	return final_pool
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	range_used = SMALL_RANGE
	damage_used = SMALL_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	
	return Globals.filled_circle(casting_unit.coords, \
		range_used + casting_unit.get_modification("range", PROPERTIES))
		
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	for direction in Globals.DIRECTIONS:
		if target_coord in Globals.rigid_line(casting_unit.coords, direction, \
		range_used + casting_unit.get_modification("range", PROPERTIES)):
			cells = Globals.wedge(casting_unit.coords, direction, \
				range_used + casting_unit.get_modification("range", PROPERTIES))
			self.direction = direction
			return cells
				
	return PoolVector3Array([])
	
func get_cost(casting_unit):
	spell_cost.work = 2
	spell_cost.timing = 3
	if casting_unit.has_special_modification("displace_3"):
		spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	var grid = Globals.get_hex_grid()
	var dict : Dictionary = {}
	
	for cell in get_flex_cells(casting_unit, target_coord):
		if grid.has_unit(cell):
			dict[cell] = str(damage_used)
			
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	match direction:
		"north":
			$sprite.rotation_degrees = 0
			opposite_dir = "south"
		"north_east":
			$sprite.rotation_degrees = 70
			opposite_dir = "south_west"
		"south_east":
			$sprite.rotation_degrees = 110
			opposite_dir = "north_west"
		"south":
			$sprite.rotation_degrees = 180
			opposite_dir = "north"
		"south_west":
			$sprite.rotation_degrees = 250
			opposite_dir = "north_east"
		"north_west":
			$sprite.rotation_degrees = 290
			opposite_dir = "south_east"
			
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var collision : bool = casting_unit.has_special_modification("collision")
	var units_to_push : Array = []
	
	for cell in cells:
		if grid.has_unit(cell):
			units_to_push.append(grid.get_unit(cell))
	
	# fix this
	while units_to_push.size() > 0:
		var unit = units_to_push.pop_back()
		grid.displace_unit(unit, direction, collision)
		unit.deal_damage(damage_used, casting_unit)
		if grid.wait_for_spell and !ignore_yield:
			yield(grid, "resolve_current_spell")
			
	grid.displace_unit(casting_unit, opposite_dir)
	
	.effect(casting_unit, target_coord)
