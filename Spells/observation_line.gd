extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 5
const PROPERTIES : Array = [ "spell", "relativity", "observation_line" ]

var valid_placement : bool = false
var orientation : int = 0
var cells : PoolVector3Array = PoolVector3Array([])

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	var spell_range : int = DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES)
	
	return Globals.filled_circle(casting_unit.coords, spell_range)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var spell_range : int = DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES)
	
	return Globals.filled_circle(casting_unit.coords, spell_range + 1)
		
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	var cube = Globals.CUBE_VECTORS
	cells = PoolVector3Array([target_coord])
	
	match orientation:
		0:
			cells.append(target_coord + cube[1])
			cells.append(target_coord + cube[4])
		1:
			cells.append(target_coord + cube[2])
			cells.append(target_coord + cube[5])
		2:
			cells.append(target_coord + cube[0])
			cells.append(target_coord + cube[3])
			
	check_valid()
	
	return cells
	
func get_cost(casting_unit):
	spell_cost.rapidity = 3
	spell_cost.timing = 3
	return .get_cost(casting_unit)
	
func check_valid() -> void:
	var grid = Globals.get_hex_grid()
	
	for cell in cells:
		if grid.has_occupant(cell):
			valid_placement = false
			return
			
	valid_placement = true
	
func rotate_orientation() -> void:
	orientation = (orientation + 1) % 3
	check_valid()
	
func animation(casting_unit, target_coord : Vector3) -> void:
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	
	for cell in cells:
		grid.create_new_obstacle("observation_line", cell)
		
	.effect(casting_unit, target_coord)
