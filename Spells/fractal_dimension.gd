extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 5
const SPLIT_RANGE : int = 3
const DEFAULT_DAMAGE : int = 0
const PROPERTIES : Array = [ "spell", "relativity", "fractal_dimension", "teleport" ]

var act : int = 1
var first_pos : Vector3 = Vector3.ZERO
var second_pos : Vector3 = Vector3.ZERO

func _ready():
	pass
	
func advance(pos : Vector3) -> void:
	if act == 1:
		act = 2
		first_pos = pos
	elif act == 2:
		act = 3
		second_pos = pos
		
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	act = 1
	first_pos = Vector3.ZERO
	second_pos = Vector3.ZERO
	
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var origin : Vector3 = casting_unit.coords
	var range_used : int = DEFAULT_RANGE
	
	if casting_unit.has_special_modification("fractal_dimension_5"):
		target_behavior = "double_target"
		range_used = SPLIT_RANGE
		
		if act == 2 or act == 3:
			origin = first_pos
			
	var final_pool : PoolVector3Array = Globals.filled_circle( \
		origin, range_used + casting_unit.get_modification("range", PROPERTIES))
		
	final_pool.remove(final_pool.find(casting_unit.coords))
	
	return final_pool
	
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	
	if damage_used > 0:
		var final_pool : PoolVector3Array = PoolVector3Array([])
		
		if act == 2:
			final_pool = Globals.line(casting_unit.coords, first_pos)
			final_pool.append_array(Globals.line(first_pos, target_coord))
		elif act == 3:
			final_pool = Globals.line(casting_unit.coords, first_pos)
			final_pool.append_array(Globals.line(first_pos, second_pos))
		else:
			final_pool = Globals.line(casting_unit.coords, target_coord)
			
		return final_pool
	else:
		return PoolVector3Array([])
	
func get_cost(casting_unit):
	spell_cost.rapidity = 2
	spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary:
	var grid = Globals.get_hex_grid()
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	var dict : Dictionary = {}
	
	if damage_used == 0:
		return dict
		
	if casting_unit.has_special_modification("fractal_dimension_3"):
		damage_used -= 1
		
	var cells : PoolVector3Array = PoolVector3Array([])
	for cell in Globals.line(casting_unit.coords, target_coord):
		if grid.has_unit(cell) and cell != casting_unit.coords:
			if casting_unit.has_special_modification("fractal_dimension_3"):
				damage_used += 1
			cells.append(cell)
			
	for cell in cells:
		dict[cell] = str(damage_used)
		
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	var og_pos : Vector3 = Vector3(casting_unit.coords.x, casting_unit.coords.y, \
		casting_unit.coords.z)
	var new_pos : Vector3 = target_coord
	
	if not targets.empty():
		first_pos = targets[0]
		second_pos = targets[1]
		new_pos = second_pos
		
	if alt_cast:
		og_pos = Vector3(casting_unit.superposition.coords.x, \
			casting_unit.superposition.coords.y, casting_unit.superposition.coords.z)
		casting_unit.superposition.teleport(new_pos)
	else:
		grid.teleport_unit(casting_unit, new_pos, casting_unit)
	
	var units : Array = []
	if damage_used > 0:
		if casting_unit.has_special_modification("fractal_dimension_3"):
			damage_used -= 1
			
		var cells : PoolVector3Array = Globals.line(og_pos, new_pos)
		
		if not targets.empty():
			cells = Globals.line(og_pos, first_pos)
			cells.append_array(Globals.line(first_pos, new_pos))
			
		for cell in cells:
			var unit = grid.get_unit(cell)
			
			if !Globals.is_valid(unit):
				continue
			if unit == casting_unit:
				continue
				
			units.append(unit)
			
			if casting_unit.has_special_modification("fractal_dimension_3"):
				damage_used += 1
				
		for unit in units:
			unit.deal_damage(damage_used)
			
			if grid.wait_for_spell and !ignore_yield:
				yield(grid, "resolve_current_spell")
				
	if casting_unit.has_special_modification("fractal_dimension_4"):
		casting_unit.apply_condition("spur", 3)
		
	.effect(casting_unit, new_pos)
