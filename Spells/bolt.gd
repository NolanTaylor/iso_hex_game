extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 3
const DEFAULT_DAMAGE : int = 1
const PROPERTIES : Array = [ "spell", "targeted", "bolt", ]

func _ready():
	pass
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = Globals.filled_circle( \
		casting_unit.coords, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
	final_pool.remove(final_pool.find(casting_unit.coords))
	
	return final_pool
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	var dict : Dictionary = {}
	
	dict[target_coord] = str(damage_used)
	
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	
	if grid.has_unit(target_coord):
		grid.get_unit(target_coord).deal_damage(DEFAULT_DAMAGE, casting_unit)
	
	.effect(casting_unit, target_coord)
