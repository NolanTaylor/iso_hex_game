extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 3
const PROPERTIES : Array = [ "spell", "kinematics", "relativity", "parallel_coherence" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = Globals.filled_circle( \
		casting_unit.coords, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
	final_pool.remove(final_pool.find(casting_unit.coords))
	
	return final_pool
	
func get_cost(casting_unit):
	spell_cost.work = 1
	spell_cost.rapidity = 1
	spell_cost.timing = 3
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var unit = grid.get_unit(target_coord)
	
	var caster_coord : Vector3 = casting_unit.coords
	
	if Globals.is_valid(unit):
		casting_unit.coords = target_coord
		unit.coords = caster_coord
		grid.grid[caster_coord].set_occupying_unit(null)
		grid.grid[target_coord].set_occupying_unit(null)
		grid.teleport_unit(casting_unit, target_coord, casting_unit)
		grid.teleport_unit(unit, caster_coord, casting_unit)
		
	.effect(casting_unit, target_coord)
