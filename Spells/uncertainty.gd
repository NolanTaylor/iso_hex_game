extends "res://Spells/spell.gd"

const PROPERTIES : Array = [ "spell", "relativity", "uncertainty" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	if not Globals.is_valid(casting_unit.superposition):
		return PoolVector3Array([])
		
	return PoolVector3Array([casting_unit.superposition.coords])
	
func get_cost(casting_unit):
	spell_cost.rapidity = 2
	if is_reaction:
		spell_cost.timing = 1
	else:
		spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var og_pos : Vector3 = Vector3(casting_unit.coords.x, casting_unit.coords.y, \
		casting_unit.coords.z)
	
	if not Globals.is_valid(casting_unit.superposition):
		.effect(casting_unit, target_coord)
		return
		
	var occupant = grid.get_unit(casting_unit.superposition.coords)
	if occupant != null:
		for dir in Globals.CUBE_VECTORS:
			var space : Vector3 = occupant.coords + dir
			if not grid.has_occupant(space):
				grid.displace_unit(occupant, dir)
				if grid.wait_for_spell and !ignore_yield:
					yield(grid, "resolve_current_spell")
				occupant.deal_damage(1, casting_unit)
				if grid.wait_for_spell and !ignore_yield:
					yield(grid, "resolve_current_spell")
				break
				
	grid.teleport_unit(casting_unit, casting_unit.superposition.coords, casting_unit)
		
	if grid.wait_for_spell and !ignore_yield:
		yield(grid, "resolve_current_spell")
		
	casting_unit.superposition.teleport(og_pos)
		
	.effect(casting_unit, target_coord)
