extends "res://Spells/spell.gd"

const DEFAULT_DAMAGE : int = 3
const PROPERTIES : Array = [ "spell", "relativity", "edge_of_chaos" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return Globals.ring(casting_unit.coords, 1)
	
func get_cost(casting_unit):
	spell_cost.rapidity = 2
	spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary:
	var grid = Globals.get_hex_grid()
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	var dict : Dictionary = {}
	
	var unit = grid.get_unit(target_coord)
	if Globals.is_valid(unit) and \
	(unit.has_condition("stun") or unit.has_condition("root") or unit.has_condition("silence")):
		damage_used *= 2
	
	dict[target_coord] = str(damage_used)
	
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	
	var unit = grid.get_unit(target_coord)
	if !Globals.is_valid(unit):
		push_error("null unit")
		return
	if unit.has_condition("stun") or unit.has_condition("root") or unit.has_condition("silence"):
		damage_used *= 2
		
	unit.deal_damage(damage_used, casting_unit)
	
	if grid.wait_for_spell and !ignore_yield:
		yield(grid, "resolve_current_spell")
	
	if casting_unit.has_special_modification("edge_of_chaos_2") and \
	not Globals.is_valid(unit):
		casting_unit.rapidity += 2
	
	.effect(casting_unit, target_coord)
