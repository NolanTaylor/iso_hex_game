extends "res://Spells/spell.gd"

const DEFAULT_DAMAGE : int = 0
const DEFAULT_RANGE : int = 1
const PROPERTIES : Array = [ "spell", "thermodynamics", "equilibrium" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return Globals.ring(casting_unit.coords, \
		DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
	
func get_cost(casting_unit):
	spell_cost.entropy = 2
	spell_cost.timing = 3
	if casting_unit.has_special_modification("equilibrium_4"):
		spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var unit = grid.get_unit(target_coord)
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	
	if Globals.is_valid(unit):
		unit.apply_condition("root")
		if casting_unit.has_special_modification("equilibrium_3"):
			unit.apply_condition("weak")
		if damage_used > 0:
			unit.deal_damage(damage_used)
			
	.effect(casting_unit, target_coord)
