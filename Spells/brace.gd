extends "res://Spells/spell.gd"

const PROPERTIES : Array = [ "spell", "kinematics", "reaction", "brace" ]

func _ready():
	pass
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return PoolVector3Array([])
	
func get_cost(casting_unit):
	spell_cost.work = 1
	spell_cost.timing = 1
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	
	.effect(casting_unit, target_coord)
