extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 3
const PROPERTIES : Array = [ "spell", "thermal", "reaction", "deposition" ]

func _ready():
	pass
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return PoolVector3Array([])
	
func get_cost(casting_unit):
	spell_cost.entropy = 2
	spell_cost.timing = 1
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	.effect(casting_unit, target_coord)
