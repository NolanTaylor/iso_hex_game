extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 2
const DEFAULT_DAMAGE : int = 1
const PROPERTIES : Array = [ "spell", "optics", "untargeted", "disperse" ]

var cells : PoolVector3Array = PoolVector3Array([])
var center_beam : PoolVector3Array = PoolVector3Array([])
var direction : String = "null"

func _ready():
	pass
	
func get_range(casting_unit) -> int:
	return DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES)
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = PoolVector3Array([])
	
	for direction in Globals.CUBE_VECTORS:
		final_pool.append_array(Globals.rigid_line(casting_unit.coords, \
			direction, get_range(casting_unit)))
		
	return final_pool
		
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	var directions : Array = Globals.DIRECTIONS
	
	for i in 6:
		if target_coord in Globals.rigid_line(casting_unit.coords, \
		directions[i], get_range(casting_unit)):
			var ccw_dir = i - 1
			var cw_dir = i + 1
			
			if ccw_dir == -1:
				ccw_dir = 5
			if cw_dir == 6:
				cw_dir = 0
				
			cells = Globals.rigid_line(casting_unit.coords, \
				directions[i], get_range(casting_unit))
			center_beam = Globals.rigid_line(casting_unit.coords, \
				directions[i], get_range(casting_unit))
			cells.append_array(Globals.rigid_line(casting_unit.coords, \
				directions[ccw_dir], get_range(casting_unit)))
			cells.append_array(Globals.rigid_line(casting_unit.coords, \
				directions[cw_dir], get_range(casting_unit)))
				
			self.direction = directions[i]
			
			return cells
				
	return PoolVector3Array([])
	
func get_cost(casting_unit):
	spell_cost.light = 2
	spell_cost.timing = 3
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	var grid = Globals.get_hex_grid()
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	var dict : Dictionary = {}
	
	for cell in get_flex_cells(casting_unit, target_coord):
		if grid.has_unit(cell):
			dict[cell] = str(damage_used)
			
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = casting_unit.position
	
	match direction:
		"north":
			position += Vector2(25, 12)
			rotation_degrees = 250
			$sprite3.position = Vector2(0, 0)
			$sprite3.rotation_degrees = -72
		"north_east":
			rotation_degrees = 290
			$sprite.rotation_degrees = 24
			$sprite3.position = Vector2(3, -8)
			$sprite3.rotation_degrees = -42
		"south_east":
			rotation_degrees = 0
		"south":
			position += Vector2(6, -16)
			rotation_degrees = 70
			$sprite3.rotation_degrees = -70
		"south_west":
			position += Vector2(15, -21)
			rotation_degrees = 110
			$sprite.position = Vector2(18, -18)
			$sprite.rotation_degrees = 30
			$sprite3.position = Vector2(0, -9)
			$sprite3.rotation_degrees = -44
		"north_west":
			position += Vector2(33, -5)
			rotation_degrees = 180
	
	$sprite2.frame = 0
	$sprite3.frame = 0
	$sprite2.play("cast")
	$sprite3.play("cast")
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	
	for cell in cells:
		var occupying_unit = grid.get_unit(cell)
		if Globals.is_valid(occupying_unit):
			occupying_unit.deal_damage(damage_used, casting_unit)
			if grid.wait_for_spell and !ignore_yield:
				yield(grid, "resolve_current_spell")
			
			if casting_unit.has_special_modification("disperse_3"):
				occupying_unit.break_guard()
			if casting_unit.has_special_modification("disperse_5") and \
			occupying_unit.coords in center_beam:
				occupying_unit.apply_condition("stun")
				
	.effect(casting_unit, target_coord)
