extends "res://Spells/spell.gd"

const PROPERTIES : Array = [ "spell", "kinematics", "precision" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return Globals.ring(casting_unit.coords, 1)
	
func get_cost(casting_unit):
	spell_cost.timing = 3
	if casting_unit.has_special_modification("precision_3"):
		spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	var damage_used : int = casting_unit.work + casting_unit.get_modification("damage", PROPERTIES)
	var dict : Dictionary = {}
	
	dict[target_coord] = str(damage_used)
	
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var damage_used : int = casting_unit.work + casting_unit.get_modification("damage", PROPERTIES)
	var pierce : bool = false
	
	var unit = grid.get_unit(target_coord)
	if !Globals.is_valid(unit):
		push_error("null unit")
		return
	if casting_unit.has_special_modification("precision_2"):
		pierce = true
		
	unit.deal_damage(damage_used, casting_unit, pierce)
	casting_unit.work = 0
	
	.effect(casting_unit, target_coord)
