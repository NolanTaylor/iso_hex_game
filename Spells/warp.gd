extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 7
const PROPERTIES : Array = [ "spell", "kinematics", "relativity", "warp", "teleport" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = Globals.filled_circle(\
		casting_unit.coords, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
	return final_pool
	
func get_cost(casting_unit):
	spell_cost.work = 2
	spell_cost.rapidity = 2
	spell_cost.timing = 3
	spell_cost.ult = true
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var unit = grid.get_unit(targets[0])
	
	if Globals.is_valid(unit):
		grid.teleport_unit(unit, targets[1], casting_unit)
		
	.effect(casting_unit, target_coord)
