extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 5
const DEFAULT_DAMAGE : int = 2
const PROPERTIES : Array = [ "spell", "thermodynamics", "fireball" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	var spell_range : int = DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES)
	
	return Globals.filled_circle(casting_unit.coords, spell_range)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var spell_range : int = DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES)
	
	return Globals.filled_circle(casting_unit.coords, spell_range + 1)
	
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	return Globals.ring(target_coord, 1)
	
func get_cost(casting_unit):
	spell_cost.entropy = 3
	spell_cost.timing = 3
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary:
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	var dict : Dictionary = {}
	
	dict[target_coord] = str(damage_used)
	
	for cell in get_flex_cells(casting_unit, target_coord):
		dict[cell] = str(damage_used)
		
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var cells : PoolVector3Array = get_flex_cells(casting_unit, target_coord)
	var spell_damage : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	cells.append(target_coord)
	
	for cell in cells:
		var unit = grid.get_unit(cell)
		
		if Globals.is_valid(unit):
			unit.deal_damage(spell_damage, casting_unit)
			if grid.wait_for_spell and !ignore_yield:
				yield(grid, "resolve_current_spell")
			
	.effect(casting_unit, target_coord)
