extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 3
const PROPERTIES : Array = [ "spell", "optics", "targeted", "blink", "teleport" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = Globals.filled_circle(\
		casting_unit.coords, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
	final_pool.remove(final_pool.find(casting_unit.coords))
	
	return final_pool
	
func get_cost(casting_unit):
	spell_cost.light = 1
	if casting_unit.has_special_modification("blink_2") and is_reaction:
		spell_cost.timing = 1
	elif casting_unit.has_special_modification("blink_4"):
		spell_cost.timing = 2
	else:
		spell_cost.timing = 3
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var hex_grid = Globals.get_hex_grid()
	
	hex_grid.teleport_unit(casting_unit, target_coord, casting_unit)
	
	.effect(casting_unit, target_coord)
