extends "res://Spells/spell.gd"

const PROPERTIES : Array = [ "spell", "kinematics", "potential_surge" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return PoolVector3Array([casting_unit.coords])
	
func get_cost(casting_unit):
	spell_cost.work = 1
	spell_cost.timing = 2
	spell_cost.ult = true
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	casting_unit.work *= 2
	
	.effect(casting_unit, target_coord)
