extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 1
const PROPERTIES : Array = [ "spell", "kinematics", "accelerate" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return Globals.filled_circle(casting_unit.coords, \
		DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
func get_cost(casting_unit):
	spell_cost.work = 1
	spell_cost.flux = 1
	spell_cost.timing = 3
	if casting_unit.has_special_modification("accelerate_2"):
		spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var unit = grid.get_unit(target_coord)
	
	if Globals.is_valid(unit):
		unit.actions += 1
		unit.swift_actions += 1
		unit.movement_left += 3
		unit.apply_ui()
		
	.effect(casting_unit, target_coord)
