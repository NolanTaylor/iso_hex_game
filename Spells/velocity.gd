extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 4
const PROPERTIES : Array = [ "spell", "kinematics", "velocity", "dash" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var hex_grid = Globals.get_hex_grid()
	var final_pool : PoolVector3Array
	
	var mode : String = "all"
	if casting_unit.has_special_modification("velocity_2"):
		mode = "obstacles"
		
	for direction in Globals.CUBE_VECTORS:
		final_pool.append_array(Globals.rigid_line(casting_unit.coords, \
			direction, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES), \
				mode, hex_grid.grid))
	for cell in final_pool:
		if hex_grid.has_occupant(cell):
			final_pool.remove(final_pool.find(cell))
			
	return final_pool
	
func get_cost(casting_unit):
	spell_cost.work = 1
	spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var hex_grid = Globals.get_hex_grid()
	
	hex_grid.teleport_unit(casting_unit, target_coord, casting_unit)
	
	.effect(casting_unit, target_coord)
