extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 2
const DEFAULT_DAMAGE : int = 2
const PROPERTIES : Array = [ "spell", "kinematics", "dissipate" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = Globals.filled_circle(\
		casting_unit.coords, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
	final_pool.remove(final_pool.find(casting_unit.coords))
	
	return final_pool
	
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_cost(casting_unit):
	spell_cost.work = 2
	spell_cost.timing = 3
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	var grid = Globals.get_hex_grid()
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	var dict : Dictionary = {}
	
	for cell in get_flex_cells(casting_unit, target_coord):
		if grid.has_unit(cell):
			var distance : int = Globals.hex_distance( \
				casting_unit.coords, grid.get_unit(cell).coords)
			dict[cell] = str(damage_used - distance + 1)
			
	return dict
	
func animation(casting_unit, target_coord : Vector3) -> void:
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	
	for cell in get_highlight_cells(casting_unit):
		var unit = grid.get_unit(cell)
		if !Globals.is_valid(unit):
			continue
		var distance : int = \
			Globals.hex_distance(casting_unit.coords, unit.coords)
		var damage : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
		damage -= distance - 1
		if damage <= 0:
			continue
		unit.deal_damage(damage, casting_unit)
		if grid.wait_for_spell and !ignore_yield:
			yield(grid, "resolve_current_spell")
	
	.effect(casting_unit, target_coord)
