extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 1
const PROPERTIES : Array = [ "spell", "magnetism", "charge" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	var final_pool : PoolVector3Array = Globals.filled_circle( \
		casting_unit.coords, DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
	final_pool.remove(final_pool.find(casting_unit.coords))
	
	return final_pool
	
func get_cost(casting_unit):
	spell_cost.flux = 2
	spell_cost.timing = 3
	if casting_unit.has_special_modification("charge_3"):
		spell_cost.timing = 2
	return .get_cost(casting_unit)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	position = Globals.grid_to_pixel(target_coord)
	
	.animation(casting_unit, target_coord)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var unit = grid.get_unit(target_coord)
	var intensity : int = 1
	
	if casting_unit.has_special_modification("magnetic_charge"):
		intensity += 1
		
	if Globals.is_valid(unit):
		unit.apply_condition("charge", intensity)
		
		if casting_unit.has_special_modification("electric_charge"):
			unit.apply_condition("spur")
			
	.effect(casting_unit, target_coord)
