extends "res://Spells/spell.gd"

const DEFAULT_RANGE : int = 1
const DEFAULT_DAMAGE : int = 1
const PROPERTIES : Array = [ "basic_attack", "stab" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return Globals.ring(casting_unit.coords, \
		DEFAULT_RANGE + casting_unit.get_modification("range", PROPERTIES))
		
func get_cost(casting_unit):
	spell_cost.timing = 3
	if casting_unit.has_special_modification("proficiency_2"):
		spell_cost.timing = 2
	if casting_unit.has_condition("spur"):
		spell_cost.timing -= casting_unit.get_condition("spur").intensity
	if is_reaction:
		spell_cost.timing = 1
	return .get_cost(casting_unit)
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	var dict : Dictionary = {}
	
	dict[target_coord] = str(damage_used)
	
	return dict
	
func cast(casting_unit, target_coord : Vector3) -> void:
	get_cost(casting_unit)
	
	match spell_cost.timing:
		1:
			casting_unit.reactions -= 1
		2:
			if casting_unit.swift_actions >= 1:
				casting_unit.swift_actions -= 1
			else:
				casting_unit.actions -= 1
		3:
			casting_unit.actions -= 1
	
	casting_unit.get_node("sprite").connect("animation_finished", self, "animation", \
		[ casting_unit, target_coord ], CONNECT_ONESHOT)
	casting_unit.attack(target_coord)
	casting_unit.apply_ui()
	
func animation(casting_unit, target_coord : Vector3) -> void:
	show()
	position = Globals.grid_to_pixel(target_coord)
	
	if casting_unit.editor_description == "fsad nyooom":
		effect(casting_unit, target_coord)
		return
	
	$sprite.frame = 0
	$sprite.connect("animation_finished", self, "effect", [ casting_unit, target_coord ], \
		CONNECT_ONESHOT)
	$sprite.play("cast")
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	var damage_used : int = DEFAULT_DAMAGE + casting_unit.get_modification("damage", PROPERTIES)
	
	var unit = grid.get_unit(target_coord)
	if Globals.is_valid(unit):
		if casting_unit.has_condition("solar"):
			unit.apply_condition("stun")
		unit.deal_damage(damage_used, casting_unit)
		if grid.wait_for_spell and !ignore_yield:
			yield(grid, "resolve_current_spell")
	if casting_unit.has_special_modification("triumph"):
		unit = grid.get_unit(target_coord)
		if !Globals.is_valid(unit):
			casting_unit.heal(1, casting_unit)
	if casting_unit.has_special_modification("footwork"):
		casting_unit.movement_left += 1
	if casting_unit.has_condition("charge"):
		casting_unit.remove_condition("charge")
	if casting_unit.has_condition("spur"):
		casting_unit.remove_condition("spur")
	
	.effect(casting_unit, target_coord)
