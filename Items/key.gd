extends "res://Items/item.gd"

const DEFAULT_HEALING : int = 1
const PROPERTIES : Array = [ "item", "drink" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return Globals.ring(casting_unit.coords, 1)
	
func effect(casting_unit, target_coord : Vector3) -> void:
	var grid = Globals.get_hex_grid()
	grid.unlock_object(casting_unit, target_coord)
	
	.effect(casting_unit, target_coord)
