extends "res://Items/item.gd"

const DEFAULT_HEALING : int = 1
const PROPERTIES : Array = [ "item", "drink" ]

func _ready():
	pass
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	return get_highlight_cells(casting_unit)
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	return PoolVector3Array([casting_unit.coords])
	
func effect(casting_unit, target_coord : Vector3) -> void:
	casting_unit.heal(DEFAULT_HEALING + casting_unit.get_modification("item", PROPERTIES))
	
	.effect(casting_unit, target_coord)
