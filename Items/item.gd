extends Node2D

export var item_name : String = "name"
export var gear_slot : String = "inventory"
export var current_slot : String = "null"
export var consumable : bool = true
export var target_type : String
export var target_behavior : String

var is_reaction : bool = false
var ignore_yield : bool = false
var targets : PoolVector3Array

signal complete_cast(spell)

func _ready():
	z_index = 2
	
func get_target_type() -> String:
	return target_type
	
func get_target_behavior() -> String:
	return target_behavior
	
func get_valid_cursor_cells(casting_unit) -> PoolVector3Array:
	# abstract function
	return PoolVector3Array([])
	
func get_highlight_cells(casting_unit) -> PoolVector3Array:
	# abstract function
	return PoolVector3Array([])
	
func get_flex_cells(casting_unit, target_coord : Vector3) -> PoolVector3Array:
	# abstract function
	return PoolVector3Array([])
	
func get_forecast(casting_unit, target_coord : Vector3) -> Dictionary: # <Vector3, String>
	# abstract function
	return {}
	
func double() -> void:
	# abstract function
	return
	
func halve() -> void:
	# abstract function
	return
	
func can_cast(casting_unit) -> bool:
	if casting_unit.actions > 0:
		return true
	else:
		return false
	
func cast(casting_unit, target_coord : Vector3) -> void:
	casting_unit.actions -= 1
		
	get_highlight_cells(casting_unit)
	get_flex_cells(casting_unit, target_coord)
	
	casting_unit.get_node("sprite").connect("animation_finished", self, "animation", \
		[ casting_unit, target_coord ], CONNECT_ONESHOT)
	casting_unit.cast(target_coord)
	casting_unit.apply_ui()
	
func multi_cast(casting_unit, target_coords : PoolVector3Array) -> void:
	targets = target_coords
	
	cast(casting_unit, Vector3.ZERO)
	
func animation(casting_unit, target_coord : Vector3) -> void:
	$sprite.frame = 0
	show()
	$sprite.connect("animation_finished", self, "effect", [ casting_unit, target_coord ], \
		CONNECT_ONESHOT)
	$sprite.play("use")
	
func effect(casting_unit, target_coord : Vector3) -> void:
	$sprite.hide()
	$sprite.stop()
	casting_unit.lose_item(current_slot)
	casting_unit.apply_ui()
	finish()
	
func finish() -> void:
	hide()
	emit_signal("complete_cast", self)

