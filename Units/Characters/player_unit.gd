extends "res://Units/unit.gd"

export var test_skills : PoolStringArray = []
export var test_items : Dictionary = {} # <String, String>

var convo_partner_coords : PoolVector3Array = PoolVector3Array([])
var fading : bool = false
var superposition
var current_cost

signal unfocused
signal reaction_finished(reaction)

func _ready():
	pass
	
func _process(delta):
	pass
	
func unit_init() -> void:
	if not has_node("character_menu"):
		return
		
	data = Config.characters[get_node("character_menu").char_name].duplicate(true)
	
	selectable = true
	obstacle_mode = "enemy_units"
	
	for child in $action_buttons.get_children():
		child.connect("button_pressed", self, "action_button_pressed")
		
	data["skills"].append_array(test_skills)
		
	for skill in data["skills"]:
		init_skill(skill)
		
	data["equipment"].merge(test_items, true)
		
	for key in data["equipment"].keys():
		init_equipment(key, data["equipment"][key])
		
	.unit_init()
	
func get_health_color() -> Color:
	return PLAYER_HEALTH_COLOR
	
func init_skill(skill : String) -> void:
	match skill:
		"health_1":
			max_health += 1
		"health_2":
			max_health += 1
		"health_3":
			max_health += 1
		"health_4":
			max_health += 1
		"health_5":
			max_health += 1
		"sturdy_1":
			apply_guard()
		"sturdy_2":
			apply_guard()
		"corruption_1":
			corrupted_health += 1
		"corruption_2":
			corrupted_health += 1
		"movement_1":
			movement += 1
		"movement_2":
			movement += 1
		"footwork":
			add_mod("special", "footwork")
		"proficiency_1":
			add_mod("damage", "stab", 1)
		"proficiency_2":
			add_mod("special", "proficiency_2")
		"triumph":
			add_mod("special", "triumph")
		"adrenaline":
			apply_condition("adrenaline")
		"endurance":
			apply_condition("endurance", 0, true)
		"defy_death":
			apply_condition("defy_death", 0, true)
		"waves_and_particles_1":
			apply_condition("waves_and_particles")
		"waves_and_particles_2":
			apply_condition("waves_and_particles")
		"kinetic_and_potential_1":
			apply_condition("kinetic_and_potential")
		"kinetic_and_potential_2":
			apply_condition("kinetic_and_potential")
		"kinetic_and_potential_3":
			apply_condition("kinetic_and_potential")
		"attract_and_repel_1":
			apply_condition("attract_and_repel")
		"attract_and_repel_2":
			apply_condition("attract_and_repel")
		"states_and_processes_1":
			apply_condition("states_and_processes")
		"states_and_processes_2":
			apply_condition("states_and_processes")
		"states_and_processes_3":
			apply_condition("states_and_processes")
		"states_and_processes_4":
			entropy = 5
		"space_and_time_1":
			apply_condition("space_and_time")
		"space_and_time_2":
			apply_condition("space_and_time")
		"wax_and_wane_1":
			apply_condition("wax_and_wane")
		"wax_and_wane_2":
			apply_condition("wax_and_wane")
		"disperse_1":
			create_new_spell_button("disperse")
		"disperse_2":
			add_mod("damage", "disperse", 1)
		"disperse_3":
			add_mod("special", "disperse_3", 0)
		"disperse_4":
			add_mod("range", "disperse", 2)
		"disperse_5":
			add_mod("special", "disperse_5")
		"blind_1":
			create_new_spell_button("blind")
		"blind_2":
			add_mod("special", "blind_2")
		"blind_3":
			add_mod("damage", "blind", 1)
		"blink_1":
			create_new_spell_button("blink")
		"blink_2":
			apply_condition("blink", 0, true)
			add_mod("special", "blink_2")
		"blink_3":
			add_mod("range", "blink", 2)
		"blink_4":
			add_mod("special", "blink_4")
		"cleanse_1":
			create_new_spell_button("cleanse")
		"cleanse_2":
			add_mod("special", "cleanse_2")
		"reflect":
			apply_condition("reflect", 0, true)
		"refract":
			apply_condition("refract", 0, true)
		"natural_refraction":
			apply_condition("natural_refraction", 0, true)
		"artificial_refraction":
			add_mod("range", "refract", 2)
		"spectrum_shatter":
			apply_condition("spectrum_shatter")
		"crepuscular_rays":
			create_new_spell_button("crepuscular_rays")
		"fata_morgana":
			create_new_spell_button("fata_morgana")
		"displace_1":
			create_new_spell_button("displace")
		"displace_2":
			add_mod("range", "displace", 1)
			add_mod("damage", "displace", 1)
			add_mod("special", "displace_2")
		"displace_3":
			add_mod("special", "displace_3")
		"precision_1":
			create_new_spell_button("precision")
		"precision_2":
			add_mod("special", "precision_2")
		"precision_3":
			add_mod("special", "precision_3")
		"velocity_1":
			create_new_spell_button("velocity")
		"velocity_2":
			add_mod("special", "velocity_2")
		"momentum":
			apply_condition("momentum_static", 0, true)
		"collision":
			add_mod("special", "collision")
		"inertia":
			apply_condition("inertia", 0, true)
		"expulse":
			apply_condition("expulse", 0, true)
		"equal_and_opposite_1":
			create_new_spell_button("equal_and_opposite")
		"dissipate":
			create_new_spell_button("dissipate")
		"absorb_1":
			apply_condition("absorb", 0, true)
		"absorb_2":
			add_mod("healing", "absorb", 2)
		"brace_1":
			apply_condition("brace", 0, true)
		"brace_2":
			add_mod("special", "brace_2")
		"potential_surge":
			create_new_spell_button("potential_surge")
		"restoration_1":
			create_new_spell_button("restoration")
		"restoration_2":
			add_mod("range", "restoration", 3)
		"restoration_3":
			add_mod("special", "restoration_3")
		"prismatic_restoration":
			add_mod("healing", "restoration", 2)
			add_mod("special", "prismatic_restoration")
		"charge_1":
			create_new_spell_button("charge")
		"charge_2":
			add_mod("range", "charge", 3)
		"charge_3":
			add_mod("special", "charge_3")
		"magnetic_charge":
			add_mod("special", "magnetic_charge")
		"electric_charge":
			add_mod("special", "electric_charge")
		"relay":
			create_new_spell_button("relay")
		"deposition":
			apply_condition("deposition")
		"insulate_1":
			create_new_spell_button("insulate")
		"insulate_2":
			add_mod("range", "insulate", 1)
		"equilibrium_1":
			create_new_spell_button("equilibrium")
		"equilibrium_2":
			add_mod("damage", "equilibrium", 1)
		"equilibrium_3":
			add_mod("special", "equilibrium_3")
		"equilibrium_4":
			add_mod("special", "equilibrium_4")
		"freeze_1":
			create_new_spell_button("freeze")
		"depressurize_1":
			create_new_spell_button("depressurize")
		"guardian":
			create_new_spell_button("guardian")
			apply_condition("guardian")
		"absolute_zero":
			create_new_spell_button("absolute_zero")
		"phase_change":
			apply_condition("phase_change", 0, true)
		"superposition":
			superposition = load("res://Units/superposition.tscn").instance()
			superposition.position = position
			superposition.real_unit = self
		"wave_function_collapse_1":
			create_new_spell_button("wave_function_collapse")
		"wave_function_collapse_2":
			apply_condition("wave_function_collapse", 0, true)
		"uncertainty_1":
			create_new_spell_button("uncertainty")
		"uncertainty_2":
			apply_condition("uncertainty_2", 0, true)
		"uncertainty_3":
			apply_condition("uncertainty_3", 0, true)
		"edge_of_chaos_1":
			create_new_spell_button("edge_of_chaos")
		"edge_of_chaos_2":
			add_mod("special", "edge_of_chaos_2")
		"fractal_dimension_1":
			create_new_spell_button("fractal_dimension")
		"fractal_dimension_2":
			add_mod("damage", "fractal_dimension", 1)
		"fractal_dimension_3":
			add_mod("special", "fractal_dimension_3")
		"fractal_dimension_4":
			add_mod("special", "fractal_dimension_4")
		"fractal_dimension_5":
			add_mod("special", "fractal_dimension_5")
		"observation_line_1":
			create_new_spell_button("observation_line")
		"observation_line_2":
			add_mod("special", "observation_line_2")
		"shroud_1":
			create_new_spell_button("shroud")
		"shroud_2":
			add_mod("range", "shroud", 2)
		"shroud_3":
			add_mod("special", "shroud_3")
		"satellite_1":
			apply_condition("satellite")
		"satellite_2":
			add_mod("special", "satellite_2")
		"field_of_stars_1":
			create_new_spell_button("field_of_stars")
		"epicycle":
			apply_condition("epicycle", 0, true)
		"rebirth":
			create_new_spell_button("rebirth")
		"solar_winds":
			create_new_spell_button("solar_winds")
		"parallel_coherence_1":
			create_new_spell_button("parallel_coherence")
		"field_fluctuation":
			apply_condition("field_fluctuation", 0, true)
		"warp":
			create_new_spell_button("warp")
		"accelerate_1":
			create_new_spell_button("accelerate")
		"accelerate_2":
			add_mod("special", "accelerate_2")
		"decay":
			create_new_spell_button("decay")
		_:
			push_error("spell doesn't exist ;-;")
			
	return
	
func init_equipment(slot : String, item : String) -> void:
	if item == "null":
		return
		
	var resource : PackedScene = load("res://Items/{0}.tscn".format([item]))
	
	if !Globals.is_valid(resource):
		return
		
	var object = resource.instance()
	
	if object.gear_slot == slot:
		match item:
			"boots":
				movement += 1
			_:
				push_error("item doesn't exist ;-;")
	
	if slot in ["inventory_1", "inventory_2", "inventory_3", "inventory_4", "inventory_5", \
	"inventory_6", "inventory_7"] and object.consumable:
		create_new_item_button(slot, object)
	
func create_new_spell_button(button : String, reaction : bool = false):
	var new_button = load("res://Global/Menus/Buttons/action_button.tscn").instance().duplicate()
	var new_spell = load("res://Spells/{0}.tscn".format([button])).instance().duplicate()
	new_spell.name = "spell"
	new_spell.hide()
	new_button.add_child(new_spell)
	new_button.get_node("button").texture_normal = load( \
		"res://Assets/GUI/Skills/skill_{0}.png".format([button]))
	new_button.material = UNSHADED
	if reaction:
		$reaction_buttons.add_child(new_button)
	else:
		$action_buttons/spell_button/action_subfolder.add_child(new_button)
		
	return new_button
	
func create_new_item_button(slot : String, item):
	var new_button = load("res://Global/Menus/Buttons/action_button.tscn").instance().duplicate()
	var item_name : String = ""
	var new_item
	
	if typeof(item) == TYPE_STRING:
		new_item = load("res://Items/{0}.tscn".format([item])).instance().duplicate()
		item_name = item
	else:
		new_item = item.duplicate()
		item_name = item.name.right(5)
		
	new_item.name = "item"
	new_item.current_slot = slot
	new_item.hide()
	new_button.add_child(new_item)
	new_button.name = slot
	new_button.get_node("button").texture_normal = load( \
		"res://Assets/GUI/Items/item_{0}.png".format([item_name]))
	new_button.material = UNSHADED
	var grid = Globals.get_hex_grid()
	if Globals.is_valid(grid):
		new_button.connect("button_pressed", grid, "action_button_pressed")
		new_button.connect("button_entered", grid, "action_mouse_enter")
		new_button.connect("button_left", grid, "action_mouse_left")
	$action_buttons/item_button/action_subfolder.add_child(new_button)
	
	return new_button
	
func clear_reaction_folder() -> void:
	for child in $reaction_buttons.get_children():
		if child.name == "cancel_button":
			continue
		else:
			$reaction_buttons.remove_child(child)
			child.queue_free()
			
func clear_partner_coords() -> void:
	convo_partner_coords = PoolVector3Array([])
	
func add_partner_coord(coord : Vector3) -> void:
	convo_partner_coords.append(coord)
	
func upkeep() -> void:
	.upkeep()
	
	deselect()
	
func endstep() -> void:
	expiring_conditions.clear()
	
	.endstep()
	
	if expiring_conditions.size() > 0 and trigger_reaction(["epicycle"]):
		triggered += 1
		var grid = Globals.get_hex_grid()
		grid.go_to_reaction_state()
		grid.wait_for_spell = true
		var reaction = yield(self, "reaction_finished")
		self.hide_reactions()
		triggered -= 1
		
		match reaction:
			"epicycle":
				var epicycle = grid.create_new_spell("epicycle", self, coords, false)
				epicycle.ignore_yield()
				yield(epicycle, "complete_cast")
				grid.cancel_reaction_state()
				
				for condition in expiring_conditions:
					condition.intensity = 1
					condition.extended = true
					
				return
			"cancel":
				grid.cancel_reaction_state()
				
	for condition in expiring_conditions:
		condition.remove_condition
		
func death(killer = null) -> void:
	Globals.dead_units.append(name)
	
	.death(killer)
	
func deal_damage(damage : int, origin = null, pierce : bool = false) -> int:
	var grid = Globals.get_hex_grid()
	var reactions : PoolStringArray = PoolStringArray([])
	
	if has_condition("brace"):
		reactions.append("brace")
		
	if not reactions.empty():
		if trigger_reaction(reactions):
			triggered += 1
			grid.go_to_reaction_state()
			grid.wait_for_spell = true
			var reaction = yield(self, "reaction_finished")
			self.hide_reactions()
			triggered -= 1
			
			match reaction:
				"brace":
					var brace = grid.create_new_spell("brace", self, coords, false)
					brace.ignore_yield()
					yield(brace, "complete_cast")
					grid.cancel_reaction_state()
					
					if has_special_modification("brace_2"):
						return .deal_damage(damage - 2, origin, pierce)
					else:
						return .deal_damage(damage - 1, origin, pierce)
				"cancel":
					grid.cancel_reaction_state()
					
	return .deal_damage(damage, origin, pierce)
	
func apply_condition(condition : String, intensity : int = 1, dynamic : bool = false):
	var grid = Globals.get_hex_grid()
	var reactions : PoolStringArray = PoolStringArray([])
	
	if has_condition("absorb") and condition in ["stun", "root", "silence"]:
		reactions.append("absorb")
		
	if not reactions.empty():
		if trigger_reaction(reactions):
			grid.go_to_reaction_state()
			grid.wait_for_spell = true
			var reaction = yield(self, "reaction_finished")
			self.hide_reactions()
			
			match reaction:
				"absorb":
					var absorb = grid.create_new_spell("absorb", self, coords, false)
					absorb.ignore_yield()
					yield(absorb, "complete_cast")
					grid.cancel_reaction_state()
					return
				"cancel":
					grid.cancel_reaction_state()
					
	return .apply_condition(condition, intensity, dynamic)
	
func acquire_item(item : String) -> bool:
	for inv_slot in data["equipment"].keys():
		if inv_slot in ["inventory_1", "inventory_2", "inventory_3", "inventory_4", \
		"inventory_5", "inventory_6", "inventory_7"]:
			if data["equipment"][inv_slot] == "null":
				data["equipment"][inv_slot] = item
				init_equipment(inv_slot, item)
				return true
				
	return false
	
func lose_item(slot : String, return_to_storage : bool = false) -> bool:
	if data["equipment"][slot] == "null":
		return false
		
	for child in $action_buttons/item_button/action_subfolder.get_children():
		if child.name == slot:
			$action_buttons/item_button/action_subfolder.remove_child(child)
			child.queue_free()
			
	if return_to_storage:
		Config.storage.append(data["equipment"][slot])
		
	data["equipment"][slot] = "null"
	return true
	
func move(new_path : PoolVector2Array, cost : int) -> void:
	selectable = false
	
	.move(new_path, cost)
	
func action_button_pressed(node) -> void:
	match node.name:
		"move_button":
			pass
			
func popup() -> void:
	var camera = Globals.get_camera()
	
	var x_thresh : int = 116 + camera.position.x
	var y_thresh : int = 116 + camera.position.y
	var right : bool = false
	
	if global_position.x < x_thresh and global_position.y > y_thresh:
		right = true
		
	camera.add_popup(popup, right)
	
	apply_ui()
	
func trigger_reaction(spells : PoolStringArray = []) -> bool:
	var grid = Globals.get_hex_grid()
	var camera = Globals.get_camera()
	var action_coords : Array = Globals.ACTION_COORDS
	var flag : bool = false
	var flip : bool = false
	
	if global_position.x < camera.position.x + 22:
		flip = true
		action_coords = Globals.ACTION_COORDS_FLIP
	var toss = grid.current_toss
	if Globals.is_valid(toss):
		var rect : Rect2 = Rect2(global_position + Vector2(-32, -48), Vector2(48, 64))
		var toss_pos : Vector2 = toss.global_position
		if rect.has_point(toss_pos):
			flip = true
			action_coords = Globals.ACTION_COORDS_FLIP
		
	if spells.empty():
		for i in $reaction_buttons.get_child_count():
			$reaction_buttons.get_child(i).display_to(action_coords[i], flip)
			
			var button = $reaction_buttons.get_child(i)
			
			if button.has_node("spell"):
				button.get_node("spell").is_reaction = true
				if button.get_node("spell").can_cast(self):
					flag = true
				else:
					button.disable()
					
				button.connect("button_pressed", self, "emit_reaction_signal", \
					[ button.get_node("spell") ], CONNECT_ONESHOT)
				button.connect("button_entered", grid, "action_mouse_enter")
				button.connect("button_left", grid, "action_mouse_left")
				button.display_to(action_coords[i+1], flip)
	else:
		clear_reaction_folder()
		
		$reaction_buttons/cancel_button.display_to(action_coords[0], flip)
		
		for i in spells.size():
			var button = create_new_spell_button(spells[i], true)
			
			if button.has_node("spell"):
				button.get_node("spell").is_reaction = true
				if button.get_node("spell").can_cast(self):
					flag = true
				else:
					button.disable()
					
			button.connect("button_pressed", self, "emit_reaction_signal", [ spells[i] ], \
				CONNECT_ONESHOT)
			button.connect("button_entered", grid, "action_mouse_enter")
			button.connect("button_left", grid, "action_mouse_left")
			button.display_to(action_coords[i+1], flip)
		
	if flag:
		popup()
		return true
	else:
		hide_reactions()
		return false
	
func hide_reactions() -> void:
	for child in $reaction_buttons.get_children():
		child.undisplay_all()
		
	popup.hide()
	
func emit_reaction_signal(button, sig) -> void:
	emit_signal("reaction_finished", sig)
	
func select() -> void:
	var camera = Globals.get_camera()
	var flip : bool = false
	
	if global_position.x < camera.position.x + 22:
		flip = true
		
	for i in $action_buttons.get_child_count():
		var child = $action_buttons.get_child(i)
		var no_display : bool = false
		
		if has_condition("stun"):
			child.disable()
			
		match child.name:
			"move_button":
				if movement_left == 0 or has_condition("root"):
					child.disable()
			"attack_button":
				if not child.get_node("spell").can_cast(self):
					child.disable()
			"spell_button":
				var has_spell : bool = false
				for grandchild in child.get_node("action_subfolder").get_children():
					if grandchild.has_node("spell"):
						if not grandchild.get_node("spell").can_cast(self):
							grandchild.disable()
						else:
							has_spell = true
							
				if has_condition("silence") or not has_spell:
					child.disable()
			"item_button":
				if child.get_node("action_subfolder").get_child_count() == 0:
					child.disable()
					
				for grandchild in child.get_node("action_subfolder").get_children():
					if grandchild.has_node("item"):
						if not grandchild.get_node("item").can_cast(self):
							grandchild.disable()
			"talk_button":
				if has_condition("silence"):
					child.disable()
				var level = get_node("../../../")
				if not level.has_convo(self.name):
					no_display = true
					
				for convo_partner in level.get_convo_partners(self.name):
					convo_partner.show_talk_icon()
					connect("unfocused", convo_partner, "hide_talk_icon", [ ], CONNECT_ONESHOT)
					
		if not no_display:
			if flip:
				child.display_to(Globals.ACTION_COORDS_FLIP[i], flip)
			else:
				child.display_to(Globals.ACTION_COORDS[i], flip)
				
	popup()
	
func deselect() -> void:
	var camera = Globals.get_camera()
	
	for child in $action_buttons.get_children():
		child.undisplay_all()
		
	camera.set_popup_text("")
	camera.return_popup()
	
func _on_cancel_button_button_pressed(node):
	emit_reaction_signal(null, "cancel")
	
func _on_action_buttons_child_entered_tree(node):
	node.material = UNSHADED
	
func _on_reaction_buttons_child_entered_tree(node):
	node.material = UNSHADED
