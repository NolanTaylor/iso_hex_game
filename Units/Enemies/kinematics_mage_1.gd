extends "res://Units/Enemies/enemy_unit.gd"

func _ready():
	pass
	
func run_turn() -> void:
	# casts dissipate as favorably as possible, otherwise just stabs the closest unit.
	var grid = Globals.get_hex_grid()
	
	var best_coord = find_best_dissipate_cast()
	
	if !Globals.is_valid(best_coord):
		var lowest_unit = find_lowest_unit(get_units_in_range_spell($stab, "PlayerUnits"))
		
		if !Globals.is_valid(lowest_unit):
			if aggroed:
				lowest_unit = find_closest_unit(get_tree().get_nodes_in_group("PlayerUnits"))
				
				var path : PoolVector2Array = path_to_unit(lowest_unit)
				move(path, get_path_cost(path))
				
				if path.size() > 0:
					yield(self, "complete_movement")
					
				if has_condition("stun"):
					yield(get_tree().create_timer(0.1), "timeout")
					emit_signal("finished_turn")
					return
					
				yield(get_tree().create_timer(PAUSE_TIME), "timeout")
				emit_signal("finished_turn")
				return
			else:
				yield(get_tree().create_timer(PAUSE_TIME), "timeout")
				emit_signal("finished_turn")
				return
		else:
			aggroed = true
			
		path = path_to_cast_tile(lowest_unit, $stab)
		move(path, get_path_cost(path))
		
		if path.size() > 0:
			yield(self, "complete_movement")
			
		if has_condition("stun"):
			yield(get_tree().create_timer(0.1), "timeout")
			emit_signal("finished_turn")
			return
			
		if lowest_unit.coords in $stab.get_highlight_cells(self):
			ready_spell_cast($stab, null, "blue")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			
			ready_spell_cast($stab, lowest_unit.coords, "red")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			grid.de_gayify_the_map()
			
			current_spell = $stab
			current_target = lowest_unit.coords
			toss_skill("stab")
			var stab = grid.create_new_spell(get_node("stab"), self, lowest_unit.coords)
			yield(stab, "complete_cast")
	else:
		var path : PoolVector2Array = path_to_tile(best_coord)
		move(path, get_path_cost(path))
		
		if path.size() > 0:
			yield(self, "complete_movement")
			
		if has_condition("stun"):
			yield(get_tree().create_timer(0.1), "timeout")
			emit_signal("finished_turn")
			return
			
		ready_spell_cast($dissipate, null, "blue")
		yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
		
		ready_spell_cast($dissipate, null, "red")
		yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
		grid.de_gayify_the_map()
		
		current_spell = $dissipate
		current_target = Vector3.ZERO
		toss_skill("dissipate")
		var dissipate = grid.create_new_spell(get_node("dissipate"), self, coords)
		yield(dissipate, "complete_cast")
		
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		
	yield(get_tree().create_timer(PAUSE_TIME), "timeout")
	emit_signal("finished_turn")
	
