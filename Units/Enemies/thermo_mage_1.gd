extends "res://Units/Enemies/enemy_unit.gd"

var has_potion : bool = true

func _ready():
	pass
	
func run_turn() -> void:
	# casts fireball as favorably as possible from as far as possible
	var grid = Globals.get_hex_grid()
	
	var best_coord = find_best_fireball_cast()
	
	if !Globals.is_valid(best_coord):
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		emit_signal("finished_turn")
		return
		
	var path : PoolVector2Array = path_to_run_away_cast_tile(best_coord, $fireball)
	move(path, get_path_cost(path))
	
	if path.size() > 0:
		yield(self, "complete_movement")
		
	if has_condition("stun"):
		yield(get_tree().create_timer(0.1), "timeout")
		emit_signal("finished_turn")
		return
		
	ready_spell_cast($fireball, best_coord, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	
	ready_spell_cast($fireball, best_coord, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	grid.de_gayify_the_map()
	
	current_spell = $fireball
	current_target = best_coord
	toss_skill("fireball")
	var fireball = grid.create_new_spell(get_node("fireball"), self, best_coord)
	yield(fireball, "complete_cast")
	
	yield(get_tree().create_timer(PAUSE_TIME), "timeout")
	emit_signal("finished_turn")
