extends "res://Units/Enemies/enemy_unit.gd"

func _ready():
	pass
	
func run_turn() -> void:
	# moves toward the closest player unit and stabs them
	var grid = Globals.get_hex_grid()
	var closest_unit = find_closest_unit(get_units_in_range_spell($stab, "PlayerUnits"))
	
	if !Globals.is_valid(closest_unit):
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		emit_signal("finished_turn")
		return
		
	var path : PoolVector2Array = PoolVector2Array([Vector2(253, 40), Vector2(253, 56), \
		Vector2(253, 72), Vector2(230, 80)])
	move(path, get_path_cost(path))
	
	if path.size() > 0:
		yield(self, "complete_movement")
		
	ready_spell_cast($stab, null, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	
	ready_spell_cast($stab, closest_unit.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	grid.de_gayify_the_map()
	
	current_spell = $stab
	current_target = closest_unit.coords
	toss_skill("stab")
	var stab = grid.create_new_spell(current_spell, self, current_target)
	yield(stab, "complete_cast")
	
	yield(get_tree().create_timer(PAUSE_TIME), "timeout")
	emit_signal("finished_turn")
