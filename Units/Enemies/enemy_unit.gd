extends "res://Units/unit.gd"

const PAUSE_TIME : float = 0.1
const HIGHLIGHT_TIME : float = 0.4

export var aggro_area : NodePath
export var reinforcement : bool = false
export var reinforce_turn : int = 0
export var reinforce_tile : Vector3 = Vector3.ZERO
export var skill_list : PoolStringArray

enum Status {
	DORMANT,
	STUNNED,
	ACTIVE,
}

var running_turn : bool = false
var aggroed : bool = false
var current_spell = null
var current_target : Vector3 = Vector3.ZERO
var unit_hit_spaces : Dictionary = { # <Unit, PoolVector3Array>
	
}

signal finished_turn

func _ready():
	connect("finished_turn", self, "end_turn")
	
func _process(delta):
	pass
	
func unit_init() -> void:
	active = false
	selectable = false
	obstacle_mode = "player_units"
	
	for skill in skill_list:
		match skill:
			"health":
				max_health += 1
			"nucleosynthesis":
				# TODO: add no guard clause
				max_health += 4
			"guard":
				guard_left += 1
			"movement":
				movement += 1
				
	.unit_init()
	
func get_health_color() -> Color:
	return ENEMY_HEALTH_COLOR
	
func death(killer = null) -> void:
	if Globals.is_valid(killer):
		if $item_drop.get_child_count() != 0:
			Globals.get_hex_grid().acquire_item(killer, $item_drop.get_child(0))
			
	if running_turn:
		emit_signal("finished_turn")
		
	.death(killer)
	
# @return
# an int representing the state of the unit.
# 0 for a dormant unit or undeployed reinforcement (should be skipped without pause)
# 1 for a incapacitated unit (should be shown on camera and paused for)
# 2 for an active unit (should be yielded for its finished signal).
func turn_checks() -> int:
	var grid = Globals.get_hex_grid()
	
	if reinforcement:
		if grid.get_turn_count() >= reinforce_turn - 1:
			reinforcement = false
			aggroed = true
			var tile : Vector3 = reinforce_tile
			while not grid.teleport_unit(self, tile):
				tile += Globals.CUBE_VECTORS[randi() % 6]
			return Status.STUNNED
		else:
			return Status.DORMANT
			
	if has_condition("stun"):
		return Status.STUNNED
		
	if !aggro_area.is_empty() and !aggroed:
		for coord in get_node(aggro_area).area:
			var unit = grid.get_unit(coord)
			if Globals.is_valid(unit) and unit.is_in_group("PlayerUnits"):
				aggroed = true
				break
				
	running_turn = true
	return Status.ACTIVE
	
func toss_enter(button) -> void:
	if !Globals.is_valid(current_spell):
		return
		
	var grid = Globals.get_hex_grid()
	var cells = current_spell.get_flex_cells(self, current_target)
	var dict : Dictionary = current_spell.get_forecast(self, current_target)
	
	cells.append(current_target)
	
	grid.highlight_toss_layer(cells, dict)
	
func toss_left(button) -> void:
	var grid = Globals.get_hex_grid()
	
	grid.de_gayify_toss_layer()
	
# @arguments
# int max_range - the distance to search for units
# String group - the group to search in. if no group is given, searches all units
# 
# @return
# an array of all units within the given range belonging to the given group.
func get_units_in_range(max_range : int, group : String = "all") -> Array:
	var grid = Globals.get_hex_grid()
	var units : Array = []
	var search : Array = []
	
	if group == "all":
		search.append(get_tree().get_nodes_in_group("PlayerUnits"))
		search.append(get_tree().get_nodes_in_group("EnemyUnits"))
		search.append(get_tree().get_nodes_in_group("NeutralUnits"))
	else:
		search = get_tree().get_nodes_in_group(group)
		
	for player_unit in search:
		if Globals.hex_distance(coords, player_unit.coords) <= max_range:
			units.append(player_unit)
			
	return units
	
# @arguments
# Node spell - the spell from which to search for units
# String group - the group to search in. if no group is given, searches all units
#
# @return
# all units in the given group that self unit can reach with the given spell.
func get_units_in_range_spell(spell, group : String = "all") -> Array:
	if !Globals.is_valid(spell):
		return []
		
	var grid = Globals.get_hex_grid()
	var search : Array = []
	
	if group == "all":
		search.append_array(get_tree().get_nodes_in_group("PlayerUnits"))
		search.append_array(get_tree().get_nodes_in_group("EnemyUnits"))
		search.append_array(get_tree().get_nodes_in_group("NeutralUnits"))
	else:
		search = get_tree().get_nodes_in_group(group)
		
	var units_in_range : Array = []
	var tiles : PoolVector3Array = grid.flood_fill(coords, movement_left, obstacle_mode)
	var og_coords = Vector3(coords.x, coords.y, coords.z)
	
	unit_hit_spaces.clear()
	
	for tile in tiles:
		coords = tile
		var highlight_coords : PoolVector3Array = spell.get_highlight_cells(self)
		
		for unit in search:
			if unit.coords in highlight_coords:
				if unit_hit_spaces.has(unit):
					var arr : PoolVector3Array = unit_hit_spaces[unit]
					arr.append(tile)
					unit_hit_spaces[unit] = PoolVector3Array(arr)
				else:
					unit_hit_spaces[unit] = PoolVector3Array([tile])
					
				if not unit in units_in_range:
					units_in_range.append(unit)
				
	coords = og_coords
	return units_in_range
	
# @arguments
# Array units - the list of units from which to select the lowest
#
# @return
# the unit with the lowest health from the given array, or null if the array was empty. in case of
# a tie, returns the first unit found.
func find_lowest_unit(units : Array):
	if units.empty():
		return null
		
	var lowest_unit = units[0]
		
	for unit in units:
		if !Globals.is_valid(unit):
			continue
		if unit.health_left < lowest_unit.health_left:
			lowest_unit = unit
			
	return lowest_unit
	
# @arguments
# Array units - the list of units from which to select the healthiest
#
# @return
# the unit with the highest health from the given array, or null if the array was empty. in case of
# a tie, returns the first unit found.
func find_healthiest_unit(units : Array):
	if units.empty():
		return null
		
	var healthiest_unit = units[0]
	
	for unit in units:
		if !Globals.is_valid(unit):
			continue
		if unit.health_left > healthiest_unit.health_left:
			healthiest_unit = unit
			
	return healthiest_unit
	
# @arguments
# Array units - the list of units from which to select the closest
#
# @return
# the closest unit from the given array, or null if the array was empty. in cast of a tie, returns
# the first unit found.
func find_closest_unit(units : Array):
	if units.empty():
		return null
		
	var closest_unit = units[0]
	
	for unit in units:
		if !Globals.is_valid(unit):
			continue
		var dist = Globals.hex_distance(unit.coords, coords)
		var closest_dist = Globals.hex_distance(closest_unit.coords, coords)
		if dist < closest_dist:
			closest_unit = unit
			
	return closest_unit
	
# @return
# the Vector3 cell from which the dissipate spell deals the most damage to player units and
# least damage to enemy units, returns null if none were found.
func find_best_dissipate_cast():
	var grid = Globals.get_hex_grid()
	
	var flood : PoolVector3Array = grid.flood_fill(coords, movement_left, obstacle_mode)
	var best_coord = null
	var best_score : int = 0
	
	for cell in flood:
		if grid.has_occupant(cell):
			continue
			
		var cur_score : int = 0
		
		for inner_cell in Globals.ring(cell, 1):
			var unit = grid.get_unit(inner_cell)
			if !Globals.is_valid(unit):
				pass
			elif unit.is_in_group("PlayerUnits"):
				cur_score += 2
			elif unit.is_in_group("EnemyUnits"):
				cur_score -= 2
			elif unit.is_in_group("NeutralUnits") and unit.captured:
				cur_score -= 99
				
		for outer_cell in Globals.ring(cell, 2):
			var unit = grid.get_unit(outer_cell)
			if !Globals.is_valid(unit):
				pass
			elif unit.is_in_group("PlayerUnits"):
				cur_score += 1
			elif unit.is_in_group("EnemyUnits"):
				cur_score -= 1
			elif unit.is_in_group("NeutralUnits") and unit.captured:
				cur_score -= 99
				
		if cur_score > best_score:
			best_score = cur_score
			best_coord = cell
			
	if best_score > 0:
		return best_coord
	else:
		return null
		
# @return
# the Vector3 cell from which the fireball spell hits the most player units and the least
# enemy units, prioritizing closer cells, returns null if none were found.
func find_best_fireball_cast():
	if not has_node("fireball"):
		return null
		
	var grid = Globals.get_hex_grid()
	
	var potential_cast_cells : PoolVector3Array = PoolVector3Array([])
	var best_coord = null
	var best_score : int = 0
	
	for unit in get_units_in_range_spell(get_node("fireball"), "PlayerUnits"):
		var cell : Vector3 = unit.coords
		
		if not potential_cast_cells.has(cell):
			potential_cast_cells.append(cell)
			
		for adj in Globals.hex_adjacent(cell):
			if not potential_cast_cells.has(adj):
				potential_cast_cells.append(adj)
				
	for target in potential_cast_cells:
		var current_score : int = 0
		
		for cell in Globals.filled_circle(target, 1):
			var unit = grid.get_unit(cell)
			
			if !Globals.is_valid(unit):
				continue
				
			if unit.is_in_group("PlayerUnits"):
				current_score += 1
			if unit.is_in_group("EnemyUnits"):
				current_score -= 1
				
		if current_score > best_score:
			best_score = current_score
			best_coord = target
		elif current_score == best_score and Globals.is_valid(best_coord):
			var current_distance : int = Globals.hex_distance(coords, target)
			var best_distance : int = Globals.hex_distance(coords, best_coord)
			
			if current_distance < best_distance:
				best_score = current_score
				best_coord = target
				
	return best_coord
	
# @arguments
# PoolVector2Array path - the path to calculate the cost of
#
# @return
# the traversal cost of the given path.
func get_path_cost(path : PoolVector2Array) -> int:
	var grid = Globals.get_hex_grid()
	var path_cost : int = 0
	
	for point in path:
		path_cost += grid.grid[Globals.pixel_to_grid(point + \
			Vector2(10, 10))].traversal_cost
	path_cost -= grid.grid[self.coords].traversal_cost
	
	return path_cost
	
# @return
# the coordinate furthest away from a player unit reachable by self unit's base movement.
func run_away() -> Vector3:
	var grid = Globals.get_hex_grid()
	var player_units : Array = get_tree().get_nodes_in_group("PlayerUnits")
	var tiles : PoolVector3Array = grid.flood_fill(coords, movement_left, obstacle_mode)
	var furthest_coord : Vector3 = Vector3(coords.x, coords.y, coords.z)
	var longest_distance : int = 0
	
	for tile in tiles:
		var shortest_distance : int = 99
		
		for unit in player_units:
			var distance = Globals.hex_distance(tile, unit.coords)
			if distance < shortest_distance:
				shortest_distance = distance
				
		if shortest_distance > longest_distance:
			furthest_coord = tile
			longest_distance = shortest_distance
			
	return furthest_coord
	
# @arguments
# int teleport_range - the range of the teleport spell
#
# @return
# the furthest unoccupied Vector3 cell from a player unit within the given range.
func teleport_away(teleport_range : int) -> Vector3:
	var grid = Globals.get_hex_grid()
	var player_units : Array = get_tree().get_nodes_in_group("PlayerUnits")
	var tiles : PoolVector3Array = Globals.filled_circle(coords, teleport_range)
	var furthest_coord : Vector3 = Vector3(coords.x, coords.y, coords.z)
	var longest_distance : int = 0
	
	for tile in tiles:
		if grid.has_occupant(tile) or !grid.has_cell(tile):
			continue
			
		var shortest_distance : int = 99
		
		for unit in player_units:
			var distance = Globals.hex_distance(tile, unit.coords)
			if distance < shortest_distance:
				shortest_distance = distance
				
		if shortest_distance > longest_distance:
			furthest_coord = tile
			longest_distance = shortest_distance
			
	return furthest_coord
	
# @arguments
# int teleport_range - the range of the teleport spell
# Node unit - the unit to teleport to
#
# @return
# the closest unoccupied Vector3 cell to the given unit within the teleport range.
func teleport_to_unit(unit, teleport_range : int) -> Vector3:
	var grid = Globals.get_hex_grid()
	var tiles : PoolVector3Array = Globals.filled_circle(coords, teleport_range)
	var closest_coord : Vector3 = Vector3(coords.x, coords.y, coords.z)
	var shortest_distance : int = 99
	
	for tile in tiles:
		if grid.has_occupant(tile):
			continue
			
		var distance : int = Globals.hex_distance(unit.coords, tile)
		
		if distance < shortest_distance:
			shortest_distance = distance
			closest_coord = tile
			
			if shortest_distance == 1:
				break
				
	return closest_coord
	
# @arguments
# Vector3 tile - the tile to path to
#
# @return
# the PoolVector2Array path to the given tile (terrain cost not included).
func path_to_tile(tile : Vector3) -> PoolVector2Array:
	var grid = Globals.get_hex_grid()
	var path : PoolVector2Array = grid.find_path(coords, tile, obstacle_mode)
	
	return path
	
# @arguments
# Node unit - the unit to move towards
# int enemy_dist - the max distance self unit can be from the target (default 1)
#
# @return
# the PoolVector2Array path to the closest space to the range around the given unit. returns an
# empty path if the self unit is already within the given distance.
func path_to_unit(unit, enemy_dist : int = 1) -> PoolVector2Array:
	var grid = Globals.get_hex_grid()
	var path : PoolVector2Array = PoolVector2Array([])
	
	for i in movement_left + 1:
		path = grid.find_path(coords, unit.coords, obstacle_mode, i)
		if path.size() >= 2 and Globals.hex_distance(get_node( \
		"/root/Globals").pixel_to_grid(path[-1]+Vector2(10, 10)), unit.coords) <= enemy_dist:
			break
			
	return path
	
# @arguments
# Node target_unit - the unit being targeted by the spell
# Node spell - the spell being cast
#
# @return
# the PoolVector2Array path to the closest space from which self unit can hit the target unit
# with the given spell. returns an empty array if self unit can already target the given unit.
func path_to_cast_tile(target_unit, spell) -> PoolVector2Array:
	var grid = Globals.get_hex_grid()
	var shortest_path : PoolVector2Array = PoolVector2Array([])
	var smallest_path_size : int = 99
	
	if not unit_hit_spaces.has(target_unit):
		get_units_in_range_spell(spell)
		
		if not unit_hit_spaces.has(target_unit):
			return shortest_path
			
	for tile in unit_hit_spaces[target_unit]:
		var path = grid.find_path(coords, tile, obstacle_mode, movement_left)
		if path.size() < smallest_path_size:
			shortest_path = path
			smallest_path_size = path.size()
			
	return shortest_path
	
# @arguments
# Vector3 target_cell - the cell being targeted by the spell
# Node spell - the spell being cast
#
# @return
# the PoolVector2Array path to the space from which self unit can hit the given target cell
# while also being as far away from player units as possible.
func path_to_run_away_cast_tile(target_cell : Vector3, spell) -> PoolVector2Array:
	var grid = Globals.get_hex_grid()
	var path : PoolVector2Array = PoolVector2Array([])
	var flood : PoolVector3Array = grid.flood_fill(coords, movement_left, obstacle_mode)
	var player_units : Array = get_tree().get_nodes_in_group("PlayerUnits")
	var og_coords : Vector3 = Vector3(coords.x, coords.y, coords.z)
	var furthest_coord : Vector3 = Vector3(coords.x, coords.y, coords.z)
	var longest_distance : int = 0
	
	for tile in flood:
		coords = tile
		
		if not target_cell in spell.get_valid_cursor_cells(self):
			continue
			
		var shortest_distance : int = 99
		
		for unit in player_units:
			var distance = Globals.hex_distance(tile, unit.coords)
			if distance < shortest_distance:
				shortest_distance = distance
				
		if shortest_distance > longest_distance:
			furthest_coord = tile
			longest_distance = shortest_distance
		elif shortest_distance == longest_distance:
			var current_distance : int = Globals.hex_distance(og_coords, tile)
			var best_distance : int = Globals.hex_distance(og_coords, \
				furthest_coord)
			
			if current_distance < best_distance:
				furthest_coord = tile
				longest_distance = shortest_distance
			
	coords = og_coords
	return path_to_tile(furthest_coord)
	
# @arguments
# Node spell - the spell self unit will cast
# Vector3 target - the cell the spell will target
# String color - the color to highlight the cells
#
# @description
# highlights the cell(s) that self unit is about to affect.
func ready_spell_cast(spell, target, color : String = "pink") -> void:
	var grid = Globals.get_hex_grid()
	var cells : PoolVector3Array
	
	if !Globals.is_valid(target):
		cells = spell.get_highlight_cells(self)
	elif typeof(target) == TYPE_STRING and target == "player_units":
		for cell in spell.get_highlight_cells(self):
			var occupant = grid.get_unit(cell)
			if Globals.is_valid(occupant) and \
			occupant.is_in_group("PlayerUnits"):
				cells.append(cell)
	else:
		cells = spell.get_flex_cells(self, target)
		cells.append(target)
		
	for cell in cells:
		if grid.has_cell(cell):
			grid.grid[cell].set_highlight(color)
			
# @return
# true if the unit has an action or swift action left, false otherwise.
func has_swift() -> bool:
	if actions > 0 or swift_actions > 0:
		return true
	else:
		return false
		
func run_turn() -> void:
	# abstract function
	return
	
func run_turn_silenced() -> void:
	# abstract function
	return
	
func end_turn() -> void:
	running_turn = false
