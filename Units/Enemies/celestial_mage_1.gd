extends "res://Units/Enemies/enemy_unit.gd"

func _ready():
	pass
	
func run_turn() -> void:
	# moves toward the highest threat unit and roots them
	var grid = Globals.get_hex_grid()
	
	# TODO: change to unit with most skill points spent, is health for now
	var units_in_range : Array = get_units_in_range_spell($starbind, "PlayerUnits")
	var healthiest_unit = find_healthiest_unit(units_in_range)
	
	for unit in units_in_range:
		if healthiest_unit.has_condition("root"):
			units_in_range.erase(healthiest_unit)
			healthiest_unit = find_healthiest_unit(units_in_range)
		else:
			break
			
	if !Globals.is_valid(healthiest_unit):
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		emit_signal("finished_turn")
		return
		
	var path : PoolVector2Array = path_to_cast_tile(healthiest_unit, $starbind)
	move(path, get_path_cost(path))
	
	if path.size() > 0:
		yield(self, "complete_movement")
		
	if has_condition("stun"):
		yield(get_tree().create_timer(0.1), "timeout")
		emit_signal("finished_turn")
		return
		
	if healthiest_unit.coords in $starbind.get_highlight_cells(self):
		ready_spell_cast($starbind, null, "blue")
		yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
		grid.de_gayify_the_map()
		ready_spell_cast($starbind, "player_units", "blue")
		yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
		ready_spell_cast($starbind, healthiest_unit.coords, "red")
		yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
		grid.de_gayify_the_map()
		
		current_spell = $starbind
		current_target = healthiest_unit.coords
		toss_skill("starbind")
		var starbind = grid.create_new_spell(get_node("starbind"), self, healthiest_unit.coords)
		yield(starbind, "complete_cast")
		
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
	
	yield(get_tree().create_timer(PAUSE_TIME), "timeout")
	emit_signal("finished_turn")
	
