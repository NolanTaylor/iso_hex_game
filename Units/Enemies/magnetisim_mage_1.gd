extends "res://Units/Enemies/enemy_unit.gd"

func _ready():
	pass
	
func run_turn() -> void:
	# heals the lowest health enemy unit or stabs a player unit
	var grid = Globals.get_hex_grid()
	
	var units_in_range : Array = get_units_in_range_spell($restoration, "EnemyUnits")
	var lowest_ally = find_lowest_unit(units_in_range)
	
	if Globals.is_valid(lowest_ally) \
	and lowest_ally.health_left <= lowest_ally.max_health - 2:
		path = path_to_cast_tile(lowest_ally, $restoration)
		move(path, get_path_cost(path))
		
		if path.size() > 0:
			yield(self, "complete_movement")
			
		if has_condition("stun"):
			yield(get_tree().create_timer(0.1), "timeout")
			emit_signal("finished_turn")
			return
			
		if lowest_ally.coords in $restoration.get_highlight_cells(self):
			ready_spell_cast($restoration, null, "blue")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			
			ready_spell_cast($restoration, lowest_ally.coords, "green")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			grid.de_gayify_the_map()
			
			current_spell = $restoration
			current_target = lowest_ally.coords
			toss_skill("restoration")
			var restoration = grid.create_new_spell(get_node("restoration"), self, lowest_ally.coords)
			yield(restoration, "complete_cast")
	else:
		var lowest_unit = find_lowest_unit(get_units_in_range_spell($stab, "PlayerUnits"))
		
		if !Globals.is_valid(lowest_unit):
			yield(get_tree().create_timer(PAUSE_TIME), "timeout")
			emit_signal("finished_turn")
			return
			
		path = path_to_cast_tile(lowest_unit, $stab)
		move(path, get_path_cost(path))
		
		if path.size() > 0:
			yield(self, "complete_movement")
			
		if has_condition("stun"):
			yield(get_tree().create_timer(0.1), "timeout")
			emit_signal("finished_turn")
			return
			
		if lowest_unit.coords in $stab.get_highlight_cells(self):
			ready_spell_cast($stab, null, "blue")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			
			ready_spell_cast($stab, lowest_unit.coords, "red")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			grid.de_gayify_the_map()
			
			current_spell = $stab
			current_target = lowest_unit.coords
			toss_skill("stab")
			var stab = grid.create_new_spell(get_node("stab"), self, lowest_unit.coords)
			yield(stab, "complete_cast")
			
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		
	yield(get_tree().create_timer(PAUSE_TIME), "timeout")
	emit_signal("finished_turn")
	
