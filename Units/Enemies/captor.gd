extends "res://Units/Enemies/enemy_unit.gd"

func _ready():
	pass
	
func run_turn() -> void:
	# moves toward the lowest health unit in range then casts bolt
	var grid = Globals.get_hex_grid()
	var lowest_unit = find_lowest_unit(get_units_in_range_spell($bolt, "PlayerUnits"))
	
	if !Globals.is_valid(lowest_unit):
		yield(get_tree().create_timer(0.1), "timeout")
		emit_signal("finished_turn")
		return
		
	var path : PoolVector2Array = path_to_cast_tile(lowest_unit, $bolt)
	move(path, 0)
	
	if path.size() > 0:
		yield(self, "complete_movement")
		
	if has_condition("stun"):
		yield(get_tree().create_timer(0.1), "timeout")
		emit_signal("finished_turn")
		return
		
	ready_spell_cast($bolt, null, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	grid.de_gayify_the_map()
	ready_spell_cast($bolt, "player_units", "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	ready_spell_cast($bolt, lowest_unit.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	grid.de_gayify_the_map()
	
	current_spell = $bolt
	current_target = lowest_unit.coords
	toss_skill("bolt")
	var bolt = grid.create_new_spell(current_spell, self, current_target)
	yield(bolt, "complete_cast")
	
	yield(get_tree().create_timer(0.1), "timeout")
	emit_signal("finished_turn")
	
