extends "res://Units/Enemies/enemy_unit.gd"

var has_potion : bool = true

func _ready():
	pass
	
func run_turn() -> void:
	# casts blind on the healthiest unit, then moves to auto attack the lowest health unit
	var grid = Globals.get_hex_grid()
	
	if health_left == 1 and has_potion:
		path = grid.find_path(coords, run_away(), "player_units")
		move(path, get_path_cost(path))
		
		if path.size() > 0:
			yield(self, "complete_movement")
			
		if has_condition("stun"):
			yield(get_tree().create_timer(0.1), "timeout")
			emit_signal("finished_turn")
			return
			
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		
		# animate drinking a potion or smth idk
		heal(1)
		has_potion = false
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		emit_signal("finished_turn")
		return
		
	var units_in_range : Array = get_units_in_range_spell($blind, "PlayerUnits")
	var healthiest_unit = find_healthiest_unit(units_in_range)
	
	for unit in units_in_range:
		if healthiest_unit.has_condition("blind"):
			units_in_range.erase(healthiest_unit)
			healthiest_unit = find_healthiest_unit(units_in_range)
		else:
			break
			
	if !Globals.is_valid(healthiest_unit):
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		emit_signal("finished_turn")
		return
		
	var path : PoolVector2Array = path_to_cast_tile(healthiest_unit, $blind)
	move(path, get_path_cost(path))
	
	if path.size() > 0:
		yield(self, "complete_movement")
		
	if has_condition("stun"):
		yield(get_tree().create_timer(0.1), "timeout")
		emit_signal("finished_turn")
		return
		
	if healthiest_unit.coords in $blind.get_highlight_cells(self):
		ready_spell_cast($blind, null, "blue")
		yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
		
		ready_spell_cast($blind, healthiest_unit.coords, "red")
		yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
		grid.de_gayify_the_map()
		
		current_spell = $blind
		current_target = healthiest_unit.coords
		toss_skill("blind")
		var blind = grid.create_new_spell(get_node("blind"), self, healthiest_unit.coords)
		yield(blind, "complete_cast")
		
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		
	if has_swift():
		var lowest_unit = find_lowest_unit(get_units_in_range_spell($stab, "PlayerUnits"))
		
		if !Globals.is_valid(lowest_unit):
			yield(get_tree().create_timer(PAUSE_TIME), "timeout")
			emit_signal("finished_turn")
			return
			
		path = path_to_cast_tile(lowest_unit, $stab)
		move(path, get_path_cost(path))
		
		if path.size() > 0:
			yield(self, "complete_movement")
			
		if has_condition("stun"):
			yield(get_tree().create_timer(0.1), "timeout")
			emit_signal("finished_turn")
			return
			
		if lowest_unit.coords in $stab.get_highlight_cells(self):
			ready_spell_cast($stab, null, "blue")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			
			ready_spell_cast($stab, lowest_unit.coords, "red")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			grid.de_gayify_the_map()
			
			current_spell = $stab
			current_target = lowest_unit.coords
			toss_skill("stab")
			var stab = grid.create_new_spell(get_node("stab"), self, lowest_unit.coords)
			yield(stab, "complete_cast")
			
	yield(get_tree().create_timer(PAUSE_TIME), "timeout")
	
	path = path_to_tile(run_away())
	move(path, get_path_cost(path))
	
	if path.size() > 0:
		yield(self, "complete_movement")
	
	yield(get_tree().create_timer(PAUSE_TIME), "timeout")
	emit_signal("finished_turn")
	
