extends "res://Units/Enemies/enemy_unit.gd"

var has_potion : bool = true

func _ready():
	pass
	
func run_turn() -> void:
	# teleports to and casts edge of chaos on the lowest health unit
	var grid = Globals.get_hex_grid()
	
	var lowest_unit = find_lowest_unit(get_units_in_range(8, "PlayerUnits"))
	var units_in_edging_range : Array = get_units_in_range_spell($edge_of_chaos, "PlayerUnits")
	
	if lowest_unit in units_in_edging_range:
		var path : PoolVector2Array = path_to_cast_tile(lowest_unit, $edge_of_chaos)
		move(path, get_path_cost(path))
		
		if path.size() > 0:
			yield(self, "complete_movement")
			
		if has_condition("stun"):
			yield(get_tree().create_timer(0.1), "timeout")
			emit_signal("finished_turn")
			return
			
		ready_spell_cast($edge_of_chaos, null, "blue")
		yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
		
		ready_spell_cast($edge_of_chaos, lowest_unit.coords, "red")
		yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
		grid.de_gayify_the_map()
		
		current_spell = $edge_of_chaos
		current_target = lowest_unit.coords
		toss_skill("edge_of_chaos")
		var edge = grid.create_new_spell(get_node("edge_of_chaos"), self, lowest_unit.coords)
		yield(edge, "complete_cast")
		
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		
		var furthest_tile : Vector3 = teleport_away(5)
		
		if furthest_tile != coords and has_swift():
			ready_spell_cast($fractal_dimension, null, "blue")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			
			ready_spell_cast($fractal_dimension, furthest_tile, "red")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			grid.de_gayify_the_map()
			
			current_spell = $fractal_dimension
			current_target = furthest_tile
			toss_skill("fractal_dimension")
			var fractal = grid.create_new_spell(get_node("fractal_dimension"), self, furthest_tile)
			yield(fractal, "complete_cast")
	else:
		var closest_tile : Vector3 = teleport_to_unit(lowest_unit, 5)
		
		if closest_tile != coords:
			ready_spell_cast($fractal_dimension, null, "blue")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			
			ready_spell_cast($fractal_dimension, closest_tile, "red")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			grid.de_gayify_the_map()
			
			current_spell = $fractal_dimension
			current_target = closest_tile
			toss_skill("fractal_dimension")
			var fractal = grid.create_new_spell(get_node("fractal_dimension"), self, closest_tile)
			yield(fractal, "complete_cast")
			
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		
		var path : PoolVector2Array = path_to_cast_tile(lowest_unit, $edge_of_chaos)
		move(path, get_path_cost(path))
		
		if path.size() > 0:
			yield(self, "complete_movement")
			
		if has_condition("stun"):
			yield(get_tree().create_timer(0.1), "timeout")
			emit_signal("finished_turn")
			return
			
		if lowest_unit.coords in $edge_of_chaos.get_highlight_cells(self) and has_swift():
			ready_spell_cast($edge_of_chaos, null, "blue")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			
			ready_spell_cast($edge_of_chaos, lowest_unit.coords, "red")
			yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
			grid.de_gayify_the_map()
			
			current_spell = $edge_of_chaos
			current_target = lowest_unit.coords
			toss_skill("edge_of_chaos")
			var edge = grid.create_new_spell(get_node("edge_of_chaos"), self, lowest_unit.coords)
			yield(edge, "complete_cast")
			
		yield(get_tree().create_timer(PAUSE_TIME), "timeout")
		
		path = path_to_tile(run_away())
		move(path, get_path_cost(path))
		
	yield(get_tree().create_timer(PAUSE_TIME), "timeout")
	emit_signal("finished_turn")
