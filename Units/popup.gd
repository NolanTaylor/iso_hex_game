extends Node2D

const UNSHADED : CanvasItemMaterial = preload("res://Global/Materials/unshaded.tres")

const LIGHT_ICON : Texture = preload("res://Assets/GUI/charHUD_light_icon.png")
const WORK_ICON : Texture = preload("res://Assets/GUI/charHUD_work_icon.png")
const FLUX_ICON : Texture = preload("res://Assets/GUI/charHUD_flux_icon.png")
const ENTROPY_ICON : Texture = preload("res://Assets/GUI/charHUD_entropy_icon.png")
const RAPIDITY_ICON : Texture = preload("res://Assets/GUI/charHUD_rapidity_icon.png")
const LUNE_ICON : Texture = preload("res://Assets/GUI/charHUD_lune_icon.png")

var current_cost
var fading : bool = false

func _ready():
	pass
	
func apply_ui(unit : Node) -> bool:
	var flag : bool = false
	
	for child in $actions.get_children():
		child.queue_free()
		
	for child in $resources.get_children():
		for grandchild in child.get_children():
			grandchild.queue_free()
		
	for child in $conditions.get_children():
		child.queue_free()
		
	for child in $health_and_guard/guard.get_children():
		child.queue_free()
		
	for i in unit.guard_left:
		var new_popup_guard : TextureRect = TextureRect.new()
		
		new_popup_guard.texture = unit.GUARD_ICON
		new_popup_guard.material = UNSHADED
		
		new_popup_guard.mouse_filter = Control.MOUSE_FILTER_PASS
		
		new_popup_guard.connect("mouse_entered", self, "set_popup_text", ["Guard"])
		new_popup_guard.connect("mouse_exited", self, "set_popup_text", [""])
		
		$health_and_guard/guard.add_child(new_popup_guard)
		
	for i in unit.max_health:
		var pop_child = $health_and_guard/health.get_child(i)
		
		if Globals.is_valid(pop_child):
			if i < unit.corrupted_health:
				pop_child.texture = unit.health_icon_corrupted
				pop_child.connect("mouse_entered", self, "set_popup_text", ["Corruption"])
				pop_child.connect("mouse_exited", self, "set_popup_text", [""])
			elif i < unit.health_left:
				pop_child.texture = unit.health_icon
				pop_child.connect("mouse_entered", self, "set_popup_text", ["Health"])
				pop_child.connect("mouse_exited", self, "set_popup_text", [""])
			else:
				pop_child.texture = unit.health_icon_empty
				pop_child.connect("mouse_entered", self, "set_popup_text", ["Empty"])
				pop_child.connect("mouse_exited", self, "set_popup_text", [""])
				
	for i in unit.light:
		add_mana_icon("light")
	for i in unit.work:
		add_mana_icon("work")
	for i in unit.flux:
		add_mana_icon("flux")
	for i in unit.entropy:
		add_mana_icon("entropy")
	for i in unit.rapidity:
		add_mana_icon("rapidity")
	for i in unit.lune:
		add_mana_icon("lune")
		
	for condition in unit.get_node("conditions").get_children():
		if condition.condition_type == "public_condition":
			var new_icon : TextureRect = TextureRect.new()
			new_icon.texture = condition.condition_icon
			new_icon.mouse_filter = Control.MOUSE_FILTER_PASS
			new_icon.material = UNSHADED
			new_icon.connect("mouse_entered", self, "set_popup_text", [condition.proper_name])
			new_icon.connect("mouse_exited", self, "set_popup_text", [""])
			$conditions.add_child(new_icon)
			
	for i in unit.actions:
		var new_action : TextureRect = TextureRect.new()
		new_action.texture = unit.ACTION_ICON
		new_action.mouse_filter = Control.MOUSE_FILTER_PASS
		new_action.material = UNSHADED
		new_action.editor_description = "action"
		new_action.connect("mouse_entered", self, "set_popup_text", ["Action"])
		new_action.connect("mouse_exited", self, "set_popup_text", [""])
		$actions.add_child(new_action)
		flag = true
	for i in unit.swift_actions:
		var new_swift : TextureRect = TextureRect.new()
		new_swift.texture = unit.SWIFT_ICON
		new_swift.mouse_filter = Control.MOUSE_FILTER_PASS
		new_swift.material = UNSHADED
		new_swift.editor_description = "swift_action"
		new_swift.connect("mouse_entered", self, "set_popup_text", ["Swift Action"])
		new_swift.connect("mouse_exited", self, "set_popup_text", [""])
		$actions.add_child(new_swift)
		flag = true
	for i in unit.reactions:
		var new_reaction : TextureRect = TextureRect.new()
		new_reaction.texture = unit.REACTION_ICON
		new_reaction.mouse_filter = Control.MOUSE_FILTER_PASS
		new_reaction.material = UNSHADED
		$actions.add_child(new_reaction)
		new_reaction.connect("mouse_entered", self, "set_popup_text", ["Reaction"])
		new_reaction.connect("mouse_exited", self, "set_popup_text", [""])
		new_reaction.editor_description = "reaction"
	if unit.attacking:
		flag = true
	if unit.movement_left > 0:
		flag = true
			
	return flag
	
func add_mana_icon(resource : String) -> void:
	var new_tex : TextureRect = TextureRect.new()
	var proper_name : String = ""
	
	new_tex.mouse_filter = Control.MOUSE_FILTER_PASS
	new_tex.material = UNSHADED
	
	match resource:
		"light":
			new_tex.texture = LIGHT_ICON
			proper_name = "Light"
			$resources/light_container.add_child(new_tex)
		"work":
			new_tex.texture = WORK_ICON
			proper_name = "Work"
			$resources/work_container.add_child(new_tex)
		"flux":
			new_tex.texture = FLUX_ICON
			proper_name = "Flux"
			$resources/flux_container.add_child(new_tex)
		"entropy":
			new_tex.texture = ENTROPY_ICON
			proper_name = "Entropy"
			$resources/entropy_container.add_child(new_tex)
		"rapidity":
			new_tex.texture = RAPIDITY_ICON
			proper_name = "Rapidity"
			$resources/rapidity_container.add_child(new_tex)
		"lune":
			new_tex.texture = LUNE_ICON
			proper_name = "Lune"
			$resources/lune_container.add_child(new_tex)
			
	new_tex.connect("mouse_entered", self, "set_popup_text", [proper_name])
	new_tex.connect("mouse_exited", self, "set_popup_text", [""])
	
func set_popup_text(text : String) -> void:
	if get_child_count() == 0:
		return
		
	$tab/tab_label.text = text
	
func preview_cost(spell_cost) -> void:
	$tex_tween.remove_all()
	for child in $resources.get_children():
		for grandchild in child.get_children():
			grandchild.modulate.a = 1.0
	for child in $actions.get_children():
		child.modulate.a = 1.0
	if Globals.is_valid(spell_cost):
		fading = false
		current_cost = spell_cost
		_on_tex_tween_tween_all_completed()
		
func blink_tex(container, resource : int, fade_in : bool) -> void:
	if !Globals.is_valid(container):
		return
	if container.get_child_count() == 0:
		return
		
	var from : float = 0.2
	var to : float = 1.0
	
	if fade_in:
		var temp = to
		to = from
		from = temp
		
	if container == $actions:
		for i in range(container.get_child_count() - 1, -1, -1):
			var child = container.get_child(i)
			var desc : String = child.editor_description
			
			if desc == "action" and resource == 3:
				$tex_tween.interpolate_property(child, "modulate:a", from, to, 0.4, \
					Tween.TRANS_LINEAR)
				return
			if desc == "swift_action" and resource == 2:
				$tex_tween.interpolate_property(child, "modulate:a", from, to, 0.4, \
					Tween.TRANS_LINEAR)
				return
			if desc == "reaction" and resource == 1:
				$tex_tween.interpolate_property(child, "modulate:a", from, to, 0.4, \
					Tween.TRANS_LINEAR)
				return
				
		var tex = container.get_child(0)
		if tex.editor_description == "action" and resource == 2:
			$tex_tween.interpolate_property(tex, "modulate:a", from, to, 0.4, \
				Tween.TRANS_LINEAR)
				
		return
		
	for i in range(container.get_child_count()):
		var child = container.get_child(i)
		if i >= container.get_child_count() - resource:
			$tex_tween.interpolate_property(child, "modulate:a", from, to, 0.4, \
				Tween.TRANS_LINEAR)
		else:
			child.modulate.a = 1.0
			
	
func _on_popup_child_entered_tree(node):
	if node is CanvasItem:
		node.use_parent_material = true
		
func _on_tex_tween_tween_all_completed():
	if fading:
		fading = false
	else:
		fading = true
		
	for i in $resources.get_child_count():
		blink_tex($resources.get_child(i), current_cost.get_resource(i), fading)
		
	blink_tex($actions, current_cost.timing, fading)
	
	$tex_tween.start()
