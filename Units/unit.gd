extends Node2D

const FONT = preload("res://Assets/Fonts/font.ttf")
const UNSHADED : CanvasItemMaterial = preload("res://Global/Materials/unshaded.tres")

const HEALTH_ICON : Texture = preload("res://Assets/GUI/charHUD_health_icon.png")
const HEALTH_ICON_EMPTY : Texture = preload("res://Assets/GUI/charHUD_healthEMPTY_icon.png")
const HEALTH_ICON_CORRUPTED : Texture = preload( \
	"res://Assets/GUI/charHUD_corrupted_health_icon.png")
const HEALTH_SMALL_ICON : Texture = preload("res://Assets/GUI/charHUD_health_small_icon.png")
const HEALTH_SMALL_ICON_EMPTY : Texture = preload( \
	"res://Assets/GUI/charHUD_healthEMPTY_small_icon.png")
const HEALTH_SMALL_ICON_CORRUPTED : Texture = preload( \
	"res://Assets/GUI/charHUD_corrupted_health_small_icon.png")
const GUARD_ICON : Texture = preload("res://Assets/GUI/charHUD_guard_icon.png")
const PLAYER_HEALTH_COLOR : Color = Color("b2f277")
const ENEMY_HEALTH_COLOR : Color = Color("c33d53")
const NEUTRAL_HEALTH_COLOR : Color = Color("73ecff")
const ACTION_ICON : Texture = preload("res://Assets/GUI/charHUD_action_icon.png")
const SWIFT_ICON : Texture = preload("res://Assets/GUI/charHUD_swift_icon.png")
const REACTION_ICON : Texture = preload("res://Assets/GUI/charHUD_reaction_icon.png")
const MOVE_ICON : Texture = preload("res://Assets/GUI/charHUD_move_icon.png")

const LIGHT_ICON : Texture = preload("res://Assets/GUI/charHUD_light_icon.png")
const WORK_ICON : Texture = preload("res://Assets/GUI/charHUD_work_icon.png")
const FLUX_ICON : Texture = preload("res://Assets/GUI/charHUD_flux_icon.png")
const ENTROPY_ICON : Texture = preload("res://Assets/GUI/charHUD_entropy_icon.png")
const RAPIDITY_ICON : Texture = preload("res://Assets/GUI/charHUD_rapidity_icon.png")
const LUNE_ICON : Texture = preload("res://Assets/GUI/charHUD_lune_icon.png")

const TOSS_START : Vector2 = Vector2(8, -12)

export var SPEED : int = 80

onready var popup = $popup

var condition_resources : Dictionary = { # <String, PackedScene>
	
}

var range_modifications : Dictionary = { # <String, int>
	
}

var damage_modifications : Dictionary = { # <String, int>
	
}

var healing_modifications : Dictionary = { # <String, int>
	
}

var item_modifications : Dictionary = { # <String, int>
	
}

var special_modifications : Dictionary = { # <String, bool>
	
}

var data : Dictionary = { # <String, Object>
	
}

var starting_modulate : Color = modulate
var disabled_modulate : Color = Color(starting_modulate.r / 2, starting_modulate.g / 2, \
	starting_modulate.b / 2)
	
var health_icon : Texture = HEALTH_ICON
var health_icon_empty : Texture = HEALTH_ICON_EMPTY
var health_icon_corrupted : Texture = HEALTH_ICON_CORRUPTED

var expiring_conditions : Array = []
var coords : Vector3 = Vector3.ZERO
var starting_coords : Vector3 = Vector3.ZERO
var obstacle_mode : String = ""
var triggered : int = 0
var selectable : bool = false
var has_hovering_cursor : bool = false
var moving : bool = false
var attacking : bool = false
var bound : bool = false
var disabled : bool = false
var active : bool = true
var permanent : bool = false
var damage_requested : bool = false
var path : PoolVector2Array = PoolVector2Array([])
var ults_cast : PoolStringArray = PoolStringArray([])

var movement : int = 3
var max_health : int = 1

var movement_left : int = 0
var health_left : int = 0
var corrupted_health : int = 0
var guard_left : int = 0
var actions : int = 1
var swift_actions : int = 1
var reactions : int = 1

var light : int = 0
var work : int = 0
var flux : int = 0
var entropy : int = 0
var rapidity : int = 0
var lune : int = 0

signal complete_movement(unit)
signal damage_dealt(dmg)
signal lost_life(dead)

func _ready():
	unit_init()
	
func _process(delta):
	if moving:
		move_along_path(SPEED * delta)
	elif attacking:
		pass
	elif bound:
		pass
	else:
		if has_hovering_cursor:
			if $sprite.get_sprite_frames().has_animation("idle"):
				$sprite.play("idle")
			has_hovering_cursor = false
		else:
			if not disabled and active:
				if $sprite.get_sprite_frames().has_animation("idle"):
					$sprite.play("idle")
			else:
				$sprite.play("freeze")
				
func unit_init() -> void:
	health_left = max_health
	
	# TODO: add extra condition for very small icons
	if max_health >= 6:
		health_icon = HEALTH_SMALL_ICON
		health_icon_empty = HEALTH_SMALL_ICON_EMPTY
		health_icon_corrupted = HEALTH_SMALL_ICON_CORRUPTED
		
		$ui_canvas/top_ui/health.add_constant_override("separation", -5)
	
	for i in range(max_health):
		var new_capsule : TextureRect = TextureRect.new()
		new_capsule.texture = health_icon_empty
		new_capsule.modulate = get_health_color()
		new_capsule.mouse_filter = Control.MOUSE_FILTER_IGNORE
		new_capsule.material = UNSHADED
		$ui_canvas/top_ui/health.add_child(new_capsule)
		
		var pop_capsule = new_capsule.duplicate()
		pop_capsule.mouse_filter = Control.MOUSE_FILTER_PASS
		new_capsule.modulate = get_health_color()
		pop_capsule.connect("mouse_entered", self, "set_popup_text", ["Health"])
		pop_capsule.connect("mouse_exited", self, "set_popup_text", [""])
		$popup/health_and_guard/health.add_child(pop_capsule)
		
	apply_ui()
	
func recheck_max_health() -> void:
	health_left = max_health
	
	for i in range(max_health):
		if Globals.is_valid($ui_canvas/top_ui/health.get_child(i)):
			continue
			
		var new_capsule : TextureRect = TextureRect.new()
		new_capsule.texture = health_icon_empty
		new_capsule.modulate = get_health_color()
		new_capsule.mouse_filter = Control.MOUSE_FILTER_IGNORE
		new_capsule.material = UNSHADED
		$ui_canvas/top_ui/health.add_child(new_capsule)
		
		var pop_capsule = new_capsule.duplicate()
		pop_capsule.mouse_filter = Control.MOUSE_FILTER_PASS
		new_capsule.modulate = get_health_color()
		pop_capsule.connect("mouse_entered", self, "set_popup_text", ["Health"])
		pop_capsule.connect("mouse_exited", self, "set_popup_text", [""])
		$popup/health_and_guard/health.add_child(pop_capsule)
		
	apply_ui()
	
func get_health_color() -> Color:
	# abstract function
	return Color.black
	
func get_modification(type : String, properties : Array) -> int:
	var final_mod : int = 0
	
	match type:
		"range":
			for property in properties:
				if property in range_modifications.keys():
					final_mod += range_modifications[property]
		"damage":
			for property in properties:
				if property in damage_modifications.keys():
					final_mod += damage_modifications[property]
		"healing":
			for property in properties:
				if property in healing_modifications.keys():
					final_mod += healing_modifications[property]
		"item":
			for property in properties:
				if property in item_modifications.keys():
					final_mod += item_modifications[property]
			
	return final_mod
	
func add_mod(type : String, mod : String, quantity : int = 1) -> void:
	match type:
		"range":
			if range_modifications.has(mod):
				range_modifications[mod] += quantity
			else:
				range_modifications[mod] = quantity
		"damage":
			if damage_modifications.has(mod):
				damage_modifications[mod] += quantity
			else:
				damage_modifications[mod] = quantity
		"healing":
			if healing_modifications.has(mod):
				healing_modifications[mod] += quantity
			else:
				healing_modifications[mod] = quantity
		"special":
			special_modifications[mod] = true
			
func cast_ult(ult : String) -> void:
	ults_cast.append(ult)
	
func has_cast_ult(ult : String) -> bool:
	return ults_cast.has(ult)
	
func has_special_modification(type : String) -> bool:
	if special_modifications.has(type):
		return special_modifications[type]
	else:
		return false
		
func has_condition(condition : String) -> bool:
	return $conditions.has_node(condition)
	
func get_condition(condition : String):
	if has_condition(condition):
		return $conditions.get_node(condition)
	else:
		return null
		
func get_all_conditions() -> Array:
	return $conditions.get_children()
	
func remove_condition(condition : String) -> void:
	if has_condition(condition):
		get_condition(condition).remove_condition()
		
func find_grid() -> Vector3:
	var grid : Vector3 = Vector3(0, 0, 0)
	
	grid.x = global_position.x / 23
	grid.y = ((global_position.y - 16) / 16) - (grid.x / 2)
	grid.z = -(grid.x + grid.y)
	
	coords = grid
	
	return grid
	
func hover() -> void:
	has_hovering_cursor = true
	
func upkeep() -> void:
	starting_coords = coords
	movement_left = movement
	actions = 1
	swift_actions = 1
	reactions = 1
	active = true
	disabled = false
	enable()
	apply_ui()
	
	for condition in $conditions.get_children():
		condition.upkeep()
		
func endstep() -> void:
	active = false
	
	for condition in $conditions.get_children():
		condition.endstep()
		
	apply_ui()
	enable()
	
func runSBAs() -> void:
	for condition in $conditions.get_children():
		condition.runSBAs()
		
func vanish() -> void:
	var grid = Globals.get_hex_grid()
	grid.cancel_move_state()
	grid.grid[coords].set_occupying_unit(null)
	queue_free()
	
func death(killer = null) -> void:
	var grid = Globals.get_hex_grid()
	
	if Globals.is_valid(killer):
		var reactions : PoolStringArray = PoolStringArray([])
		
		if killer.has_condition("uncertainty_3"):
			reactions.append("uncertainty")
		if reactions.empty():
			grid.grid[coords].set_occupying_unit(null)
			queue_free()
			return
			
		if not killer.trigger_reaction(reactions):
			grid.grid[coords].set_occupying_unit(null)
			queue_free()
			return
		killer.triggered += 1
		grid.wait_for_spell = true
		grid.go_to_reaction_state()
		var reaction = yield(killer, "reaction_finished")
		killer.hide_reactions()
		killer.triggered -= 1
		
		match reaction:
			"uncertainty":
				var uncertainty = grid.create_new_spell("uncertainty", killer, killer.coords, \
					false, false)
				uncertainty.is_reaction = true
				uncertainty.ignore_yield()
				uncertainty.cast(killer, Vector3.ZERO)
				yield(uncertainty, "complete_cast")
				grid.cancel_reaction_state()
			"cancel":
				grid.cancel_reaction_state()
				
	grid.grid[coords].set_occupying_unit(null)
	queue_free()
	
func request_damage() -> void:
	damage_requested = true
	
func deal_damage(damage : int, origin = null, pierce : bool = false) -> int:
	var grid = Globals.get_hex_grid()
	var reactions : Dictionary = {} # <unit, Array<String>>
	var damage_dealt : int = 0
	var modified_damage : int = damage
	
	if has_condition("immune"):
		return
	if has_condition("vulnerable"):
		modified_damage += 1
		
	if damage_requested:
		damage_requested = false
		emit_signal("damage_dealt", modified_damage)
		return
		
	if has_condition("spectrum_shatter_dynamic"):
		var condition = get_condition("spectrum_shatter_dynamic")
		for trigger in condition.triggers.keys():
			if Globals.hex_distance(coords, trigger.coords) <= \
			condition.triggers[trigger]:
				if reactions.has(trigger):
					reactions[trigger].append("spectrum_shatter")
				else:
					reactions[trigger] = ["spectrum_shatter"]
	if has_condition("guardian_dynamic") and (guard_left == 0 or pierce) and \
	not has_cast_ult("guardian") and modified_damage >= health_left:
		var condition = get_condition("guardian_dynamic")
		for trigger in condition.triggers.keys():
			if reactions.has(trigger):
				reactions[trigger].append("guardian")
			else:
				reactions[trigger] = ["guardian"]
				
	for unit in reactions.keys():
		if !Globals.is_valid(unit):
			continue
		if not unit.trigger_reaction(reactions[unit]):
			continue
			
		triggered += 1
		grid.current_unit = unit
		grid.go_to_reaction_state()
		grid.wait_for_spell = true
		if is_in_group("EnemyUnits"):
			grid.get_parent().current_enemy = self
		var reaction = yield(unit, "reaction_finished")
		unit.hide_reactions()
		triggered -= 1
		
		match reaction:
			"spectrum_shatter":
				var spectrum_shatter = grid.create_new_spell("spectrum_shatter", \
					unit, coords, false)
				spectrum_shatter.ignore_yield()
				yield(spectrum_shatter, "complete_cast")
				modified_damage *= 2
				grid.cancel_reaction_state()
			"guardian":
				triggered += 1
				var guardian = grid.create_new_spell("guardian", unit, coords, false, false)
				guardian.is_reaction = true
				grid.go_to_reaction_target_state(guardian)
				var target : Vector3 = yield(grid, "reaction_target_selected")
				guardian.ignore_yield()
				guardian.cast(unit, target)
				yield(guardian, "complete_cast")
				grid.cancel_reaction_state()
				triggered -= 1
				return
			"cancel":
				grid.cancel_reaction_state()
				
	if pierce or guard_left == 0:
		damage_dealt = modified_damage
	elif modified_damage > 0:
		guard_left -= 1
		guard_broken(modified_damage)
		
	if damage_dealt < 0:
		damage_dealt = 0
	elif damage_dealt > 0:
		shake(float(damage_dealt) / 4)
		
	return lose_life(damage_dealt, origin)
	
func lose_life(loss : int, origin = null) -> int:
	health_left -= loss
	
	if health_left <= 0:
		if has_condition("endurance") and not has_cast_ult("endurance"):
			cast_ult("endurance")
			toss_skill("endurance")
			health_left = 1
		elif has_condition("defy_death") and not has_cast_ult("defy_death"):
			cast_ult("defy_death")
			toss_skill("defy_death")
			apply_condition("immune", 2)
			apply_condition("silence", 2)
			apply_condition("pass", 2)
			apply_condition("fading", 2)
			health_left = 0
		else:
			emit_signal("lost_life", true)
			death(origin)
			return loss
			
	emit_signal("lost_life", false)
	apply_ui()
	
	return loss
	
func heal(heal : int, origin = null) -> int:
	health_left += heal
	
	if health_left > max_health:
		var overheal : int = max_health - health_left
		health_left = max_health
		apply_ui()
		return (heal - overheal)
		
	apply_ui()
	return heal
	
func apply_guard(amount : int = 1) -> void:
	guard_left += amount
	
func guard_broken(trample : int = 0) -> void:
	var grid = Globals.get_hex_grid()
	
	if has_condition("phase_change"):
		toss_skill("phase_change")
		for dir in Globals.CUBE_VECTORS:
			var unit = grid.get_unit(coords + dir)
			if Globals.is_valid(unit):
				unit.deal_damage(1, self)
				
func break_guard() -> void:
	guard_left = 0
	guard_broken(0)
	apply_ui()
	
func shake(intensity : float = 1.0) -> void:
	var flip : float = 1.0
	for i in range(6 + round(intensity * 8.0)):
		$sprite.position.x = (2 + (randi() % 4)) * intensity * flip
		flip *= -1.0
		yield(get_tree().create_timer(0.02), "timeout")
		
	$sprite.position.x = 0
	
func apply_condition(condition : String, intensity : int = 1, dynamic : bool = false):
	if dynamic:
		var new_condition = load("res://Conditions/condition.tscn").instance()
		new_condition.condition_type = "private_condition"
		new_condition.name = condition
		new_condition.material = UNSHADED
		$conditions.add_child(new_condition)
		return new_condition
	elif has_condition(condition):
		get_condition(condition).reapply(intensity)
		return get_condition(condition)
	else:
		var new_condition
		
		if not condition_resources.has(condition):
			condition_resources[condition] = load( \
				"res://Conditions/{0}_condition.tscn".format([condition]))
				
		new_condition = condition_resources[condition].instance()
		new_condition.name = condition
		new_condition.material = UNSHADED
		$conditions.add_child(new_condition)
		new_condition.initial_apply(intensity)
		return new_condition
		
func show_talk_icon() -> void:
	$tween.interpolate_property($talk_icon, "position", Vector2(11, -22), Vector2(11, -29), \
		0.6, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	$tween.interpolate_property($talk_icon, "rotation_degrees", -45.0, 0.0, 0.6, \
		Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	$tween.interpolate_property($talk_icon, "modulate:a", 0.0, 1.0, 0.4, Tween.TRANS_LINEAR)
	$tween.start()
	$talk_icon.show()
	
func hide_talk_icon() -> void:
	$talk_icon.hide()
	
func toss_skill(skill : String):
	var grid = Globals.get_hex_grid()
	var new_button = load("res://Global/Menus/Buttons/action_button.tscn").instance().duplicate()
	
	new_button.position = TOSS_START
	new_button.modulate.a = 0.0
	new_button.get_node("button").texture_normal = load( \
		"res://Assets/GUI/Skills/skill_{0}.png".format([skill]))
	if is_in_group("EnemyUnits"):
		new_button.connect("button_entered", self, "toss_enter")
		new_button.connect("button_left", self, "toss_left")
	
	grid.current_toss = new_button
	$toss.add_child(new_button)
	$toss_tween.interpolate_property(new_button, "modulate:a", 0.6, 1.0, 0.4, \
		Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$toss_tween.start()
	
	return new_button
	
func hide_toss() -> void:
	$toss.hide()
	
func hold_toss() -> void:
	$toss.hold = true
	
func resume_toss() -> void:
	$toss.hold = false
	$toss.show()
	
	for child in $toss.get_children():
		$toss_tween.remove(child)
		$toss_tween.interpolate_property(child, "modulate:a", 1.0, 0.6, 0.4, \
			Tween.TRANS_LINEAR, Tween.EASE_IN)
		
	$toss_tween.start()
	
func fizzle_toss() -> void:
	$toss.hold = false
	
	for child in $toss.get_children():
		child.queue_free()
		
	$toss_tween.remove_all()
	Globals.get_hex_grid().current_toss = null
	
func disable() -> void:
	if not permanent:
		disabled = true
		$sprite.modulate = disabled_modulate
	
func enable() -> void:
	disabled = false
	$sprite.modulate = starting_modulate
	
func apply_ui() -> void:
	if guard_left == 0:
		for guard in $ui_canvas/top_ui/guard.get_children():
			guard.queue_free()
	for i in guard_left:
		if i >= $ui_canvas/top_ui/guard.get_child_count():
			var new_canvas_guard : TextureRect = TextureRect.new()
			
			new_canvas_guard.texture = GUARD_ICON
			new_canvas_guard.mouse_filter = Control.MOUSE_FILTER_IGNORE
			
			$ui_canvas/top_ui/guard.add_child(new_canvas_guard)
			
	for i in max_health:
		var top_child = $ui_canvas/top_ui/health.get_child(i)
		
		if Globals.is_valid(top_child):
			if i < health_left:
				top_child.texture = health_icon
				if i < corrupted_health:
					top_child.texture = health_icon_corrupted
			else:
				top_child.texture = health_icon_empty
			
	for condition in $conditions.get_children():
		match condition.name:
			"blind":
				swift_actions = 0
				reactions = 0
				
	if Globals.is_valid(popup):
		popup.apply_ui(self)
		
	enable()
	
func attack(target : Vector3, decor : bool = false) -> void:
	attacking = true
	$sprite.play("attack")
	
	if decor:
		return
		
	apply_ui()
	
func cast(target : Vector3, decor : bool = false) -> void:
	attacking = true
	$sprite.play("cast")
	
	if decor:
		return
		
	apply_ui()
	
func move(new_path : PoolVector2Array, cost : int) -> void:
	if new_path.size() == 0:
		return
	path = new_path
	movement_left -= cost
	moving = true
	
	var grid = Globals.get_hex_grid()
	grid.move_unit_to(self, Globals.pixel_to_grid(new_path[-1]+Vector2(10, 10)))
	
	if has_condition("momentum_static") and Globals.hex_distance(coords, \
	starting_coords) >= 3:
		apply_condition("momentum")
		
func move_along_path(distance : float) -> void:
	var grid = Globals.get_hex_grid()
	var vectors : PoolVector3Array = Globals.CUBE_VECTORS
	var start_point : Vector2 = position
	
	for i in range(path.size()):
		var distance_to_next := start_point.distance_to(path[0])
		var offset : Vector2 = Vector2(10, 10)
		var next_coord : Vector3 = Globals.pixel_to_grid(path[0] + offset)
		var next_dir : Vector3 = Globals.pixel_to_grid(path[0]) - \
			Globals.pixel_to_grid(position)
		var next_cell = grid.get_cell(next_coord)
		
		if Globals.is_valid(next_cell) and distance_to_next <= 2 \
		and is_in_group("EnemyUnits") and !has_condition("pass") and \
		(next_cell.current_terrain == "stars" or next_cell.current_terrain == "stars_origin"):
			var ana = null
			var origin : Vector3 = Vector3.ZERO
			
			if next_cell.current_terrain == "stars_origin":
				origin = next_coord
			else:
				for dir in vectors:
					var adj_cell = grid.get_cell(next_coord + dir)
					if Globals.is_valid(adj_cell):
						if adj_cell.current_terrain == "stars_origin":
							origin = next_coord + dir
							
			for player in get_tree().get_nodes_in_group("PlayerUnits"):
				if player.has_condition("field_of_stars"):
					var condition = player.get_condition("field_of_stars")
					if condition.origins.has(origin):
						ana = player
						break
						
			if Globals.is_valid(ana) and ana.trigger_reaction(["stab"]):
				grid.move_unit_to(self, next_coord)
				position = Globals.grid_to_pixel(next_coord)
				$sprite.play("idle")
			
				ana.triggered += 1
				moving = false
				grid.current_unit = ana
				grid.go_to_reaction_state()
				var reaction = yield(ana, "reaction_finished")
				ana.hide_reactions()
				ana.triggered -= 1
				
				match reaction:
					"stab":
						ana.reactions -= 1
						grid.apply_terrain(origin, "null")
						for dir in vectors:
							grid.apply_terrain(origin + dir, "null")
						var stab = grid.create_new_spell( \
							"stab", ana, coords, false, true, true)
						stab.ignore_yield()
						yield(stab, "complete_cast")
						grid.cancel_reaction_state()
					"cancel":
						apply_condition("pass")
						grid.cancel_reaction_state()
						
				moving = true
		if Globals.is_valid(next_cell) and distance_to_next <= 2 \
		and is_in_group("EnemyUnits") and !has_condition("pass") and \
		next_cell.coords in grid.get_seth_trip():
			var seth = null
			
			for player in get_tree().get_nodes_in_group("PlayerUnits"):
				if player.has_condition("wave_function_collapse"):
					seth = player
					
			if Globals.is_valid(seth) and \
			seth.trigger_reaction(["wave_function_collapse"]):
				grid.move_unit_to(self, next_coord)
				position = Globals.grid_to_pixel(next_coord)
				$sprite.play("idle")
				
				seth.triggered += 1
				moving = false
				grid.current_unit = seth
				grid.go_to_reaction_state()
				var reaction = yield(seth, "reaction_finished")
				seth.hide_reactions()
				seth.triggered -= 1
				
				match reaction:
					"wave_function_collapse":
						var collapse = grid.create_new_spell("wave_function_collapse", \
						seth, seth.coords, false, false)
						grid.wait_for_spell = true
						collapse.is_reaction = true
						collapse.ignore_yield()
						collapse.cast(seth, seth.coords)
						yield(collapse, "complete_cast")
						grid.cancel_reaction_state()
					"cancel":
						apply_condition("pass")
						grid.cancel_reaction_state()
						
				moving = true
			
		if next_dir == vectors[0]:
			$sprite.play("run_n")
		elif next_dir == vectors[1]:
			$sprite.play("run_ne")
		elif next_dir == vectors[2]:
			$sprite.play("run_se")
		elif next_dir == vectors[3]:
			$sprite.play("run_s")
		elif next_dir == vectors[4]:
			$sprite.play("run_sw")
		elif next_dir == vectors[5]:
			$sprite.play("run_nw")
			
		if has_condition("stun"):
			$sprite.play("idle")
			position = path[0]
			moving = false
			emit_signal("complete_movement")
			break
		elif distance <= distance_to_next and distance >= 0.0:
			position = start_point.linear_interpolate(path[0], distance / distance_to_next)
			break
		elif distance <= 0.0:
			position = path[0]
			moving = false
			if is_in_group("PlayerUnits"):
				selectable = true
			emit_signal("complete_movement", self)
			apply_ui()
			break
		distance -= distance_to_next
		start_point = path[0]
		path.remove(0)
		if path.size() == 0:
			moving = false
			if is_in_group("PlayerUnits"):
				selectable = true
			emit_signal("complete_movement", self)
			apply_ui()
			break
			
func _on_sprite_animation_finished():
	if $sprite.animation == "attack" or $sprite.animation == "cast":
		$sprite.stop()
		attacking = false
		apply_ui()
	elif $sprite.animation == "bind":
		$sprite.stop()
		$sprite.frame = 9
		bound = true
		
func _on_toss_tween_tween_completed(object, key):
	if $toss.hold:
		object.modulate.a = 1.0
		return
	if object.modulate.a == 1.0:
		$toss_tween.remove(object)
		$toss_tween.interpolate_property(object, "modulate:a", 1.0, 0.6, 0.4, \
			Tween.TRANS_LINEAR, Tween.EASE_IN)
		$toss_tween.start()
	else:
		$toss_tween.remove(object)
		$toss.remove_child(object)
		object.queue_free()
		Globals.get_hex_grid().current_toss = null
	
func _on_ui_canvas_child_entered_tree(node):
	if node is CanvasItem:
		node.material = UNSHADED
	
func _on_popup_child_entered_tree(node):
	if node is CanvasItem:
		node.material = UNSHADED
	
func _on_toss_child_entered_tree(node):
	if node is CanvasItem:
		node.material = UNSHADED
