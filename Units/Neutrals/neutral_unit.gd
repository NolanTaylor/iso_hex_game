extends "res://Units/unit.gd"

export var captured : bool = false
export var skill_list : PoolStringArray

func _ready():
	pass
	
func unit_init() -> void:
	if captured:
		$sprite.animation = "bind"
		$sprite.frame = 9
		bound = true
	
	active = false
	selectable = false
	obstacle_mode = "enemy_units"
	
	for skill in skill_list:
		match skill:
			"health":
				max_health += 1
			"guard":
				guard_left += 1
			"movement":
				movement += 1
				
	.unit_init()
	
func get_health_color() -> Color:
	return NEUTRAL_HEALTH_COLOR
