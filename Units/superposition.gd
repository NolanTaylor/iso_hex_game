extends AnimatedSprite

var real_unit
var coords : Vector3 = Vector3.ZERO

func _ready():
	pass
	
func teleport(new_pos : Vector3) -> void:
	coords = new_pos
	position = Globals.grid_to_pixel(new_pos)
	
func get_modification(arg1, arg2):
	return real_unit.get_modification(arg1, arg2)
	
func has_special_modification(arg1):
	return real_unit.has_special_modification(arg1)
