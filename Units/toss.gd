extends Node2D

const INITIAL_VELOCITY : Vector2 = Vector2(-24, -12)
const ACCELERATION : Vector2 = Vector2(16, 48)

var velocities : Dictionary = {} # <int, Vector2>
var hold : bool = false

func _ready():
	pass
	
func _process(delta):
	for i in get_child_count():
		var child = get_child(i)
		if hold:
			continue
		child.position += velocities[i] * delta
		velocities[i] += ACCELERATION * delta
	
func _on_toss_child_entered_tree(node):
	if node.has_node("button"):
		node.toss_mode = true
		velocities[get_child_count() - 1] = INITIAL_VELOCITY
