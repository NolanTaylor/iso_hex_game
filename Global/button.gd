extends Node

func buttoned():
	match self.name:
		"sword":
			set_attack_action()
		"spear":
			set_attack_action()
		"step":
			set_attack_action()
		"recover":
			pass
		"disengage":
			pass
			
	get_parent().get_parent().kill_all_children(get_parent())
	
func set_attack_action() -> void:
	get_parent().get_parent().mouse = "attack"
	get_parent().get_parent().weapon = self.name
	get_parent().get_parent().set_range = true
	
func _ready():
	pass
