extends "res://Global/Tiles/Obstacles/obstacle.gd"

const unlocked : Texture = preload("res://Assets/Tilesets/Obstacles/chest_open.png")

var opened : bool = false

func _ready():
	pass
	
func unlock():
	opened = true
	$sprite.texture = unlocked
	
	return $item_drop.get_child(0)
