extends Node2D

export var traversal_cost : int = 0
export var extra_tiles : PoolVector3Array = []
export var targetable : bool = false
export var unlockable : bool = false

var coords : Vector3 = Vector3(0, 0, 0)
var properties : PoolStringArray = PoolStringArray([])

func _ready():
	pass
	
func find_grid() -> Vector3:
	var grid : Vector3 = Vector3(0, 0, 0)
	
	grid.x = global_position.x / 23
	grid.y = ((global_position.y - 16) / 16) - (grid.x / 2)
	grid.z = -(grid.x + grid.y)
	
	coords = grid
	
	return grid
