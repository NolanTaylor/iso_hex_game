extends YSort

enum State {
	STATE_FREE,
	STATE_SELECT_ACTION,
	STATE_SELECT_REACTION,
	STATE_REACTION_TARGET,
	STATE_CURSOR_SELECT_ACTION,
	STATE_MOVE,
	STATE_TARGET,
	STATE_SPELL,
	STATE_TALK,
	STATE_CHOOSE,
}

onready var camera = get_parent().get_node("camera")

var mouse : Vector2 = Vector2(0, 0)
var mouse_state : int = State.STATE_FREE
var current_coords : Vector3 = Vector3(0, 0, 0)
var current_unit = null # unit
var casting_unit = null # unit
var current_spell = null # spell
var current_toss = null # toss
var current_path : PoolVector2Array = PoolVector2Array([])
var current_path_cost : int = 0
var highlight_coords : PoolVector3Array = PoolVector3Array([])
var highlight_temp : PoolVector3Array = PoolVector3Array([])
var highlight_aux : PoolVector3Array = PoolVector3Array([])
var just_chosen : Vector3 = Vector3(0, 0, 0)
var max_targets : int = 1
var target_type_buffer : String = ""
var act : int = 0
var tutorial_act : int = -1

var control : bool = true
var wait_for_spell : bool = false
var astar : AStar2D = AStar2D.new()

var grid : Dictionary = { # <Vector3, Cell>
	
}

signal unit_moved(unit, location)
signal reaction_target_selected(target)
signal resolve_current_spell

func _ready():
	for i in get_child_count():
		var child = get_child(i)
		child.id = i
		if !child.decor:
			grid[child.find_grid()] = child
		
	for key in grid.keys():
		for i in range(6):
			if grid.has(key + Globals.CUBE_VECTORS[i]):
				grid[key].set_adjacent(i, grid[key + Globals.CUBE_VECTORS[i]])
		
	init_astar()
	
	Globals.set_hex_grid(self)
	get_parent().connect("confirm_spell", self, "multi_target_spell_cast")
	
func _process(delta):
	mouse = get_global_mouse_position()
	current_coords = Globals.pixel_to_grid(mouse)
	
	if !control:
		return
		
	for coord in highlight_coords:
		if grid.has(coord):
			grid[coord].set_highlight("blue")
			
	match mouse_state:
		State.STATE_FREE:
			if has_unit(current_coords):
				var unit = get_unit(current_coords)
				if Globals.is_valid(unit) and unit.has_method("hover"):
					unit.hover()
		State.STATE_SELECT_ACTION:
			continue
		State.STATE_SELECT_REACTION:
			if Globals.is_valid(current_unit):
				current_unit.hover()
		State.STATE_REACTION_TARGET:
			current_unit.hover()
				
			for coord in highlight_temp:
				if has_cell(coord):
					grid[coord].set_highlight("pink")
					
			if current_coords in highlight_coords and has_cell(current_coords):
				grid[current_coords].set_highlight("red")
				
			match current_spell.target_behavior:
				"single_target":
					if current_coords in highlight_coords and has_cell(current_coords):
						grid[current_coords].set_highlight("red")
						
						for coord in current_spell.get_flex_cells(current_unit, current_coords):
							if has_cell(coord):
								highlight_temp.append(coord)
								grid[coord].set_highlight("red")
								
						var forecast = current_spell.get_forecast(current_unit, current_coords)
						for key in forecast.keys():
							if grid.has(key):
								grid[key].set_display_text(forecast[key])
				"dual_target", "double_target", "triple_target", "multi_target":
					for coord in highlight_aux:
						if has_cell(coord):
							grid[coord].set_highlight("pink")
						
					if current_coords == just_chosen:
						return
					else:
						just_chosen = Vector3.ZERO
					
					if current_coords in highlight_coords and has_cell(current_coords):
						grid[current_coords].set_highlight("red")
		State.STATE_CURSOR_SELECT_ACTION:
			pass
		State.STATE_MOVE:
			var new_path = find_path(current_unit.coords, current_coords, \
				current_unit.obstacle_mode)
			get_parent().set_cursor_tooltip(-1)
			if current_coords in highlight_coords and has_cell(current_coords):
				current_path = new_path
				current_path_cost = 0
				for point in current_path:
					current_path_cost += grid[Globals.pixel_to_grid(point + \
						Vector2(10, 10))].traversal_cost
				current_path_cost -= grid[current_unit.coords].traversal_cost
				get_parent().set_cursor_tooltip(current_path_cost, current_unit.movement_left)
			get_parent().draw_path(current_path)
			current_unit.hover()
		State.STATE_TARGET:
			var color : String = "red"
			
			current_unit.hover()
			
			var use_unit = current_unit
			if Input.is_action_just_pressed("toggle_superposition") and \
			Globals.is_valid(current_unit.superposition):
				use_unit = current_unit.superposition
				go_to_target_state(current_spell, use_unit)
			elif Input.is_action_pressed("toggle_superposition") and \
			Globals.is_valid(current_unit.superposition):
				use_unit = current_unit.superposition
			elif Input.is_action_just_released("toggle_superposition") and \
			Globals.is_valid(current_unit.superposition):
				use_unit = current_unit
				go_to_target_state(current_spell, use_unit)
				
			for coord in current_spell.get_highlight_cells(use_unit):
				if has_cell(coord):
					grid[coord].hide_display_text()
					
			for coord in highlight_temp:
				if has_cell(coord) and not coord in highlight_coords:
					grid[coord].set_highlight("null")
					
			highlight_temp.resize(0)
			
			match current_spell.target_behavior:
				"single_target", "buffer_target":
					if current_coords in highlight_coords and has_cell(current_coords):
						if current_spell.PROPERTIES.has("observation_line"):
							if not current_spell.valid_placement:
								color = "pink"
						grid[current_coords].set_highlight(color)
						
						for coord in current_spell.get_flex_cells(use_unit, current_coords):
							if has_cell(coord):
								highlight_temp.append(coord)
								grid[coord].set_highlight(color)
								
						var forecast = current_spell.get_forecast(use_unit, current_coords)
						for key in forecast.keys():
							if grid.has(key):
								grid[key].set_display_text(forecast[key])
				"dual_target", "double_target", "triple_target", "multi_target":
					for coord in highlight_aux:
						if has_cell(coord):
							grid[coord].set_highlight("pink")
							
					for coord in current_spell.get_flex_cells(use_unit, current_coords):
						if has_cell(coord) and current_coords in highlight_coords:
							highlight_temp.append(coord)
							grid[coord].set_highlight(color)
						elif has_cell(coord) and \
						current_spell.PROPERTIES.has("fractal_dimension") and \
						current_spell.act == 3:
							highlight_temp.append(coord)
							grid[coord].set_highlight(color)
							
					if current_coords == just_chosen:
						return
					else:
						just_chosen = Vector3.ZERO
					
					if current_coords in highlight_coords and has_cell(current_coords):
						grid[current_coords].set_highlight("red")
		State.STATE_TALK:
			current_unit.hover()
			
			for coord in highlight_coords:
				grid[coord].set_highlight("blue")
				
			for coord in highlight_temp:
				grid[coord].set_highlight("null")
				
			highlight_temp.resize(0)
			
			if current_coords in highlight_coords and has_cell(current_coords):
				grid[current_coords].set_highlight("red")
		State.STATE_CHOOSE:
			for coord in highlight_coords:
				grid[coord].set_highlight("blue")
				
			for coord in highlight_temp:
				grid[coord].set_highlight("null")
				
			highlight_temp.resize(0)
			
			for coord in highlight_aux:
				grid[coord].set_highlight("pink")
				
			if current_coords == just_chosen:
				return
			else:
				just_chosen = Vector3.ZERO
			
			if current_coords in highlight_coords and has_cell(current_coords):
				grid[current_coords].set_highlight("red")
				
			if grid.has(current_coords):
				var pos : Vector2 = grid[current_coords].position + Vector2(0, 16)
				if pos.x < 40 or pos.x > 260 or pos.y < 50 or pos.y > 180:
					get_parent().hide_cursor()
				else:
					get_parent().get_node("cursor").position = pos
					get_parent().show_cursor()
					
func mouse_input(event):
	if !control or get_parent().cutscene_mode:
		return
	if event is InputEventMouseButton and event.pressed:
		if event.button_index == BUTTON_LEFT:
			match mouse_state:
				State.STATE_FREE:
					print(current_coords)
					
					if not current_coords in grid.keys():
						return
						
					var unit_under_cursor = get_unit(current_coords)
					
					if !Globals.is_valid(unit_under_cursor):
						if tutorial_act >= 0 and tutorial_act <= 3:
							return
						if tutorial_act == 4:
							get_parent().tween_new_paper("select_end")
						get_parent().get_node("cursor").select()
						go_to_cursor_select_state()
					elif unit_under_cursor.is_in_group("PlayerUnits") \
					and unit_under_cursor.selectable and not unit_under_cursor.disabled:
						unit_under_cursor.select()
						current_unit = unit_under_cursor
						mouse_state = State.STATE_SELECT_ACTION
						get_parent().hide_cursor()
						if tutorial_act == 0:
							tutorial_act = 1
							get_parent().tween_new_paper("select_move")
					elif unit_under_cursor.is_in_group("EnemyUnits"):
						pass
				State.STATE_SELECT_ACTION:
					current_unit.deselect()
					current_unit.emit_signal("unfocused")
					current_unit = null
					mouse_state = State.STATE_FREE
					get_parent().show_cursor()
				State.STATE_SELECT_REACTION:
					pass
				State.STATE_REACTION_TARGET:
					if current_coords in highlight_coords and has_cell(current_coords):
						match current_spell.target_behavior:
							"single_target":
								emit_signal("reaction_target_selected", current_coords)
							"dual_target", "double_target", "triple_target", "multi_target":
								if current_coords in highlight_aux:
									highlight_aux.remove(highlight_aux.find(current_coords))
								else:
									highlight_aux.append(current_coords)
									
								if highlight_aux.size() <= max_targets:
									get_parent().enable_target_confirm()
								else:
									get_parent().disable_target_confirm()
									
								just_chosen = current_coords
					else:
						cancel_reaction_target_state()
				State.STATE_CURSOR_SELECT_ACTION:
					cancel_cursor_select_state()
				State.STATE_MOVE:
					if tutorial_act == 2:
						if current_coords == Vector3(9, -1, -8):
							get_parent().tween_new_paper("select_attack")
							tutorial_act = 3
						else:
							return
					if current_coords in highlight_coords and has_cell(current_coords):
						control = false
						current_unit.move(current_path, current_path_cost)
						current_unit.connect("complete_movement", self, \
							"resume_control_after_move", [ ], CONNECT_ONESHOT)
						get_parent().hide_path()
						get_parent().set_cursor_tooltip(-1)
						de_gayify_the_map()
					else:
						cancel_move_state()
				State.STATE_TARGET:
					if current_coords in highlight_coords and has_cell(current_coords):
						if current_spell.PROPERTIES.has("observation_line"):
							if not current_spell.valid_placement:
								return
						match current_spell.target_behavior:
							"single_target":
								go_to_spell_state()
								if current_spell.PROPERTIES.has("warp"):
									highlight_aux.append(current_coords)
									multi_target_spell_cast()
									return
								create_new_spell(current_spell, current_unit, current_coords)
								de_gayify_the_map()
								if tutorial_act == 3:
									get_parent().tween_new_paper("select_empty")
									tutorial_act = 4
							"dual_target", "double_target", "triple_target", "multi_target":
								if current_coords in highlight_aux:
									highlight_aux.remove(highlight_aux.find(current_coords))
								else:
									highlight_aux.append(current_coords)
									
								if current_spell.PROPERTIES.has("fractal_dimension"):
									current_spell.advance(current_coords)
									de_gayify_the_map()
									highlight_aux = PoolVector3Array([ \
										current_spell.first_pos, current_spell.second_pos])
									for coord in current_spell.get_highlight_cells(current_unit):
										if not has_unit(coord) and not has_obstacle(coord):
											highlight_coords.append(coord)
											
								if highlight_aux.size() <= max_targets:
									get_parent().enable_target_confirm()
								else:
									get_parent().disable_target_confirm()
									
								just_chosen = current_coords
							"buffer_target":
								current_spell.target_type = target_type_buffer
								current_spell.target_behavior = "single_target"
								go_to_target_state(current_spell)
								highlight_aux = PoolVector3Array([current_coords])
					else:
						cancel_target_state()
				State.STATE_TALK:
					if current_coords in highlight_coords and has_cell(current_coords):
						get_parent().run_dialog([current_unit.name, \
							grid[current_coords].occupying_unit.name])
						current_unit.emit_signal("unfocused")
					else:
						cancel_talk_state()
				State.STATE_CHOOSE:
					if current_coords in highlight_coords and has_cell(current_coords):
						if current_coords in highlight_aux:
							highlight_aux.remove(highlight_aux.find(current_coords))
						else:
							highlight_aux.append(current_coords)
							
						just_chosen = current_coords
		elif event.button_index == BUTTON_RIGHT:
			match mouse_state:
				State.STATE_FREE:
					if current_coords in grid.keys():
						var unit_under_cursor = get_unit(current_coords)
						if Globals.is_valid(unit_under_cursor):
							if unit_under_cursor.is_in_group("PlayerUnits"):
								control = false
								camera.lock = true
								unit_under_cursor.get_node("character_menu").global_position = \
									Vector2(0, 0) + camera.position
								unit_under_cursor.get_node("character_menu").show()
								get_parent().hide_cursor()
							elif unit_under_cursor.is_in_group("EnemyUnits"):
								print("right clicked on a enemy unit")
						else:
							print("right clicked on nothing")
				State.STATE_SELECT_ACTION:
					pass
				State.STATE_SELECT_REACTION:
					pass
				State.STATE_REACTION_TARGET:
					pass
				State.STATE_CURSOR_SELECT_ACTION:
					pass
				State.STATE_MOVE:
					cancel_move_state()
				State.STATE_TARGET:
					cancel_target_state()
				State.STATE_TALK:
					cancel_talk_state()
				State.STATE_CHOOSE:
					if current_coords in grid.keys():
						var unit_under_cursor = get_unit(current_coords)
						if !Globals.is_valid(unit_under_cursor):
							return
						if not unit_under_cursor.has_node("character_menu"):
							return
						control = false
						unit_under_cursor.get_node("character_menu").global_position = \
							Vector2(0, 0)
						unit_under_cursor.get_node("character_menu").show()
						unit_under_cursor.get_node("character_menu").connect("exit_menu", self, \
							"exit_char_menu", [ ], CONNECT_ONESHOT)
						get_parent().hide_cursor()
						
func _input(event):
	if event.is_action_pressed("rotate_spell"):
		if Globals.is_valid(current_spell) and \
		current_spell.PROPERTIES.has("observation_line"):
			current_spell.rotate_orientation()
			
func exit_char_menu(menu) -> void:
	control = true
	camera.lock = false
	menu.hide()
	get_parent().show_cursor()
	get_parent().reset_grid_and_cam()
	
func highlight_toss_layer(cells : PoolVector3Array, dict : Dictionary) -> void:
	if mouse_state == State.STATE_REACTION_TARGET:
		highlight_temp = cells
	else:
		for cell in cells:
			if has_cell(cell):
				grid[cell].set_highlight("pink")
				
		for key in dict:
			if has_cell(key):
				grid[key].set_display_text(dict[key])
				
func action_button_pressed(button) -> void:
	if Globals.is_valid(current_unit) and \
	current_unit.is_in_group("PlayerUnits"):
		camera.set_popup_text("")
		
	match button.name:
		"move_button":
			if tutorial_act == 1:
				tutorial_act = 2
				get_parent().tween_new_paper("select_adjacent")
			go_to_move_state()
		"talk_button":
			go_to_talk_state(current_unit.convo_partner_coords)
		
	if button.has_node("spell"):
		go_to_target_state(button.get_node("spell"))
	elif button.has_node("item"):
		go_to_target_state(button.get_node("item"))
		
func action_mouse_enter(button) -> void:
	match button.name:
		"move_button":
			highlight_coords = flood_fill(current_unit.coords, current_unit.movement_left, \
				current_unit.obstacle_mode)
			camera.set_popup_text("Move")
			return
		"attack_button":
			camera.set_popup_text("Attack")
		"spell_button":
			camera.set_popup_text("Spell")
		"item_button":
			camera.set_popup_text("Item")
		"cancel_button":
			camera.set_popup_text("Cancel")
		"talk_button":
			highlight_coords = current_unit.convo_partner_coords
			camera.set_popup_text("Talk")
			return
			
	if button.has_node("spell"):
		button.get_node("spell").get_valid_cursor_cells(current_unit)
		highlight_coords = button.get_node("spell").get_highlight_cells(current_unit)
		camera.set_popup_text(button.get_node("spell").proper_name)
		camera.preview_cost(button.get_node("spell").get_cost(current_unit))
		if button.get_node("spell").PROPERTIES.has("reflect") or \
		button.get_node("spell").PROPERTIES.has("spectrum_shatter"):
			var enemy = get_parent().current_enemy
			if Globals.is_valid(enemy):
				highlight_coords.append(enemy.coords)
			elif Globals.is_valid(casting_unit):
				highlight_coords.append(casting_unit.coords)
		if button.get_node("spell").PROPERTIES.has("stab") and \
		button.get_node("spell").is_reaction:
			var enemy = get_parent().current_enemy
			if Globals.is_valid(enemy):
				highlight_coords = PoolVector3Array([enemy.coords])
	elif button.has_node("item"):
		highlight_coords = button.get_node("item").get_highlight_cells(current_unit)
		camera.set_popup_text(button.get_node("item").item_name)
		var new_cost = load("res://Global/DataStructures/casting_cost.tscn").instance()
		# add conditionals for items/skills and shit
		new_cost.timing = 3
		camera.preview_cost(new_cost)
	
func action_mouse_left(button) -> void:
	de_gayify_the_map()
	if Globals.is_valid(current_unit) and \
	current_unit.is_in_group("PlayerUnits"):
		camera.set_popup_text("")
		if button.has_node("spell"):
			camera.preview_cost(null)
			
func go_to_move_state() -> void:
	current_unit.deselect()
	current_path = PoolVector2Array([])
	current_path_cost = 0
	mouse_state = State.STATE_MOVE
	get_parent().show_cursor()
	get_parent().show_path()
	
func cancel_move_state() -> void:
	mouse_state = State.STATE_FREE
	current_unit.emit_signal("unfocused")
	current_unit = null
	get_parent().hide_path()
	get_parent().set_cursor_tooltip(-1)
	de_gayify_the_map()
	
func move_unit_to(unit, destination : Vector3):
	if grid.has(unit.coords) and grid.has(destination):
		get_cell(unit.coords).set_occupying_unit(null)
		get_cell(destination).set_occupying_unit(unit)
		unit.coords = destination
		get_parent().runSBAs()
		emit_signal("unit_moved", unit, destination)
	else:
		push_error("unit coords or destination doesn't exist")
	
func resume_control_after_move(unit) -> void:
	control = true
	cancel_move_state()
	
func resume_control_after_cast(spell) -> void:
	control = true
	get_parent().show_cursor()
	cancel_target_state()
	
func go_to_enemy_turn() -> void:
	control = false
	get_parent().hide_cursor()
	
func go_to_player_turn() -> void:
	control = true
	mouse_state = State.STATE_FREE
	get_parent().show_cursor()
	
func go_to_current_turn() -> void:
	if get_parent().player_turn:
		go_to_player_turn()
	else:
		go_to_enemy_turn()
	
func go_to_target_state(spell, unit = current_unit) -> void:
	current_unit.deselect()
	current_spell = spell
	mouse_state = State.STATE_TARGET
	de_gayify_the_map()
	get_parent().show_cursor()
	var highlight_cells : PoolVector3Array = spell.get_valid_cursor_cells(unit)
	match spell.get_target_type():
		"player_unit":
			for coord in highlight_cells:
				if has_unit(coord):
					if get_unit(coord).is_in_group("PlayerUnits"):
						highlight_coords.append(coord)
				if has_obstacle(coord):
					if get_obstacle(coord).targetable:
						highlight_coords.append(coord)
		"enemy_unit":
			for coord in highlight_cells:
				if has_unit(coord):
					if get_unit(coord).is_in_group("EnemyUnits"):
						highlight_coords.append(coord)
				if has_obstacle(coord):
					if get_obstacle(coord).targetable:
						highlight_coords.append(coord)
		"unit", "any_unit":
			for coord in highlight_cells:
				if has_unit(coord):
					highlight_coords.append(coord)
				if has_obstacle(coord):
					if get_obstacle(coord).targetable:
						highlight_coords.append(coord)
		"unit_to_unoccupied":
			for coord in highlight_cells:
				if has_unit(coord):
					highlight_coords.append(coord)
			target_type_buffer = "unoccupied_space"
		"non_unit", "unoccupied_space", "unoccupied":
			for coord in highlight_cells:
				if not has_unit(coord) and not has_obstacle(coord):
					highlight_coords.append(coord)
		"unlockable":
			for coord in highlight_cells:
				var occ = get_obstacle(coord)
				if Globals.is_valid(occ) and occ.unlockable:
					highlight_coords.append(coord)
		"any":
			for coord in highlight_cells:
				if has_cell(coord):
					highlight_coords.append(coord)
	match spell.get_target_behavior():
		"single_target":
			max_targets = 1
		"dual_target", "double_target":
			max_targets = 2
			get_parent().show_target_confirm()
			get_parent().enable_target_confirm()
		"triple_target":
			max_targets = 3
			get_parent().show_target_confirm()
			get_parent().enable_target_confirm()
			
func cancel_target_state() -> void:
	mouse_state = State.STATE_FREE
	current_unit = null
	current_spell = null
	get_parent().hide_target_confirm()
	de_gayify_the_map()
	
func go_to_reaction_state() -> void:
	control = true
	mouse_state = State.STATE_SELECT_REACTION
	get_parent().hide_cursor()
	
func cancel_reaction_state() -> void:
	control = false
	wait_for_spell = false
	Globals.get_camera().return_popup()
	get_parent().hide_cursor()
	de_gayify_the_map()
	emit_signal("resolve_current_spell")
	
func go_to_reaction_target_state(spell) -> void:
	control = true
	current_spell = spell
	mouse_state = State.STATE_REACTION_TARGET
	de_gayify_the_map()
	get_parent().show_cursor()
	get_parent().unpause_cursor()
	
	var highlight_cells : PoolVector3Array = spell.get_valid_cursor_cells(current_unit)
	match spell.get_target_type():
		"player_unit":
			for coord in highlight_cells:
				if has_unit(coord):
					if get_unit(coord).is_in_group("PlayerUnits"):
						highlight_coords.append(coord)
				if has_obstacle(coord):
					if get_obstacle(coord).targetable:
						highlight_coords.append(coord)
		"enemy_unit":
			for coord in highlight_cells:
				if has_unit(coord):
					if get_unit(coord).is_in_group("EnemyUnits"):
						highlight_coords.append(coord)
				if has_obstacle(coord):
					if get_obstacle(coord).targetable:
						highlight_coords.append(coord)
		"unit", "any_unit":
			for coord in highlight_cells:
				if has_unit(coord):
					highlight_coords.append(coord)
				if has_obstacle(coord):
					if get_obstacle(coord).targetable:
						highlight_coords.append(coord)
		"non_unit", "unoccupied_space":
			for coord in highlight_cells:
				if not has_unit(coord) and not has_obstacle(coord):
					highlight_coords.append(coord)
		"any":
			for coord in highlight_cells:
				if has_cell(coord):
					highlight_coords.append(coord)
	match spell.get_target_behavior():
		"single_target":
			max_targets = 1
		"dual_target", "double_target":
			max_targets = 2
			get_parent().show_target_confirm()
			get_parent().enable_target_confirm()
		"triple_target":
			max_targets = 3
			get_parent().show_target_confirm()
			get_parent().enable_target_confirm()
			
func go_to_spell_state() -> void:
	control = false
	mouse_state = State.STATE_SPELL
	get_parent().hide_cursor()
	
func cancel_reaction_target_state() -> void:
	pass
	
func go_to_talk_state(targets : PoolVector3Array) -> void:
	current_unit.deselect()
	mouse_state = State.STATE_TALK
	de_gayify_the_map()
	get_parent().show_cursor()
	highlight_coords = targets
	
func cancel_talk_state() -> void:
	mouse_state = State.STATE_FREE
	current_unit.emit_signal("unfocused")
	current_unit = null
	de_gayify_the_map()
	
func go_to_cursor_select_state() -> void:
	mouse_state = State.STATE_CURSOR_SELECT_ACTION
	get_parent().pause_cursor()
	
func cancel_cursor_select_state() -> void:
	mouse_state = State.STATE_FREE
	get_parent().unpause_cursor()
	get_parent().get_node("cursor").deselect()
	
func de_gayify_the_map() -> void:
	for key in grid.keys():
		grid[key].set_highlight("null")
		grid[key].hide_display_text()
	highlight_coords.resize(0)
	highlight_temp.resize(0)
	highlight_aux.resize(0)
	
func de_gayify_toss_layer() -> void:
	if mouse_state == State.STATE_REACTION_TARGET:
		for key in grid.keys():
			grid[key].hide_display_text()
		for coord in highlight_temp:
			if not coord in highlight_coords and not coord in highlight_aux:
				grid[coord].set_highlight("null")
		highlight_temp.resize(0)
	else:
		de_gayify_the_map()
	
func multi_target_spell_cast():
	if mouse_state == State.STATE_REACTION_TARGET:
		get_parent().hide_target_confirm()
		emit_signal("reaction_target_selected", highlight_aux)
		return
		
	control = false
	get_parent().hide_cursor()
	get_parent().hide_target_confirm()
	
	var new_spell = current_spell.duplicate()
	
	get_node("../objects/effects").add_child(new_spell)
	new_spell.z_index = 2
	new_spell.connect("complete_cast", self, "resume_control_after_cast", [ ], CONNECT_ONESHOT)
	new_spell.connect("unit_targeted", self, "unit_targeted")
	new_spell.connect("unit_in_aoe", self, "unit_in_aoe")
	new_spell.connect("cast_spell", self, "spell_cast")
	new_spell.hide()
	new_spell.multi_cast(current_unit, highlight_aux)
	de_gayify_the_map()
	return new_spell
	
func wait_for_spell() -> bool:
	return wait_for_spell
	
func create_new_spell(spell, casting_unit, target_coord : Vector3, resume_after_cast : bool = \
true, auto_cast : bool = true, ignore_cost : bool = false):
	var new_spell
	
	var alt_cast : bool = false
	if Input.is_action_pressed("toggle_superposition") and \
	Globals.is_valid(casting_unit.superposition):
		alt_cast = true
		
	if typeof(spell) == TYPE_STRING:
		new_spell = load("res://Spells/{0}.tscn".format([spell])).instance()
		if !Globals.is_valid(new_spell):
			return null
	else:
		new_spell = spell.duplicate()
		if spell.PROPERTIES.has("observation_line"):
			new_spell.orientation = spell.orientation
			
	get_node("../objects/effects").add_child(new_spell)
	
	if casting_unit.is_in_group("PlayerUnits") and resume_after_cast:
		new_spell.connect("complete_cast", self, "resume_control_after_cast", [ ], \
			CONNECT_ONESHOT)
	if new_spell.PROPERTIES.has("spell"):
		new_spell.connect("unit_targeted", self, "unit_targeted")
		new_spell.connect("unit_in_aoe", self, "unit_in_aoe")
		new_spell.connect("cast_spell", self, "spell_cast")
		
	new_spell.hide()
	new_spell.alt_cast = alt_cast
	new_spell.init(casting_unit)
	
	if auto_cast:
		if ignore_cost:
			new_spell.animation(casting_unit, target_coord)
		else:
			new_spell.cast(casting_unit, target_coord)
			
	return new_spell
	
func create_new_unit(unit, target : Vector3, unit_type : String = "player_unit") -> void:
	unit.coords = target
	unit.position = Globals.grid_to_pixel(target)
	get_cell(target).set_occupying_unit(unit)
	
	match unit_type:
		"player", "player_unit":
			unit.add_to_group("PlayerUnits")
			get_parent().get_node("objects/player_units").add_child(unit)
			unit.upkeep()
			if unit.has_node("character_menu"):
				unit.get_node("character_menu").connect("exit_menu", self, "exit_char_menu")
			for child in unit.get_node("action_buttons").get_children():
				child.connect("button_pressed", self, "action_button_pressed")
				child.connect("button_entered", self, "action_mouse_enter")
				child.connect("button_left", self, "action_mouse_left")
				for grandchild in child.get_node("action_subfolder").get_children():
					grandchild.connect("button_pressed", self, "action_button_pressed")
					grandchild.connect("button_entered", self, "action_mouse_enter")
					grandchild.connect("button_left", self, "action_mouse_left")
		"enemy", "enemy_unit":
			unit.add_to_group("EnemyUnits")
			get_parent().get_node("objects/enemy_units").add_child(unit)
		"neutral", "neutral_unit":
			unit.add_to_group("NeutralUnits")
			get_parent().get_node("objects/neutral_units").add_child(unit)
		_:
			pass
			
func create_new_obstacle(obstacle, target : Vector3, properties : Array = []):
	var new_obstacle
	
	if typeof(obstacle) == TYPE_STRING:
		new_obstacle = load("res://Global/Tiles/Obstacles/{0}.tscn".format([obstacle])).instance()
		if !Globals.is_valid(new_obstacle):
			return
	else:
		new_obstacle = obstacle.duplicate()
		
	new_obstacle.position = Globals.grid_to_pixel(target)
	new_obstacle.find_grid()
	new_obstacle.properties = PoolStringArray(properties)
	if has_cell(new_obstacle.coords):
		grid[new_obstacle.coords].set_occupying_obstacle(new_obstacle)
	for extra_coord in new_obstacle.extra_tiles:
		if has_cell(new_obstacle.coords + extra_coord):
			grid[new_obstacle.coords + extra_coord].set_occupying_obstacle(new_obstacle)
			
	get_parent().get_node("objects/obstacles").add_child(new_obstacle)
	
	return new_obstacle
	
func teleport_unit(unit, destination : Vector3, origin = null) -> bool:
	if not has_cell(destination) or has_occupant(destination):
		return false
		
	if Globals.is_valid(origin) and origin.has_condition("field_fluctuation"):
		unit.apply_condition("charge")
	move_unit_to(unit, destination)
	unit.position = Globals.grid_to_pixel(destination)
	
	return true
	
func displace_unit(unit, direction, collision : bool = false) -> bool:
	if unit.has_condition("inertia"):
		return false
		
	var destination : Vector3 = Vector3(0, 0, 0)
	
	if typeof(direction) == TYPE_STRING:
		destination = unit.coords + Globals.direction_to_vector(direction)
	elif typeof(direction) == TYPE_VECTOR3:
		destination = unit.coords + direction
	
	if has_obstacle(destination) or has_unit(destination):
		if collision:
			unit.apply_condition("stun")
			
		return false
	else:
		teleport_unit(unit, destination)
		
		return true
		
func acquire_item(acquisitioner, item) -> void:
	if acquisitioner.is_in_group("PlayerUnits") and !acquisitioner.data.empty():
		var item_name : String = ""
		
		if typeof(item) == TYPE_STRING:
			item_name = item
		else:
			item_name = item.name
			
		if acquisitioner.acquire_item(item_name):
			get_parent().popup_item_obtained(acquisitioner.get_node( \
				"character_menu/skill_tree")._name, item.item_name)
		else:
			# change this to player selects which item gets sent to storage
			Config.storage.append(item_name)
			
func unlock_object(unlocker, coord : Vector3) -> void:
	var obj = get_obstacle(coord)
	
	if !Globals.is_valid(obj):
		return
		
	if not obj.unlockable:
		return
		
	var drop = obj.unlock()
	
	if !Globals.is_valid(drop):
		return
		
	acquire_item(unlocker, drop)
	
func spell_cast(casting_unit, target_coord, spell) -> void:
	var reactions : Dictionary = {} # <unit, Array<String>>
	
	if casting_unit.has_condition("deposition_dynamic"):
		var condition = casting_unit.get_condition("deposition_dynamic")
		for trigger in condition.triggers.keys():
			if Globals.hex_distance(casting_unit.coords, \
			trigger.coords) <= condition.triggers[trigger]:
				if reactions.has(trigger):
					reactions[trigger].append("deposition")
				else:
					reactions[trigger] = ["deposition"]
	if casting_unit.has_condition("satellite_dynamic"):
		var condition = casting_unit.get_condition("satellite_dynamic")
		for trigger in condition.triggers.keys():
			if Globals.hex_distance(casting_unit.coords, \
			trigger.coords) <= condition.triggers[trigger]:
				if reactions.has(trigger):
					reactions[trigger].append("satellite")
				else:
					reactions[trigger] = ["satellite"]
					
	for unit in reactions.keys():
		if !Globals.is_valid(unit):
			continue
		if not unit.trigger_reaction(reactions[unit]):
			continue
			
		current_unit = unit
		casting_unit.triggered += 1
		casting_unit.hold_toss()
		go_to_reaction_state()
		var reaction = yield(unit, "reaction_finished")
		unit.hide_reactions()
		casting_unit.triggered -= 1
		
		match reaction:
			"deposition":
				var deposition = create_new_spell("deposition", unit, \
					casting_unit.coords, false)
				deposition.ignore_yield()
				yield(deposition, "complete_cast")
				spell.counter()
				cancel_reaction_state()
				return
			"satellite":
				var satellite = create_new_spell("satellite", unit, casting_unit.coords, false)
				satellite.ignore_yield()
				yield(satellite, "complete_cast")
				var stab = create_new_spell("stab", unit, casting_unit.coords, false)
				stab.ignore_yield()
				yield(stab, "complete_cast")
				var dead : bool = yield(casting_unit, "lost_life")
				if dead or casting_unit.has_condition("silence") or \
				casting_unit.has_condition("stun"):
					spell.counter()
					cancel_reaction_state()
					return
				cancel_reaction_state()
			"cancel":
				cancel_reaction_state()
				
	spell.resolve()
	
func unit_targeted(casting_unit, target_coord, spell) -> void:
	var targeted_unit = get_unit(target_coord)
	var targeted_obstacle = get_obstacle(target_coord)
	
	self.casting_unit = casting_unit
	
	spell.hold_resolution()
	
	if Globals.is_valid(targeted_obstacle) and \
	targeted_obstacle.properties.has("relay"):
		targeted_obstacle.queue_free()
		
		for cell in Globals.filled_circle(targeted_obstacle.coords, 2):
			var unit = get_unit(cell)
			
			if Globals.is_valid(unit):
				go_to_spell_state()
				var spell_copy = create_new_spell(spell, casting_unit, unit.coords, \
					false, true, true)
				yield(spell_copy, "complete_cast")
				if wait_for_spell():
					yield(self, "resolve_current_spell")
					
		spell.counter()
		return
		
	if !Globals.is_valid(targeted_unit):
		spell.resolve()
		return
		
	if not targeted_unit.is_in_group("PlayerUnits"):
		spell.resolve()
		return
		
	if targeted_unit.has_condition("illusion"):
		targeted_unit.vanish()
		spell.counter()
		return
		
	current_unit = targeted_unit
	
	var reactions : PoolStringArray = PoolStringArray([])
		
	if targeted_unit.has_condition("reflect"):
		reactions.append("reflect")
	if targeted_unit.has_condition("refract"):
		reactions.append("refract")
	if targeted_unit.has_condition("uncertainty_2"):
		reactions.append("uncertainty")
	if reactions.empty():
		spell.resolve()
		return
		
	if not targeted_unit.trigger_reaction(reactions):
		spell.resolve()
		return
		
	targeted_unit.triggered += 1
	go_to_reaction_state()
	casting_unit.hold_toss()
	var reaction = yield(targeted_unit, "reaction_finished")
	targeted_unit.hide_reactions()
	targeted_unit.triggered -= 1
	var counter : bool = false
	
	match reaction:
		"reflect":
			var reflect = create_new_spell("reflect", targeted_unit, targeted_unit.coords, false)
			yield(reflect, "complete_cast")
			var spell_copy = create_new_spell(spell, casting_unit, casting_unit.coords, false, \
				true, true)
			yield(spell_copy, "complete_cast")
			counter = true
			cancel_reaction_state()
		"refract":
			var refract = create_new_spell("refract", targeted_unit, targeted_unit.coords, false)
			yield(refract, "complete_cast")
			var flag = false
			for cell in refract.get_highlight_cells(targeted_unit):
				if has_unit(cell):
					flag = true
				if has_obstacle(cell) and get_obstacle(cell).targetable:
					flag = true
			if not flag:
				spell.counter()
				cancel_reaction_state()
				return
			go_to_reaction_target_state(refract)
			var target_s = yield(self, "reaction_target_selected")
			var target_1 : Vector3 = Vector3.ZERO
			var target_2 : Vector3 = Vector3.ZERO
			cancel_reaction_state()
			var spell_copy = null # spell
			if typeof(target_s) == TYPE_VECTOR3:
				target_1 = target_s
			elif typeof(target_s) == TYPE_VECTOR3_ARRAY:
				var spell_copy_2 = null # spell
				if target_s.size() == 0:
					target_1 = targeted_unit.coords
				if target_s.size() >= 1:
					target_1 = target_s[0]
				if target_s.size() >= 2:
					target_2 = target_s[1]
					spell_copy_2 = create_new_spell(spell, casting_unit, target_2, \
						false, true, true)
			spell_copy = create_new_spell(spell, casting_unit, target_1, false, true, true)
			yield(spell_copy, "complete_cast")
			counter = true
		"uncertainty":
			var uncertainty = create_new_spell("uncertainty", targeted_unit, Vector3.ZERO, \
				 false, false)
			uncertainty.is_reaction = true
			uncertainty.ignore_yield()
			uncertainty.cast(targeted_unit, Vector3.ZERO)
			yield(uncertainty, "complete_cast")
			counter = true
			cancel_reaction_state()
		"cancel":
			counter = false
			cancel_reaction_state()
			
	if counter:
		spell.counter()
	else:
		spell.resolve()
		yield(spell, "complete_cast")
		
	go_to_current_turn()
	
func unit_in_aoe(casting_unit, target_coord, flex_cells, spell) -> void:
	var cells : PoolVector3Array = PoolVector3Array([])
	cells.append_array(flex_cells)
	if not cells.has(target_coord):
		cells.append(target_coord)
		
	var triggered_units : Array = []
	
	for cell in cells:
		var unit = get_unit(cell)
		
		if !Globals.is_valid(unit):
			continue
		if unit.is_in_group("PlayerUnits"):
			triggered_units.append(unit)
			
	if triggered_units.empty():
		spell.resolve()
		return
		
	casting_unit.hold_toss()
	
	for unit in triggered_units:
		current_unit = unit
		var reactions : PoolStringArray = PoolStringArray([])
		
		if unit.has_condition("blink"):
			reactions.append("blink")
		if unit.has_condition("uncertainty_2"):
			reactions.append("uncertainty")
		if reactions.empty():
			continue
			
		if not unit.trigger_reaction(reactions):
			continue
		unit.triggered += 1
		go_to_reaction_state()
		var reaction = yield(unit, "reaction_finished")
		unit.hide_reactions()
		unit.triggered -= 1
		
		match reaction:
			"blink":
				var blink = create_new_spell("blink", unit, unit.coords, false, false)
				blink.is_reaction = true
				go_to_reaction_target_state(blink)
				var new_target = yield(self, "reaction_target_selected")
				blink.ignore_yield()
				blink.cast(unit, new_target)
				yield(blink, "complete_cast")
				cancel_reaction_state()
			"uncertainty":
				var uncertainty = create_new_spell("uncertainty", unit, unit.coords, false, false)
				uncertainty.is_reaction = true
				uncertainty.ignore_yield()
				uncertainty.cast(unit, Vector3.ZERO)
				yield(uncertainty, "complete_cast")
				cancel_reaction_state()
			"cancel":
				cancel_reaction_state()
				
	spell.resolve()
	yield(spell, "complete_cast")
	go_to_current_turn()
	
func apply_terrain(coord : Vector3, terrain : String, modifiers : Array = []) -> void:
	var cell = get_cell(coord)
	
	if !Globals.is_valid(cell):
		return
		
	cell.set_terrain(terrain, modifiers)
	
func get_seth_trip() -> PoolVector3Array:
	var seth = null
	
	for unit in get_tree().get_nodes_in_group("PlayerUnits"):
		if unit.has_condition("wave_function_collapse"):
			seth = unit
			break
			
	if Globals.is_valid(seth) and \
	Globals.is_valid(seth.superposition):
		return Globals.line(seth.coords, seth.superposition.coords)
	else:
		return PoolVector3Array([])
	
func get_turn_count() -> int:
	return get_parent().turn_count
	
func get_cell(coord : Vector3):
	if has_cell(coord):
		return grid[coord]
	else:
		push_error("coordinate doesn't exist")
		return null
		
func get_all_cells() -> Array:
	return grid.values()
	
func get_all_coords() -> PoolVector3Array:
	return PoolVector3Array(grid.keys())
	
func get_unit(coord : Vector3):
	if has_cell(coord):
		return get_cell(coord).get_occupying_unit()
	else:
		return null
	
func get_obstacle(coord : Vector3):
	if has_cell(coord):
		return get_cell(coord).get_occupying_obstacle()
	else:
		return null
	
func has_cell(coord : Vector3) -> bool:
	return grid.has(coord)
	
func has_unit(coord : Vector3) -> bool:
	if not has_cell(coord):
		return false
		
	if Globals.is_valid(get_cell(coord).get_occupying_unit()):
		return true
	else:
		return false
		
func has_obstacle(coord : Vector3) -> bool:
	if not has_cell(coord):
		return false
		
	if Globals.is_valid(get_cell(coord).get_occupying_obstacle()):
		return true
	else:
		return false
		
func has_occupant(coord : Vector3) -> bool:
	if has_unit(coord) or has_obstacle(coord):
		return true
	else:
		return false
	
func find_path(start : Vector3, end : Vector3, mode : String, cap : int = -1) -> PoolVector2Array:
	if grid.has(start) and grid.has(end):
		do_obstacles(mode)
		astar.set_point_disabled(grid[end].id, false)
		
		var offset : Vector2 = Vector2(10, 10)
		var new_path : PoolVector2Array = astar.get_point_path(grid[start].id, grid[end].id)
		
		if cap >= 0 and new_path.size() >= 2:
			var path_end : Vector2
			
			if cap < new_path.size():
				path_end = new_path[cap]
			else:
				path_end = new_path[-2]
				
			var c : Vector3 = Globals.pixel_to_grid(path_end + offset)
			
			if c != start:
				while new_path.size() >= 2 and grid[c].is_occupied():
					astar.set_point_disabled(grid[c].id, true)
					new_path = astar.get_point_path(grid[start].id, grid[end].id)
					if new_path.size() < 2:
						break
					if cap < new_path.size():
						path_end = new_path[cap]
					else:
						path_end = new_path[-2]
					c = Globals.pixel_to_grid(path_end + offset)
					
			if cap < new_path.size():
				new_path.resize(cap+1)
			
		undo_obstacles()
		
		return new_path
	
	return PoolVector2Array([])
	
func do_obstacles(obstacle_mode : String) -> void:
	match obstacle_mode:
		"nothing":
			return
		"player_units":
			for unit in get_node("../objects/player_units").get_children():
				if has_cell(unit.coords):
					astar.set_point_disabled(grid[unit.coords].id)
		"enemy_units":
			for unit in get_node("../objects/enemy_units").get_children():
				if has_cell(unit.coords):
					astar.set_point_disabled(grid[unit.coords].id)
		"units", "all_units":
			for unit in get_node("../objects/player_units").get_children():
				if has_cell(unit.coords):
					astar.set_point_disabled(grid[unit.coords].id)
			for unit in get_node("../objects/enemy_units").get_children():
				if has_cell(unit.coords):
					astar.set_point_disabled(grid[unit.coords].id)
				
	for obstacle in get_node("../objects/obstacles").get_children():
		if obstacle.traversal_cost == 99:
			if not grid.has(obstacle.coords):
				continue
			astar.set_point_disabled(grid[obstacle.coords].id)
			for tile in obstacle.extra_tiles:
				astar.set_point_disabled(grid[obstacle.coords + tile].id)
			
func undo_obstacles() -> void:
	for cell in grid.values():
		astar.set_point_disabled(cell.id, false)
	
func flood_fill(start : Vector3, max_distance : int, obstacle_mode : String) -> PoolVector3Array:
	var final_pool : PoolVector3Array = PoolVector3Array([])
	var stack : Array = [start]
	var aux_removal : PoolVector3Array = PoolVector3Array([])
	
	while not stack.empty():
		var current = stack.pop_back()
		
		if not grid.has(current):
			continue
		if current in final_pool:
			continue
			
		var path : PoolVector2Array = find_path(start, current, obstacle_mode)
		
		if path.size() == 0:
			continue
			
		var path_cost : int = 0
		
		for point in path:
			path_cost += grid[Globals.pixel_to_grid(point + \
				Vector2(10, 10))].traversal_cost
			
		path_cost -= grid[start].traversal_cost
		
		if path_cost > max_distance:
			continue
			
		final_pool.append(current)
		
		for direction in Globals.CUBE_VECTORS:
			var coord : Vector3 = current + direction
			
			if coord in final_pool:
				continue
				
			stack.append(coord)
			
	var final_final_pool : PoolVector3Array = PoolVector3Array([])
	
	for obstacle in get_node("../objects/obstacles").get_children():
		if obstacle.traversal_cost == 99:
			aux_removal.append(obstacle.coords)
			for tile in obstacle.extra_tiles:
				aux_removal.append(obstacle.coords + tile)
				
	for unit in get_tree().get_nodes_in_group("AllUnits"):
		aux_removal.append(unit.coords)
		
	for coord in final_pool:
		if not coord in aux_removal:
			final_final_pool.append(coord)
			
	final_final_pool.append(start)
	
	return final_final_pool
	
func init_astar() -> void:
	for cell in grid.values():
		astar.add_point(cell.id, Globals.grid_to_pixel(cell.coords), \
			cell.traversal_cost)
		
		for i in range(6):
			if Globals.is_valid(cell.get_adjacent(i)):
				astar.connect_points(cell.id, cell.get_adjacent(i).id)
