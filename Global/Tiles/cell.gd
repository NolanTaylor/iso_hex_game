extends Node2D

export var traversal_cost : int = 1
export var decor : bool = false

var coords : Vector3 =  Vector3(0, 0, 0)
var id : int = 0
var terrain_duration : int = 0
var highlight_color : String = "null"
var current_terrain : String = "null"
var occupying_unit = null
var occupying_obstacle = null

var adjacent_cells : Array = [
	null,
	null,
	null,
	null,
	null,
	null,
]

func _ready():
	pass
	
func _process(delta):
	pass
	
func upkeep() -> void:
	match current_terrain:
		"pressurize":
			terrain_duration -= 1
			if terrain_duration == 0:
				set_terrain("null")
		"depressurize":
			terrain_duration -= 1
			if terrain_duration == 0:
				set_terrain("null")
		"stars":
			pass
		"stars_origin":
			pass
		"rot":
			pass
		"null":
			return
		_:
			return
	
func endstep() -> void:
	match current_terrain:
		"pressurize":
			pass
		"depressurize":
			pass
		"stars":
			pass
		"stars_origin":
			pass
		"rot":
			pass
		"null":
			return
		_:
			return
		
func runSBAs() -> void:
	return
	
func find_grid() -> Vector3:
	var grid : Vector3 = Vector3(0, 0, 0)
	
	grid.x = global_position.x / 23
	grid.y = ((global_position.y - 16) / 16) - (grid.x / 2)
	grid.z = -(grid.x + grid.y)
	
	coords = grid
	
	return grid
	
func get_adjacent(index : int):
	return adjacent_cells[index]
	
func set_adjacent(index : int, cell) -> void:
	adjacent_cells[index] = cell
	
func get_adjacent_all() -> Array:
	return adjacent_cells
	
func get_occupying_unit():
	return occupying_unit
	
func set_occupying_unit(unit) -> void:
	occupying_unit = unit
	
func get_occupying_obstacle():
	return occupying_obstacle
	
func set_occupying_obstacle(obstacle) -> void:
	occupying_obstacle = obstacle
	
func is_occupied() -> bool:
	if Globals.is_valid(occupying_unit):
		return true
	if Globals.is_valid(occupying_obstacle):
		return true
		
	return false
	
func set_terrain(terrain : String, modifiers : Array = []) -> void:
	var new_terrain : String = "null"
	
	for child in $terrain.get_children():
		child.emitting = false
		
	match terrain:
		"pressurize":
			if current_terrain == "depressurize":
				new_terrain = "null"
			else:
				new_terrain = "pressurize"
				$terrain/pressurize.emitting = true
		"depressurize":
			if current_terrain == "pressurize":
				new_terrain = "null"
			else:
				new_terrain = "depressurize"
				$terrain/depressurize.emitting = true
		"stars":
			new_terrain = "stars"
			$terrain/stars.emitting = true
		"stars_origin":
			new_terrain = "stars_origin"
			$terrain/stars.emitting = true
		"rot":
			new_terrain = "rot"
			$terrain/rot.emitting = true
		_:
			pass
			
	current_terrain = new_terrain
	
	match current_terrain:
		"pressurize":
			traversal_cost = 2
			terrain_duration = 2
		"depressurize":
			traversal_cost = 2
			terrain_duration = 2
		"stars":
			pass
		"stars_origin":
			pass
		"rot":
			pass
		"null":
			traversal_cost = 1
			terrain_duration = 0
		_:
			pass
	
func set_highlight(color : String) -> void:
	if color == highlight_color:
		return
	for child in $highlights.get_children():
		child.hide()
		
	match color:
		"blue":
			$highlights/blue_highlight.show()
		"red":
			$highlights/red_highlight.show()
		"pink":
			$highlights/pink_highlight.show()
		"green":
			$highlights/green_highlight.show()
			
	highlight_color = color
	
func set_display_text(text : String) -> void:
	$display/label.text = text
	
func hide_display_text() -> void:
	$display/label.text = ""
