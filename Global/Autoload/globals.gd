extends Node

# /* -------------------------- CONSTS ----------------------------------- */

const ACTION_COORDS : Array = [
	Vector2(-8, 0),
	Vector2(-22, -8),
	Vector2(-8, -16),
	Vector2(-22, -24),
	Vector2(-8, -32),
	Vector2(-22, -40),
]

const ACTION_COORDS_FLIP : Array = [
	Vector2(24, 0),
	Vector2(38, -8),
	Vector2(24, -16),
	Vector2(38, -24),
	Vector2(24, -32),
	Vector2(38, -40),
]

const CUBE_VECTORS : Array = [
	Vector3(0, -1, 1),
	Vector3(1, -1, 0),
	Vector3(1, 0, -1),
	Vector3(0, 1, -1),
	Vector3(-1, 1, 0),
	Vector3(-1, 0, 1),
]

const DIRECTIONS : Array = [
	"north",
	"north_east",
	"south_east",
	"south",
	"south_west",
	"north_west",
]

# /* ------------------------- VARIABLES ---------------------------------- */

var buffered_scene : String = "null"
var current_hex_grid : Node = null
var current_camera : Node = null
var scale : int = 1

var picked_units : PoolStringArray = PoolStringArray([])
var dead_units : PoolStringArray = PoolStringArray([])

# /* ----------------------- HEX FUNCTIONS -------------------------------- */

static func hex_add(a : Vector3, b : Vector3) -> Vector3:
	return Vector3(a.x + b.x, a.y + b.y, a.z + b.z)
	
static func hex_subtract(a : Vector3, b : Vector3) -> Vector3:
	return Vector3(a.x - b.x, a.y - b.y, a.z - b.z)
	
static func hex_scale(hex : Vector3, factor : int) -> Vector3:
	return Vector3(hex.x * factor, hex.y * factor, hex.z * factor)
	
static func hex_round(frac : Vector3) -> Vector3:
	var q = round(frac.x)
	var r = round(frac.y)
	var s = round(frac.z)
	
	var q_diff = abs(q - frac.x)
	var r_diff = abs(r - frac.y)
	var s_diff = abs(s - frac.z)
	
	if q_diff > r_diff and q_diff > s_diff:
		q = -r - s
	elif r_diff > s_diff:
		r = -q - s
	else:
		s = -q - r
		
	return Vector3(q, r, s)
	
static func hex_lerp(a, b, t) -> Vector3:
	return Vector3(
		lerp(a.x, b.x, t),
		lerp(a.y, b.y, t),
		lerp(a.z, b.z, t)
	)
	
static func hex_distance(a : Vector3, b : Vector3) -> int:
	var vec : Vector3 = hex_subtract(a, b)
	return int((abs(vec.x) + abs(vec.y) + abs(vec.z)) / 2)
	
static func hex_direction(a : Vector3, b : Vector3) -> Vector3:
	var hex := hex_subtract(b, a)
	
	if hex.x == 0 and abs(hex.y) == abs(hex.z):
		if hex.y < 0 and hex.z > 0:
			return CUBE_VECTORS[0]
		if hex.y > 0 and hex.z < 0:
			return CUBE_VECTORS[3]
	if hex.y == 0 and abs(hex.x) == abs(hex.z):
		if hex.x < 0 and hex.z > 0:
			return CUBE_VECTORS[5]
		if hex.x > 0 and hex.z < 0:
			return CUBE_VECTORS[2]
	if hex.z == 0 and abs(hex.x) == abs(hex.y):
		if hex.x < 0 and hex.y > 0:
			return CUBE_VECTORS[4]
		if hex.x > 0 and hex.y < 0:
			return CUBE_VECTORS[1]
		
	return Vector3.ZERO
	
static func hex_adjacent(hex : Vector3) -> PoolVector3Array:
	var pool : PoolVector3Array = PoolVector3Array([])
	
	for c in CUBE_VECTORS:
		pool.append(hex + c)
		
	return pool
	
static func hex_opposite(hex : Vector3) -> Vector3:
	if hex == CUBE_VECTORS[0]:
		return CUBE_VECTORS[3]
	elif hex == CUBE_VECTORS[1]:
		return CUBE_VECTORS[4]
	elif hex == CUBE_VECTORS[2]:
		return CUBE_VECTORS[5]
	elif hex == CUBE_VECTORS[3]:
		return CUBE_VECTORS[0]
	elif hex == CUBE_VECTORS[4]:
		return CUBE_VECTORS[1]
	elif hex == CUBE_VECTORS[5]:
		return CUBE_VECTORS[2]
	else:
		return Vector3.ZERO
	
static func direction_to_vector(direction : String) -> Vector3:
	match direction:
		"north":
			return CUBE_VECTORS[0]
		"north_east":
			return CUBE_VECTORS[1]
		"south_east":
			return CUBE_VECTORS[2]
		"south":
			return CUBE_VECTORS[3]
		"south_west":
			return CUBE_VECTORS[4]
		"north_west":
			return CUBE_VECTORS[5]
		_:
			return Vector3(0, 0, 0)
	
# /* ----------------------- HIGHLIGHT PATTERNS ---------------------------- */

static func filled_circle(origin : Vector3, radius : int) -> PoolVector3Array:
	var final_pool : PoolVector3Array = PoolVector3Array([])
	
	if radius == 0:
		final_pool.append(origin)
		return final_pool
	
	for i in range(-radius, radius + 1):
		for j in range(max(-radius, -i - radius), min(radius, -i + radius) + 1):
			var k = -i - j
			final_pool.append(Vector3(origin.x + i, origin.y + j, origin.z + k))
			
	return final_pool
	
static func ring(origin : Vector3, radius : int, thicc : int = 1) -> PoolVector3Array:
	var final_pool : PoolVector3Array = PoolVector3Array([])
	
	if radius < thicc:
		return final_pool
	
	var filled_pool : PoolVector3Array = filled_circle(origin, radius)
	var inner_pool : PoolVector3Array = filled_circle(origin, radius - thicc)
	
	for coord in filled_pool:
		if not coord in inner_pool:
			final_pool.append(coord)
	
	return final_pool
	
static func wedge(origin : Vector3, direction : String, radius : int) -> PoolVector3Array:
	var final_pool : PoolVector3Array = PoolVector3Array([])
	
	var key_values : PoolIntArray = PoolIntArray([])
	
	match direction:
		"north":
			key_values = PoolIntArray([0, 5, 1])
		"north_east":
			key_values = PoolIntArray([1, 0, 2])
		"south_east":
			key_values = PoolIntArray([2, 1, 3])
		"south":
			key_values = PoolIntArray([3, 2, 4])
		"south_west":
			key_values = PoolIntArray([4, 3, 5])
		"north_west":
			key_values = PoolIntArray([5, 4, 0])
			
	var start_of_cone : Vector3 = hex_add(origin, CUBE_VECTORS[key_values[0]])
	final_pool.append(start_of_cone)
	
	for i in range(1, radius):
		var left_anchor : Vector3 = hex_add(start_of_cone, hex_scale(CUBE_VECTORS[key_values[1]], i))
		var right_anchor : Vector3 = hex_add(start_of_cone, hex_scale(CUBE_VECTORS[key_values[2]], i))
		
		var anchor : Vector3 = left_anchor
		
		for j in i + 1:
			final_pool.append(anchor)
			
			anchor = hex_add(anchor, CUBE_VECTORS[key_values[2]])
			
		anchor = right_anchor
		
		for j in i:
			final_pool.append(anchor)
			
			anchor = hex_add(anchor, CUBE_VECTORS[key_values[1]])
	
	return final_pool
	
static func line(start_point : Vector3, end_point : Vector3) -> PoolVector3Array:
	var N : int = hex_distance(start_point, end_point)
	var results : PoolVector3Array = PoolVector3Array([])
	
	for i in range(N):
		results.append(hex_round(hex_lerp(start_point, end_point, 1.0 / N * i)))
		
	return results
	
static func rigid_line(origin : Vector3, direction, length : int, obstacle_mode : String = \
"none", grid : Dictionary = { }) -> PoolVector3Array:
	var final_pool : PoolVector3Array = PoolVector3Array([])
	
	var use_dir : Vector3
	if typeof(direction) == TYPE_STRING:
		use_dir = direction_to_vector(direction)
	elif typeof(direction) == TYPE_VECTOR3:
		use_dir = direction
	
	for i in range(1, length + 1):
		var next_vector : Vector3 = hex_add(origin, hex_scale(use_dir, i))
		
		if grid.empty():
			final_pool.append(next_vector)
			continue
		if not grid.has(next_vector):
			break
			
		match obstacle_mode:
			"all":
				if is_valid(grid[next_vector].get_occupying_unit()):
					break
				if is_valid(grid[next_vector].get_occupying_obstacle()):
					break
			"units":
				if is_valid(grid[next_vector].get_occupying_unit()):
					break
			"obstacles":
				if is_valid(grid[next_vector].get_occupying_obstacle()):
					break
					
		final_pool.append(next_vector)
		
	return final_pool
	
# /* ----------------------------------------------------------------------- */
	
static func pixel_to_grid(pos : Vector2) -> Vector3:
	var grid : Vector3 = Vector3(0, 0, 0)
	var is_odd : bool = false
	var row : int
	var column : int = pos.x / 23
	
	if pos.x < 0:
		column -= 1
	
	var rel_x : float = pos.x - (column * 23)
	var rel_y : float
	
	if column & 1 == 1:
		is_odd = true
	
	if is_odd:
		row = int((pos.y - 8) / 16)
		if pos.y < 8:
			row -= 1
		rel_y = pos.y - ((row * 16) + 8)
	else:
		row = int((pos.y - 16) / 16)
		if pos.y < 16:
			row -= 1
		rel_y = pos.y - ((row * 16) + 16)
		
	if rel_x < rel_y - 8:
		column -= 1
		row += 1
		if is_odd:
			row -= 1
	elif rel_x < -rel_y + 8:
		column -= 1
		row -= 1
		if not is_odd:
			row += 1
			
	grid.x = column
	grid.y = row - (column + (column & 1)) / 2
	grid.z = -(grid.x + grid.y)
	
	return grid
	
static func grid_to_pixel(grid : Vector3) -> Vector2:
	var pos : Vector2 = Vector2(0, 0)
	
	pos.x = (grid.x * 23)
	pos.y = ((grid.y + (grid.x / 2)) * 16) + 16
	
	return pos
	
func set_hex_grid(grid) -> void:
	current_hex_grid = grid
	
func get_hex_grid() -> Node:
	return current_hex_grid
	
func set_camera(cam) -> void:
	current_camera = cam
	
func get_camera() -> Node:
	return current_camera
	
func finish_job(name : String) -> void:
	Config.dialogue_index += 1
	Config.jobs.erase(name)
	
	for job in Config.job_queue[0]:
		Config.jobs.append(job)
		
	Config.job_queue.remove(0)
	
func set_inherit_material(node) -> void:
	if node is CanvasItem:
		if node.has_node("has_material"):
			return
		node.use_parent_material = true
		
	node.connect("child_entered_tree", self, "set_inherit_material")
	
static func is_valid(object) -> bool:
	if object == null:
		return false
	if typeof(object) == TYPE_OBJECT and \
	(!is_instance_valid(object) or object.is_queued_for_deletion()):
		return false
	else:
		return true
		
static func own(node, new_owner) -> void:
	if not node == new_owner and (not node.owner or node.filename):
		node.owner = new_owner
	if node.get_child_count():
		for kid in node.get_children():
			own(kid, new_owner)
