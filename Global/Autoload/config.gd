extends Node

var config = ConfigFile.new()
var err = config.load("user://save.cfg")
var dialogue_index : int = 0

var storage : Array = [ # <String>
	"boots",
	"potion_of_healing_1",
]

var characters : Dictionary = {
	"grace": {
		"skill_points": 0,
		"skills": ["waves_and_particles_1", "health_1", "corruption_1"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "mirror",
			"right_arm": "knife",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"foo": {
		"skill_points": 0,
		"skills": ["health_1"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "null",
			"right_arm": "null",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"cherry": {
		"skill_points": 0,
		"skills": ["kinetic_and_potential_1", "displace_1", "health_1", "health_2"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "sword",
			"right_arm": "sword",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"acacius": {
		"skill_points": 0,
		"skills": ["attract_and_repel_1", "restoration_1", "health_1"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "sword",
			"right_arm": "sword",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"illiana": {
		"skill_points": 0,
		"skills": ["states_and_processes_1", "insulate_1", "equilibrium_1", "health_1", \
			"health_2", "isobaric_process", "corruption_1"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "null",
			"right_arm": "null",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"fiametta": {
		"skill_points": 0,
		"skills": ["states_and_processes_1", "states_and_processes_2", "pressurize_1", \
			"pressurize_2", "combustion_1", "combustion_2", "rune_smith", "health_1", "health_2", \
			"movement_1", "isobaric_process", "isentropic_process"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "null",
			"right_arm": "null",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"seth": {
		"skill_points": 0,
		"skills": ["space_and_time_1", "space_and_time_2", "superposition", "uncertainty_1", \
			"wave_function_collapse_1", "health_1", "health_2", "movement_1", "movement_2", \
			"corruption_1", "decay"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "null",
			"right_arm": "null",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"avram": {
		"skill_points": 0,
		"skills": ["attract_and_repel_1", "discharge_1", "coercivity_field_1", "health_1",
			"health_2", "sturdy_1", "corruption_1", "siphon"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "null",
			"right_arm": "null",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"ozias": {
		"skill_points": 0,
		"skills": ["waves_and_particles_1", "disperse_1", "blind_1", "blind_2", "health_1", \
			"health_2", "movement_1"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "null",
			"right_arm": "null",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"anastasia": {
		"skill_points": 0,
		"skills": ["wax_and_wane_1", "shroud_1", "health_1", "health_2", "movement_1", \
			"sprint_1", "proficiency_4"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "null",
			"right_arm": "null",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
	
	"esmeray": {
		"skill_points": 0,
		"skills": ["wax_and_wane_1", "eclipse_1", "eclipse_2", "event_horizon_1",
			"nucleosynthesis", "health_1", "health_2", "health_3", "proficiency_1",
			"adrenaline", "corruption_1", "corruption_2", "rot"],
		
		"equipment": {
			"head": "null",
			"torso": "null",
			"left_arm": "null",
			"right_arm": "null",
			"legs": "null",
			"inventory_1": "null",
			"inventory_2": "null",
			"inventory_3": "null",
			"inventory_4": "null",
			"inventory_5": "null",
			"inventory_6": "null",
			"inventory_7": "null",
			"rune_1": "null",
			"rune_2": "null",
			"rune_3": "null",
		},
	},
}

var aux_team : Array = [
	"avram",
]

var jobs : Array = [
	"job_1",
]

var job_queue : Array = [
	["job_2", "job_3"],
	["job_5"],
]

func _ready():
	if err == OK:
		print("yee haw")
	else:
		print("nee naw")
		
		var file = File.new()
		file.open("user://save.cfg", File.WRITE)
		err = config.load("user://save.cfg")
		
	#if config.has_section_key("Characters", "Characters"):
	#	characters = config.get_value("Characters", "Characters")
	
func save():
	if err == OK:
		config.set_value("Characters", "Characters", characters)
		config.set_value("Storage", "Storage", storage)
		
		config.save("user://save.cfg")
	else:
		push_error("el loades no beuno")
		
func remove_from_storage(item_name) -> void:
	storage.erase(item_name)
