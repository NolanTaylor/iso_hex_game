extends Node

const SKILLS : Dictionary = {
	"aberration": {
		"name": "Aberration",
		"description": "Upon completing {their} move action, {name} applies 1 [url=vulnerable]vulnerable[/url] to enemy units within two tiles.",
		"prereqs": "",
		"tags": [
			"Passive",
			"Radius: 2",
		],
	},
	
	"absolute_zero": {
		"name": "Absolute Zero",
		"description": "[url=stunned]Stun[/url] all units in a four-tile cone, dealing two damage.",
		"prereqs": "",
		"tags": [
			"4 entropy",
			"Action",
			"Once per combat",
			"Damage: 2",
		],
	},
	
	"absorb_1": {
		"name": "Absorb I",
		"description": "If {name} would be [url=stunned]stunned[/url], [url=silence]silenced[/url], or [url=rooted]rooted[/url], {they} may ignore the effect.",
		"prereqs": "",
		"tags": [
			"2 work",
			"Reaction",
		],
	},
	
	"absorb_2": {
		"name": "Absorb II",
		"description": "Absorb restores two health to {name}.",
		"prereqs": "[url=absorb_1]Absorb I[/url]",
		"tags": [
			"2 work",
			"Reaction",
		],
	},
	
	"accelerate_1": {
		"name": "Accelerate I",
		"description": "{name} or an adjacent unit gains one action, one swift action, and three movement until the end of turn.",
		"prereqs": "",
		"tags": [
			"1 flux",
			"1 work",
			"Action",
			"Range: 1"
		],
	},
	
	"accelerate_2": {
		"name": "Accelerate II",
		"description": "Accelerate costs a swift action.",
		"prereqs": "[url=accelerate_1]Accelerate I[/url]",
		"tags": [
			"1 flux",
			"1 work",
			"Swift Action",
			"Range: 1"
		],
	},
	
	"accretion": {
		"name": "Accretion",
		"description": "Healing is doubled on {name} and {they} can heal above {their} maximum health. Overhealing this way is not doubled. At the end of {their} turn, if {name} is above {their} maximum health, {they} loses half the excess (rounded up).",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"ac_electrolysis": {
		"name": "AC Electrolysis",
		"description": "{name} heals all allies within the radius instead of {them}self.",
		"prereqs": "[url=electrolysis_2]Electrolysis II[/url]",
		"tags": [
			"5 flux",
			"Action",
			"Once per combat",
			"Duration: 3+",
			"Radius: 3",
		],
	},
	
	"adiabatic_process": {
		"name": "Adiabatic Process",
		"description": "The effects of [url=thermal_terrain]pressurized[/url]and [url=thermal_terrain]depressurized[/url] areas are doubled against enemies in the same area as {name}.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"adrenaline": {
		"name": "Adrenaline",
		"description": "If {name} starts {their} turn at or below one third of {their} maximum health (rounded), {they} restores one health.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"alchemy_1": {
		"name": "Alchemy I",
		"description": "Items cost a swift action to use.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"alchemy_2": {
		"name": "Alchemy II",
		"description": "idk what alchemy 2 does lol",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"alignment": {
		"name": "Alignment",
		"description": "{name}'s basic attacks have an additional range and pass through units, dealing reduced damage for each unit hit.",
		"prereqs": "[url=proficiency_4]Proficiency IV[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"aphelion": {
		"name": "Aphelion",
		"description": "At the end of {their} turn, {name} gets an additional reaction if two or more enemies are within two tiles of {them}.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"artificial_refraction": {
		"name": "Artificial Refraction",
		"description": "Refract's range is increased by two.",
		"prereqs": "[url=refract]Refract[/url]",
		"tags": [
			"1 light",
			"Reaction",
			"Range: +2",
		],
	},
	
	"attract_and_repel_1": {
		"name": "Attract and Repel I",
		"description": "{name} gains [url=flux]flux[/url] at the start of {their} turn up to a maxium of three.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"attract_and_repel_2": {
		"name": "Attract and Repel II",
		"description": "{name} gains an additional [url=flux]flux[/url] each turn up to a maximum of five.",
		"prereqs": "[url=attract_and_repel_1]Attract and Repel I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"battery_1": {
		"name": "Battery I",
		"description": "After casting a spell, if {name} has 0 [url=flux]flux[/url] {they} gains one movement.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"battery_2": {
		"name": "Battery II",
		"description": "Movement gain is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"blight": {
		"name": "Blight",
		"description": "{name} kills {them}self, dealing damage equal to {their} missing health to adjacent units (missing health is calculated before the unit kills themselves but after the corrupted health is spent as a cost).",
		"prereqs": "",
		"tags": [
			"1 corruption",
			"Action",
		],
	},
	
	"blind_1": {
		"name": "Blind I",
		"description": "Target unit within three tiles is [url=blind]blinded[/url] until the end of their next turn.",
		"prereqs": "",
		"tags": [
			"2 light",
			"Swift Action",
			"Range: 3",
		],
	},
	
	"blind_2": {
		"name": "Blind II",
		"description": "Blind applies 2 [url=lethargy]lethargy[/url].",
		"prereqs": "[url=blind_1]Blind I[/url]",
		"tags": [
			"2 light",
			"Swift Action",
			"Range: 3",
		],
	},
	
	"blind_3": {
		"name": "Blind III",
		"description": "Blind also deals one damage.",
		"prereqs": "[url=blind_1]Blind I[/url]",
		"tags": [
			"2 light",
			"Swift Action",
			"Damage: 1",
			"Range: 3",
		],
	},
	
	"blind": {
		"name": "Blind",
		"description": "A blinded unit cannot take swift actions or reactions.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"blink_1": {
		"name": "Blink I",
		"description": "{name} [url=teleport]teleports[/url] into an unoccupied space within three tiles.",
		"prereqs": "",
		"tags": [
			"1 light",
			"Action",
			"Range: 3",
		],
	},
	
	"blink_2": {
		"name": "Blink II",
		"description": "Blink may be triggered and cast as a reaction when {name} is standing in the effect area of an untargeted spell.",
		"prereqs": "[url=blink_1]Blink I[/url]",
		"tags": [
			"1 light",
			"Reaction",
			"Action",
			"Range: 3",
		],
	},
	
	"blink_3": {
		"name": "Blink III",
		"description": "Blink's range is increased by two.",
		"prereqs": "[url=blink_1]Blink I[/url]",
		"tags": [
			"1 light",
			"Action",
			"Range: +2",
		],
	},
	
	"blink_4": {
		"name": "Blink IV",
		"description": "Blink costs a swift action.",
		"prereqs": "[url=blink_1]Blink I[/url]",
		"tags": [
			"1 light",
			"Swift Action",
			"Range: 3",
		],
	},
	
	"brace_1": {
		"name": "Brace I",
		"description": "Whenever {name} is dealt damage, {they} may reduce the damage taken by one.",
		"prereqs": "",
		"tags": [
			"1 work",
			"Reaction",
			"Reduction: 1",
		],
	},
	
	"brace_2": {
		"name": "Brace II",
		"description": "Brace's damage reduction is increased by one.",
		"prereqs": "[url=brace_1]Brace I[/url]",
		"tags": [
			"1 work",
			"Reaction",
			"Reduction: +1",
		],
	},
	
	"causality": {
		"name": "Causality",
		"description": "Whenever {name} loses one or more health, {they} may gain one [url=guard]guard[/url].",
		"prereqs": "",
		"tags": [
			"1 lune",
			"1 rapidity",
			"Reaction",
		],
	},
	
	"chain_reaction": {
		"name": "Chain Reaction",
		"description": "Deal one damage to target unit within two tiles. Chain Reaction recasts itself from that location if there's another valid target within range.",
		"prereqs": "",
		"tags": [
			"5 entropy",
			"Action",
			"Once per combat",
			"Damage: 1",
			"Range: 2",
		],
	},
	
	"charged": {
		"name": "Charged",
		"description": "A charged unit's next basic attack this turn deals an additional damage.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"charge_1": {
		"name": "Charge I",
		"description": "Target adjacent unit's next basic attack this turn [url=charged]deals an additional damage[/url].",
		"prereqs": "",
		"tags": [
			"2 flux",
			"Action",
			"Range: 1",
		],
	},
	
	"charge_2": {
		"name": "Charge II",
		"description": "Charge's range is increased by three.",
		"prereqs": "[url=charge_1]Charge I[/url]",
		"tags": [
			"2 flux",
			"Action",
			"Range: +3",
		],
	},
	
	"charge_3": {
		"name": "Charge III",
		"description": "Charge costs a swift action.",
		"prereqs": "[url=charge_1]Charge I[/url]",
		"tags": [
			"2 flux",
			"Swift Action",
			"Range: 1",
		],
	},
	
	"chrome_dipoles": {
		"name": "Chrome Dipoles",
		"description": "Mark two targets within three tiles as dipole partners. Until the end of combat, if a spell would target one partner, that spell is copied and targets the other.",
		"prereqs": "",
		"tags": [
			"3 light",
			"3 flux",
			"Action",
			"Once per combat",
			"Range: 3",
		],
	},
	
	"cleanse_1": {
		"name": "Cleanse I",
		"description": "Remove all conditions from {name} or an adjacent unit.",
		"prereqs": "",
		"tags": [
			"3 light",
			"Action",
			"Range: 1",
		],
	},
	
	"cleanse_2": {
		"name": "Cleanse II",
		"description": "Cleanse costs a swift action.",
		"prereqs": "[url=cleanse_1]Cleanse I[/url]",
		"tags": [
			"3 light",
			"Swift Action",
			"Range: 1",
		],
	},
	
	"coalesce": {
		"name": "Coalesce",
		"description": "Until the end of {their} next turn, {name}'s basic attacks hit every unit in range.",
		"prereqs": "",
		"tags": [
			"2 lune",
			"2 rapidity",
			"Action",
			"Once per combat",
		],
	},
	
	"coerced": {
		"name": "Coerced",
		"description": "A coerced unit gains an additional movement at the start of their next turn.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"coercivity_field_1": {
		"name": "Coercivity Field I",
		"description": "im thinking of changing guard to how it works in wildermyth so this passive should also be changed lol",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"coercivity_field_2": {
		"name": "Coercivity Field II",
		"description": "{name}'s [url=guard]guard[/url] cannot be [url=pierce]pierced[/url].",
		"prereqs": "[url=coercivity_field_1]Coercivity Field I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"collision": {
		"name": "Collision",
		"description": "If a spell or ability would cause another unit to collide with something, that unit becomes [url=stunned]stunned[/url].",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"combustion_1": {
		"name": "Combustion I",
		"description": "Deal one damage to target unit within four tiles.",
		"prereqs": "",
		"tags": [
			"1 entropy",
			"Action",
			"Damage: 1",
			"Range: 4",
		],
	},
	
	"combustion_2": {
		"name": "Combustion II",
		"description": "Combustion deals an additional damage.",
		"prereqs": "[url=combustion_1]Combustion I[/url]",
		"tags": [
			"1 entropy",
			"Action",
			"Damage: +1",
			"Range: 4",
		],
	},
	
	"combustion_3": {
		"name": "Combustion III",
		"description": "Combustion costs a swift action.",
		"prereqs": "[url=combustion_1]Combustion I[/url]",
		"tags": [
			"1 entropy",
			"Swift Action",
			"Damage: 1",
			"Range: 4",
		],
	},
	
	"combustion_4": {
		"name": "Combustion IV",
		"description": "idk what this spell does",
		"prereqs": "[url=combustion_1]Combustion I[/url]",
		"tags": [
			"1 entropy",
			"Action",
			"Damage: 1",
			"Range: 4",
		],
	},
	
	"combustion_5": {
		"name": "Combustion V",
		"description": "Combustion deals one damage to an additional target within three tiles of the first target.",
		"prereqs": "[url=combustion_1]Combustion I[/url]",
		"tags": [
			"1 entropy",
			"Action",
			"Damage: 1+1",
			"Range: 4+3",
		],
	},
	
	"conduct_1": {
		"name": "Conduct I",
		"description": "{name} loses two health and gains one [url=guard]guard[/url].",
		"prereqs": "",
		"tags": [
			"2 flux",
			"Action",
		],
	},
	
	"conduct_2": {
		"name": "Conduct II",
		"description": "Conduct costs a swift action.",
		"prereqs": "",
		"tags": [
			"2 flux",
			"Swift Action",
		],
	},
	
	"constellation_1": {
		"name": "Constellation I",
		"description": "Any number of target units within two tiles share their conditions among each other. Constellation costs an additional [url=lune]lune[/url] for each target beyond the first.",
		"prereqs": "",
		"tags": [
			"1+X lune",
			"Action",
		],
	},
	
	"constellation_2": {
		"name": "Constellation II",
		"description": "Constellation may target {name} at no additional cost.",
		"prereqs": "[url=constellation_1]Constellation I[/url]",
		"tags": [
			"1+X lune",
			"Action",
		],
	},
	
	"corrode": {
		"name": "Corrode",
		"description": "[url=rot]Rot[/url] spaces in a two-tile cone.",
		"prereqs": "",
		"tags": [
			"1 corruption",
			"Action",
			"Range: 2",
		],
	},
	
	"corruption_1": {
		"name": "Corruption I",
		"description": "{name} starts combat with one health corrupted.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"corruption_2": {
		"name": "Corruption II",
		"description": "An additional health is corrupted.",
		"prereqs": "[url=corruption_1]Corruption I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"corruption_3": {
		"name": "Corruption III",
		"description": "An additional health is corrupted.",
		"prereqs": "[url=corruption_1]Corruption I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"cosmological_horizon": {
		"name": "Cosmological Horizon",
		"description": "Event Horizon's duration is increased by one turn.",
		"prereqs": "[url=event_horizon_2]Event Horizon II[/url]",
		"tags": [
			"3 lune",
			"Reaction",
			"Action",
			"Duration: +1",
			"Radius: 2",
		],
	},
	
	"crepuscular_rays": {
		"name": "Crepuscular Rays",
		"description": "[url=stunned]Stun[/url] up to three target units within five tiles.",
		"prereqs": "[url=event_horizon_2]Event Horizon II[/url]",
		"tags": [
			"4 light",
			"Action",
			"Once per combat",
			"Range: 5",
		],
	},
	
	"dash": {
		"name": "Dash",
		"description": "Dashes move a unit in a straight line and ignore terrain costs. However, dashes are hindered by intervening obstacles, including anything that unit wouldn't be able to move through normally.",
		"prereqs": "",
		"tags": [
			"Movement",
		],
	},
	
	"dawnlit_arrow": {
		"name": "Dawnlit Arrow",
		"description": "{name}'s next basic attack this turn deals two [url=charged]additional damage[/url].",
		"prereqs": "[url=proficiency_4]Proficiency IV[/url]",
		"tags": [
			"1 lune",
			"Swift Action",
		],
	},
	
	"dc_electrolysis": {
		"name": "DC Electrolysis",
		"description": "Electrolysis' healing and damage are increased by one.",
		"prereqs": "[url=electrolysis_2]Electrolysis II[/url]",
		"tags": [
			"5 flux",
			"Action",
			"Once per combat",
			"Healing: +1",
			"Damage: +1",
		],
	},
	
	"decay": {
		"name": "Decay",
		"description": "Apply 99 [url=vulnerable]vulnerable[/url] to target unit within three tiles.",
		"prereqs": "",
		"tags": [
			"5 corruption",
			"Swift Action",
			"Range: 3",
		],
	},
	
	"defy_death": {
		"name": "Defy Death",
		"description": "When {name} dies, {they} gains [url=immune]immune[/url], [url=silence]silence[/url], [url=pass]pass[/url], and [url=fading]fading[/url] for two turns.",
		"prereqs": "",
		"tags": [
			"Passive",
			"Once per combat",
			"Duration: 2",
		],
	},
	
	"depressurize_1": {
		"name": "Depressurize I",
		"description": "[url=thermal_terrain]Depressurize[/url] a circle of two-tile radius within five tiles. The area becomes [url=difficult_terrain]difficult terrain[/url]for two turns.",
		"prereqs": "",
		"tags": [
			"3 entropy",
			"Action",
			"Duration: 2",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"depressurize_2": {
		"name": "Depressurize II",
		"description": "Depressurize applies [url=weak]weak[/url] to units in its area when cast and at the start of their turn.",
		"prereqs": "[url=depressurize_1]Depressurize I[/url]",
		"tags": [
			"3 entropy",
			"Action",
			"Duration: 2",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"depressurize_3": {
		"name": "Depressurize III",
		"description": "Depressurize lasts an additional turn.",
		"prereqs": "[url=depressurize_1]Depressurize I[/url]",
		"tags": [
			"3 entropy",
			"Action",
			"Duration: +1",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"depressurize_4": {
		"name": "Depressurize IV",
		"description": "Units leaving the [url=thermal_terrain]depressurized[/url] area have their movement interrupted and become [url=rooted]rooted[/url].",
		"prereqs": "[url=depressurize_1]Depressurize I[/url]",
		"tags": [
			"3 entropy",
			"Action",
			"Duration: 2",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"diamagnetic_relay": {
		"name": "Diamagnetic Relay",
		"description": "Relay doubles the effects of spells it copies (if applicable).",
		"prereqs": "[url=relay]Relay[/url]",
		"tags": [
			"3 flux",
			"Action",
			"Radius: 2",
			"Range: 2",
		],
	},
	
	"difficult_terrain": {
		"name": "Difficult Terrain",
		"description": "Difficult terrain costs two movement to enter rather than one.",
		"prereqs": "",
		"tags": [
			"Terrain",
		],
	},
	
	"diffuse_reflection": {
		"name": "Diffuse Reflection",
		"description": "Reflect halves the effect (rounded up) and can target up to two more valid units within the original range.",
		"prereqs": "[url=Reflect]Reflect[/url]",
		"tags": [
			"1 light",
			"Reaction",
		],
	},
	
	"discharge_1": {
		"name": "Discharge I",
		"description": "Deal half X damage (rounded down) to enemy units within three tiles.",
		"prereqs": "",
		"tags": [
			"X flux",
			"Action",
			"Damage: X/2",
			"Range: 3",
		],
	},
	
	"discharge_2": {
		"name": "Discharge II",
		"description": "Discharge deals an additional damage.",
		"prereqs": "[url=discharge_1]Discharge I[/url]",
		"tags": [
			"X flux",
			"Action",
			"Damage: (X/2)+1",
			"Range: 3",
		],
	},
	
	"disperse_1": {
		"name": "Disperse I",
		"description": "Fire light in three adjacent directions up to two tiles away, dealing one damage to units hit.",
		"prereqs": "",
		"tags": [
			"2 light",
			"Action",
			"Damage: 1",
			"Range: 2",
		],
	},
	
	"disperse_2": {
		"name": "Disperse II",
		"description": "Disperse deals an additional damage.",
		"prereqs": "[url=disperse_1]Disperse I[/url]",
		"tags": [
			"2 light",
			"Action",
			"Damage: +1",
			"Range: 2",
		],
	},
	
	"disperse_3": {
		"name": "Disperse III",
		"description": "Disperse breaks [url=guard]guard[/url] on units hit (after damage).",
		"prereqs": "[url=disperse_1]Disperse I[/url]",
		"tags": [
			"2 light",
			"Action",
			"Damage: 1",
			"Range: 2",
		],
	},
	
	"disperse_4": {
		"name": "Disperse IV",
		"description": "Disperse's range is increased by two.",
		"prereqs": "[url=disperse_1]Disperse I[/url]",
		"tags": [
			"2 light",
			"Action",
			"Damage: 1",
			"Range: +2",
		],
	},
	
	"disperse_5": {
		"name": "Disperse V",
		"description": "Disperse's center beam [url=stunned]stuns[/url].",
		"prereqs": "[url=disperse_1]Disperse I[/url]",
		"tags": [
			"2 light",
			"Action",
			"Damage: 1",
			"Range: 2",
		],
	},
	
	"displacement": {
		"name": "Displacement",
		"description": "Displacements force a unit into an unoccupied adjacent space.",
		"prereqs": "",
		"tags": [
			"Movement",
		],
	},
	
	"displace_1": {
		"name": "Displace I",
		"description": "Deal one damage and [url=displacement]push[/url] away units in a two-tile cone. {name} [url=displacement]pushes[/url] {them}self backwards.",
		"prereqs": "",
		"tags": [
			"2 work",
			"Action",
			"Damage: 1",
			"Range: 2",
		],
	},
	
	"displace_2": {
		"name": "Displace II",
		"description": "Displace's range and damage are increased by one.",
		"prereqs": "[url=displace_1]Displace I[/url]",
		"tags": [
			"2 work",
			"Action",
			"Damage: +1",
			"Range: +1",
		],
	},
	
	"displace_3": {
		"name": "Displace III",
		"description": "Displace costs a swift action.",
		"prereqs": "[url=displace_1]Displace I[/url]",
		"tags": [
			"2 work",
			"Swift Action",
			"Damage: 1",
			"Range: 2",
		],
	},
	
	"displace_4": {
		"name": "Displace IV",
		"description": "Displace consumes all [url=work]work[/url] and its damage, range, and [url=displacement]push[/url] scale with the amount of [url=work]work[/url] spent.",
		"prereqs": "[url=displace_1]Displace I[/url]",
		"tags": [
			"X work",
			"Action",
			"Damage: 0-5",
			"Range: 1-7",
		],
	},
	
	"dissipate": {
		"name": "Dissipate",
		"description": "{name} deals two damage to adjacent units and one damage to units two tiles away.",
		"prereqs": "",
		"tags": [
			"2 work",
			"Action",
			"Damage: 1-2",
			"Radius: 2",
		],
	},
	
	"dusklit_arrow": {
		"name": "Dusklit Arrow",
		"description": "{name}'s next basic attack this turn [url=rooted]roots[/url] the target.",
		"prereqs": "[url=proficiency_4]Proficiency IV[/url]",
		"tags": [
			"1 lune",
			"Swift Action",
		],
	},
	
	"dynamical_horizon": {
		"name": "Dynamical Horizon",
		"description": "Event Horizon extends its effects to {name}'s allies as well as {them}self.",
		"prereqs": "[url=event_horizon_2]Event Horizon II[/url]",
		"tags": [
			"3 lune",
			"Reaction",
			"Action",
			"Radius: 2",
		],
	},
	
	"eclipse_1": {
		"name": "Eclipse I",
		"description": "Deal two damage to units two tiles away in a crescent shape.",
		"prereqs": "",
		"tags": [
			"2 lune",
			"Action",
			"Damage: 2",
			"Range: 2",
		],
	},
	
	"eclipse_2": {
		"name": "Eclipse II",
		"description": "Eclipse costs a swift action.",
		"prereqs": "[url=eclipse_1]Eclipse I[/url]",
		"tags": [
			"2 lune",
			"Swift Action",
			"Damage: 2",
			"Range: 2",
		],
	},
	
	"eclipse_3": {
		"name": "Eclipse III",
		"description": "Eclipse restores one health to {name}.",
		"prereqs": "[url=eclipse_1]Eclipse I[/url]",
		"tags": [
			"2 lune",
			"Action",
			"Damage: 2",
			"Range: 2",
		],
	},
	
	"eclipse_4": {
		"name": "Eclipse IV",
		"description": "Eclipse [url=silence]silences[/url] units hit.",
		"prereqs": "[url=eclipse_1]Eclipse I[/url]",
		"tags": [
			"2 lune",
			"Action",
			"Damage: 2",
			"Range: 2",
		],
	},
	
	"edge_of_chaos_1": {
		"name": "Edge of Chaos I",
		"description": "Deal three damage to an adjacent target. Damage is doubled against targets who are [url=stunned]stunned[/url], [url=rooted]rooted[/url], or [url=silence]silenced[/url].",
		"prereqs": "",
		"tags": [
			"2 rapidity",
			"Swift Action",
			"Damage: 3",
			"Range: 1",
		],
	},
	
	"edge_of_chaos_2": {
		"name": "Edge of Chaos II",
		"description": "If Edge of Chaos kills a unit, refund its [url=rapidity]rapidity[/url] cost.",
		"prereqs": "[url=edge_of_chaos_1]Edge of Chaos I[/url]",
		"tags": [
			"2 rapidity",
			"Swift Action",
			"Damage: 3",
			"Range: 1",
		],
	},
	
	"electric_charge": {
		"name": "Electric Charge",
		"description": "The targeted unit's next basic attack's timing cost is [url=spurred]reduced[/url].",
		"prereqs": "[url=charge_3]Charge III[/url]",
		"tags": [
			"2 flux",
			"Swift Action",
			"Range: 4",
		],
	},
	
	"electrolysis_1": {
		"name": "Electrolysis I",
		"description": "At the end of {their} turn for three turns {name} deals one damage to enemies within a three-tile radius and restores one health.",
		"prereqs": "",
		"tags": [
			"5 flux",
			"Action",
			"Once per combat",
			"Duration: 3",
			"Radius: 3",
		],
	},
	
	"electrolysis_2": {
		"name": "Electrolysis II",
		"description": "Whenever one or more enemies are dealt damage by Electrolysis, its duration is extended by one turn.",
		"prereqs": "[url=electrolysis_1]Electrolysis I[/url]",
		"tags": [
			"5 flux",
			"Action",
			"Once per combat",
			"Duration: 3+",
			"Radius: 3",
		],
	},
	
	"electromagnetic_relay": {
		"name": "Electromagnetic Relay",
		"description": "The casting range of relay is increased by two and its radius by one.",
		"prereqs": "[url=relay]Relay[/url]",
		"tags": [
			"3 flux",
			"Action",
			"Radius: +1",
			"Range: +2",
		],
	},
	
	"endurance": {
		"name": "Endurance",
		"description": "If {name} would take lethal damage, {they} is reduced to one health instead.",
		"prereqs": "",
		"tags": [
			"Passive",
			"Once per combat",
		],
	},
	
	"enthalpy_1": {
		"name": "Enthalpy I",
		"description": "At the end of {their} turn, if {name} didn't move during {their} turn, {they} gains an [url=enthalpy]additional entropy[/url] at the beginning of {their} next turn.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"enthalpy_2": {
		"name": "Enthalpy II",
		"description": "At the end of {their} turn, for every two movement {name} has left, {they} gains an [url=enthalpy]additional entropy[/url] at the beginning of {their} next turn.",
		"prereqs": "[url=enthalpy_1]Enthalpy I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"enthalpy": {
		"name": "Enthalpy",
		"description": "A unit with enthalpy gains an [url=entropy]entropy[/url] at the beginning of their turn.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"entropy": {
		"name": "Entropy",
		"description": "Used to cast Thermodynamics spells.",
		"prereqs": "",
		"tags": [
			"Resource",
		],
	},
	
	"epicycle": {
		"name": "Epicycle",
		"description": "Whenever one or more conditions would expire on {name}, {they} may extend the duration of those conditions for one more turn. Conditions cannot be extended more than once this way.",
		"prereqs": "",
		"tags": [
			"2 lune",
			"Reaction",
		],
	},
	
	"equal_and_opposite_1": {
		"name": "Equal and Opposite I",
		"description": "{name} [url=displacement]pushes[/url] or [url=displacement]pulls[/url] {them}self and another target unit toward or away from each other in a line out to five tiles.",
		"prereqs": "",
		"tags": [
			"2 work",
			"Action",
			"Range: 5",
		],
	},
	
	"equilibrium_1": {
		"name": "Equilibirum I",
		"description": "[url=rooted]Root[/url] an adjacent unit.",
		"prereqs": "",
		"tags": [
			"2 entropy",
			"Action",
			"Range: 1",
		],
	},
	
	"equilibrium_2": {
		"name": "Equilibirum II",
		"description": "Equilibrium deals one damage.",
		"prereqs": "[url=equilibrium_1]Equilibrium I[/url]",
		"tags": [
			"2 entropy",
			"Action",
			"Damage: 1",
			"Range: 1",
		],
	},
	
	"equilibrium_3": {
		"name": "Equilibirum III",
		"description": "Equilibrium applies 1 [url=weak]weak[/url].",
		"prereqs": "[url=equilibrium_1]Equilibrium I[/url]",
		"tags": [
			"2 entropy",
			"Action",
			"Range: 1",
		],
	},
	
	"equilibrium_4": {
		"name": "Equilibirum IV",
		"description": "Equilibrium costs a swift action.",
		"prereqs": "[url=equilibrium_1]Equilibrium I[/url]",
		"tags": [
			"2 entropy",
			"Swift Action",
			"Range: 1",
		],
	},
	
	"equinox": {
		"name": "Equinox",
		"description": "Every two turns, {name} gets an additional swift action.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"escape_velocity_1": {
		"name": "Escape Velocity I",
		"description": "If a moving enemy unit who started their turn within two tiles of {name} would leave that radius, {name} may halt their movement, pulling them toward {them} and [url=rooted]rooting[/url] them for the turn.",
		"prereqs": "",
		"tags": [
			"1 lune",
			"Reaction",
		],
	},
	
	"escape_velocity_2": {
		"name": "Escape Velocity II",
		"description": "Escape Velocity deals one damage.",
		"prereqs": "[url=escape_velocity_1]Escape Velocity I[/url]",
		"tags": [
			"1 lune",
			"Reaction",
			"Damage: 1",
		],
	},
	
	"event_horizon_1": {
		"name": "Event Horizon I",
		"description": "{name} creates an event horizon around {them} in a two-tile radius. Until {their} next turn, {they} is immune to all effects whose source originates outside the radius.",
		"prereqs": "",
		"tags": [
			"3 lune",
			"Action",
			"Radius: 2",
		],
	},
	
	"event_horizon_2": {
		"name": "Event Horizon II",
		"description": "Event Horizon may be cast as a reaction whenever {name} is targeted by a spell or is standing in the effect area of a spell whose source is more than two tiles away from {them}.",
		"prereqs": "[url=event_horizon_1]Event Horizon I[/url]",
		"tags": [
			"3 lune",
			"Reaction",
			"Action",
			"Radius: 2",
		],
	},
	
	"excited": {
		"name": "Excited",
		"description": "An excited unit's next spell cast has its range increased by one.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"excite_state_1": {
		"name": "Excite State I",
		"description": "{name} or an adjacent unit's next spell this turn has its [url=excited]range increased by one[/url].",
		"prereqs": "",
		"tags": [
			"2 entropy",
			"Swift Action",
			"Range: 1",
		],
	},
	
	"excite_state_2": {
		"name": "Excite State II",
		"description": "The range of the unit's next spell is [url=very_excited]doubled[/url] instead.",
		"prereqs": "[url=excite_state_1]Excite State I[/url]",
		"tags": [
			"2 entropy",
			"Swift Action",
			"Range: 1",
		],
	},
	
	"expulse": {
		"name": "Expulse",
		"description": "Whenever {name} spends three or more [url=work]work[/url] at once, {they} gains one [url=guard]guard[/url].",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"fading": {
		"name": "Fading",
		"description": "A unit with fading disappears at the end of their turn.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"fata_morgana": {
		"name": "Fata Morgana",
		"description": "{name} creates a copy of {them}self with [url=illusion]illusion[/url] in an unoccupied adjacent space. The copy retains all of {name}'s functions, conditions, and can be controlled independently of {them}self.",
		"prereqs": "",
		"tags": [
			"5 light",
			"Action",
			"Once per combat",
			"Range: 1",
		],
	},
	
	"fateseal": {
		"name": "Fateseal",
		"description": "Silence all enemy units.",
		"prereqs": "",
		"tags": [
			"5 lune",
			"Action",
			"Once per combat",
			"Range: Global",
		],
	},
	
	"ferromagnetic_relay": {
		"name": "Ferromagnetic Relay",
		"description": "Relay costs 2 [url=flux]flux[/url] and a swift action. The relay structure also has an additional health and guard.",
		"prereqs": "[url=relay]Relay[/url]",
		"tags": [
			"2 flux",
			"Swift Action",
			"Radius: 2",
			"Range: 2",
		],
	},
	
	"fester": {
		"name": "Fester",
		"description": "Create a rotling [url=pet]pet[/url] in an adjacent unoccupied [url=rot]rotted[/url]tile.",
		"prereqs": "",
		"tags": [
			"1 corruption",
			"Action",
		],
	},
	
	"field_fluctuation": {
		"name": "Field Fluctuation",
		"description": "Whenever {name} or a unit {name} targets with a spell exits a [url=teleport]teleport[/url] or [url=dash]dash[/url], that unit becomes [url=charged]charged[/url].",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"field_of_stars_1": {
		"name": "Field of Stars I",
		"description": "Mark a circle of two-tile radius within five tiles. {name} may attack units that move through the area once as a reaction.",
		"prereqs": "",
		"tags": [
			"1 lune",
			"Action",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"field_of_stars_2": {
		"name": "Field of Stars II",
		"description": "an interesting and engaging upgrade for field of stars :3c",
		"prereqs": "[url=field_of_stars_1]Field of Stars I[/url]",
		"tags": [
			"1 lune",
			"Action",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"flux": {
		"name": "Flux",
		"description": "Used to cast Magnetism spells.",
		"prereqs": "",
		"tags": [
			"Resource",
		],
	},
	
	"footwork": {
		"name": "Footwork",
		"description": "Whenever {name} uses a basic attack, {they} gains one movement.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"fractal_dimension_1": {
		"name": "Fractal Dimension I",
		"description": "{name} [url=teleport]teleports[/url] into an unoccupied space within five tiles.",
		"prereqs": "",
		"tags": [
			"2 rapidity",
			"Swift Action",
			"Range: 5",
		],
	},
	
	"fractal_dimension_2": {
		"name": "Fractal Dimension II",
		"description": "Fractal Dimension deals one damage to units between {name} and {their} new position.",
		"prereqs": "[url=fractal_dimension_1]Fractal Dimension I[/url]",
		"tags": [
			"2 rapidity",
			"Swift Action",
			"Damage: 1",
			"Range: 5",
		],
	},
	
	"fractal_dimension_3": {
		"name": "Fractal Dimension III",
		"description": "Fractal Dimension deals damage equal to the number of units hit.",
		"prereqs": "[url=fractal_dimension_2]Fractal Dimension II[/url]",
		"tags": [
			"2 rapidity",
			"Swift Action",
			"Damage: 1/unit",
			"Range: 5",
		],
	},
	
	"fractal_dimension_4": {
		"name": "Fractal Dimension IV",
		"description": "After exiting Fractal Dimension, {name}'s next basic attack this turn [url=spurred]doesn't cost an action[/url].",
		"prereqs": "[url=fractal_dimension_1]Fractal Dimension I[/url]",
		"tags": [
			"2 rapidity",
			"Swift Action",
			"Range: 5",
		],
	},
	
	"fractal_dimension_5": {
		"name": "Fractal Dimension V",
		"description": "Fractal Dimension's [url=teleport]teleport[/url] is split into two consecutive [url=teleport]teleports[/url] of three tiles each.",
		"prereqs": "[url=fractal_dimension_1]Fractal Dimension I[/url]",
		"tags": [
			"2 rapidity",
			"Swift Action",
			"Range: 3x2",
		],
	},
	
	"freeze_1": {
		"name": "Freeze I",
		"description": "{name} creates an impassable ice obstacle in an unoccupied space within five tiles. The structure has two health.",
		"prereqs": "",
		"tags": [
			"2 entropy",
			"Action",
			"Health: 2",
			"Range: 5",
		],
	},
	
	"freeze_2": {
		"name": "Freeze II",
		"description": "The ice obstacle's health is increased by two.",
		"prereqs": "[url=freeze_1]Freeze I[/url]",
		"tags": [
			"2 entropy",
			"Action",
			"Health: +2",
			"Range: 5",
		],
	},
	
	"freeze_3": {
		"name": "Freeze III",
		"description": "Freeze costs a swift action.",
		"prereqs": "[url=freeze_1]Freeze I[/url]",
		"tags": [
			"2 entropy",
			"Swift Action",
			"Health: 2",
			"Range: 5",
		],
	},
	
	"gravity_well": {
		"name": "Gravity Well",
		"description": "{name} creates a black hole in an occupied space within five tiles. At the end of Ozias' turn for three turns the hole [url=displacement]pulls[/url] units within two tiles toward it, applying 99 [url=lethargy]lethargy[/url].",
		"prereqs": "",
		"tags": [
			"3 light",
			"3 rapidity",
			"Action",
			"Once per combat",
			"Duration: 3",
			"Range: 5",
		],
	},
	
	"guardian": {
		"name": "Guardian",
		"description": "{name} [url=teleport]teleports[/url] into an unoccupied space adjacent to an allied unit. Both units gain [url=immune]immunity[/url] until the beginning of their next turns. Guardian may be cast as a reaction if a unit would take lethal damage.",
		"prereqs": "",
		"tags": [
			"5 entropy",
			"Reaction",
			"Action",
			"Once per combat",
			"Range: Global",
		],
	},
	
	"health_1": {
		"name": "Health I",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"health_2": {
		"name": "Health II",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"health_3": {
		"name": "Health III",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"health_4": {
		"name": "Health IV",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"health_5": {
		"name": "Health V",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"health_6": {
		"name": "Health VI",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"health_7": {
		"name": "Health VII",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"health_8": {
		"name": "Health VIII",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"health_9": {
		"name": "Health IX",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"health_10": {
		"name": "Health X",
		"description": "{name}'s maximum health is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"hysteresis": {
		"name": "Hysteresis",
		"description": "Whenever an allied unit is dealt damage within three tiles of {name}, {they} may delay that damage for a turn.",
		"prereqs": "",
		"tags": [
			"1 light",
			"Reaction",
			"Range: 3",
		],
	},
	
	"ignite": {
		"name": "Ignite",
		"description": "Deal increasing damage along a line three tiles thick out to six tiles and [url=thermal_terrain]pressurize[/url] the area covered for two turns.",
		"prereqs": "",
		"tags": [
			"4 entropy",
			"Action",
			"Once per combat",
			"Damage: 1-3",
			"Range: 6",
		],
	},
	
	"illusion": {
		"name": "Illusion",
		"description": "A unit with illusion disappears when targeted with a spell or attack.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"immune": {
		"name": "Immune",
		"description": "An immune unit cannot receive damage of any kind from any source.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"induct_1": {
		"name": "Induct I",
		"description": "{name} loses two health and applies 1 [url=weak]weak[/url] and 1 [url=vulnerable]vulnerable[/url] to enemies within a three-tile radius.",
		"prereqs": "",
		"tags": [
			"2 flux",
			"Action",
			"Radius: 3",
		],
	},
	
	"induct_2": {
		"name": "Induct II",
		"description": "Induct costs a swift action.",
		"prereqs": "[url=induct_1]Induct I[/url]",
		"tags": [
			"2 flux",
			"Swift Action",
			"Radius: 3",
		],
	},
	
	"inertia": {
		"name": "Inertia",
		"description": "{name} is immune to [url=displacement]displacements[/url].",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"insulate_1": {
		"name": "Insulate I",
		"description": "If {name} has no [url=guard]guard[/url], {they} gains one [url=guard]guard[/url].",
		"prereqs": "",
		"tags": [
			"1 entropy",
			"Swift Action",
		],
	},
	
	"insulate_2": {
		"name": "Insulate II",
		"description": "Insulate can target adjacent units.",
		"prereqs": "[url=insulate_1]Insulate I[/url]",
		"tags": [
			"1 entropy",
			"Swift Action",
			"Range: 1",
		],
	},
	
	"intrinsic_coercivity": {
		"name": "Intrinsic Coercivity",
		"description": "guard has been changed, pls fix this ability",
		"prereqs": "[url=coercivity_field_2]Coercivity Field II[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"isenthalpic_process": {
		"name": "Isenthalpic Process",
		"description": "Damage dealt to {name} is reduced by one while {they}'s standing in a [url=thermal_terrain]pressurized[/url] or [url=thermal_terrain]depressurized[/url] area.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"isentropic_process": {
		"name": "Isentropic Process",
		"description": "{name} deals an additional damage when {they}'s standing in a [url=thermal_terrain]pressurized[/url] or [url=thermal_terrain]depressurized[/url] area.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"isobaric_process": {
		"name": "Isobaric Process",
		"description": "{name} is immune to the effects of [url=thermal_terrain]pressurized[/url] and [url=thermal_terrain]depressurized[/url] areas.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"isochoric_process": {
		"name": "Isobaric Process",
		"description": "If {name} starts {their} turn in a [url=thermal_terrain]pressurized[/url] or [url=thermal_terrain]depressurized[/url] area, she restores one health.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"isothermal_process": {
		"name": "Isothermal Process",
		"description": "If {name} starts {their} turn in a [url=thermal_terrain]pressurized[/url] or [url=thermal_terrain]depressurized[/url] area, {they} gains one movement.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"killing_horizon": {
		"name": "Killing Horizon",
		"description": "Event Horizon deals one damage to enemy units inside its radius when cast.",
		"prereqs": "[url=event_horizon_2]Event Horizon II[/url]",
		"tags": [
			"3 lune",
			"Reaction",
			"Action",
			"Damage: 1",
			"Radius: 2",
		],
	},
	
	"kinetic_and_potential_1": {
		"name": "Kinetic and Potential I",
		"description": "{name} gains [url=work]work[/url] at the start of {their} turn up to a maximum of three.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"kinetic_and_potential_2": {
		"name": "Kinetic and Potential II",
		"description": "{name} gains an additional [url=work]work[/url] each turn turn up to a maximum of five.",
		"prereqs": "[url=kinetic_and_potential_1]Kinetic and Potential I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"kinetic_and_potential_3": {
		"name": "Kinetic and Potential III",
		"description": "[url=work]Work[/url] may exceed its maximum. At the end of {their} turn, {name} loses one [url=work]work[/url] if it's over the maximum.",
		"prereqs": "[url=kinetic_and_potential_2]Kinetic and Potential II[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"languish": {
		"name": "Languish",
		"description": "Apply 3 [url=lethargy]lethargy[/url] to all units within two tiles.",
		"prereqs": "",
		"tags": [
			"1 corruption",
			"Action",
			"Radius: 2",
		],
	},
	
	"lethargy": {
		"name": "Lethargy",
		"description": "A lethargic unit loses one movement at the beginning of their next turn (cannot reduce below one).",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"light": {
		"name": "Light",
		"description": "Used to cast Optics spells.",
		"prereqs": "",
		"tags": [
			"Resource",
		],
	},
	
	"luminance": {
		"name": "Luminance",
		"description": "At the end of {their} turn, {name} applies 1-3 [url=lethargy]lethargy[/url] to nearby enemies in a three tile radius depending on their proximity to {name}.",
		"prereqs": "",
		"tags": [
			"Passive",
			"Radius: 3",
		],
	},
	
	"lunar_cascade_1": {
		"name": "Lunar Cascade I",
		"description": "{name} creates three orbiting moons around {them}. As a reaction, {they} may fire a moon at a unit that moves within two tiles of {them}. Moons restore two health to allies and deal two damage to enemies.",
		"prereqs": "",
		"tags": [
			"4 lune",
			"Action",
			"Radius: 2",
		],
	},
	
	"lunar_cascade_2": {
		"name": "Lunar Cascade II",
		"description": "Moons explode, affecting adjacent units with half the damage or healing (rounded down).",
		"prereqs": "[url=lunar_cascade_1]Lunar Cascade I[/url]",
		"tags": [
			"4 lune",
			"Action",
			"Radius: 2",
		],
	},
	
	"lune": {
		"name": "Lune",
		"description": "Used to cast Celestial spells.",
		"prereqs": "",
		"tags": [
			"Resource",
		],
	},
	
	"magnetic_charge": {
		"name": "Magnetic Charge",
		"description": "Charge's bonus damage is increased by one.",
		"prereqs": "[url=charge_3]Charge III[/url]",
		"tags": [
			"2 flux",
			"Swift Action",
			"Range: 4",
		],
	},
	
	"mercurial_summoning": {
		"name": "Mercurial Summoning",
		"description": "Create an elemental [url=pet]pet[/url] in an adjacent unoccupied [url=thermal_terrain]thermal[/url] space.",
		"prereqs": "",
		"tags": [
			"2 light",
			"2 entropy",
			"Action",
		],
	},
	
	"momentum": {
		"name": "Momentum",
		"description": "After moving three or more tiles at once, {name}'s next spell or basic attack deals an additional damage.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"moonbeam": {
		"name": "Moonbeam",
		"description": "idk what this spell does yet :3",
		"prereqs": "",
		"tags": [
			";-;",
		],
	},
	
	"moonfall_1": {
		"name": "Moonfall I",
		"description": "{name} creates a two-tile radius moon above the ground within five tiles. After two turns, the moon crashes, restoring two health to allies and dealing two damage to enemies.",
		"prereqs": "",
		"tags": [
			"4 lune",
			"Action",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"moonfall_2": {
		"name": "Moonfall II",
		"description": "Moonfall's radius is increased by one.",
		"prereqs": "[url=moonfall_1]Moonfall I[/url]",
		"tags": [
			"4 lune",
			"Action",
			"Radius: +1",
			"Range: 5",
		],
	},
	
	"moonlit_arrow": {
		"name": "Moonlit Arrow",
		"description": "{name}'s next basic attack this turn restores health to the target for the damage it would deal instead.",
		"prereqs": "[url=proficiency_4]Proficiency IV[/url]",
		"tags": [
			"1 lune",
			"Swift Action",
		],
	},
	
	"movement_1": {
		"name": "Movement I",
		"description": "{name}'s movement is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"movement_2": {
		"name": "Movement II",
		"description": "{name}'s movement is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"movement_3": {
		"name": "Movement III",
		"description": "{name}'s movement is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"movement_4": {
		"name": "Movement IV",
		"description": "{name}'s movement is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"movement_5": {
		"name": "Movement V",
		"description": "{name}'s movement is increased by one.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"natural_refraction": {
		"name": "Natural Refraction",
		"description": "{name} may copy the refracted spell and choose new targets for the copy.",
		"prereqs": "[url=refract]Refract[/url]",
		"tags": [
			"1 light",
			"Reaction",
			"Range: 2",
		],
	},
	
	"normal_coercivity": {
		"name": "Normal Coercivity",
		"description": "imagine changing this ability bc guard got reworked lmao",
		"prereqs": "[url=coercivity_field_2]Coercivity Field II[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"nucleosynthesis": {
		"name": "Nucleosynthesis",
		"description": "{name}'s maximum health is increased by four. {name} cannot gain [url=guard]guard[/url].",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"observation_line_1": {
		"name": "Observation Line I",
		"description": "{name} creates a three-tile line barrier in a series of unoccupied spaces within five tiles. Those spaces become [url=impassable_terrain]impassable terrain[/url] for three turns.",
		"prereqs": "",
		"tags": [
			"3 rapidity",
			"Action",
			"Range: 5",
		],
	},
	
	"observation_line_2": {
		"name": "Observation Line II",
		"description": "The observation line is only impassable to enemies. {name} and {their} allies may treat it as normal terrain.",
		"prereqs": "[url=observation_line_1]Observation Line I[/url]",
		"tags": [
			"3 rapidity",
			"Action",
			"Range: 5",
		],
	},
	
	"parallel_coherence_1": {
		"name": "Parallel Coherence I",
		"description": "{name} [url=teleport]swaps places[/url] with target unit within three tiles.",
		"prereqs": "",
		"tags": [
			"1 rapidity",
			"1 work",
			"Action",
			"Range: 3",
		],
	},
	
	"parallel_coherence_2": {
		"name": "Parallel Coherence II",
		"description": "i have no clue what this upgrade does, pls figure out later :3",
		"prereqs": "[url=parallel_coherence_1]Parallel Coherence I[/url]",
		"tags": [
			"1 rapidity",
			"1 work",
			"Action",
			"Range: 3",
		],
	},
	
	"paramagnetic_relay": {
		"name": "Paramagnetic Relay",
		"description": "On cast, relay can be set to only copy spells to allied or enemy units.",
		"prereqs": "[url=relay]Relay[/url]",
		"tags": [
			"3 flux",
			"Action",
			"Radius: 2",
			"Range: 2",
		],
	},
	
	"pass": {
		"name": "Pass",
		"description": "A unit with pass may move through enemy-occupied spaces as though they were unoccupied, but still may not end their movement there.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"perihelion": {
		"name": "Perihelion",
		"description": "At the beginning of {their} turn, {name} gets an additional swift action if two or more allies are within two tiles of {them}.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"pestilence": {
		"name": "Pestilence",
		"description": "When {name} dies, {they} [url=rot]rot[/url]s the space {they} occupied and each adjacent tile.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"phase_change": {
		"name": "Phase Change",
		"description": "Whenever damage breaks {name}'s [url=guard]guard[/url], {they} deals one damage to adjacent enemies.",
		"prereqs": "",
		"tags": [
			"Passive",
			"Damage: 1",
			"Radius: 1",
		],
	},
	
	"phototransduction": {
		"name": "Phototransduction",
		"description": "{name} and units within two tiles of {them} cannot drop below one health. When this effect expires after two turns, restore three health to all units in the radius.",
		"prereqs": "",
		"tags": [
			"5 light",
			"Action",
			"Once per combat",
			"Radius: 2",
			"Duration: 2",
		],
	},
	
	"pierce": {
		"name": "Pierce",
		"description": "A piercing attack ignores [url=guard]guard[/url].",
		"prereqs": "",
		"tags": [
			"Modifier",
		],
	},
	
	"polarize": {
		"name": "Polarize",
		"description": "{name} detonates all relays, [url=rooted]rooting[/url] nearby units and restoring three health.",
		"prereqs": "[url=relay]Relay[/url]",
		"tags": [
			"5 flux",
			"Action",
			"Once per combat",
			"Healing: 3",
		],
	},
	
	"polytropic_process": {
		"name": "Polytropic Process",
		"description": "The effects of other, non-polytropic, processes are doubled (if applicable) for {name} and other allied units in the same [url=thermal_terrain]pressurized[/url] or [url=thermal_terrain]depressurized[/url] area.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"potential_surge": {
		"name": "Potential Surge",
		"description": "{name} doubles {their} [url=work]work[/url].",
		"prereqs": "[url=kinetic_and_potential_3]Kinetic and Potential III[/url]",
		"tags": [
			"1 work",
			"Swift Action",
			"Once per combat",
		],
	},
	
	"precision_1": {
		"name": "Precision I",
		"description": "Deal X damage to an adjacent target.",
		"prereqs": "",
		"tags": [
			"X work",
			"Action",
			"Damage: X",
			"Range: 1",
		],
	},
	
	"precision_2": {
		"name": "Precision II",
		"description": "Precision [url=pierce]pierces[/url].",
		"prereqs": "[url=precision_1]Precision I[/url]",
		"tags": [
			"X work",
			"Action",
			"Damage: X",
			"Range: 1",
		],
	},
	
	"precision_3": {
		"name": "Precision III",
		"description": "Precision costs a swift action.",
		"prereqs": "[url=precision_1]Precision I[/url]",
		"tags": [
			"X work",
			"Swift Action",
			"Damage: X",
			"Range: 1",
		],
	},
	
	"pressure_reservoir": {
		"name": "Pressure Reservoir",
		"description": "{name} [url=displacement]pulls[/url] units within a three tile radius toward {them} one space, dealing two damage. {name} gains [url=immune]immunity[/url] to all damage until {their} next turn.",
		"prereqs": "",
		"tags": [
			"5 entropy",
			"Action",
			"Once per combat",
			"Damage: 2",
			"Radius: 3"
		],
	},
	
	"pressurize_1": {
		"name": "Pressurize I",
		"description": "[url=thermal_terrain]Pressurize[/url] a circle of two-tile radius within five tiles. The area becomes [url=difficult_terrain]difficult terrain[/url]for two turns.",
		"prereqs": "",
		"tags": [
			"3 entropy",
			"Action",
			"Duration: 2",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"pressurize_2": {
		"name": "Pressurize II",
		"description": "Pressurize applies [url=vulnerable]vulnerable[/url] to units in its area when cast and at the start of their turn.",
		"prereqs": "[url=pressurize_1]Pressurize I[/url]",
		"tags": [
			"3 entropy",
			"Action",
			"Duration: 2",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"pressurize_3": {
		"name": "Pressurize III",
		"description": "Pressurize's radius is increased by one.",
		"prereqs": "[url=pressurize_1]Pressurize I[/url]",
		"tags": [
			"3 entropy",
			"Action",
			"Duration: 2",
			"Radius: +1",
			"Range: 5",
		],
	},
	
	"pressurize_4": {
		"name": "Pressurize IV",
		"description": "Pressurize deals two damage to units in its area when cast.",
		"prereqs": "[url=pressurize_1]Pressurize I[/url]",
		"tags": [
			"3 entropy",
			"Action",
			"Duration: 2",
			"Radius: 2",
			"Range: 5",
		],
	},
	
	"prismatic_restoration": {
		"name": "Prismatic Restoration",
		"description": "Restoration's healing is increased by two.",
		"prereqs": "[url=restoration_1]Restoration I[/url]",
		"tags": [
			"1 light",
			"1 flux",
			"Action",
			"Healing: +2",
		],
	},
	
	"proficiency_1": {
		"name": "Proficiency I",
		"description": "{name}'s basic attacks deal an additional damage.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"proficiency_2": {
		"name": "Proficiency II",
		"description": "{name}'s basic attacks cost a swift action.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"proficiency_3": {
		"name": "Proficiency III",
		"description": "As a reaction, {name} may attack enemy units that exit {their} basic attack range.",
		"prereqs": "",
		"tags": [
			"Reaction",
		],
	},
	
	"proficiency_4": {
		"name": "Proficiency IV",
		"description": "{name}'s basic attack range is increased by two. Her basic attacks cannot target adjacent units.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"quantum_decoherence": {
		"name": "Quantum Decoherence",
		"description": "{name} creates up to six additional superpositions equidistant away from {them}self, they gain 2 [url=fading]fading[/url].",
		"prereqs": "[url=superposition]Superposition[/url]",
		"tags": [
			"4 rapidity",
			"Action",
			"Once per combat",
			"Range: 4",
		],
	},
	
	"rapidity": {
		"name": "Rapidity",
		"description": "Used to cast Relativity spells.",
		"prereqs": "",
		"tags": [
			"Resource",
		],
	},
	
	"rebirth": {
		"name": "Rebirth",
		"description": "Resurrect a dead ally into an adjacent unoccupied space.",
		"prereqs": "",
		"tags": [
			"3 lune",
			"Action",
			"Once per combat",
			"Range: 1",
		],
	},
	
	"reflect": {
		"name": "Reflect",
		"description": "When targeted with a spell, {name} may reflect the effects back to its source.",
		"prereqs": "",
		"tags": [
			"1 light",
			"Reaction",
		],
	},
	
	"refract": {
		"name": "Refract",
		"description": "When targeted with a spell, {name} may redirect the effects to another valid target within two tiles.",
		"prereqs": "",
		"tags": [
			"1 light",
			"Reaction",
		],
	},
	
	"relay": {
		"name": "Relay",
		"description": "{name} creates a magnetic relay in an unoccupied space within two tiles. Whenever a spell targets the relay, the spell is copied and cast on every unit within two tiles of the relay and the relay is destroyed.",
		"prereqs": "",
		"tags": [
			"3 flux",
			"Action",
			"Radius: 2",
			"Range: 2",
		],
	},
	
	"remanence_coercivity": {
		"name": "Remanence Coercivity",
		"description": "guard rework fix pls",
		"prereqs": "[url=coercivity_field_2]Coercivity Field II[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"restoration_1": {
		"name": "Restoration I",
		"description": "Restore two health to target adjacent unit.",
		"prereqs": "",
		"tags": [
			"2 flux",
			"Action",
			"Healing: 2",
			"Range: 1",
		],
	},
	
	"restoration_2": {
		"name": "Restoration II",
		"description": "Restoration's range is increased by three.",
		"prereqs": "[url=restoration_1]Restoration I[/url]",
		"tags": [
			"2 flux",
			"Action",
			"Healing: 2",
			"Range: +3",
		],
	},
	
	"restoration_3": {
		"name": "Restoration III",
		"description": "Restoration costs 1 [url=flux]flux[/url].",
		"prereqs": "[url=restoration_1]Restoration I[/url]",
		"tags": [
			"1 flux",
			"Action",
			"Healing: 2",
			"Range: 1",
		],
	},
	
	"restoration_4": {
		"name": "Restoration IV",
		"description": "idk",
		"prereqs": "[url=restoration_1]Restoration I[/url]",
		"tags": [
			"2 flux",
			"Action",
			"Healing: 2",
			"Range: 1",
		],
	},
	
	"rooted": {
		"name": "Rooted",
		"description": "A rooted unit cannot [url=dash]dash[/url] or take the move action.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"rot": {
		"name": "Rot",
		"description": "Rotted tiles are [url=difficult_terrain]difficult terrain[/url] and [url=silence]silence[/url] units that start their turn on them.",
		"prereqs": "",
		"tags": [
			"Terrain",
		],
	},
	
	"satellite_1": {
		"name": "Satellite I",
		"description": "Whenever an enemy within basic attack range of {name} casts a spell, {name} may attack that enemy.",
		"prereqs": "",
		"tags": [
			"1 lune",
			"Reaction",
		],
	},
	
	"satellite_2": {
		"name": "Satellite II",
		"description": "As part of {their} reaction attack, {name} may move up to one tile toward an enemy unit.",
		"prereqs": "[url=satellite_1]Satellite I[/url]",
		"tags": [
			"1 lune",
			"Reaction",
		],
	},
	
	"semi_state": {
		"name": "Semi State",
		"description": "Conditions applied to {name} are doubled\n(if applicable).",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"shimmer": {
		"name": "Shimmer",
		"description": "{name} is immune to [url=rooted]root[/url] and [url=lethargy]lethargy[/url] and ignores [url=difficult_terrain]difficult terrain[/url].",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"shove_1": {
		"name": "Shove I",
		"description": "[url=displacement]Push[/url] an adjacent unit away from {name}.",
		"prereqs": "",
		"tags": [
			"Swift Action",
			"Range: 1",
		],
	},
	
	"shove_2": {
		"name": "Shove II",
		"description": "Shove may be used as a reaction whenever a unit ends their movement adjacent to {name}.",
		"prereqs": "[url=shove_1]Shove I[/url]",
		"tags": [
			"Reaction",
			"Swift Action",
			"Range: 1",
		],
	},
	
	"shroud_1": {
		"name": "Shroud I",
		"description": "{name} or an adjacent unit becomes [url=shroud]shrouded[/url] until their next turn.",
		"prereqs": "",
		"tags": [
			"3 lune",
			"Action",
			"Range: 1",
		],
	},
	
	"shroud_2": {
		"name": "Shroud II",
		"description": "Shroud's range is increased by two.",
		"prereqs": "[url=shroud_1]Shroud I[/url]",
		"tags": [
			"3 lune",
			"Action",
			"Range: +2",
		],
	},
	
	"shroud_3": {
		"name": "Shroud III",
		"description": "Shroud's duration is increased by one turn.",
		"prereqs": "[url=shroud_1]Shroud I[/url]",
		"tags": [
			"3 lune",
			"Action",
			"Duration: +1",
			"Range: 1",
		],
	},
	
	"shroud": {
		"name": "Shroud",
		"description": "A shrouded unit cannot be the target of spells.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"silence": {
		"name": "Silence",
		"description": "A silenced unit cannot talk or cast spells.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"siphon": {
		"name": "Siphon",
		"description": "Deal three damage to target unit within two tiles and restore health equal to the damage dealt.",
		"prereqs": "",
		"tags": [
			"1 corruption",
			"Action",
			"Damage: 3",
			"Range: 2",
		],
	},
	
	"solar": {
		"name": "Solar",
		"description": "A solar unit's basic attacks [url=stunned]stun[/url].",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"solar_winds": {
		"name": "Solar Winds",
		"description": "Until the end of {their} next turn, {name}'s [url=solar]basic attacks stun[/url].",
		"prereqs": "",
		"tags": [
			"2 lune",
			"2 light",
			"Action",
			"Once per combat",
		],
	},
	
	"solstice": {
		"name": "Solstice",
		"description": "Every four turns, {name} gets an additional action and swift action.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"space_and_time_1": {
		"name": "Space and Time I",
		"description": "{name} gains [url=rapidity]rapidity[/url] at the start of {their} turn up to a maximum of three.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"space_and_time_2": {
		"name": "Space and Time II",
		"description": "{name} gains an additional [url=rapidity]rapidity[/url] each turn up to a maximum of five.",
		"prereqs": "[url=space_and_time_1]Space and Time I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"space_and_time_3": {
		"name": "Space and Time III",
		"description": "If {name} has a positive amount of [url=rapidity]rapidity[/url], {they} may spend more than {they} has, going into \"debt\" at the cost of an additional [url=rapidity]rapidity.",
		"prereqs": "[url=space_and_time_1]Space and Time I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"spectrum_shatter": {
		"name": "Spectrum Shatter",
		"description": "Whenever another unit is dealt damage within two tiles of {name}, {they} may double the damage dealt.",
		"prereqs": "",
		"tags": [
			"2 light",
			"Reaction",
		],
	},
	
	"specular_reflection": {
		"name": "Specular Reflection",
		"description": "Reflect doubles the effect back at its source (if applicable).",
		"prereqs": "[url=reflect]Reflect[/url]",
		"tags": [
			"1 light",
			"Reaction",
		],
	},
	
	"sprint_1": {
		"name": "Sprint I",
		"description": "{name} gains two additional movement this turn.",
		"prereqs": "",
		"tags": [
			"Action",
		],
	},
	
	"sprint_2": {
		"name": "Sprint II",
		"description": "Sprint costs a swift action.",
		"prereqs": "[url=sprint_1]Sprint I[/url]",
		"tags": [
			"Swift Action",
		],
	},
	
	"sprint_3": {
		"name": "Sprint III",
		"description": "Sprint's movement bonus is increased by one.",
		"prereqs": "[url=sprint_1]Sprint I[/url]",
		"tags": [
			"Action",
		],
	},
	
	"spurred": {
		"name": "Spurred",
		"description": "A spurred unit's next basic attack has a lower timing cost.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"starlit_arrow": {
		"name": "Starlit Arrow",
		"description": "{name}'s next basic attack this turn [url=guard]guards[/url] the target for half the damage it would deal instead (rounded down).",
		"prereqs": "[url=proficiency_4]Proficiency IV[/url]",
		"tags": [
			"1 lune",
			"Swift Action",
		],
	},
	
	"states_and_processes_1": {
		"name": "States and Processes I",
		"description": "{name} gains [url=entropy]entropy[/url] at the start of {their} turn up to a maximum of three.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"states_and_processes_2": {
		"name": "States and Processes II",
		"description": "{name} gains an additional [url=entropy]entropy[/url] each turn up to a maximum of five.",
		"prereqs": "[url=states_and_processes_1]States and Processes I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"states_and_processes_3": {
		"name": "States and Processes III",
		"description": "If {name} starts {their} turn with no [url=entropy]entropy[/url], {they} gains an additional [url=entropy]entropy[/url].",
		"prereqs": "[url=states_and_processes_2]States and Processes II[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"states_and_processes_4": {
		"name": "States and Processes IV",
		"description": "{name} starts combat with five [url=entropy]entropy[/url].",
		"prereqs": "[url=states_and_processes_2]States and Processes II[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"steady_state_process": {
		"name": "Steady State Process",
		"description": "The effects of [url=thermal_terrain]pressurized[/url]\nand [url=thermal_terrain]depressurized[/url] areas are halved on allies in the same area as {name} (rounded down).",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"stunned": {
		"name": "Stunned",
		"description": "A stunned unit cannot move or take any action until the end of their turn.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"sturdy_1": {
		"name": "Sturdy I",
		"description": "{name} starts combat with one [url=guard]guard[/url].",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"sturdy_2": {
		"name": "Sturdy II",
		"description": "{name} starts combat with an additional [url=guard]guard[/url].",
		"prereqs": "[url=sturdy_1]Sturdy I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"sturdy_3": {
		"name": "Sturdy III",
		"description": "{name} starts combat with an additional [url=guard]guard[/url].",
		"prereqs": "[url=sturdy_1]Sturdy I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"sturdy_4": {
		"name": "Sturdy IV",
		"description": "{name} starts combat with an additional [url=guard]guard[/url].",
		"prereqs": "[url=sturdy_1]Sturdy I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"sublimation": {
		"name": "Sublimation",
		"description": "When {name} or a unit within two tiles of {them} casts a spell, {name} may copy that spell and choose new targets for the copy.",
		"prereqs": "",
		"tags": [
			"2 entropy",
			"Reaction",
		],
	},
	
	"sunlit_arrow": {
		"name": "Sunlit Arrow",
		"description": "{name}'s next basic attack this turn grants the target additional movement for the damage it would deal instead.",
		"prereqs": "[url=proficiency_4]Proficiency IV[/url]",
		"tags": [
			"1 lune",
			"Swift Action",
		],
	},
	
	"supernova": {
		"name": "Supernova",
		"description": "{name} dies, dealing damage to nearby units equal to {their} current health / 2^(n-1) (rounded down) where n is the distance from {name}.",
		"prereqs": "",
		"tags": [
			"5 lune",
			"Action",
			"Once per combat",
		],
	},
	
	"superposition": {
		"name": "Superposition",
		"description": "{name} exists in a position between two states. When {they} casts a spell or makes an attack, {they} may have it originate from either position.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"teleport": {
		"name": "Teleport",
		"description": "Teleports move a unit from one tile into another unoccupied tile regardless of terrain costs or obstacles between them.",
		"prereqs": "",
		"tags": [
			"Movement",
		],
	},
	
	"temporal_dilation": {
		"name": "Temporal Dilation",
		"description": "{name} takes two additional turns after this one.",
		"prereqs": "",
		"tags": [
			"5 rapidity",
			"Action",
			"Once per combat",
		],
	},
	
	"thermal_terrain": {
		"name": "Thermal Terrain",
		"description": "Pressurized and depressurized areas create different effects depending on their caster's skills. They are mutually exclusive and overlapping different pressure areas cancels them out.",
		"prereqs": "",
		"tags": [
			"Terrain",
		],
	},
	
	"torque": {
		"name": "Torque",
		"description": "{name} [url=displacement]pushes[/url] units counter-clockwise around {them} in a 2-3 tile ring up to three tiles, dealing three damage.",
		"prereqs": "",
		"tags": [
			"5 work",
			"Action",
			"Damage: 3",
			"Radius: 2-3",
		],
	},
	
	"transivity_1": {
		"name": "Transivity I",
		"description": "Restore two health to target unit within two tiles. {name} loses two health.",
		"prereqs": "",
		"tags": [
			"1 flux",
			"Swift Action",
			"Healing: 2",
			"Range: 3",
		],
	},
	
	"transivity_2": {
		"name": "Transivity II",
		"description": "Transivity restores an additional health.",
		"prereqs": "[url=transivity_1]Transivity I[/url]",
		"tags": [
			"1 flux",
			"Swift Action",
			"Healing: +1",
			"Range: 3",
		],
	},
	
	"triumph_1": {
		"name": "Triumph I",
		"description": "Whenever {name} kills a unit with a basic attack, {they} restores one health.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"triumph_2": {
		"name": "Triumph II",
		"description": "Whenever {name} kills a unit with a basic attack, {they} gain three movement.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"twilit_arrow": {
		"name": "Twilit Arrow",
		"description": "{name}'s next basic attack this turn [url=silence]silences[/url] the target.",
		"prereqs": "[url=proficiency_4]Proficiency IV[/url]",
		"tags": [
			"1 lune",
			"Swift Action",
		],
	},
	
	"uncertainty_1": {
		"name": "Uncertainty I",
		"description": "{name} swaps positions with {their} superposition. If another unit was occupying that space, they are dealt one damage and [url=displacement]pushed[/url] into the nearest unoccupied space.",
		"prereqs": "[url=superposition]Superposition[/url]",
		"tags": [
			"2 rapidity",
			"Swift Action",
		],
	},
	
	"uncertainty_2": {
		"name": "Uncertainty II",
		"description": "Uncertainty may be triggered and cast as a reaction when {name} is targeted by a spell or when standing in the area of effect of a spell.",
		"prereqs": "[url=uncertainty_1]Uncertainty I[/url]",
		"tags": [
			"2 rapidity",
			"Reaction",
			"Swift Action",
		],
	},
	
	"uncertainty_3": {
		"name": "Uncertainty III",
		"description": "Uncertainty may be cast as a reaction after {name} kills a unit.",
		"prereqs": "[url=uncertainty_1]Uncertainty I[/url]",
		"tags": [
			"2 rapidity",
			"Reaction",
			"Swift Action",
		],
	},
	
	"velocity_1": {
		"name": "Velocity I",
		"description": "{name} [url=dash]dashes[/url] up to four tiles in a line.",
		"prereqs": "",
		"tags": [
			"2 work",
			"Swift Action",
			"Range: 4",
		],
	},
	
	"velocity_2": {
		"name": "Velocity II",
		"description": "{name} gains 1 [url=pass]pass[/url] (applied before the dash).",
		"prereqs": "[url=velocity_1]Velocity I[/url]",
		"tags": [
			"2 work",
			"Swift Action",
			"Range: 4",
		],
	},
	
	"very_excited": {
		"name": "Very Excited",
		"description": "A very excited unit's next spell has its range doubled.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"vigilance_1": {
		"name": "Vigilance I",
		"description": "{name} has an additional reaction each turn.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"vigilance_2": {
		"name": "Vigilance II",
		"description": "{name} has an additional reaction each turn.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"volumetric_expansion": {
		"name": "Volumetric Expansion",
		"description": "Whenever {name} enters or leaves a [url=thermal_terrain]pressurized[/url] or [url=thermal_terrain]depressurized[/url] area, {they} may apply the effects of {their} processes to other allied units in the area.",
		"prereqs": "",
		"tags": [
			"1 entropy",
			"Reaction",
		],
	},
	
	"vulnerable": {
		"name": "Vulnerable",
		"description": "A vulnerable unit receives an additional damage from all sources.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"warp": {
		"name": "Warp",
		"description": "[url=teleport]Teleport[/url] target unit into an unoccupied space within seven tiles.",
		"prereqs": "",
		"tags": [
			"2 rapidity",
			"2 work",
			"Action",
			"Once per combat",
			"Range: 7",
		],
	},
	
	"waves_and_particles_1": {
		"name": "Waves and Particles I",
		"description": "{name} gains [url=light]light[/url] at the start of {their} turn up to a maximum of three.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"waves_and_particles_2": {
		"name": "Waves and Particles II",
		"description": "{name} gains an additional [url=light]light[/url] each turn up to a maximum of five.",
		"prereqs": "[url=waves_and_particles_1]Waves and Particles I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"wave_function_collapse_1": {
		"name": "Wave Function Collapse I",
		"description": "{name}'s superposition collapses in on {them}, [url=stunned]stunning[/url] all units caught between the two positions.",
		"prereqs": "[url=superposition]Superposition[/url]",
		"tags": [
			"3 rapidity",
			"Action",
		],
	},
	
	"wave_function_collapse_2": {
		"name": "Wave Function Collapse II",
		"description": "Wave Function Collapse may be triggered and cast as a reaction whenever an enemy unit passes between {name} and {their} superposition.",
		"prereqs": "[url=wave_function_collapse_1]Wave Function Collapse I[/url]",
		"tags": [
			"3 rapidity",
			"Reaction",
			"Action",
		],
	},
	
	"wax_and_wane_1": {
		"name": "Wax and Wane I",
		"description": "{name} gains [url=lune]lune[/url] at the start of {their} turn up to a maximum of three.",
		"prereqs": "",
		"tags": [
			"Passive",
		],
	},
	
	"wax_and_wane_2": {
		"name": "Wax and Wane II",
		"description": "{name} gains an additional [url=lune]lune[/url] each turn up to a maximum of five.",
		"prereqs": "[url=wax_and_wane_1]Wax and Wane I[/url]",
		"tags": [
			"Passive",
		],
	},
	
	"weak": {
		"name": "Weak",
		"description": "A weakened unit deals one less damage with attacks and spells.",
		"prereqs": "",
		"tags": [
			"Condition",
		],
	},
	
	"work": {
		"name": "Work",
		"description": "Used to cast Kinematics spells.",
		"prereqs": "",
		"tags": [
			"Resource",
		],
	},
}

const ITEMS : Dictionary = {
	
}
