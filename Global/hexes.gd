extends Node2D

var finished : bool = false

func _ready():
	for hex in get_children():
		hex.modulate.a = 0.0
		
func change_color(color : Color) -> void:
	for hex in get_children():
		hex.modulate = color
		hex.modulate.a = 0.0
	
func animate_hexes_in(tween : Tween) -> void:
	finished = false
	tween.remove_all()
	
	for hex in get_children():
		tween.interpolate_property(hex, "modulate:a", 0.0, 1.0, 0.2, Tween.TRANS_LINEAR)
		tween.start()
		yield(get_tree().create_timer(0.01), "timeout")
		
func animate_hexes_out(tween : Tween) -> void:
	finished = true
	tween.remove_all()
	
	for hex in get_children():
		tween.interpolate_property(hex, "modulate:a", 1.0, 0.0, 0.2, Tween.TRANS_LINEAR)
		tween.start()
		yield(get_tree().create_timer(0.01), "timeout")
