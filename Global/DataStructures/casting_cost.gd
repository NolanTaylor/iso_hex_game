extends Node2D

var light : int = 0
var work : int = 0
var flux : int = 0
var entropy : int = 0
var rapidity : int = 0
var lune : int = 0
var corruption : int = 0
var timing : int = 0
var ult : bool = false

func _ready():
	pass
	
func get_resource(idx : int) -> int:
	match idx:
		-1:
			return timing
		0:
			return light
		1:
			return work
		2:
			return flux
		3:
			return entropy
		4:
			return rapidity
		5:
			return lune
		6:
			return corruption
		_:
			return -1
