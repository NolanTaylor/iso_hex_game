extends Node2D

var area : PoolVector3Array = PoolVector3Array([])

signal area_seized(salad)

func _ready():
	init_area()
	
	var grid = Globals.get_hex_grid()
	grid.connect("unit_moved", self, "check_seize")
	
func init_area() -> PoolVector3Array:
	area = PoolVector3Array([])
	
	for child in get_children():
		var coord : Vector3 = Vector3.ZERO
		
		coord.x = child.global_position.x / 23
		coord.y = ((child.global_position.y - 16) / 16) - (coord.x / 2)
		coord.z = -(coord.x + coord.y)
		
		area.append(coord)
		
	return area
	
func check_seize(unit, location : Vector3) -> void:
	if location in area and unit.is_in_group("PlayerUnits"):
		emit_signal("area_seized", unit)
