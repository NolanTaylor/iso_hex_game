extends Node2D

var area : PoolVector3Array = PoolVector3Array([])

func _ready():
	init_area()
	hide()
	
func init_area() -> PoolVector3Array:
	area = PoolVector3Array([])
	
	for child in get_children():
		var coord : Vector3 = Vector3.ZERO
		
		coord.x = child.global_position.x / 23
		coord.y = ((child.global_position.y - 16) / 16) - (coord.x / 2)
		coord.z = -(coord.x + coord.y)
		
		area.append(coord)
		
	return area
