extends "res://Global/Levels/level.gd"

var moving_enemy_index : int = 0
var attack_flag : bool = false

func _ready():
	if get_parent().name == "root":
		$hex_grid.tutorial_act = 0
		tween_new_paper("select_grace")
	
func _process(delta):
	pass
	
func tween_new_paper(paper : String):
	if defeat:
		return
	for child in $selects.get_children():
		if child.visible:
			$tween.interpolate_property(child, "position:y", 0, -48, 1.4, Tween.TRANS_ELASTIC)
	if paper == "hide_all":
		$tween.start()
		return
	var select_node = $selects.get_node(paper)
	if paper == "select_adjacent":
		$arrow.show()
	else:
		$arrow.hide()
	select_node.show()
	$tween.interpolate_property(select_node, "position:y", -48, 0, 1.4, Tween.TRANS_ELASTIC)
	$tween.start()
	
func end_turn() -> void:
	if $hex_grid.tutorial_act == 4:
		tween_new_paper("hide_all")
		.end_turn()
	
func defeat() -> void:
	hide_cursor()
	$objects/enemy_units/phrank.hide()
	$stabbing.frame = 0
	$stabbing.show()
	$stabbing.play("stabbing")
	yield(get_tree().create_timer(2.4), "timeout")
	fade_out(2.0)
	yield(get_tree().create_timer(4.8), "timeout")
	Config.characters["grace"]["skill_points"] = 1
	get_tree().change_scene("res://Global/Menus/Dialog/Tutorial/dialog_tutorial_3.tscn")
	
func _on_stabbing_animation_finished():
	$stabbing.frame = 19
