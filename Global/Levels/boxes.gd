extends Node2D

const DOWN_TIME : float = 9.6
const UP_TIME : float = 12.8
const DROP_TIME : float = 0.6

func _ready():
	move_box_down($box_1)
	move_box_up($box_4)
	yield(get_tree().create_timer(3.2), "timeout")
	move_box_down($box_2)
	move_box_up($box_5)
	yield(get_tree().create_timer(3.2), "timeout")
	move_box_down($box_3)
	move_box_up($box_6)
	yield(get_tree().create_timer(3.2), "timeout")
	move_box_up($box_7)
	
func move_box_down(box) -> void:
	$tween.interpolate_property(box, "position", Vector2(30, -64), Vector2(321, 37), DOWN_TIME, \
		Tween.TRANS_LINEAR)
	$tween.interpolate_property(box.get_node("box_shadow"), "scale", Vector2(0.0, 0.0), \
		Vector2(1.0, 1.0), DROP_TIME, Tween.TRANS_LINEAR)
	$tween.start()
	
func move_box_up(box) -> void:
	$tween.interpolate_property(box, "position", Vector2(10, 22), Vector2(321, -88), UP_TIME, \
		Tween.TRANS_LINEAR)
	$tween.interpolate_property(box.get_node("box_shadow"), "scale", Vector2(0.0, 0.0), \
		Vector2(1.0, 1.0), DROP_TIME, Tween.TRANS_LINEAR)
	$tween.start()
	
func _on_tween_tween_completed(object, key):
	if object.name == "box_shadow":
		return
		
	$tween.remove(object)
	
	match object.name:
		"box_1", "box_2", "box_3":
			move_box_down(object)
		"box_4", "box_5", "box_6", "box_7":
			move_box_up(object)
	
