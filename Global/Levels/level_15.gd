extends "res://Global/Levels/level.gd"

var moving_enemy_index : int = 0
var attack_flag : bool = false

func _ready():
	pass
	
func _process(delta):
	pass
	
func init_dialog() -> void:
	dialog = {
		
	}
	
func defeat() -> void:
	pass
	
func victory() -> void:
	self.connect("fade_complete", self, "next_scene")
	fade_out()
	
func next_scene() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_15/dialog_level_15_2.tscn")
	
