extends "res://Global/Levels/level.gd"

var moving_enemy_index : int = 0
var attack_flag : bool = false

func _ready():
	pass
	
func _process(delta):
	pass
	
func init_dialog() -> void:
	dialog = {
		["foo", "acacius"]: [
			["transition", "fade_in"],
			["foo", BOTTOM_LEFT, true, "rhee heehe hee."],
			["transition", "fade_out"],
		],
		
		["grace", "foo"]: [
			["transition", "fade_in"],
			["grace", BOTTOM_LEFT, true, "Stay close to me Foo."],
			["foo", BOTTOM_RIGHT, false, "Okay."],
			["grace_lookaway", BOTTOM_LEFT, true, "If you wander too far I won't be able\nto protect you."],
			["transition", "fade_out"],
		]
	}
	
func check_victory() -> bool:
	if $objects/neutral_units.get_child_count() == 0:
		return true
	else:
		return false
		
func defeat() -> void:
	pass
	
func victory() -> void:
	self.connect("fade_complete", self, "next_scene")
	fade_out()
	
func next_scene() -> void:
	Config.characters["grace"]["skill_points"] += 1
	Config.characters["cherry"]["skill_points"] += 1
	Config.characters["acacius"]["skill_points"] += 1
	Config.characters["illiana"]["skill_points"] += 1
	get_tree().change_scene("res://Global/Menus/Dialog/Level_2/dialog_level_2_3.tscn")
