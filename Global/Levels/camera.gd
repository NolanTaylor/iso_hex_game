extends Camera2D

const CAM_SIZE : Vector2 = Vector2(320, 200)
const CAM_VELOCITY : int = 80

onready var a : Vector2 = get_node("../camera_constraints/point_a").position
onready var b : Vector2 = get_node("../camera_constraints/point_b").position

var mouse_start_pos : Vector2 = Vector2.ZERO
var screen_start_position : Vector2 = Vector2.ZERO
var moving : Vector2 = Vector2.ZERO
var popup_parent = null
var following_unit = null

var lock : bool = false
var centering : bool = false
var dragging : bool = false

signal finished_centering

func _process(delta):
	if moving != Vector2.ZERO:
		position.x += moving.x * CAM_VELOCITY * delta
		position.y += moving.y * CAM_VELOCITY * delta
		
		if position.x < a.x:
			position.x = a.x
		if position.y < a.y:
			position.y = a.y
		if position.x + CAM_SIZE.x > b.x:
			position.x = b.x - CAM_SIZE.x
		if position.y + CAM_SIZE.y > b.y:
			position.y = b.y - CAM_SIZE.y
			
	if Globals.is_valid(following_unit) and lock:
		position = following_unit.global_position - CAM_SIZE / 2
		
		if position.x < a.x:
			position.x = a.x
		if position.y < a.y:
			position.y = a.y
		if position.x + CAM_SIZE.x > b.x:
			position.x = b.x - CAM_SIZE.x
		if position.y + CAM_SIZE.y > b.y:
			position.y = b.y - CAM_SIZE.y
			
func _input(event):
	if lock:
		return
		
	if event.is_action_released("move_camera_left"):
		moving.x = 0
		if Input.is_action_pressed("move_camera_right"):
			moving.x = 1
	if event.is_action_released("move_camera_right"):
		moving.x = 0
		if Input.is_action_pressed("move_camera_left"):
			moving.x = -1
	if event.is_action_released("move_camera_up"):
		moving.y = 0
		if Input.is_action_pressed("move_camera_down"):
			moving.y = 1
	if event.is_action_released("move_camera_down"):
		moving.y = 0
		if Input.is_action_pressed("move_camera_up"):
			moving.y = -1
		
	if event.is_action_pressed("move_camera_left"):
		moving.x = -1
	elif event.is_action_pressed("move_camera_right"):
		moving.x = 1
		
	if event.is_action_pressed("move_camera_up"):
		moving.y = -1
	elif event.is_action_pressed("move_camera_down"):
		moving.y = 1
		
	if event.is_action("drag"):
		if event.is_pressed():
			mouse_start_pos = event.position
			screen_start_position = position
			dragging = true
		else:
			dragging = false
	elif event is InputEventMouseMotion and dragging:
		position = zoom * (mouse_start_pos - event.position) + screen_start_position
		
		if position.x < a.x:
			position.x = a.x
			mouse_start_pos.x = event.position.x
			screen_start_position.x = position.x
		if position.y < a.y:
			position.y = a.y
			mouse_start_pos.y = event.position.y
			screen_start_position.y = position.y
		if position.x + CAM_SIZE.x > b.x:
			position.x = b.x - CAM_SIZE.x
			mouse_start_pos.x = event.position.x
			screen_start_position.x = position.x
		if position.y + CAM_SIZE.y > b.y:
			position.y = b.y - CAM_SIZE.y
			mouse_start_pos.y = event.position.y
			screen_start_position.y = position.y
			
func lock() -> void:
	lock = true
	
func unlock() -> void:
	lock = false
	following_unit = null
	moving = Vector2.ZERO
	
func center_unit(unit) -> void:
	var cam_target_pos : Vector2 = unit.global_position - CAM_SIZE / 2
	
	if cam_target_pos.x < a.x:
		cam_target_pos.x = a.x
	if cam_target_pos.y < a.y:
		cam_target_pos.y = a.y
	if cam_target_pos.x + CAM_SIZE.x > b.x:
		cam_target_pos.x = b.x - CAM_SIZE.x
	if cam_target_pos.y + CAM_SIZE.y > b.y:
		cam_target_pos.y = b.y - CAM_SIZE.y
		
	centering = true
	following_unit = null
	
	$camera_tween.interpolate_property(self, "position", global_position, cam_target_pos, 0.4, \
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$camera_tween.connect("tween_completed", self, "centered", [ ], CONNECT_ONESHOT)
	$camera_tween.start()
	
func centered(object, key) -> void:
	centering = false
	emit_signal("finished_centering")
	
func stalk_unit(unit) -> void:
	center_unit(unit)
	yield(self, "finished_centering")
	following_unit = unit
	
func add_popup(popup : Node, right : bool = false) -> void:
	return_popup()
	
	var start : Vector2 = Vector2(4, 204)
	var end : Vector2 = Vector2(4, 148)
	var start_right : Vector2 = Vector2(224, 204)
	var end_right : Vector2 = Vector2(224, 148)
	
	popup.global_position = start
	popup.get_node("tab").position = Vector2.ZERO
	popup.hide()
	
	if right:
		$tween.interpolate_property(popup, "position", start_right, end_right, 0.8, \
			Tween.TRANS_ELASTIC)
	else:
		$tween.interpolate_property(popup, "position", start, end, 0.8, Tween.TRANS_ELASTIC)
	$tween.interpolate_property(popup.get_node("tab"), "position:y", 15, -13, 1.2, \
		Tween.TRANS_ELASTIC)
		
	$tween.start()
	popup.show()
	
	popup_parent = popup.get_parent()
	popup_parent.remove_child(popup)
	popup_parent.popup = popup
	$popup.add_child(popup)
	
func return_popup() -> void:
	var popup = $popup.get_child(0)
	if Globals.is_valid(popup):
		popup.hide()
		$popup.remove_child(popup)
		popup_parent.add_child(popup)
		popup_parent.popup = popup
	
func set_popup_text(text : String) -> void:
	if $popup.get_child_count() == 0:
		return
		
	$popup.get_child(0).set_popup_text(text)
	
func preview_cost(cost) -> void:
	if $popup.get_child_count() == 0:
		return
		
	$popup.get_child(0).preview_cost(cost)
	
func blink_tex(container, resource : int, fade_in : bool) -> void:
	if $popup.get_child_count() == 0:
		return
		
	$popup.get_child(0).blink_tex(container, resource, fade_in)
