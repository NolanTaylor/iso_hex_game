extends Node2D

const TOP_LEFT : Vector2 = Vector2(16, 4)
const TOP_RIGHT : Vector2 = Vector2(240, 4)
const BOTTOM_LEFT : Vector2 = Vector2(16, 132)
const BOTTOM_RIGHT : Vector2 = Vector2(240, 132)

var cutscene_mode : bool = false

var player_turn : bool = true
var victory : bool = false
var defeat : bool = false
var stall_for_event : bool = false
var turn_count : int = 0
var act : int = 0

var current_enemy = null

var dialog : Dictionary = {
	
}

signal fade_complete
signal confirm_spell
signal event_finished
signal push_or_pull(decision)

func _ready():
	$camera.make_current()
	$camera/global_button.connect("gui_input", $hex_grid, "mouse_input")
	
	Globals.dead_units.resize(0)
	Globals.set_camera($camera)
	init_dialog()
	
	var units_to_replace = get_tree().get_nodes_in_group("PlaceholderUnits")
	var picked_units : PoolStringArray = Globals.picked_units
	
	while not picked_units.empty():
		if units_to_replace.empty():
			break
		var next_unit : String = picked_units[0]
		var placeholder_unit = units_to_replace[0]
		var new_unit = load("res://Units/Characters/{0}.tscn".format([next_unit]))
		
		if Globals.is_valid(new_unit):
			var child_to_add = new_unit.instance()
			child_to_add.position = placeholder_unit.position
			child_to_add.add_to_group("PlayerUnits")
			child_to_add.add_to_group("AllUnits")
			$objects/player_units.add_child(child_to_add)
			
		picked_units.remove(0)
		units_to_replace.remove(0)
		placeholder_unit.queue_free()
		
	for unit in units_to_replace:
		$objects/player_units.remove_child(unit)
		unit.queue_free()
	
	var units_arr : Array
	
	for unit in $objects/player_units.get_children():
		unit.add_to_group("PlayerUnits")
		unit.add_to_group("AllUnits")
		units_arr.push_back(unit)
	for unit in $objects/enemy_units.get_children():
		unit.add_to_group("EnemyUnits")
		unit.add_to_group("AllUnits")
		units_arr.push_back(unit)
	for unit in $objects/neutral_units.get_children():
		unit.add_to_group("NeutralUnits")
		unit.add_to_group("AllUnits")
		units_arr.push_back(unit)
	
	for child in units_arr:
		child.find_grid()
		if $hex_grid.grid.has(child.coords):
			$hex_grid.grid[child.coords].set_occupying_unit(child)
		if child.is_in_group("PlayerUnits") and child.has_node("action_buttons"):
			if Globals.is_valid(child.superposition):
				child.superposition.coords = child.coords
			child.upkeep()
			if child.has_node("character_menu"):
				child.get_node("character_menu").connect("exit_menu", $hex_grid, "exit_char_menu")
			for grandchild in child.get_node("action_buttons").get_children():
				grandchild.connect("button_pressed", $hex_grid, "action_button_pressed")
				grandchild.connect("button_entered", $hex_grid, "action_mouse_enter")
				grandchild.connect("button_left", $hex_grid, "action_mouse_left")
				for great_grandchild in grandchild.get_node("action_subfolder").get_children():
					great_grandchild.connect("button_pressed", $hex_grid, "action_button_pressed")
					great_grandchild.connect("button_entered", $hex_grid, "action_mouse_enter")
					great_grandchild.connect("button_left", $hex_grid, "action_mouse_left")
			child.get_node("reaction_buttons/cancel_button").connect("button_entered", $hex_grid, \
				"action_mouse_enter")
			child.get_node("reaction_buttons/cancel_button").connect("button_left", $hex_grid, \
				"action_mouse_left")
			if Globals.is_valid(child.superposition):
				get_node("objects/player_units").add_child(child.superposition)
				
		child.enable()
		
	for child in $objects/obstacles.get_children():
		child.find_grid()
		if $hex_grid.grid.has(child.coords):
			$hex_grid.grid[child.coords].set_occupying_obstacle(child)
		for extra_coord in child.extra_tiles:
			if $hex_grid.grid.has(child.coords + extra_coord):
				$hex_grid.grid[child.coords + extra_coord].set_occupying_obstacle(child)
				
	if self == get_tree().current_scene:
		runSBAs()
	
func _process(delta):
	if cutscene_mode or defeat or victory:
		return
	if player_turn:
		if not $cursor.get_pause_state():
			$cursor.position = Globals.grid_to_pixel($hex_grid.current_coords)
		
		if Input.is_action_just_pressed("end_turn"):
			if $hex_grid.mouse_state == $hex_grid.State.STATE_FREE:
				popup_end_turn()
	if $hex_grid.mouse_state == $hex_grid.State.STATE_REACTION_TARGET and \
	not $cursor.get_pause_state():
		$cursor.position = Globals.grid_to_pixel($hex_grid.current_coords)
		
	if check_defeat():
		defeat = true
		defeat()
	if check_victory() and !victory:
		victory = true
		victory()
		
func init_dialog() -> void:
	# abstract function
	pass
	
func reset_grid_and_cam() -> void:
	Globals.set_camera($camera)
	Globals.set_hex_grid($hex_grid)
	
func get_convo_partners(name : String) -> Array:
	var partners : Array = []
	
	for key in dialog.keys():
		if key[0] == name:
			partners.append(key[1])
		if key[1] == name:
			partners.append(key[0])
			
	var partner_objs : Array = []
	for partner in partners:
		var partner_obj
		
		if $objects/player_units.has_node(partner):
			partner_obj = $objects/player_units.get_node(partner)
		elif $objects/enemy_units.has_node(partner):
			partner_obj = $objects/enemy_units.get_node(partner)
		elif $objects/neutral_units.has_node(partner):
			partner_obj = $objects/neutral_units.get_node(partner)
			
		if !Globals.is_valid(partner_obj):
			continue
			
		partner_objs.append(partner_obj)
		
	return partner_objs
	
func has_convo(name : String) -> bool:
	var unit = $objects/player_units.get_node(name)
	
	if !Globals.is_valid(unit):
		return false
	
	var partners : Array = []
	var flag : bool = false
	
	for key in dialog.keys():
		if key[0] == name:
			partners.append(key[1])
		if key[1] == name:
			partners.append(key[0])
			
	unit.clear_partner_coords()
	
	for partner in partners:
		var partner_obj
		
		if $objects/player_units.has_node(partner):
			partner_obj = $objects/player_units.get_node(partner)
		elif $objects/enemy_units.has_node(partner):
			partner_obj = $objects/enemy_units.get_node(partner)
		elif $objects/neutral_units.has_node(partner):
			partner_obj = $objects/neutral_units.get_node(partner)
			
		if !Globals.is_valid(partner_obj):
			continue
			
		for adj in Globals.ring(unit.coords, 1):
			if adj == partner_obj.coords:
				flag = true
				unit.add_partner_coord(partner_obj.coords)
				
	return flag
	
func run_dialog(pair : Array) -> void:
	toggle_cutscene_mode()
	var new_dialog = load("res://Global/Menus/Dialog/dialog.tscn").instance()
	new_dialog.name = "dialog"
	new_dialog.z_index = 4
	new_dialog.fade = false
	new_dialog.connect("terminated", self, "resume_level_after_dialogue", [ ], CONNECT_ONESHOT)
	add_child(new_dialog)
	$dialog/canvas_layer/portrait_frame.hide()
	$dialog/canvas_layer/portrait.hide()
	$dialog/canvas_layer/label.hide()
	$dialog.show()
	
	var arr1 : Array = [pair[0], pair[1]]
	var arr2 : Array = [pair[1], pair[0]]
	
	for key in dialog.keys():
		if arr1 == key or arr2 == key:
			$dialog.dialog = dialog[key]
			dialog.erase(key)
			
func check_defeat() -> bool:
	if $objects/player_units.get_child_count() == 0:
		return true
	else:
		return false
		
func check_victory() -> bool:
	return false
	
func defeat() -> void:
	# abstract function
	pass
	
func victory() -> void:
	# abstract function
	pass
	
func run_enemy_turn() -> void:
	for enemy in $objects/enemy_units.get_children():
		current_enemy = enemy
		
		match enemy.turn_checks():
			0:
				emit_signal("finished_turn")
			1:
				$camera.center_unit(enemy)
				yield(get_tree().create_timer(0.8), "timeout")
				emit_signal("finished_turn")
			2:
				$camera.stalk_unit(enemy)
				if $camera.centering:
					yield($camera, "finished_centering")
				enemy.run_turn()
				
		if enemy.running_turn:
			yield(enemy, "finished_turn")
		if $hex_grid.wait_for_spell():
			yield($hex_grid, "resolve_current_spell")
			
		$hex_grid.de_gayify_the_map()
		
	run_level_specific_events()
	
	if stall_for_event:
		yield(self, "event_finished")
		stall_for_event = false
		
	current_enemy = null
	start_turn()
	
func run_level_specific_events() -> void:
	# abstract function
	pass
	
func runSBAs() -> void:
	var units : Array
	
	units.append_array(get_tree().get_nodes_in_group("PlayerUnits"))
	units.append_array(get_tree().get_nodes_in_group("EnemyUnits"))
	units.append_array(get_tree().get_nodes_in_group("NeutralUnits"))
	
	for unit in units:
		unit.runSBAs()
	
func popup_info() -> void:
	$popups/objective_popup.popup()
	
func popup_settings() -> void:
	$popups/system_popup.pull_up()
	
func popup_exit() -> void:
	$popups/exit_confirm.popup()
	
func popup_end_turn() -> void:
	$popups/end_turn_confirm.popup()
	
func popup_equal_and_opposite() -> void:
	hide_cursor()
	$popups/equal_and_opposite_popup.show()
	
func popup_item_obtained(obtainer : String, item_name : String) -> void:
	# bc of when this popup can occur, consider just pausing the entire game
	$popups/item_obtained_accept/label.bbcode_text = \
		"holy shit, {0} just got a {1}.".format([obtainer, item_name])
	$popups/item_obtained_accept.popup()
	
func end_turn() -> void:
	$camera.lock()
	$popups/turn_change_popup.turn_animation(Color.red)
	$hex_grid.control = false
	hide_cursor()
	yield($popups/turn_change_popup, "animation_finished")
	player_turn = false
	$hex_grid.go_to_enemy_turn()
	act = 0
	for cell in $hex_grid.get_all_cells():
		cell.endstep()
	for child in get_tree().get_nodes_in_group("PlayerUnits"):
		child.endstep()
		if $hex_grid.wait_for_spell:
			yield($hex_grid, "resolve_current_spell")
	for child in get_tree().get_nodes_in_group("EnemyUnits"):
		child.upkeep()
	run_enemy_turn()
	
func start_turn() -> void:
	turn_count += 1
	$popups/turn_change_popup.turn_animation(Color.blue)
	yield($popups/turn_change_popup, "animation_finished")
	$camera.unlock()
	player_turn = true
	$hex_grid.go_to_player_turn()
	for cell in $hex_grid.get_all_cells():
		cell.upkeep()
	for child in get_tree().get_nodes_in_group("PlayerUnits"):
		child.upkeep()
	for child in get_tree().get_nodes_in_group("EnemyUnits"):
		child.endstep()
		
func fade_in(duration : float = 1.0) -> void:
	$fade/color_rect.modulate = Color(1, 1, 1, 1)
	$fade/color_rect.show()
	
	$fade/tween.interpolate_property($fade/color_rect, "modulate:a", \
		1.0, 0.0, duration, Tween.TRANS_LINEAR)
	$fade/tween.start()
	
func fade_out(duration : float = 1.0) -> void:
	$fade/color_rect.modulate = Color(1, 1, 1, 0)
	$fade/color_rect.show()
	
	$fade/tween.interpolate_property($fade/color_rect, "modulate:a", \
		0.0, 1.0, duration, Tween.TRANS_LINEAR)
	$fade/tween.start()
		
func toggle_cutscene_mode() -> void:
	if cutscene_mode:
		cutscene_mode = false
		show_cursor()
		for child in $objects/player_units.get_children():
			child.get_node("ui_canvas").show()
			child.permanent = false
		for child in $objects/enemy_units.get_children():
			child.get_node("ui_canvas").show()
			child.permanent = false
	else:
		cutscene_mode = true
		hide_cursor()
		$hex_grid.de_gayify_the_map()
		for child in $objects/player_units.get_children():
			child.get_node("ui_canvas").hide()
			child.permanent = true
		for child in $objects/enemy_units.get_children():
			child.get_node("ui_canvas").hide()
			child.permanent = true
			
func show_target_confirm() -> void:
	$popups/target_confirm.show()
	
func hide_target_confirm() -> void:
	$popups/target_confirm.hide()
	
func disable_target_confirm() -> void:
	$popups/target_confirm.disabled = true
	
func enable_target_confirm() -> void:
	$popups/target_confirm.disabled = false
	
func hide_cursor() -> void:
	$cursor.hide()
	
func show_cursor() -> void:
	if cutscene_mode or defeat or victory:
		return
	$cursor.show()
	
func pause_cursor() -> void:
	$cursor.pause()
	
func unpause_cursor() -> void:
	$cursor.unpause()
	
func set_cursor_tooltip(current : int, maximum : int = 0) -> void:
	if current == -1:
		$cursor/label.text = ""
	else:
		$cursor/label.text = str(current) + "/" + str(maximum)
		
func is_tooltip_visible() -> bool:
	if $cursor/label.text != "":
		return true
	return false
	
func hide_path() -> void:
	$path.hide()
	
func show_path() -> void:
	$path.show()
	
func draw_path(path) -> void:
	$path.points = path
	show_path()
	
func resume_level_after_dialogue() -> void:
	get_node("dialog").queue_free()
	toggle_cutscene_mode()
	$hex_grid.mouse_state = $hex_grid.State.STATE_FREE
	
func _on_end_turn_confirm_confirmed():
	end_turn()
	
func _on_end_turn_confirm_popup_hide():
	$hex_grid.cancel_cursor_select_state()
	
func _on_exit_confirm_confirmed():
	# save progress (bookmark)
	get_tree().change_scene("res://Global/Menus/UIMenus/main_menu.tscn")
	
func _on_exit_confirm_popup_hide():
	$hex_grid.cancel_cursor_select_state()
	
func _on_target_confirm_pressed():
	emit_signal("confirm_spell")
	
func _on_tween_tween_completed(object, key):
	emit_signal("fade_complete")
	
func _on_push_button_pressed():
	$popups/equal_and_opposite_popup.hide()
	emit_signal("push_or_pull", "push")
	
func _on_pull_button_pressed():
	$popups/equal_and_opposite_popup.hide()
	emit_signal("push_or_pull", "pull")
	
func _on_objective_popup_confirmed():
	$hex_grid.cancel_cursor_select_state()
