extends "res://Global/Levels/level.gd"

var moving_enemy_index : int = 0
var attack_flag : bool = false

func _ready():
	pass
	
func _process(delta):
	pass
	
func init_dialog() -> void:
	dialog = {
		["grace", "cherry"]: [
			["transition", "fade_in"],
			["grace_lookaway", BOTTOM_LEFT, true, "This basement is kinda creepy."],
			["cherry", BOTTOM_RIGHT, false, "It's just a basement."],
			["grace_hmm", BOTTOM_LEFT, true, "It's old. Most of the newer buildings\nhigher up don't have these types of\narches."],
			["cherry_narrow", BOTTOM_RIGHT, false, "So it's the architecture that's creepy\nand not the three men in here\nactively seeking to kill us?"],
			["grace", BOTTOM_LEFT, true, "Yeah."],
			["transition", "fade_out"],
		]
	}
	
func check_victory() -> bool:
	if $objects/enemy_units.get_child_count() == 0:
		return true
	else:
		return false
		
func defeat() -> void:
	pass
	
func victory() -> void:
	self.connect("fade_complete", self, "next_scene")
	fade_out()
	
func next_scene() -> void:
	Config.characters["grace"]["skill_points"] += 1
	Config.characters["cherry"]["skill_points"] += 1
	get_tree().change_scene("res://Global/Menus/Dialog/Level_1/dialog_level_1_4.tscn")
