extends Node2D

const DARK : float = 0.6
const LIGHT : float = 1.0
const TIME : float = 8.0

func _ready():
	for lamp in get_tree().get_nodes_in_group("Lamps"):
		dim(lamp)
		yield(get_tree().create_timer(6.0), "timeout")
		
	$timer.start(rand_range(24.0, 32.0))
	
func dim(lamp) -> void:
	$tween.interpolate_property(lamp, "energy", LIGHT, DARK, TIME, Tween.TRANS_LINEAR, \
		Tween.EASE_IN_OUT)
	$tween.start()
	
func brighten(lamp) -> void:
	$tween.interpolate_property(lamp, "energy", DARK, LIGHT, TIME, Tween.TRANS_LINEAR, \
		Tween.EASE_IN_OUT)
	$tween.start()
	
func flicker(lamp) -> void:
	$tween.remove(lamp)
	
	for i in range(5 + (randi() % 4)):
		lamp.energy = 1.2
		yield(get_tree().create_timer(0.05), "timeout")
		lamp.energy = 0.0
		yield(get_tree().create_timer(0.05), "timeout")
		
	yield(get_tree().create_timer(rand_range(2.0, 5.0)), "timeout")
	$tween.interpolate_property(lamp, "energy", 0.0, LIGHT, TIME, Tween.TRANS_LINEAR, \
		Tween.EASE_IN_OUT)
	$tween.start()
	
func _on_tween_tween_completed(object, key):
	$tween.remove(object)
	
	if object.energy >= LIGHT:
		dim(object)
	else:
		brighten(object)
	
func _on_timer_timeout():
	flicker(get_tree().get_nodes_in_group("Lamps")[randi() % 3])
	$timer.start(rand_range(24.0, 32.0))
