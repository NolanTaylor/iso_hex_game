extends "res://Global/Levels/level.gd"

var escaped_units : PoolStringArray = []

func _ready():
	get_node("%seize_area").connect("area_seized", self, "unit_escaped")
	
func _process(delta):
	pass
	
func init_dialog() -> void:
	dialog = {
	
	}
	
func unit_escaped(unit) -> void:
	escaped_units.append(unit.name)
	unit.connect("complete_movement", self, "spanish_or_vanish", [ ], CONNECT_ONESHOT)
	
func spanish_or_vanish(unit) -> void:
	yield(get_tree().create_timer(0.2), "timeout")
	unit.vanish()
	
func check_victory() -> bool:
	if $objects/player_units.get_child_count() == 0 and escaped_units.size() > 0:
		return true
	else:
		return false
	
func defeat() -> void:
	pass
	
func victory() -> void:
	self.connect("fade_complete", self, "next_scene")
	fade_out()
	
func next_scene() -> void:
	Config.characters["grace"]["skill_points"] += 1
	Config.characters["cherry"]["skill_points"] += 1
	Config.characters["acacius"]["skill_points"] += 1
	Config.characters["illiana"]["skill_points"] += 1
	get_tree().change_scene("res://Global/Menus/Dialog/Level_3/dialog_level_3_2.tscn")
