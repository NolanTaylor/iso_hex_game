extends Sprite

var tile : Vector2
var paused : bool = false

func _ready():
	$action_buttons/end_turn_button.connect( \
		"button_pressed", self, "end_turn_button_pressed")
	$action_buttons/info_button.connect( \
		"button_pressed", self, "info_button_pressed")
	$action_buttons/settings_button.connect( \
		"button_pressed", self, "settings_button_pressed")
	$action_buttons/exit_button.connect( \
		"button_pressed", self, "exit_button_pressed")

func _process(delta):
	pass

func select() -> void:
	var camera = Globals.get_camera()
	var flip : bool = false
	
	if position.x < camera.position.x + 22:
		flip = true
		
	for i in $action_buttons.get_child_count():
		var child = $action_buttons.get_child(i)
		if flip:
			child.display_to(Globals.ACTION_COORDS_FLIP[i], flip)
		else:
			child.display_to(Globals.ACTION_COORDS[i], flip)
			
func deselect() -> void:
	for i in $action_buttons.get_child_count():
		$action_buttons.get_child(i).undisplay_all()
		
func pause() -> void:
	paused = true
	
func unpause() -> void:
	paused = false
	
func get_pause_state() -> bool:
	return paused
	
func end_turn_button_pressed(button) -> void:
	deselect()
	get_parent().popup_end_turn()
	
func info_button_pressed(button) -> void:
	deselect()
	get_parent().popup_info()
	
func settings_button_pressed(button) -> void:
	deselect()
	get_parent().popup_settings()
	
func exit_button_pressed(button) -> void:
	get_parent().popup_exit()
