extends Node2D

export var char_name : String

signal exit_menu(menu)

func _ready():
	pass
	
func set_editable() -> void:
	$skill_tree.editable = true
	$equipment_menu.editable = true
	$equipment_menu/equip_tab.show()
	
func destretch() -> void:
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_2D, \
		SceneTree.STRETCH_ASPECT_KEEP, get_viewport_rect().size)
	
func _on_x_button_pressed() -> void:
	destretch()
	reset_tabs()
	$skill_tree.hide()
	$skill_tree.exit_routine()
	$equipment_menu.hide()
	$equipment_menu.exit_routine()
	$info_menu.hide()
	$info_menu.exit_routine()
	emit_signal("exit_menu", self)
	
func reset_tabs() -> void:
	$tabs/skills_button.rect_position.x = 216
	$tabs/equipment_button.rect_position.x = 216
	$tabs/info_button.rect_position.x = 216
	
func _on_skills_button_pressed():
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT, \
		SceneTree.STRETCH_ASPECT_KEEP, get_viewport_rect().size)
		
	reset_tabs()
	$tabs/skills_button.rect_position.x = 208
	$skill_tree.show()
	$equipment_menu.hide()
	$info_menu.hide()
	$skill_tree.update()
	
func _on_equipment_button_pressed():
	reset_tabs()
	$equipment_menu.enter_routine()
	$tabs/equipment_button.rect_position.x = 208
	$skill_tree.hide()
	$equipment_menu.show()
	$info_menu.hide()
	
func _on_info_button_pressed():
	reset_tabs()
	$tabs/info_button.rect_position.x = 208
	$skill_tree.hide()
	$equipment_menu.hide()
	$info_menu.show()
	$info_menu.update()
	
func _on_root_character_menu_child_entered_tree(node):
	node.connect("child_entered_tree", Globals, "set_inherit_material")
	
	if node is CanvasItem:
		node.use_parent_material = true
