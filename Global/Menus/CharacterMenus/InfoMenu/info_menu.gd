extends Node2D

func _ready():
	pass
	
func update() -> void:
	var sprite : AnimatedSprite = get_node("../../sprite").duplicate()
	var canvas : Node2D = get_node("../../ui_canvas").duplicate()
	var popup : Node2D = get_node("../../popup").duplicate()
	
	sprite.position = Vector2(20, 48)
	canvas.position = Vector2(20, 48)
	popup.position = Vector2(4, 148)
	popup.show()
	sprite.play("idle")
	
	if get_child_count() > 2:
		for child in get_children():
			if child.get_index() >= 2:
				child.queue_free()
		
	add_child(sprite)
	add_child(canvas)
	add_child(popup)
	
func exit_routine() -> void:
	pass
