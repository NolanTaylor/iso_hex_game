extends Node2D

const THUMBTACK_DOWN = preload("res://Assets/GUI/menu_thumbtack.png")
const THUMBTACK_UP = preload("res://Assets/GUI/menu_thumbtack_raise.png")

export var proper_name : String = ""

var unlocked : bool = false
var unlockable : bool = false
var modulate_locked : Color = Color(0, 0, 0)
var modulate_unlocked : Color = Color(0, 0, 0)

signal skill_pressed(skill)
signal skill_entered(skill)
signal skill_left

func _ready():
	modulate_unlocked = self.modulate
	modulate_locked = Color(modulate.r * 0.5, modulate.g * 0.5, modulate.b * 0.5)
	
func lock() -> void:
	unlocked = false
	modulate = modulate_locked
	$thumbtack.hide()
	
func unlock() -> void:
	unlocked = true
	modulate = modulate_unlocked
	$thumbtack.show()
	
func _on_button_pressed():
	emit_signal("skill_pressed", self)
	
func _on_button_mouse_entered():
	if unlocked:
		$thumbtack.texture = THUMBTACK_UP
	else:
		modulate = modulate_unlocked
		
	emit_signal("skill_entered", self)
	
func _on_button_mouse_exited():
	if unlocked:
		$thumbtack.texture = THUMBTACK_DOWN
	else:
		modulate = modulate_locked
		
	emit_signal("skill_left")
	
