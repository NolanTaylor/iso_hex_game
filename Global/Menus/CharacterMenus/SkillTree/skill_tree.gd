extends Node2D

const UNSHADED : CanvasItemMaterial = preload("res://Global/Materials/unshaded.tres")
const DIRECTION : Array = [
	Vector2(0, -16),
	Vector2(16, -8),
	Vector2(16, 8),
	Vector2(0, 16),
	Vector2(-16, 8),
	Vector2(-16, -8),
]

export var _name : String = "Name"
export var pronoun_1 : String = "they"
export var pronoun_2 : String = "them"
export var pronoun_3 : String = "their"
export var pronoun_3_possessive : String = "theirs"

onready var char_name = get_parent().char_name
onready var skills = Config.characters[char_name]["skills"]
onready var skill_popup = preload("res://Global/Menus/CharacterMenus/SkillTree/skill_popup.tscn")

var current_skill = null
var editable : bool = false
var popup_exiting_anim_in_progress : bool = false
var skill_points : int = 0
var mouse_pos : Vector2 = get_global_mouse_position()
var mouse_velocity : Vector2 = get_global_mouse_position() - mouse_pos
var adjacency_list : Dictionary = { } # <String, Array<Skill>>

func _ready():
	var vertices : Array = [] # <Node>
	var edges : Array = [] # <Node>
	var trees : Array = [] # <Node>
	
	trees.append_array([$physical_tree, $magical_tree, $school_tree])
	
	for school in $subclasses.get_children():
		trees.append(school)
	
	for tree in trees:
		vertices = tree.get_node("vertices").get_children()
		edges = tree.get_node("edges").get_children()
		
		var coord_list : Dictionary = { } # <Vector2, String>
		
		for v in vertices:
			v.connect("skill_pressed", self, "skill_pressed")
			v.connect("skill_entered", self, "skill_hovered")
			v.connect("skill_left", self, "skill_unhovered")
			v.lock()
			if v.name in skills:
				v.unlock()
			coord_list[v.position] = v.name
			adjacency_list[v.name] = []
			
		for e in edges:
			var connections : Array = [] # <String>
			
			if coord_list.has(e.points[0] + e.position):
				connections.push_back(coord_list[e.points[0] + e.position])
			if coord_list.has(e.points[1] + e.position):
				connections.push_back(coord_list[e.points[1] + e.position])
			
			if connections.size() != 2:
				push_error("edges must connect exactly two vertices >:(")
			else:
				adjacency_list[connections[0]].append(connections[1])
				adjacency_list[connections[1]].append(connections[0])
				
			e.set_line()
			
	reset_menu()
	
func _process(delta):
	mouse_velocity = get_global_mouse_position() - mouse_pos
	mouse_pos = get_global_mouse_position()
	
func update() -> void:
	skill_points = Config.characters[char_name]["skill_points"]
	var skills : Array = Config.characters[char_name]["skills"]
	
	for skill in $vertices.get_children():
		skill.unlockable = false
		
		if skill.name in skills:
			skill.unlock()
		else:
			skill.lock()
			
			for adjacent_skill in adjacency_list[skill.name]:
				if adjacent_skill.unlocked:
					skill.unlockable = true
					
func skill_pressed(skill) -> void:
	if $popups.get_child_count() == 0:
		enqueue_popup(skill.name)
		
func enqueue_popup(skill_name : String) -> void:
	var popup = skill_popup.instance()
	
	if !Globals.is_valid(popup):
		push_error("skill popup doesn't exist ;-;")
		return
		
	popup.customize(skill_name)
	
	if editable:
		var skill = $vertices.get_node(skill_name)
		if Globals.is_valid(skill):
			popup.show_points()
			popup.set_points(skill_points)
			if skill.unlockable and skill_points >= 1:
				popup.enable_unlock()
			else:
				popup.disable_unlock()
	else:
		popup.disable_unlock()
		popup.hide_points()
	if $popups.get_child_count() >= 1:
		popup.get_node("unlock_button").hide()
		popup.hide_points()
		
	var bbct = popup.get_node("description").bbcode_text
	popup.get_node("description").bbcode_text = bbct.format( \
		{
			"name": _name,
			"they": pronoun_1,
			"them": pronoun_2,
			"their": pronoun_3,
			"theirs": pronoun_3_possessive,
		})
		
	popup.get_node("description").connect("meta_clicked", self, "link_clicked")
	popup.get_node("prereqs").connect("meta_clicked", self, "link_clicked")
	popup.get_node("x_button").connect("pressed", self, "popup_exited")
	popup.get_node("unlock_button").connect("pressed", self, "popup_unlocked", [skill_name])
	if popup.has_node("preview_button"):
		popup.get_node("preview_button").connect("pressed", self, "school_previewed", \
			[skill_name])
		
	popup.char_name = self.char_name
	popup.material = UNSHADED
	popup.position.y = 200
	$tween.interpolate_property(popup, "position", Vector2(0, 200), \
		Vector2.ZERO, 0.8, Tween.TRANS_QUAD)
	
	$popups.add_child(popup)
	popup.z_index = $popups.get_child_count()
	$tween.start()
	
func hide_all_trees() -> void:
	$physical_tree.hide()
	$magical_tree.hide()
	$school_tree.hide()
	
	for tree in $subclasses.get_children():
		tree.hide()
		
func set_note_text(text : String) -> void:
	$note/label.text = text
	
func skill_hovered(skill) -> void:
	if Data.SKILLS.has(skill.name):
		$note/label.text = Data.SKILLS[skill.name]["name"]
	
func skill_unhovered() -> void:
	$note/label.text = ""
	
func link_clicked(meta) -> void:
	enqueue_popup(meta)
	
func popup_exited() -> void:
	if $popups.get_child_count() == 0:
		push_error("popups has no children")
		return
	if popup_exiting_anim_in_progress:
		return
		
	var last_child = $popups.get_child($popups.get_child_count() - 1)
	$tween.interpolate_property(last_child, "position", Vector2.ZERO, \
		Vector2(0, 200), 0.8, Tween.TRANS_QUAD)
	$tween.connect("tween_completed", self, "kill_youngest_child", \
		[ ], CONNECT_ONESHOT)
	$tween.start()
	popup_exiting_anim_in_progress = true
	
func popup_unlocked(skill_name : String) -> void:
	Config.characters[char_name]["skills"].append(skill_name)
	Config.characters[char_name]["skill_points"] -= 1
	popup_exited()
	update()
	
func school_previewed(school_name : String) -> void:
	popup_exited()
	for school in $subclasses.get_children():
		school.hide()
		
	if $subclasses.has_node(school_name):
		$subclasses.get_node(school_name).show()
	
func kill_youngest_child(object : Object, key : NodePath) -> void:
	if $popups.get_child_count() == 0:
		push_error("popups has no children")
		return
		
	var last_child = $popups.get_child($popups.get_child_count() - 1)
	$popups.remove_child(last_child)
	last_child.queue_free()
	popup_exiting_anim_in_progress = false
	
func exit_routine() -> void:
	for child in $popups.get_children():
		$popups.remove_child(child)
		child.queue_free()
	
func reset_menu() -> void:
	pass
	
func _on_physical_button_pressed():
	hide_all_trees()
	$physical_tree.show()
	
func _on_magical_button_pressed():
	hide_all_trees()
	$magical_tree.show()
	
func _on_school_button_pressed():
	hide_all_trees()
	$school_tree.show()
	
func _on_root_skill_tree_visibility_changed():
	if visible:
		_on_magical_button_pressed()
		set_note_text("")
