extends Node2D

func _ready():
	pass
	
func set_text(text : String, skill : String) -> void:
	$label.rect_position = Vector2(2, -1)
	$label.text = text
	
	var font : DynamicFont = $label.get("custom_fonts/font")
	var text_size : Vector2 = font.get_string_size(text)
	$sticky.rect_size = text_size + Vector2(3, 0)
	
	$icon.position.x = $sticky.rect_position.x + $sticky.rect_size.x - 3
	$icon.position.y = $sticky.rect_position.y - 3
	
	if "light" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_light_icon.png")
	elif "work" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_work_icon.png")
	elif "flux" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_flux_icon.png")
	elif "entropy" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_entropy_icon.png")
	elif "rapidity" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_rapidity_icon.png")
	elif "lune" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_lune_icon.png")
	elif "corruption" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_corrupted_health_icon.png")
	elif "Swift" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_swift_icon.png")
	elif "Action" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_action_icon.png")
	elif "action" in text:
		$icon.texture = load("res://Assets/GUI/charHUD_reaction_icon.png")
	else:
		$icon.texture = load("res://Assets/GUI/charHUD_{0}_icon.png".format([skill]))
		
