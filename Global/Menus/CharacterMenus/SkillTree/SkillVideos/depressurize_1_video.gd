extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var illiana = $objects/player_units/illiana
onready var target = $objects/player_units/illiana.coords + Vector3(2, 0, -2)

var depressurize

func _ready():
	illiana.permanent = true
	
func play_video() -> void:
	depressurize = $hex_grid.create_new_spell("depressurize", illiana, target, false, false)
	highlight(depressurize.get_highlight_cells(illiana), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(depressurize.get_flex_cells(illiana, target), "red")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	depressurize.cast(illiana, target)
	yield(get_tree().create_timer(5.0), "timeout")
	emit_signal("video_finished")
	
