extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var cherry = $objects/player_units/cherry
onready var celestial = $objects/enemy_units/celestial

func _ready():
	cherry.permanent = true
	
func play_video() -> void:
	var starbind = $hex_grid.create_new_spell("starbind", celestial, cherry.coords, false, false)
	highlight(starbind.get_highlight_cells(celestial), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(cherry.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(cherry.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	starbind.cast(celestial, cherry.coords)
	yield(starbind, "complete_cast")
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	var absorb = $hex_grid.create_new_spell("absorb", cherry, cherry.coords, false, false)
	absorb.cast(cherry, cherry.coords)
	yield(absorb, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
