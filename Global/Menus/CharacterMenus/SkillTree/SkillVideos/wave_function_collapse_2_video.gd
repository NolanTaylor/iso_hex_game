extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var seth = $objects/player_units/seth
onready var captor = $objects/enemy_units/captor
onready var dest : Vector3 = captor.coords + Vector3(0, 2, -2)

func _ready():
	seth.permanent = true
	seth.superposition.teleport(seth.coords + Vector3(3, 0, -3))
	
func play_video() -> void:
	captor.apply_condition("pass")
	var path = $hex_grid.find_path(captor.coords, dest, "player_units", captor.movement)
	captor.move(path, 0)
	yield(captor, "complete_movement")
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	var wave = $hex_grid.create_new_spell("wave_function_collapse", seth, seth.coords, \
		false, false)
	highlight(wave.get_highlight_cells(seth), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	wave.cast(seth, seth.coords)
	yield(wave, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
