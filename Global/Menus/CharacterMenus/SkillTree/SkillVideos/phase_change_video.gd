extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var kin = $objects/enemy_units/kin

func _ready():
	if unit.guard_left == 0:
		unit.guard_left = 1
		unit.apply_ui()
	
func play_video() -> void:
	var dissipate = $hex_grid.create_new_spell("dissipate", kin, unit.coords, false, false)
	highlight(dissipate.get_highlight_cells(kin), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(dissipate.get_highlight_cells(kin), "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	dissipate.cast(kin, unit.coords)
	kin.toss_skill("dissipate")
	yield(dissipate, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
