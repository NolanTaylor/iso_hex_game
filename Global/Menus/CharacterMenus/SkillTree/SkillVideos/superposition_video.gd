extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var seth = $objects/player_units/seth
onready var captor = $objects/enemy_units/captor

func _ready():
	seth.permanent = true
	
	var c : Vector3 = captor.coords + Vector3(1, -1, 0)
	seth.superposition.position = Globals.grid_to_pixel(c)
	seth.superposition.coords = c
	
func play_video() -> void:
	var stab = $hex_grid.create_new_spell("stab", seth, captor.coords, false, false)
	highlight(seth.coords, "blue")
	highlight(seth.superposition.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(seth.superposition.coords, "green")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(stab.get_highlight_cells(seth.superposition), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	stab.cast(seth, captor.coords)
	yield(stab, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
