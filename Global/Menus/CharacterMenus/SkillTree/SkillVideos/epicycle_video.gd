extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var anastasia = $objects/player_units/anastasia
onready var acacius = $objects/player_units/acacius

func _ready():
	anastasia.permanent = true
	acacius.permanent = true
	
func play_video() -> void:
	var charge = $hex_grid.create_new_spell("charge", acacius, anastasia.coords, false, false)
	highlight(charge.get_highlight_cells(acacius), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(anastasia.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(anastasia.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	charge.cast(acacius, anastasia.coords)
	yield(charge, "complete_cast")
	var epi = $hex_grid.create_new_spell("epicycle", anastasia, anastasia.coords, false, false)
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
