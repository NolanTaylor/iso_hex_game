extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var illiana = $objects/player_units/illiana
onready var fia = $objects/player_units/fia
onready var captor = $objects/enemy_units/captor

func _ready():
	illiana.permanent = true
	fia.permanent = true
	
func play_video() -> void:
	var target : Vector3 = fia.coords + Globals.CUBE_VECTORS[4]
	var guardian = $hex_grid.create_new_spell("guardian", illiana, target, false, false)
	highlight(guardian.get_highlight_cells(illiana), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	highlight(fia.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	guardian.cast(illiana, target)
	yield(guardian, "complete_cast")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	var bolt = $hex_grid.create_new_spell("bolt", captor, fia.coords, false, false)
	highlight(bolt.get_highlight_cells(captor), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(illiana.coords, "blue")
	highlight(fia.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(fia.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	captor.toss_skill("bolt")
	bolt.cast(captor, fia.coords)
	yield(bolt, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
