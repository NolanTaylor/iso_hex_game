extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var seth = $objects/player_units/seth
onready var captor = $objects/enemy_units/captor

func _ready():
	seth.permanent = true
	seth.superposition.teleport(seth.coords + Vector3(3, 0, -3))
	
func play_video() -> void:
	var wave = $hex_grid.create_new_spell("wave_function_collapse", seth, seth.coords, \
		false, false)
	highlight(wave.get_highlight_cells(seth), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(seth.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(seth.coords, "red")
	highlight(wave.get_flex_cells(seth, seth.coords), "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	wave.cast(seth, seth.coords)
	yield(wave, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
