extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var anastasia = $objects/player_units/anastasia
onready var captor = $objects/enemy_units/captor
onready var target = anastasia.coords + Vector3(2, 0, -2)
onready var destination = target + Vector3(1, -1, 0)

func _ready():
	anastasia.permanent = true
	
func play_video() -> void:
	var field = $hex_grid.create_new_spell("field_of_stars", anastasia, target, false, false)
	highlight(field.get_highlight_cells(anastasia), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(field.get_flex_cells(anastasia, target), "red")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	field.cast(anastasia, target)
	yield(field, "complete_cast")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	captor.apply_condition("pass")
	var path = $hex_grid.find_path(captor.coords, destination, "player_units", captor.movement)
	captor.move(path, 0)
	yield(captor, "complete_movement")
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	var stab = $hex_grid.create_new_spell("stab", anastasia, captor.coords, false)
	yield(stab, "complete_cast")
	$hex_grid.apply_terrain(target, "null")
	for dir in Globals.CUBE_VECTORS:
		$hex_grid.apply_terrain(target + dir, "null")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
