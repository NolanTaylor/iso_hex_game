extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var anastasia = $objects/player_units/anastasia
onready var ozias = $objects/player_units/ozias
onready var captor = $objects/enemy_units/captor

func _ready():
	anastasia.permanent = true
	ozias.permanent = true
	ozias.health_left = 1
	ozias.guard_left = 0
	ozias.cast_ult("endurance")
	ozias.cast_ult("defy_death")
	ozias.apply_ui()
	
func play_video() -> void:
	var bolt = $hex_grid.create_new_spell("bolt", captor, ozias.coords, false, false)
	highlight(bolt.get_highlight_cells(captor), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(anastasia.coords, "blue")
	highlight(ozias.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(ozias.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	var target : Vector3 = Vector3(ozias.coords.x, ozias.coords.y, ozias.coords.z)
	bolt.cast(captor, ozias.coords)
	captor.toss_skill("bolt")
	yield(bolt, "complete_cast")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	var rebirth = $hex_grid.create_new_spell("rebirth", anastasia, target, false, false)
	highlight(rebirth.get_highlight_cells(anastasia), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "green")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	rebirth.cast(anastasia, target)
	yield(rebirth, "complete_cast")
	var new_ozias = $hex_grid.create_new_unit( \
		load("res://Units/Characters/ozias.tscn").instance(), target)
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
