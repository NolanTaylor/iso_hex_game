extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var grace = $objects/player_units/grace
onready var captor = $objects/enemy_units/captor
onready var jake = $objects/enemy_units/jake
onready var celestial = $objects/enemy_units/celestial

func _ready():
	grace.permanent = true
	
func play_video() -> void:
	var rays = $hex_grid.create_new_spell("crepuscular_rays", grace, grace.coords, false, false)
	highlight(rays.get_highlight_cells(grace), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	highlight(jake.coords, "blue")
	highlight(celestial.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "pink")
	highlight(jake.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(jake.coords, "pink")
	highlight(celestial.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	rays.multi_cast(grace, PoolVector3Array([captor.coords, jake.coords, celestial.coords]))
	yield(rays, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
