extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var fil = $objects/enemy_units/fil

func _ready():
	unit.health_left = 1
	fil.health_left = 1
	unit.apply_ui()
	fil.apply_ui()
	
func play_video() -> void:
	var stab = $hex_grid.create_new_spell("stab", unit, fil.coords, false, false)
	highlight(stab.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(fil.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(fil.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	stab.cast(unit, fil.coords)
	yield(stab, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
