extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var target : Vector3

func _ready():
	pass
	
func play_video() -> void:
	target = unit.coords + Globals.CUBE_VECTORS[2] * 2
	var freeze = $hex_grid.create_new_spell("freeze", unit, target, false, false)
	highlight(freeze.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	freeze.cast(unit, target)
	yield(freeze, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
