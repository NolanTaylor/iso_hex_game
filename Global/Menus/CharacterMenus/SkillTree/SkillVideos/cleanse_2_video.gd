extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

func _ready():
	pass
	
func play_video() -> void:
	var cleanse = $hex_grid.create_new_spell("cleanse", unit, unit.coords, false, false)
	highlight(cleanse.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(unit.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(unit.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	cleanse.cast(unit, unit.coords)
	yield(cleanse, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
