extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var captor = $objects/enemy_units/captor

func _ready():
	pass
	
func play_video() -> void:
	var stab = $hex_grid.create_new_spell("stab", unit, captor.coords, false, false)
	highlight(stab.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	stab.cast(unit, captor.coords)
	yield(get_tree().create_timer(3.0), "timeout")
	emit_signal("video_finished")
	
