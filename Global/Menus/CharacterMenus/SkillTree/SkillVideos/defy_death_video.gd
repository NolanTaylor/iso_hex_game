extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var trafficker = $objects/enemy_units/fil

func _ready():
	unit.health_left = 1
	unit.guard_left = 0
	unit.apply_ui()
	
func play_video() -> void:
	var stab = $hex_grid.create_new_spell("stab", trafficker, unit.coords, false, false)
	highlight(stab.get_highlight_cells(trafficker), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(unit.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(unit.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	stab.cast(trafficker, unit.coords)
	trafficker.toss_skill("stab")
	yield(stab, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
