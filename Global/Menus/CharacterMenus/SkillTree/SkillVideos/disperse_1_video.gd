extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

func _ready():
	pass
	
func play_video() -> void:
	var target = unit.coords + Vector3(1, 0, -1)
	var disperse = $hex_grid.create_new_spell("disperse", unit, target, false, false)
	highlight(disperse.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(disperse.get_flex_cells(unit, target), "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	disperse.cast(unit, target)
	yield(disperse, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
