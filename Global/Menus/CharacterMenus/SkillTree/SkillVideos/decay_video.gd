extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var cherry = $objects/player_units/cherry
onready var captor = $objects/enemy_units/captor

func _ready():
	pass
	
func play_video() -> void:
	var decay = $hex_grid.create_new_spell("decay", unit, captor.coords, false, false)
	highlight(decay.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	decay.cast(unit, captor.coords)
	yield(decay, "complete_cast")
	captor.apply_ui()
	yield(get_tree().create_timer(1.0), "timeout")
	var stab = $hex_grid.create_new_spell("stab", cherry, captor.coords, false, false)
	highlight(stab.get_highlight_cells(cherry), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	stab.cast(cherry, captor.coords)
	yield(stab, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
