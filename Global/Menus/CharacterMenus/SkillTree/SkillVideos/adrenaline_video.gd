extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

func _ready():
	unit.health_left = 1
	unit.apply_ui()
	
func play_video():
	unit.upkeep()
	yield(get_tree().create_timer(LOOP_TIME * 3), "timeout")
	emit_signal("video_finished")
