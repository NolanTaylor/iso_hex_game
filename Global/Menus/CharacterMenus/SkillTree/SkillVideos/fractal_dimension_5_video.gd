extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var captor_1 = $objects/enemy_units/captor_1
onready var captor_2 = $objects/enemy_units/captor_2

func _ready():
	pass
	
func play_video() -> void:
	var og_pos = unit.coords
	var target_1 = unit.coords + Vector3(0, 2, -2)
	var target_2 = unit.coords + Vector3(3, -1, -2)
	var fractal = $hex_grid.create_new_spell("fractal_dimension", unit, target_1, false, false)
	var highlight_cells : PoolVector3Array = fractal.get_highlight_cells(unit)
	for coord in highlight_cells:
		if coord == og_pos or coord == $objects/obstacles/root_tree.coords or \
		coord == captor_1.coords or coord == captor_2.coords:
			continue
		highlight(coord, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	fractal.advance(target_1)
	highlight(fractal.get_flex_cells(unit, target_1), "red")
	highlight(target_1, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight_cells = fractal.get_highlight_cells(unit)
	for coord in highlight_cells:
		if coord == og_pos or coord == $objects/obstacles/root_tree.coords or \
		coord == captor_1.coords or coord == captor_2.coords:
			continue
		highlight(coord, "blue")
	highlight(target_1, "pink")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	fractal.advance(target_2)
	highlight(fractal.get_flex_cells(unit, target_2), "red")
	highlight(target_2, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	fractal.multi_cast(unit, PoolVector3Array([target_1, target_2]))
	yield(fractal, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
