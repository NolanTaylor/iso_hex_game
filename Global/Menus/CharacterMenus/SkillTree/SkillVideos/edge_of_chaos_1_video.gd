extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var seth = $objects/player_units/seth
onready var captor = $objects/enemy_units/captor

func _ready():
	seth.permanent = true
	
func play_video() -> void:
	var edge = $hex_grid.create_new_spell("edge_of_chaos", seth, captor.coords, false, false)
	highlight(edge.get_highlight_cells(seth), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	edge.cast(seth, captor.coords)
	yield(edge, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
