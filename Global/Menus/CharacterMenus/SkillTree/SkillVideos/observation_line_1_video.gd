extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var target : Vector3

func _ready():
	pass
	
func play_video() -> void:
	target = unit.coords + Globals.CUBE_VECTORS[2]
	var obs_l = $hex_grid.create_new_spell("observation_line", unit, target, false, false)
	highlight(obs_l.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(obs_l.get_flex_cells(unit, target), "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(obs_l.get_flex_cells(unit, target), "blue")
	obs_l.rotate_orientation()
	highlight(obs_l.get_flex_cells(unit, target), "pink")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(obs_l.get_flex_cells(unit, target), "blue")
	obs_l.rotate_orientation()
	highlight(obs_l.get_flex_cells(unit, target), "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	obs_l.cast(unit, target)
	yield(obs_l, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	var mov : Vector3 = unit.coords + Vector3(2, -1, -1)
	grid[unit.coords].set_occupying_unit(unit)
	highlight($hex_grid.flood_fill(unit.coords, unit.movement, "enemy_units"), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	hide_path()
	$hex_grid.de_gayify_the_map()
	var path = $hex_grid.find_path(unit.coords, mov, "enemy_units", unit.movement)
	unit.move(path, 0)
	yield(unit, "complete_movement")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
