extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var grace = $objects/player_units/grace

func _ready():
	grace.permanent = true
	
func play_video() -> void:
	var parallel = $hex_grid.create_new_spell("parallel_coherence", unit, \
		grace.coords, false, false)
	highlight(parallel.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(grace.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(grace.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	parallel.cast(unit, grace.coords)
	yield(parallel, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
