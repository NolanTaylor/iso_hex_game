extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var captor = $objects/enemy_units/captor

func _ready():
	pass
	
func play_video() -> void:
	var solar_winds = $hex_grid.create_new_spell("solar_winds", unit, unit.coords, false, false)
	highlight(unit.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(unit.coords, "green")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	solar_winds.cast(unit, unit.coords)
	yield(solar_winds, "complete_cast")
	var stab = $hex_grid.create_new_spell("stab", unit, captor.coords, false, false)
	highlight(stab.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	stab.cast(unit, captor.coords)
	yield(stab, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
