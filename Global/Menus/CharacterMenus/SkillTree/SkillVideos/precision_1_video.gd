extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var cherry = $objects/player_units/cherry
onready var captor = $objects/enemy_units/captor

func _ready():
	cherry.permanent = true
	
func play_video() -> void:
	var precision = $hex_grid.create_new_spell("precision", cherry, captor.coords, false, false)
	highlight(precision.get_highlight_cells(cherry), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	precision.cast(cherry, captor.coords)
	yield(precision, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
