extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var illiana = $objects/player_units/illiana
onready var captor = $objects/enemy_units/captor

func _ready():
	illiana.permanent = true
	
func play_video() -> void:
	var bolt = $hex_grid.create_new_spell("bolt", captor, illiana.coords, false, false)
	highlight(bolt.get_highlight_cells(captor), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(illiana.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(illiana.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	captor.toss_skill("bolt")
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	captor.hold_toss()
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	var deposition = $hex_grid.create_new_spell("deposition", illiana, captor.coords, false, false)
	deposition.cast(illiana, captor.coords)
	captor.fizzle_toss()
	yield(deposition, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
