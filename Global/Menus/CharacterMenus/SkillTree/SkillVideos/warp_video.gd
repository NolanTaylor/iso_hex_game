extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var illiana = $objects/player_units/illiana

func _ready():
	illiana.permanent = true
	
func play_video() -> void:
	var target : Vector3 = unit.coords + Vector3(1, 1, -2)
	var warp = $hex_grid.create_new_spell("warp", unit, illiana.coords, false, false)
	highlight(warp.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(unit.coords, "blue")
	highlight(illiana.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(illiana.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	var highlight_cells : PoolVector3Array = warp.get_highlight_cells(unit)
	for coord in highlight_cells:
		if $hex_grid.has_occupant(coord):
			continue
		highlight(coord, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	warp.targets = PoolVector3Array([illiana.coords, target])
	warp.cast(unit, illiana.coords)
	yield(warp, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
