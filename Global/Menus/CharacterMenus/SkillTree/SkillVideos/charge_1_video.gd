extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var grace = $objects/player_units/grace
onready var celestial = $objects/enemy_units/celestial

func _ready():
	grace.permanent = true
	
func play_video() -> void:
	var charge = $hex_grid.create_new_spell("charge", unit, grace.coords, false, false)
	highlight(charge.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(grace.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(grace.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	charge.cast(unit, grace.coords)
	yield(charge, "complete_cast")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	var stab = $hex_grid.create_new_spell("stab", grace, celestial.coords, false, false)
	highlight(stab.get_highlight_cells(grace), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(celestial.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(celestial.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	stab.cast(grace, celestial.coords)
	yield(stab, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
