extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

func _ready():
	pass
	
func play_video() -> void:
	var og_pos = unit.coords
	var target = unit.coords + Vector3(2, 0, -2)
	var fractal = $hex_grid.create_new_spell("fractal_dimension", unit, target, false, false)
	var highlight_cells : PoolVector3Array = fractal.get_highlight_cells(unit)
	for coord in highlight_cells:
		if coord == og_pos or coord == $objects/obstacles/root_tree.coords:
			continue
		highlight(coord, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	fractal.cast(unit, target)
	yield(fractal, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
