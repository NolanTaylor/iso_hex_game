extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var cherry = $objects/player_units/cherry

func _ready():
	cherry.permanent = true
	
func play_video() -> void:
	var surge = $hex_grid.create_new_spell("potential_surge", cherry, cherry.coords, false, false)
	highlight(cherry.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(cherry.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	surge.cast(cherry, cherry.coords)
	yield(surge, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
