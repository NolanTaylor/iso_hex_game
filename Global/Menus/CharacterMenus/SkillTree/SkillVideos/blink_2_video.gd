extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var grace = $objects/player_units/grace
onready var thermo = $objects/enemy_units/thermo
onready var og_pos = grace.coords
onready var target = grace.coords + Vector3(2, -1, -1)

func _ready():
	grace.permanent = true
	
func play_video() -> void:
	var fireball = $hex_grid.create_new_spell("fireball", thermo, grace.coords, false, false)
	highlight(fireball.get_highlight_cells(thermo), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	var flex : PoolVector3Array = fireball.get_flex_cells(thermo, grace.coords)
	flex.append(og_pos)
	highlight(flex, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(flex, "pink")
	thermo.toss_skill("fireball")
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	thermo.hold_toss()
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	var blink = $hex_grid.create_new_spell("blink", grace, target, false, false)
	var highlight_cells : PoolVector3Array = blink.get_highlight_cells(grace)
	for coord in highlight_cells:
		if coord == og_pos or coord == $objects/obstacles/root_tree.coords or coord in flex:
			continue
		if grid.has(coord):
			grid[coord].set_highlight("blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	blink.cast(grace, target)
	yield(blink, "complete_cast")
	thermo.resume_toss()
	fireball.cast(thermo, og_pos)
	yield(fireball, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
