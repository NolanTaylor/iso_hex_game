extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var cherry = $objects/player_units/cherry
onready var captor = $objects/enemy_units/captor

func _ready():
	cherry.permanent = true
	if not Config.characters["cherry"]["skills"].has("collision"):
		cherry.init_skill("collision")
		
func play_video() -> void:
	cherry.work = 2
	var displace = $hex_grid.create_new_spell("displace", cherry, captor.coords, false, false)
	highlight(displace.get_highlight_cells(cherry), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(displace.get_valid_cursor_cells(cherry), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(displace.get_flex_cells(cherry, captor.coords), "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	displace.cast(cherry, captor.coords)
	yield(displace, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
