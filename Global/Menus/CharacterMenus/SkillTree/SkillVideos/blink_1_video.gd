extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var grace = $objects/player_units/grace
onready var og_pos = grace.coords
onready var target = grace.coords + Vector3(2, 0, -2)

func _ready():
	grace.permanent = true
	
func play_video() -> void:
	var blink = $hex_grid.create_new_spell("blink", grace, target, false, false)
	var highlight_cells : PoolVector3Array = blink.get_highlight_cells(grace)
	for coord in highlight_cells:
		if coord == og_pos or coord == $objects/obstacles/root_bush.coords or coord == \
		$objects/obstacles/root_tree.coords:
			continue
		highlight(coord, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	blink.cast(grace, target)
	yield(blink, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
