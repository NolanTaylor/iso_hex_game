extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var cherry = $objects/player_units/cherry
onready var captor = $objects/enemy_units/captor

func _ready():
	cherry.permanent = true
	
func play_video() -> void:
	var target : Vector3 = cherry.coords + Vector3(2, 1, -3)
	grid[cherry.coords].set_occupying_unit(unit)
	highlight($hex_grid.flood_fill(cherry.coords, cherry.movement, "enemy_units"), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	hide_path()
	$hex_grid.de_gayify_the_map()
	var path = $hex_grid.find_path(cherry.coords, target, "enemy_units", cherry.movement)
	cherry.move(path, 0)
	yield(cherry, "complete_movement")
	cherry.apply_condition("momentum")
	var stab = $hex_grid.create_new_spell("stab", cherry, captor.coords, false, false)
	highlight(stab.get_highlight_cells(cherry), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	stab.cast(cherry, captor.coords)
	yield(stab, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
