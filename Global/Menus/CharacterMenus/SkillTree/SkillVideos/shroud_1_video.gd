extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

func _ready():
	pass
	
func play_video() -> void:
	var shroud = $hex_grid.create_new_spell("shroud", unit, unit.coords, false, false)
	highlight(shroud.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(unit.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(unit.coords, "green")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	shroud.cast(unit, unit.coords)
	yield(shroud, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
