extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var seth = $objects/player_units/seth
onready var captor = $objects/enemy_units/captor

func _ready():
	seth.permanent = true
	seth.superposition.teleport(seth.coords + Vector3(3, 0, -3))
	captor.health_left = 1
	captor.apply_ui()
	
func play_video() -> void:
	var stab = $hex_grid.create_new_spell("stab", seth, captor.coords, false, false)
	highlight(stab.get_highlight_cells(seth), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	stab.cast(seth, captor.coords)
	yield(stab, "complete_cast")
	var uncertainty = $hex_grid.create_new_spell("uncertainty", seth, seth.coords, false, false)
	highlight(uncertainty.get_highlight_cells(seth), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	uncertainty.cast(seth, seth.coords)
	yield(uncertainty, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
