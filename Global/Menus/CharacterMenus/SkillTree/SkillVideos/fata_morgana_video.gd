extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var grace = $objects/player_units/grace
onready var captor = $objects/enemy_units/captor
onready var placeholder = $objects/player_units/placeholder
onready var target : Vector3 = grace.coords + Vector3(1, -1, 0)

func _ready():
	grace.permanent = true
	
func play_video() -> void:
	var fata_morgana = $hex_grid.create_new_spell("fata_morgana", grace, target, false, false)
	highlight(fata_morgana.get_highlight_cells(grace), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	fata_morgana.cast(grace, target)
	yield(fata_morgana, "complete_cast")
	yield(get_tree().create_timer(1.0), "timeout")
	var copy = $objects/player_units.get_child(1)
	var blind = $hex_grid.create_new_spell("blind", copy, captor.coords, false, false)
	highlight(blind.get_highlight_cells(copy), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	blind.cast(copy, captor.coords)
	yield(blind, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
