extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var placeholder = $objects/player_units/placeholder
onready var target = $objects/enemy_units/captor.coords

var blind

func _ready():
	pass
	
func play_video() -> void:
	blind = $hex_grid.create_new_spell("blind", unit, target, false, false)
	highlight(blind.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(target, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	blind.cast(unit, target)
	yield(blind, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
