extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var jake = $objects/enemy_units/jake

func _ready():
	unit.request_damage()
	
func play_video() -> void:
	var stab = $hex_grid.create_new_spell("stab", jake, unit.coords, false, false)
	highlight(stab.get_highlight_cells(jake), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(unit.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(unit.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	stab.cast(jake, unit.coords)
	jake.toss_skill("stab")
	yield(stab, "complete_cast")
	var brace = $hex_grid.create_new_spell("brace", unit, unit.coords, false, false)
	brace.cast(unit, unit.coords)
	yield(brace, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
