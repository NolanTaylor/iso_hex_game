extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var cherry = $objects/player_units/cherry
onready var og_pos = cherry.coords
onready var target = cherry.coords + Vector3(3, 0, -3)

func _ready():
	cherry.permanent = true
	
func play_video() -> void:
	var velocity = $hex_grid.create_new_spell("velocity", cherry, target, false, false)
	highlight(velocity.get_highlight_cells(cherry), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	velocity.cast(cherry, target)
	yield(velocity, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
