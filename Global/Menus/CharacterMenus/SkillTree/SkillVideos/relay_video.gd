extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var acacius = $objects/player_units/acacius
onready var illiana = $objects/player_units/illiana
onready var captor = $objects/enemy_units/captor

func _ready():
	acacius.permanent = true
	illiana.permanent = true
	
func play_video() -> void:
	var target : Vector3 = acacius.coords + Vector3(2, -1, -1)
	var relay = $hex_grid.create_new_spell("relay", acacius, target, false, false)
	highlight(relay.get_highlight_cells(acacius), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	relay.cast(acacius, target)
	yield(relay, "complete_cast")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	var equilibrium = $hex_grid.create_new_spell("equilibrium", illiana, target, false, false)
	highlight(equilibrium.get_highlight_cells(illiana), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(target, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(target, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	equilibrium.cast(illiana, target)
	yield(get_tree().create_timer(7.0), "timeout")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
