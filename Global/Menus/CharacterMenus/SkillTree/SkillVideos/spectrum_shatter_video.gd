extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var grace = $objects/player_units/grace
onready var cherry = $objects/player_units/cherry
onready var captor = $objects/enemy_units/captor

var dmg : int = 0

func _ready():
	captor.request_damage()
	captor.connect("damage_dealt", self, "damage_received")
	
func play_video() -> void:
	var stab = $hex_grid.create_new_spell("stab", cherry, captor.coords, false, false)
	highlight(stab.get_highlight_cells(cherry), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	stab.cast(cherry, captor.coords)
	yield(stab, "complete_cast")
	var spectrum_shatter = $hex_grid.create_new_spell("spectrum_shatter", grace, captor.coords, \
		false, false)
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	spectrum_shatter.cast(grace, captor.coords)
	yield(spectrum_shatter, "complete_cast")
	captor.deal_damage(dmg * 2)
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
func damage_received(damage : int) -> void:
	dmg = damage
