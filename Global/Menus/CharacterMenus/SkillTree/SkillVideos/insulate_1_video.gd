extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

func _ready():
	unit.guard_left = 0
	unit.apply_ui()
	
func play_video() -> void:
	var insulate = $hex_grid.create_new_spell("insulate", unit, unit.coords, false, false)
	highlight(insulate.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(unit.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(unit.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	insulate.cast(unit, unit.coords)
	yield(insulate, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
