extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var seth = $objects/player_units/seth
onready var kin = $objects/enemy_units/kin

func _ready():
	seth.permanent = true
	seth.superposition.teleport(seth.coords + Vector3(3, 0, -3))
	
func play_video() -> void:
	var dissipate = $hex_grid.create_new_spell("dissipate", kin, seth.coords, false, false)
	highlight(dissipate.get_highlight_cells(kin), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(dissipate.get_highlight_cells(kin), "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	kin.toss_skill("dissipate")
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	kin.hold_toss()
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	var uncertainty = $hex_grid.create_new_spell("uncertainty", seth, seth.coords, false, false)
	highlight(uncertainty.get_highlight_cells(seth), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	uncertainty.cast(seth, seth.coords)
	yield(uncertainty, "complete_cast")
	kin.resume_toss()
	dissipate.cast(kin, kin.coords)
	yield(dissipate, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
