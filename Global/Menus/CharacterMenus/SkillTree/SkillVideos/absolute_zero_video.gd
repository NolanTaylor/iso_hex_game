extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var illiana = $objects/player_units/illiana
onready var captor = $objects/enemy_units/captor

func _ready():
	illiana.permanent = true
	
func play_video() -> void:
	var zero = $hex_grid.create_new_spell("absolute_zero", illiana, captor.coords, false, false)
	highlight(zero.get_highlight_cells(illiana), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(zero.get_valid_cursor_cells(illiana), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(zero.get_flex_cells(illiana, captor.coords), "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	zero.cast(illiana, captor.coords)
	yield(zero, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
