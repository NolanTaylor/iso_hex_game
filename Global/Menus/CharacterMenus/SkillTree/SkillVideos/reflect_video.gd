extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var grace = $objects/player_units/grace
onready var captor = $objects/enemy_units/captor

func _ready():
	grace.permanent = true
	
func play_video() -> void:
	var bolt = $hex_grid.create_new_spell("bolt", captor, grace.coords, false, false)
	highlight(bolt.get_highlight_cells(captor), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(grace.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(grace.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	captor.toss_skill("bolt")
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	captor.hold_toss()
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	var reflect = $hex_grid.create_new_spell("reflect", grace, captor.coords, false, false)
	reflect.cast(grace, captor.coords)
	bolt.cast(captor, captor.coords)
	captor.fizzle_toss()
	yield(bolt, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
