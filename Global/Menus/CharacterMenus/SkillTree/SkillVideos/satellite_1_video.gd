extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var cherry = $objects/player_units/cherry
onready var captor = $objects/enemy_units/captor

func _ready():
	pass
	
func play_video() -> void:
	var bolt = $hex_grid.create_new_spell("bolt", captor, cherry.coords, false, false)
	highlight(bolt.get_highlight_cells(captor), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(cherry.coords, "blue")
	highlight(unit.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(cherry.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	captor.toss_skill("bolt")
	yield(get_tree().create_timer(TOSS_TIME), "timeout")
	captor.hold_toss()
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(captor.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	var stab = $hex_grid.create_new_spell("stab", unit, captor.coords, false, false)
	stab.cast(unit, captor.coords)
	bolt.cast(captor, cherry.coords)
	captor.resume_toss()
	yield(stab, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
