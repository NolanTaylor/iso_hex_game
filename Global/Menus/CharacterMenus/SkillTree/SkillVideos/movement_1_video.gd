extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

func _ready():
	pass
	
func play_video() -> void:
	var target : Vector3 = unit.coords + Vector3(3, -2, -1)
	grid[unit.coords].set_occupying_unit(unit)
	highlight($hex_grid.flood_fill(unit.coords, unit.movement, "enemy_units"), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	hide_path()
	$hex_grid.de_gayify_the_map()
	var path = $hex_grid.find_path(unit.coords, target, "enemy_units", unit.movement)
	unit.move(path, 0)
	yield(unit, "complete_movement")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
