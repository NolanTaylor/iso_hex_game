extends "res://Global/Menus/CharacterMenus/SkillTree/spell_video.gd"

onready var esmeray = $objects/player_units/esmeray

func _ready():
	esmeray.health_left = 1
	esmeray.apply_ui()
	
func play_video() -> void:
	var restoration = $hex_grid.create_new_spell("restoration", unit, esmeray.coords, false, false)
	highlight(restoration.get_highlight_cells(unit), "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	highlight(esmeray.coords, "blue")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	highlight(esmeray.coords, "red")
	yield(get_tree().create_timer(HIGHLIGHT_TIME), "timeout")
	$hex_grid.de_gayify_the_map()
	restoration.cast(unit, esmeray.coords)
	yield(restoration, "complete_cast")
	yield(get_tree().create_timer(LOOP_TIME), "timeout")
	emit_signal("video_finished")
	
