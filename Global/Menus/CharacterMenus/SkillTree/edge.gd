extends Line2D

const GRAVITY : float = 1.0
const SPRING_MAX_LENGTH : float = 8.0
const SPRING_REST_LENGTH : float = 4.0
const SPRING_CONSTANT : float = 2.0
const DAMPING_COEFFICIENT : float = 0.1
const LINE_COLOR : Color = Color("#a10c0c")

var seg : PoolVector2Array = PoolVector2Array([])

var p0_in : Vector2 = Vector2(0, 0)
var p0_v : Vector2 = Vector2(0, 0)
var p0_out : Vector2 = Vector2(0, 0)

var p1_in : Vector2 = Vector2(0, 0)
var p1_v : Vector2 = Vector2(0, 0)
var p1_out : Vector2 = Vector2(0, 0)

var control_point : Vector2 = Vector2(8, 8)
var cp_velocity : Vector2 = Vector2(0, 0)

var curve : Curve2D = Curve2D.new()

func _ready():
	pass
	
func _process(delta):
	curve.clear_points()
	
	cp_velocity.y += GRAVITY
	
	var k := SPRING_CONSTANT
	var x : Vector2 = $control_point.position - Vector2(8, 8)
	var abs_x : Vector2 = Vector2(abs(x.x), abs(x.y))
	var xm := sqrt(pow(x.x, 2) + pow(x.y, 2))
	var d := Vector2(0, SPRING_REST_LENGTH)
	var b := DAMPING_COEFFICIENT
	var v := cp_velocity
	if xm != 0:
		cp_velocity = -k*(x-d)*(abs_x/xm)-(b*v)
	
	$control_point.position += cp_velocity * delta
	control_point = $control_point.position
	control_point = control_point.floor()
	
	p0_out = control_point - p0_v
	p1_in = control_point - p1_v
	
	curve.add_point(p0_v, p0_in, p0_out)
	curve.add_point(p1_v, p1_in, p1_out)
	
	seg = curve.tessellate()
	
	update()
	
func _input(event):
	var m_pos := get_global_mouse_position()
	m_pos = m_pos - (self.global_position + Vector2(8, 8))
	
	if pow(m_pos.x, 2) + pow(m_pos.y, 2) <= pow(8, 2):
		if event is InputEventMouseMotion:
			cp_velocity -= event.speed * 8
	
# stolen code yipeee :D
func _draw():
	for i in seg.size() - 1:
		var p1 = seg[i]
		var p2 = seg[i + 1]
		
		draw_line(p1, p2, LINE_COLOR, -1.0)
		
#		if p2.x - p1.x == 0 or p2.y - p1.y == 0:
#			draw_line(p1, p2, LINE_COLOR, 1.01)
#		if abs(p2.x - p1.x) > abs(p2.y - p1.y):
#			_draw_horizontal_segments(p1, p2)
#		else:
#			_draw_vertical_segments(p1, p2)
		
func _draw_horizontal_segments(p1 : Vector2, p2 : Vector2):
	var width := abs((p2.x - p1.x) / (p2.y - p1.y)) * sign(p2.x - p1.x)
	if width == 0:
		width = 1
	
	var float_x := p1.x
	
	var sign_y := sign(p2.x - p1.x)
	for int_y in abs((p2.x - p1.x) / width):
		var y : int = (int_y - p1.y) * sign_y
		var ax := int(float_x)
		var bx := int(float_x + width)
		
		var a := Vector2(ax, y)
		var b := Vector2(bx, y)
		draw_line(a, b, LINE_COLOR, 1.01)
		
		float_x += width
		
func _draw_vertical_segments(p1 : Vector2, p2 : Vector2):
	var height := abs((p2.x - p1.x) / (p2.y - p1.y)) * sign(p2.y - p1.y)
	if height == 0:
		height = 1
	
	var float_y := p1.y
	
	var sign_x := sign(p2.x - p1.x)
	for int_x in abs((p2.x - p1.x) / height):
		var x : int = (int_x - p1.x) * sign_x
		var ay := int(float_y)
		var by := int(float_y + height)
		
		var a := Vector2(x, ay)
		var b := Vector2(x, by)
		draw_line(a, b, LINE_COLOR, 1.01)
		
		float_y += height
	
func set_line() -> void:
	p0_v = self.points[0] + Vector2(8, 8)
	p1_v = self.points[1] + Vector2(8, 8)
	
	self.default_color = Color(0, 0, 0, 0)
	$control_point/editor_icon.hide()
