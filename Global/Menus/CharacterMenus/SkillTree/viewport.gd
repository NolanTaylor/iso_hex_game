extends Viewport

var video : PackedScene

func _ready():
	$camera.current = true
	
func create_video(new_video : PackedScene) -> void:
	video = new_video
	var video_obj = video.instance()
	video_obj.connect("video_finished", self, "recreate_video", [video_obj])
	add_child(video_obj)
	$camera.current = true
	
func recreate_video(old_video) -> void:
	remove_child(old_video)
	old_video.queue_free()
	var new_video_obj = video.instance()
	new_video_obj.connect("video_finished", self, "recreate_video", [new_video_obj])
	add_child(new_video_obj)
	$camera.current = true
