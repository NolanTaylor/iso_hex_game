extends Node2D

const STICKY_LEFT : Array = [ # <Vector2>
	Vector2(17, 170),
	Vector2(17, 154),
	Vector2(18, 138),
	Vector2(19, 122),
]

const STICKY_RIGHT : Array = [ # <Vector2>
	Vector2(145, 170),
	Vector2(145, 154),
	Vector2(145, 138),
	Vector2(145, 122),
]

var skill_tag = preload("res://Global/Menus/CharacterMenus/SkillTree/tag.tscn")

var char_name = "grace"

func _ready():
	pass
	
func customize(skill_name : String):
	var data : Dictionary = Data.SKILLS[skill_name]
	var video = load( \
	"res://Global/Menus/CharacterMenus/SkillTree/SkillVideos/{0}_video.tscn".format([skill_name]))
	
	var left_count : int = 0
	var right_count : int = 0
	
	$name.text = data["name"]
	$description.bbcode_text = data["description"]
	if data["prereqs"] != "":
		$prereqs.show()
		$prereqs.bbcode_text = "Prerequisites:\n" + data["prereqs"]
		left_count += 1
		right_count += 1
	else:
		$prereqs.hide()
		
	for tag in data["tags"]:
		var new_tag = skill_tag.instance()
		
		match get_side(tag):
			"left":
				new_tag.position = STICKY_LEFT[left_count]
				left_count += 1
			"right":
				new_tag.position = STICKY_RIGHT[right_count]
				right_count += 1
				
		new_tag.set_text(tag, skill_name)
		
		$tags.add_child(new_tag)
		
	if !Globals.is_valid(video):
		return
	$viewport_container/viewport.create_video(video)
	
func get_side(tag : String) -> String:
	if "light" in tag or \
	"work" in tag or \
	"flux" in tag or \
	"entropy" in tag or \
	"rapidity" in tag or \
	"lune" in tag or \
	"corruption" in tag:
		return "right"
		
	if "action".to_lower() in tag.to_lower():
		return "right"
		
	if tag in ["Passive", "Condition", "Resource", "Movement", "Terrain", "Modifier"]:
		return "right"
		
	return "left"
	
func disable_unlock() -> void:
	$unlock_button.disabled = true
	
func enable_unlock() -> void:
	$unlock_button.disabled = false
	
func show_points() -> void:
	$points.show()
	
func hide_points() -> void:
	$points.hide()
	
func set_points(points : int) -> void:
	$points/points_label.text = str(points)
	
