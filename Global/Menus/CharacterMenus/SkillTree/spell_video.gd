extends "res://Global/Levels/level.gd"

const HIGHLIGHT_TIME : float = 0.6
const TOSS_TIME : float = 0.3
const LOOP_TIME : float = 1.2

export var placeholder_unit : bool = false
export var aux_skill : String = "null"

onready var grid = $hex_grid.grid

var unit
var old_groups : Dictionary = { # <String, Array<unit>>
	"PlayerUnits": [],
	"EnemyUnits": [],
	"NeutralUnits": [],
}

signal video_finished

func _ready():
	for p in get_tree().get_nodes_in_group("PlayerUnits"):\
		if p.get_parent() != $objects/player_units:
			old_groups["PlayerUnits"].append(p)
			p.remove_from_group("PlayerUnits")
			p.remove_from_group("AllUnits")
	for e in get_tree().get_nodes_in_group("EnemyUnits"):
		if e.get_parent() != $objects/enemy_units:
			old_groups["EnemyUnits"].append(e)
			e.remove_from_group("EnemyUnits")
			e.remove_from_group("AllUnits")
	for n in get_tree().get_nodes_in_group("NeutralUnits"):
		if n.get_parent() != $objects/neutral_units:
			old_groups["NeutralUnits"].append(n)
			n.remove_from_group("NeutralUnits")
			n.remove_from_group("AllUnits")
		
	if placeholder_unit:
		var grid = Globals.get_hex_grid()
		var char_name : String = get_node("../../../").char_name
		var unit_packed = load("res://Units/Characters/{0}.tscn".format([char_name]))
		var placeholder = $objects/player_units/placeholder
		unit = unit_packed.instance()
		unit.position = placeholder.position
		unit.permanent = true
		grid.grid[unit.find_grid()].set_occupying_unit(unit)
		unit.add_to_group("PlayerUnits")
		$objects/player_units.add_child(unit)
		placeholder.queue_free()
		
		if not Config.characters[char_name]["skills"].has(aux_skill):
			unit.init_skill(aux_skill)
			unit.recheck_max_health()
			
	yield(get_tree().create_timer(2.0), "timeout")
	play_video()
	
func play_video() -> void:
	# abstract function
	pass
	
func highlight(cells, color : String) -> void:
	if typeof(cells) == TYPE_VECTOR3:
		if grid.has(cells):
			grid[cells].set_highlight(color)
	elif typeof(cells) == TYPE_VECTOR3_ARRAY:
		for coord in cells:
			if grid.has(coord):
				grid[coord].set_highlight(color)
				
func _on_root_spell_video_tree_exiting():
	for p in get_tree().get_nodes_in_group("PlayerUnits"):
		p.remove_from_group("PlayerUnits")
	for e in get_tree().get_nodes_in_group("EnemyUnits"):
		e.remove_from_group("EnemyUnits")
	for n in get_tree().get_nodes_in_group("NeutralUnits"):
		n.remove_from_group("NeutralUnits")
		
	for key in old_groups.keys():
		for u in old_groups[key]:
			u.add_to_group(key)
			u.add_to_group("AllUnits")
