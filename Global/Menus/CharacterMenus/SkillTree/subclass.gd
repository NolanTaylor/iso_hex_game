extends Node2D

func _ready():
	pass
	
func show_all() -> void:
	show()
	$edges.show()
	$vertices.show()
	
func hide_all() -> void:
	hide()
	$edges.hide()
	$vertices.hide()
