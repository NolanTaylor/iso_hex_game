extends Node2D

export var texture : Texture
export var rarity : String = "common"

var editable : bool = false

signal unequip(object)
signal collapse

func _ready():
	pass
	
func disable_unequip_button() -> void:
	$unequip_button.hide()
	$unequip_button/unequip_button.disabled = true
	
func enable_unequip_button() -> void:
	$unequip_button.show()
	$unequip_button/unequip_button.disabled = false
	
func _on_unequip_button_pressed():
	if editable:
		emit_signal("unequip", self)
	
func _on_collapse_button_pressed():
	emit_signal("collapse")
