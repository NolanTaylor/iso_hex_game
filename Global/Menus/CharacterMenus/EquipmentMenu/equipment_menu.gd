extends Node2D

onready var char_name = get_parent().char_name
onready var empty = preload("res://Assets/GUI/equipment_menu_blank_slot.png")
onready var inv_item = preload("res://Global/Menus/UIMenus/inventory_item.tscn")
onready var equipment = Config.characters[char_name]["equipment"]
onready var inventory = Config.storage

var editable : bool = false
var current_hover : String = "null"
var holding_item = null
var curr_key : Sprite = null
var offset : Vector2 = Vector2.ZERO
var current_index : int = 0
var equip_menu_up : bool = false
var rects : Dictionary = {} # <Sprite, Rect2>

var equipment_objects : Dictionary = { # <String, Object>
	"head": null,
	"torso": null,
	"left_arm": null,
	"right_arm": null,
	"legs": null,
	"inventory_1": null,
	"inventory_2": null,
	"inventory_3": null,
	"inventory_4": null,
	"inventory_5": null,
	"inventory_6": null,
	"inventory_7": null,
	"rune_1": null,
	"rune_2": null,
	"rune_3": null,
}

var color_code : Dictionary = { # <String, Color>
	"common": Color("ffffff"),
	"uncommon": Color("7ddee4"),
	"rare": Color("f8b642"),
	"epic": Color("926fee"),
	"unique": Color("ef992a"),
}

var popups : Dictionary = {} # <String, PackedScene>

func _ready():
	set_equipment_all()
	set_inventory()
	
	var z : Vector2 = Vector2(4, 5)
	rects[$head] = Rect2($head.position, $head/head_button.rect_size)
	rects[$torso] = Rect2($torso.position, $torso/torso_button.rect_size)
	rects[$left_arm] = Rect2($left_arm.position, $left_arm/left_arm_button.rect_size)
	rects[$right_arm] = Rect2($right_arm.position, $right_arm/right_arm_button.rect_size)
	rects[$legs] = Rect2($legs.position, $legs/legs_button.rect_size)
	rects[$inventory_1] = Rect2($inventory_1.position + z, $inventory_1/inv_1_button.rect_size)
	rects[$inventory_2] = Rect2($inventory_2.position + z, $inventory_2/inv_2_button.rect_size)
	rects[$inventory_3] = Rect2($inventory_3.position + z, $inventory_3/inv_3_button.rect_size)
	rects[$inventory_4] = Rect2($inventory_4.position + z, $inventory_4/inv_4_button.rect_size)
	rects[$inventory_5] = Rect2($inventory_5.position + z, $inventory_5/inv_5_button.rect_size)
	rects[$inventory_6] = Rect2($inventory_6.position + z, $inventory_6/inv_6_button.rect_size)
	rects[$inventory_7] = Rect2($inventory_7.position + z, $inventory_7/inv_7_button.rect_size)
	rects[$rune_1] = Rect2($rune_1.position + z, $rune_1/rune_1_button.rect_size)
	rects[$rune_2] = Rect2($rune_2.position + z, $rune_2/rune_2_button.rect_size)
	rects[$rune_3] = Rect2($rune_3.position + z, $rune_3/rune_3_button.rect_size)
	
	$equip_mask.range_item_cull_mask = 2
	$equip_tab.light_mask = 2
	$equip_tab/equip_button.light_mask = 2
	$equip_tab/equip_button/label.light_mask = 2
	$equip_tab/body.light_mask = 2
	$equip_tab/items.light_mask = 2
	
func _process(delta):
	if Globals.is_valid(holding_item):
		if Input.is_action_just_released("select"):
			$holding_label.text = ""
			$holding_label.rect_position = Vector2.ZERO
			if Globals.is_valid(curr_key):
				curr_key.modulate.a = 1.0
				var flag : bool = true
				var slot : String = holding_item.item.gear_slot
				if curr_key.editor_description == slot:
					flag = false
				if flag:
					curr_key.texture = empty
				else:
					$equip_tab/items.remove_child(holding_item)
					var item_name : String = holding_item.item.name
					item_name = item_name.right(5)
					Config.characters[char_name]["equipment"][curr_key.name] = \
						item_name
					Config.remove_from_storage(item_name)
					set_equipment_all()
					set_inventory()
			holding_item = null
			return
			
		$holding_label.rect_position = get_global_mouse_position() - offset
		
		for key in rects.keys():
			if rects[key].has_point(get_global_mouse_position()):
				if key.texture == empty:
					curr_key = key
					key.texture = holding_item.token_texture
					key.modulate.a = 0.5
			elif key.modulate.a == 0.5:
				curr_key = null
				key.texture = empty
				key.modulate.a = 1.0
	
func set_equipment_all() -> void:
	equipment = Config.characters[char_name]["equipment"]
	
	set_equipment("head")
	set_equipment("torso")
	set_equipment("left_arm")
	set_equipment("right_arm")
	set_equipment("legs")
	set_equipment("inventory_1")
	set_equipment("inventory_2")
	set_equipment("inventory_3")
	set_equipment("inventory_4")
	set_equipment("inventory_5")
	set_equipment("inventory_6")
	set_equipment("inventory_7")
	set_equipment("rune_1")
	set_equipment("rune_2")
	set_equipment("rune_3")
	
	reset_info_text()
	
func set_equipment(slot : String) -> void:
	var sprite
	match slot:
		"head":
			sprite = $head
		"torso":
			sprite = $torso
		"left_arm":
			sprite = $left_arm
		"right_arm":
			sprite = $right_arm
		"legs":
			sprite = $legs
		"inventory_1":
			sprite = $inventory_1
		"inventory_2":
			sprite = $inventory_2
		"inventory_3":
			sprite = $inventory_3
		"inventory_4":
			sprite = $inventory_4
		"inventory_5":
			sprite = $inventory_5
		"inventory_6":
			sprite = $inventory_6
		"inventory_7":
			sprite = $inventory_7
		"rune_1":
			sprite = $rune_1
		"rune_2":
			sprite = $rune_2
		"rune_3":
			sprite = $rune_3
		
	var obj = get_equipment_obj(slot)
	if !Globals.is_valid(obj):
		sprite.texture = empty
	else:
		equipment_objects[slot] = obj.instance()
		sprite.texture = equipment_objects[slot].texture
		sprite.modulate = color_code[equipment_objects[slot].rarity]
	
func set_inventory() -> void:
	inventory = Config.storage
	
	for child in $equip_tab/items.get_children():
		$equip_tab/items.remove_child(child)
		child.queue_free()
		
	for item in inventory:
		var new_item = inv_item.instance()
		new_item.use_parent_material = true
		new_item.light_mask = 2
		new_item.position = Vector2(0, 29)
		new_item.position.y += $equip_tab/items.get_child_count() * 12
		new_item.instance_item(item, "null", "item")
		new_item.get_node("button").connect("button_down", self, "item_hold", [new_item])
		$equip_tab/items.add_child(new_item)
	
func reset_info_text() -> void:
	if $popup.get_child_count() >= 1 and !$tween.is_active():
		current_hover = "null"
		current_index = 0
		
		var child_to_tween = $popup.get_child(0)
		$tween.interpolate_property(child_to_tween, "position:y", 72, 200, 0.4, Tween.TRANS_QUAD)
		$tween.connect("tween_completed", self, "kill_popup_after_tween", [ ], CONNECT_ONESHOT)
		$tween.start()
		
func show_equipment(type : String, index : int = 0) -> void:
	var popup = equipment_objects[type]
	
	if !Globals.is_valid(popup):
		if not $tween.is_active():
			reset_info_text()
			
		return
		
	if $popup.get_child_count() > 0:
		if $popup.get_child(0).name == popup.name:
			return
		else:
			reset_info_text()
			yield($tween, "tween_completed")
			
	current_hover = type
	current_index = index
	
	if equip_menu_up:
		equip_menu_up = false
		collapse_equip_menu()
	
	var new_popup = popup.duplicate()
	new_popup.position = Vector2(177, 200)
	new_popup.editable = editable
	if editable:
		new_popup.enable_unequip_button()
	else:
		new_popup.disable_unequip_button()
	new_popup.connect("unequip", self, "unequip_item")
	new_popup.connect("collapse", self, "reset_info_text")
	$popup.add_child(new_popup)
	$tween.interpolate_property(new_popup, "position:y", 200, 72, 0.4, Tween.TRANS_QUAD)
	$tween.start()
	
func item_hold(item) -> void:
	if Globals.is_valid(holding_item):
		return
		
	holding_item = item
	offset = get_global_mouse_position() - item.get_node("name_label").rect_global_position
	$holding_label.text = item.item.item_name
	$holding_label.rect_position = item.get_node("name_label").rect_global_position
	
func unequip_item(item) -> void:
	Config.storage.append(equipment[current_hover])
	
	equipment_objects[current_hover] = null
	Config.characters[char_name]["equipment"][current_hover] = "null"
	
	set_equipment_all()
	set_inventory()
	reset_info_text()
	
func get_equipment_obj(type : String) -> PackedScene:
	var equipment_name : String = equipment[type]
	var popup : PackedScene = null
	
	if equipment_name in popups.keys():
		popup = popups[equipment_name]
	else:
		popup = load("res://Global/Menus/CharacterMenus/EquipmentMenu/EquipmentPopups/" + \
			equipment_name + "_popup.tscn")
		popups[equipment_name] = popup
		
	return popup
	
func kill_popup_after_tween(node, nodepath) -> void:
	if $popup.get_child_count() >= 1:
		var child_to_kill = $popup.get_child(0)
		$popup.remove_child(child_to_kill)
		child_to_kill.queue_free()
		
func enter_routine() -> void:
	set_equipment_all()
	set_inventory()
		
func exit_routine() -> void:
	kill_popup_after_tween(null, null)
	equip_menu_up = false
	$equip_tab.position = Vector2(193, 177)
	
func raise_equip_menu() -> void:
	$tween.interpolate_property($equip_tab, "position:y", 178, 70, 0.4, Tween.TRANS_QUAD)
	$tween.start()
	
func collapse_equip_menu() -> void:
	$tween.interpolate_property($equip_tab, "position:y", 70, 177, 0.4, Tween.TRANS_QUAD)
	$tween.start()
	
func _on_background_button_pressed():
	reset_info_text()
	
func _on_head_button_pressed():
	show_equipment("head")
	
func _on_torso_button_pressed():
	show_equipment("torso")
	
func _on_right_arm_button_pressed():
	show_equipment("right_arm")
	
func _on_left_arm_button_pressed():
	show_equipment("left_arm")
	
func _on_legs_button_pressed():
	show_equipment("legs")
	
func _on_inv_1_button_pressed():
	show_equipment("inventory_1")
	
func _on_inv_2_button_pressed():
	show_equipment("inventory_2")
	
func _on_inv_3_button_pressed():
	show_equipment("inventory_3")
	
func _on_inv_4_button_pressed():
	show_equipment("inventory_4")
	
func _on_inv_5_button_pressed():
	show_equipment("inventory_5")
	
func _on_inv_6_button_pressed():
	show_equipment("inventory_6")
	
func _on_inv_7_button_pressed():
	show_equipment("inventory_7")
	
func _on_rune_1_button_pressed():
	show_equipment("rune_1")
	
func _on_rune_2_button_pressed():
	show_equipment("rune_2")
	
func _on_rune_3_button_pressed():
	show_equipment("rune_3")
	
func _on_equip_button_pressed():
	if $popup.get_child_count() > 0:
		return
	if equip_menu_up:
		equip_menu_up = false
		collapse_equip_menu()
	else:
		equip_menu_up = true
		raise_equip_menu()
