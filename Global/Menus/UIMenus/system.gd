extends Node2D

var page : int = 0

func _ready():
	pass
	
func flip_page(dir : bool) -> void:
	if dir:
		page += 1
	else:
		page -= 1
		
	hide_all_pages()
	match page:
		0:
			$left_button.hide()
			$system.show()
		1:
			$left_button.show()
			$right_button.show()
			$controls.show()
		2:
			$right_button.hide()
			$display_audio.show()
		
func hide_all_pages() -> void:
	$system.hide()
	$controls.hide()
	$display_audio.hide()
	
func pull_up() -> void:
	show()
	$tween.interpolate_property(self, "position:y", 200, 0, 1.0, Tween.TRANS_SINE)
	$tween.start()
	
func exit_routine() -> void:
	page = 0
	hide_all_pages()
	$left_button.hide()
	$right_button.show()
	$system.show()
	
func _on_x_button_pressed():
	get_parent().show_tabs()
	$tween.interpolate_property(self, "position:y", 0, 200, 1.0, Tween.TRANS_SINE)
	$tween.start()
	
func _on_left_button_pressed():
	flip_page(false)
	
func _on_right_button_pressed():
	flip_page(true)
