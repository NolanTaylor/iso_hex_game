extends Node2D

const SORT_POS_Y : Array = [13, 25, 37, 49]
const UP_ARROW = preload("res://Assets/GUI/menu_up.png")
const DOWN_ARROW = preload("res://Assets/GUI/menu_down.png")

var menu_down : bool = false

func _ready():
	collect_items()
	
func collect_items() -> void:
	# grab items from config
	connect_items()
	
func connect_items() -> void:
	for child in $items.get_children():
		child.get_node("button").connect("pressed", self, "popup", [child, child.position.y])
		
func popup(item, item_pos_y) -> void:
	$popup.position.y = item_pos_y - 1
	$popup.show()
	$tween.interpolate_property($popup, "position:x", 183, 236, 0.4, Tween.TRANS_ELASTIC)
	$tween.start()
	
func popdown() -> void:
	$tween.interpolate_property($popup, "position:x", 236, 183, 0.4, Tween.TRANS_ELASTIC)
	$tween.start()
	
func pull_up() -> void:
	show()
	$popup.hide()
	$tween.interpolate_property(self, "position:y", 200, 0, 1.0, Tween.TRANS_SINE)
	$tween.start()
	
func _on_x_button_pressed():
	get_parent().show_tabs()
	$tween.interpolate_property(self, "position:y", 0, 200, 1.0, Tween.TRANS_SINE)
	$tween.start()
	
func _on_bg_button_pressed():
	if $popup.position.x > 180:
		popdown()
	
func _on_sort_by_button_pressed():
	if menu_down:
		menu_down = false
		for i in range(1, 5):
			$tween.interpolate_property($sort_menu.get_child(i), "position:y", \
				SORT_POS_Y[i-1], 0, 0.1 * i, Tween.TRANS_LINEAR)
		$sort_menu/sort_by_sticky/arrow.texture = DOWN_ARROW
	else:
		menu_down = true
		for i in range(1, 5):
			$sort_menu.get_child(i).show()
			$tween.interpolate_property($sort_menu.get_child(i), "position:y", 0, \
				SORT_POS_Y[i-1], 0.1 * i, Tween.TRANS_LINEAR)
			$sort_menu/sort_by_sticky/arrow.texture = UP_ARROW
	$tween.connect("tween_all_completed", self, "sort_expand_finished", [ ], CONNECT_ONESHOT)
	$tween.start()
	
func sort_expand_finished() -> void:
	if not menu_down:
		for i in range(1, 5):
			$sort_menu.get_child(i).hide()
