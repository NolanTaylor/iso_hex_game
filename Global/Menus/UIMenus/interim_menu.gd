extends Node2D

const MARKET = preload("res://Global/Menus/UIMenus/market_menu.tscn")

export var tutorial_act : int = -1

var mouse : Vector2 = Vector2(0, 0)
var current_coords : Vector3 = Vector3(0, 0, 0)
var cell_positions : PoolVector2Array = PoolVector2Array()
var control : bool = true

func _ready():
	$camera.current = true
	
	$office.toggle_cutscene_mode()
	$office.get_node("cursor").hide()
	
	for cell in $office/hex_grid.get_children():
		if cell.position.x > 40 and cell.position.x < 270 and \
			cell.position.y > 50 and cell.position.y < 155:
			cell_positions.append(cell.position)
		
	for character in $units/characters.get_children():
		character.find_grid()
		if character.has_node("character_menu"):
			character.get_node("character_menu").connect("exit_menu", self, "exit_char_menu")
			character.get_node("character_menu").set_editable()
			character.enable()
			character.get_node("sprite").play("idle")
		
	Config.save()
	
	if tutorial_act == 0:
		tween_new_paper("select_grace")
	
func _process(delta):
	if !control:
		return
		
	mouse = get_global_mouse_position()
	current_coords = Globals.pixel_to_grid(mouse)
	
	$units/cursor.position = Globals.grid_to_pixel(current_coords)
	if $units/cursor.position in cell_positions:
		$units/cursor.visible = true
	else:
		$units/cursor.visible = false
		
func _input(event):
	if event is InputEventMouseButton and event.pressed:
		if event.button_index == BUTTON_RIGHT and $units/cursor.visible:
			for character in $units/characters.get_children():
				if Globals.pixel_to_grid($units/cursor.position + \
				Vector2(10, 10)) == character.coords:
					if tutorial_act == 0:
						if character.name == "grace":
							tutorial_act = 1
							tween_new_paper("select_skill")
							character.get_node( \
								"character_menu/tabs/skills/skills_button").connect( \
								"pressed", self, "hide_select", [ ], CONNECT_ONESHOT)
						else:
							return
					if tutorial_act == 2:
						tutorial_act = 3
						tween_new_paper("hide_all")
							
					if character.has_node("character_menu"):
						character.get_node("character_menu").global_position = Vector2(0, 0)
						character.get_node("character_menu").show()
						$units/cursor.visible = false
						hide_tabs()
						control = false
						
func folder_move_finished(object : Object, key : NodePath) -> void:
	pass
	
func disable_tabs() -> void:
	for tab in $tabs.get_children():
		if tab is TextureButton:
			tab.disabled = true
		
func enable_tabs() -> void:
	for tab in $tabs.get_children():
		if tab is TextureButton:
			tab.disabled = false
			
func hide_tabs() -> void:
	control = false
	disable_tabs()
	$tween.interpolate_property($tabs, "position:y", 168, 200, 1.0, Tween.TRANS_CUBIC)
	$tween.start()
	
func show_tabs(object = null, node_path = null) -> void:
	control = true
	$tween.interpolate_property($tabs, "position:y", 200, 168, 1.0, Tween.TRANS_CUBIC)
	$tween.start()
	yield(get_tree().create_timer(1.0), "timeout")
	enable_tabs()
	
func exit_char_menu(menu) -> void:
	menu.hide()
	show_tabs()
	
	if tutorial_act == 1:
		tutorial_act = 2
		tween_new_paper("select_job")
	
func tween_new_paper(paper : String) -> void:
	if not has_node("selects"):
		return
	for child in $selects.get_children():
		if child.visible:
			$tween.interpolate_property(child, "position:y", \
				0, -48, 1.4, Tween.TRANS_ELASTIC)
	if paper == "hide_all":
		$tween.start()
		return
	if paper == "select_skill":
		$selects/select_grace.hide()
	var select_node = $selects.get_node(paper)
	select_node.show()
	$tween.interpolate_property(select_node, "position:y", \
		-48, 0, 1.4, Tween.TRANS_ELASTIC)
	$tween.start()
	
func hide_select() -> void:
	tween_new_paper("hide_all")
	
func _on_support_button_pressed():
	hide_tabs()
	$support.pull_up()
	
	if tutorial_act == 2:
		tutorial_act = 3
		tween_new_paper("hide_all")
	
func _on_market_button_pressed():
	hide_tabs()
	var new_market = MARKET.instance()
	add_child(new_market)
	
	if tutorial_act == 2:
		tutorial_act = 3
		tween_new_paper("hide_all")
	
func _on_storage_button_pressed():
	hide_tabs()
	$storage.pull_up()
	
	if tutorial_act == 2:
		tutorial_act = 3
		tween_new_paper("hide_all")
	
func _on_system_button_pressed():
	hide_tabs()
	$system.pull_up()
	
	if tutorial_act == 2:
		tutorial_act = 3
		tween_new_paper("hide_all")
