extends Node2D

var item = null
var token_texture : Texture

func _ready():
	light_mask = 2
	$box.light_mask = 2
	$name_label.light_mask = 2
	$owner_label.light_mask = 2
	$button.light_mask = 2
	$equipped_button.light_mask = 2
	
func instance_item(item_name : String, item_owner : String, gear_slot : String) -> void:
	var package = load("res://Items/{0}.tscn".format([item_name]))
	var note = load( \
		"res://Global/Menus/CharacterMenus/EquipmentMenu/EquipmentPopups/{0}_popup.tscn".format(\
			[item_name]))
	if package == null or note == null:
		return
	item = package.instance()
	token_texture = note.instance().texture
	$name_label.text = item.item_name
	$owner_label.text = item_owner
