extends Node2D

func _ready():
	pass
	
func pull_up() -> void:
	show()
	$tween.interpolate_property(self, "position:y", 200, 0, 1.0, Tween.TRANS_SINE)
	$tween.start()

func _on_x_button_pressed():
	get_parent().show_tabs()
	$tween.interpolate_property(self, "position:y", 0, 200, 1.0, Tween.TRANS_SINE)
	$tween.start()
