extends Node2D

const THUMBTACK_DOWN = preload("res://Assets/GUI/menu_thumbtack.png")
const THUMBTACK_UP = preload("res://Assets/GUI/menu_thumbtack_raise.png")

var pulling_out : bool = false

func _ready():
	show()
	$board_body.mode = RigidBody2D.MODE_RIGID
	
func _physics_process(delta):
	if pulling_out:
		$hang_point_left.move_and_slide(Vector2(0, -240))
		$hang_point_right.move_and_slide(Vector2(0, -240))
		
		if $hang_point_left.position.y < -200:
			hide()
			pulling_out = false
			get_parent().remove_child(self)
			queue_free()
			
func hide_all_menus() -> void:
	$board_body/gear.hide()
	$board_body/items.hide()
	$board_body/runes.hide()
	$board_body/sell.hide()
	
func _on_x_button_pressed():
	get_parent().show_tabs()
	pulling_out = true
	
func _on_gear_button_pressed():
	hide_all_menus()
	yield(get_tree().create_timer(0.1), "timeout")
	$board_body/gear.show()
	
func _on_items_button_pressed():
	hide_all_menus()
	yield(get_tree().create_timer(0.1), "timeout")
	$board_body/items.show()
	
func _on_runes_button_pressed():
	hide_all_menus()
	
func _on_sell_button_pressed():
	hide_all_menus()
	
func _on_gear_button_mouse_entered():
	$board_body/tabs/gear/gear_thumbtack.texture = THUMBTACK_UP
	
func _on_gear_button_mouse_exited():
	$board_body/tabs/gear/gear_thumbtack.texture = THUMBTACK_DOWN
	
func _on_items_button_mouse_entered():
	$board_body/tabs/items/items_thumbtack.texture = THUMBTACK_UP
	
func _on_items_button_mouse_exited():
	$board_body/tabs/items/items_thumbtack.texture = THUMBTACK_DOWN
	
func _on_runes_button_mouse_entered():
	$board_body/tabs/runes/runes_thumbtack.texture = THUMBTACK_UP
	
func _on_runes_button_mouse_exited():
	$board_body/tabs/runes/runes_thumbtack.texture = THUMBTACK_DOWN
	
func _on_sell_button_mouse_entered():
	$board_body/tabs/sell/sell_thumbtack.texture = THUMBTACK_UP
	
func _on_sell_button_mouse_exited():
	$board_body/tabs/sell/sell_thumbtack.texture = THUMBTACK_DOWN
