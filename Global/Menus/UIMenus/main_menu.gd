extends Node2D

onready var x_pos : int = $new_game.position.x

var press_any : bool = true

func _ready():
	pass
	
func _input(event):
	if not press_any:
		return
		
	if event is InputEventKey or event is InputEventMouseButton:
		press_any = false
		$continue_label.hide()
		$new_game.show()
		$load.show()
		$system.show()
		$tween.interpolate_property($new_game, "position:x", -80, x_pos, 0.4, Tween.TRANS_ELASTIC)
		$tween.interpolate_property($load, "position:x", -80, x_pos, 0.6, Tween.TRANS_ELASTIC)
		$tween.interpolate_property($system, "position:x", -80, x_pos, 0.8, Tween.TRANS_ELASTIC)
		$tween.start()
	
func _on_new_game_button_pressed():
	$black.modulate.a = 0.0
	$black.show()
	$tween.interpolate_property($black, "modulate:a", 0.0, 1.0, \
		1.0, Tween.TRANS_LINEAR)
	$tween.start()
	yield(get_tree().create_timer(1.0), "timeout")
	get_tree().change_scene("res://Global/Menus/Dialog/Tutorial/dialog_tutorial_1.tscn")
	
func _on_load_button_pressed():
	pass # Replace with function body.
	
func _on_system_button_pressed():
	pass # Replace with function body.
