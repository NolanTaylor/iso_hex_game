extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["show", "fil"],
		["show", "phrank"],
		["move", "fil", PoolVector2Array([Vector2(207, 24), Vector2(230, 32), Vector2(230, 48), \
			Vector2(230, 64)]), "continue"],
		["pause", 0.1],
		["move", "phrank", PoolVector2Array([Vector2(207, 24), Vector2(230, 32), Vector2(253, 40)])],
		["pause", 0.8],
		["show", "body_bag"],
		["transition", "fade_in"],
		["phrank", BOTTOM_RIGHT, false, "Well at least she's still alive, didn't\nknow if the enchantments would last\nthis long."],
		["transition", "fade_out"],
		["move", "grace", PoolVector2Array([Vector2(-23, 168), Vector2(23, 152)])],
		["transition", "fade_in"],
		["grace", TOP_LEFT, true, "Wow, the fog really settles low down\nhere this early."],
		["transition", "fade_out"],
		["move", "grace", PoolVector2Array([Vector2(46, 144), Vector2(69, 136), Vector2(69, 120), \
			Vector2(92, 112), Vector2(138, 96)])],
		["transition", "fade_in"],
		["fil", BOTTOM_LEFT, true, "Hi!"],
		["grace_hmm", TOP_LEFT, true, "What's in the bag?"],
		["fil", BOTTOM_LEFT, true, "A carpet..."],
		["grace_unamused", TOP_LEFT, true, "The carpet's wearing shoes?"],
		["phrank", BOTTOM_RIGHT, false, "Fil, hurry up. Stop chatting with\nrandom people."],
		["fil", BOTTOM_LEFT, true, "I think she's a cop, Phrank."],
		["phrank", BOTTOM_RIGHT, false, "Cops don't come down here, too poor\nfor them."],
		["grace_lookaway", TOP_LEFT, true, "We're an independent rescue service."],
		["fil", BOTTOM_LEFT, true, "Oh I've heard of those!"],
		["phrank", BOTTOM_RIGHT, false, "Fine, whatever, kill her. We need to\nbe at the west bridge by noon."],
		["transition", "fade_out"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/tutorial.tscn")
