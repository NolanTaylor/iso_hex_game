extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["move", "grace", PoolVector2Array([Vector2(207, 72), Vector2(207, 88), Vector2(184, 96), \
			Vector2(161, 104)])],
		["pause", 1.0], # possibly replace with pick up papers animation
		["transition", "fade_in"],
		["cherry", BOTTOM_LEFT, true, "It's like 6:00, what are you doing\nhere so early?"],
		["grace", BOTTOM_RIGHT, false, "Just looking for work, is there\nanything I can do?"],
		["cherry", BOTTOM_LEFT, true, "Wait for the rest of the excavators\nto get in, you shouldn't be taking any\njobs on your own."],
		["grace_lookaway", BOTTOM_RIGHT, false, "C'mon, I can handle something small,\nyou must have something for me."],
		["cherry", BOTTOM_LEFT, true, "You're still new Grace. Just enjoy\nyour morning and you can take a job\nwhen the others get here."],
		["grace_pleading", BOTTOM_RIGHT, false, "Pleeaaase! I know you have so much\nwork piled up. Let me take something."],
		["cherry_lookaway", BOTTOM_LEFT, true, "..."],
		["transition", "fade_out"],
		["move", "cherry", PoolVector2Array([Vector2(46, 80), Vector2(46, 96), Vector2(46, 128)])],
		["pause", 1.0],
		["transition", "fade_in"],
		["cherry", BOTTOM_LEFT, true, "Fine, here's a case we've been sitting\non for a while."],
		["cherry", BOTTOM_LEFT, true, "Cerene Souros, went missing about a\nweek ago. Last seen in district three\nbelow the East Skybridge."],
		["cherry", BOTTOM_LEFT, true, "We know there are trafficking\noperations in that area, see if you\ncan get any more information."],
		["grace_happy", BOTTOM_RIGHT, false, "Thank you Cherry, you're the best!"],
		["transition", "fade_out"],
		["move", "grace", PoolVector2Array([Vector2(161, 88), Vector2(184, 80), Vector2(207, 72), \
			Vector2(207, 56)])],
		["transition", "fade_in"],
		["cherry", BOTTOM_LEFT, true, "And Grace."],
		["grace", BOTTOM_RIGHT, false, "... yeah?"],
		["cherry", BOTTOM_LEFT, true, "Don't die."],
		["grace_dismiss", BOTTOM_RIGHT, false, "Pff. You worry too much."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Tutorial/dialog_tutorial_2.tscn")
