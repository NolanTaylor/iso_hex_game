extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["pause", 2.0],
		["play_animation", $level/objects/obstacles/grace_on_bed/anim, "default"],
		["pause", 1.0],
		["transition", "fade_in"],
		["grace_tired", BOTTOM_RIGHT, false, "Ugh."],
		["cherry", BOTTOM_LEFT, true, "I thought I told you not to die."],
		["grace_pout", BOTTOM_RIGHT, false, "It was foggy and I was caught off\nguard."],
		["grace_pleading", BOTTOM_RIGHT, false, "Let me try again, I know where\nthey're going."],
		["cherry", BOTTOM_LEFT, true, "It's already done, I sent Seth on it."],
		["grace_raise", BOTTOM_RIGHT, false, "What? And Cerene?"],
		["cherry_lookaway", BOTTOM_LEFT, true, "Seth killed her, she's been\nresurrected and returned to her\nfamily."],
		["grace_lookaway", BOTTOM_RIGHT, false, "What time is it?"],
		["cherry", BOTTOM_LEFT, true, "19:00, everyone else has already\ngone home."],
		["grace_surprise", BOTTOM_RIGHT, false, "Why so late!?"],
		["cherry_lookaway", BOTTOM_LEFT, true, "It got busy today, guess nobody had\nthe time."],
		["grace_down", BOTTOM_RIGHT, false, "..."],
		["transition", "fade_out"],
		["pause", 0.4], # play cherry standing up animation
		["hide", "cherry_sitting"],
		["show", "cherry"],
		["move", "cherry", PoolVector2Array([Vector2(115, 72), Vector2(115, 88), Vector2(115, 104), \
			Vector2(92, 112), Vector2(69, 120), Vector2(69, 136), Vector2(69, 152)])],
		["transition", "fade_in"],
		["cherry", TOP_LEFT, true, "Anyway, go get some sleep, being\ndead doesn't count."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_1/dialog_level_1_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
