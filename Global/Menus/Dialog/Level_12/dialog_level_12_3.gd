extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["illiana", BOTTOM_RIGHT, false, "Thanks for your help today Acacius,\nI'm going to head up for the night if\nyou're good to lock up?"],
		["acacius", BOTTOM_LEFT, true, "Certainly. But if you don't mind me\nasking, why do you live here instead\nof with Fia?"],
		["illiana_happy", BOTTOM_RIGHT, false, "Ah, well we live together on the\nweekends."],
		["illiana_lookaway", BOTTOM_RIGHT, false, "But it's easier for me to stay here\nduring the week as her place is quite\nfar."],
		["acacius", BOTTOM_LEFT, true, "I see, that makes sense. Well\nanyways, have a good night Illy."],
		["illiana_smile", BOTTOM_RIGHT, false, "You too Acacius. And Grace."],
		["grace_neutral", BOTTOM_LEFT, false, "Hm?"],
		["illiana_happy", BOTTOM_RIGHT, false, "Have a good night."],
		["grace_lookaway", BOTTOM_LEFT, true, "Oh, yeah. Good night Illiana."],
		["transition", "fade_out"],
		["move", "acacius", PoolVector2Array([Vector2(207, 104), Vector2(207, 56), \
			Vector2(230, 48)]), "continue"],
		["pause", 0.4],
		["move", "grace", PoolVector2Array([Vector2(138, 128), Vector2(161, 120), Vector2(184, 112), \
			Vector2(184, 96), Vector2(184, 80), Vector2(207, 72), Vector2(207, 56)])],
		["pause", 0.4],
		["hide", "grace"],
		["hide", "acacius"],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_12/dialog_level_12_4.tscn")
