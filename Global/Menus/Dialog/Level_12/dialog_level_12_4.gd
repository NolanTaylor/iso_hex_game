extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"], # move grace and acacius
		["transition", "fade_in"],
		["acacius", BOTTOM_RIGHT, false, "What's on your mind Grace?"],
		["grace_neutral", BOTTOM_LEFT, true, "What?"],
		["acacius", BOTTOM_RIGHT, false, "You seem distracted, is something\nbothering you?"],
		["grace_lookaway", BOTTOM_LEFT, true, "Oh, nothing. I was just thinking."],
		["acacius", BOTTOM_RIGHT, false, "Well, what thoughts have got you so\nenraptured then?"],
		["grace_frown", BOTTOM_LEFT, true, "What would it be like in a world\nwhere people die permanently?"],
		["acacius", BOTTOM_RIGHT, false, "Oh... Uh."],
		["acacius", BOTTOM_RIGHT, false, "I'm not sure I understand."],
		["grace_hmm", BOTTOM_LEFT, true, "Like, if instead of the ethereal realm\nor resurrection, once you died it was\nlike before you were born."],
		["grace_down_left", BOTTOM_LEFT, true, "You just don't exist anymore."],
		["acacius", BOTTOM_RIGHT, false, "Do people still remember you? Or did\nyou never exist at all?"],
		["grace_neutral", BOTTOM_LEFT, true, "I assume people still remember you.\nIt's just you who's gone."],
		["acacius", BOTTOM_RIGHT, false, "Then you're not truly dead. You live\non in other people's minds."],
		["grace_furrow", BOTTOM_LEFT, true, "But you can't make new memories or\nexperience new things, you're frozen."],
		["acacius", BOTTOM_RIGHT, false, "Is that any different from being\nstuck in ethereal forever?"],
		["acacius", BOTTOM_RIGHT, false, "If you die and everybody's forgotten\nabout you there's nobody to resurrect\nyou, you're just as frozen in time."],
		["grace_neutral", BOTTOM_LEFT, true, "But you still have conscious in the\nethereal, it's just really boring."],
		["grace_down", BOTTOM_LEFT, true, "In the void you're completely gone,\nthere's nothing."],
		["acacius", BOTTOM_RIGHT, false, "That's a difficult concept to imagine."],
		["grace_down_left", BOTTOM_LEFT, true, "Yeah..."],
		["grace_neutral", BOTTOM_LEFT, true, "I think life would be a lot more\nvalued and respected."],
		["grace_lookaway", BOTTOM_LEFT, true, "To know that killing someone is to\nerase them from existence."],
		["grace_dismiss", BOTTOM_LEFT, true, "There would be no violence, no wars."],
		["acacius", BOTTOM_RIGHT, false, "..."],
		["acacius", BOTTOM_RIGHT, false, "I'm not sure Grace."],
		["acacius", BOTTOM_RIGHT, false, "The consequences of our actions\ncertainly aren't as heavy as infinite\nnothingness."],
		["acacius", BOTTOM_RIGHT, false, "But there still exist bad people who\ndo bad things."],
		["grace_furrow", BOTTOM_LEFT, true, "But to consign somebody to an\neternal void?"],
		["acacius", BOTTOM_RIGHT, false, "I'm not saying there would be mass\nkillings every day."],
		["acacius", BOTTOM_RIGHT, false, "But I believe you're greatly\noverestimating the empathy people\nhave for each other."],
		["grace_down_left", BOTTOM_LEFT, true, "I guess..."],
		["acacius", BOTTOM_RIGHT, false, "Why does this come to mind?"],
		["grace_lookaway", BOTTOM_LEFT, true, "Oh, no reason. It was just a thought."],
		["acacius", BOTTOM_RIGHT, false, "I see. Well, try not to get too caught\nup with it."],
		["acacius", BOTTOM_RIGHT, false, "The world we live in has its own\nhost of problems. I'll see you in the\nmorning Grace."],
		["grace_down", BOTTOM_LEFT, true, "Yeah, see you."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/UIMenus/Interims/interim_menu_13.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
