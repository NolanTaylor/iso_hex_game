extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["pause", 0.8],
		["show", "grace"],
		["pause", 0.8],
		["transition", "fade_in"],
		["seth", TOP_LEFT, true, "Grace, it's 10:00, what's kept you?"],
		["grace_tired", TOP_RIGHT, false, "It's so bright in here."],
		["seth", TOP_LEFT, true, "That would be called the Sun."],
		["grace_disgust", TOP_RIGHT, false, "Ugh. I have a horrible headache."],
		["fia", BOTTOM_RIGHT, false, "Have you tried killing yourself?"],
		["grace_tired", TOP_RIGHT, false, "No, I think I'll go do that though."],
		["fia", BOTTOM_RIGHT, false, "Here, let me help."],
		["transition", "fade_out"],
		["move", "fia", PoolVector2Array([Vector2(207, 136), Vector2(207, 120), Vector2(207, 72), \
			Vector2(230, 64)])],
		["pause", 0.4],
		["hide", "grace"],
		["hide", "fia"],
		["transition", "fade_out_screen"],
		["pause", 1.2],
		["transition", "fade_in_screen"],
		["pause", 0.8],
		["show", "grace"],
		["show", "fia"],
		["pause", 0.4],
		["transition", "fade_in"],
		["seth", BOTTOM_LEFT, true, "Feeling better?"],
		["grace", BOTTOM_RIGHT, false, "Yeah, back to normal. Sorry about\nthat, I'm not sure what happened this\nmorning."],
		["transition", "fade_out"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/Interims/interim_menu_12.tscn")
