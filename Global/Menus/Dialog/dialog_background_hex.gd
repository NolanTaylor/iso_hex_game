extends Sprite

const TIME : float = 4.0

var fading : bool = false

func _ready():
	pass
	
func fade_into(color : Color) -> void:
	$tween.interpolate_property(self, "modulate", Color(1.0, 1.0, 1.0), color, \
		TIME, Tween.TRANS_LINEAR)
	$tween.start()
	$timer.start(TIME)
	$timer.connect("timeout", self, "fade_out", [ ], CONNECT_ONESHOT)
	fading = true
	
func fade_out(color : Color = Color(1.0, 1.0, 1.0)) -> void:
	$tween.interpolate_property(self, "modulate", modulate, color, \
		TIME, Tween.TRANS_LINEAR)
	$timer.start(TIME)
	$timer.connect("timeout", self, "fading_false", [ ], CONNECT_ONESHOT)
	
func fading_false() -> void:
	fading = false
