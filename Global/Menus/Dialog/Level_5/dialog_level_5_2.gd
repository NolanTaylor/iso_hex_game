extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["cherry", BOTTOM_LEFT, true, "Are you sure it's safe that you keep\nbringing Foo with you?"],
		["grace", BOTTOM_RIGHT, false, "Of course, why wouldn't it be?"],
		["cherry_lookaway", BOTTOM_LEFT, true, "I mean if you were in construction\nor something and he falls off a\nbuilding, no big deal, just resurrect him."],
		["cherry", BOTTOM_LEFT, true, "But this is different. We work in\nproximity of people who kidnap and\nransom others for a living."],
		["cherry_concern", BOTTOM_LEFT, true, "Foo could very easily be taken alive\nand we won't be able to do anything."],
		["grace_frown", BOTTOM_RIGHT, false, "If he gets taken, I'll be there to kill\nhim."],
		["grace_furrow", BOTTOM_RIGHT, false, "And I'm much more comfortable\nhaving him with me where I can keep\nan eye on him than stuck in here."],
		["cherry_raise", BOTTOM_LEFT, true, "You don't think he'll be safe here?\nIlliana's got protective wards\neverywhere."],
		["grace_lookaway", BOTTOM_RIGHT, false, "I trust myself more. And I just..."],
		["grace_down_left", BOTTOM_RIGHT, false, "Don't want to let him out of my sight."],
		["cherry_roll", BOTTOM_LEFT, true, "It's up to you I guess."],
		["transition", "fade_out"],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_5/dialog_level_5_1.tscn")
