extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["move", "anastasia", Vector3(8, -1, -7), "continue"],
		["pause", 0.4],
		["move", "ozias", Vector3(7, -1, -6)],
		["pause", 0.4],
		["transition", "fade_in"],
		["anastasia", BOTTOM_RIGHT, false, "This is the place."],
		["ozias", BOTTOM_LEFT, true, "This is an old storehouse."],
		["anastasia", BOTTOM_RIGHT, true, "Yup."],
		["ozias", BOTTOM_LEFT, true, "Ana, I hate to say it but I think that scrying mage scammed us."],
		["anastasia", BOTTOM_RIGHT, false, "Nonsense, he was telling the truth. I can tell."],
		["ozias", BOTTOM_LEFT, true, "Oh, did you pick up a Detect Lies spell when I wasn't watching?"],
		["anastasia", BOTTOM_RIGHT, false, "Even better..."],
		["anastasia", BOTTOM_RIGHT, false, "Intuition!"],
		["ozias", BOTTOM_LEFT, true, "..."],
		["ozias", BOTTOM_LEFT, true, "Even if you are right what are we gonna do? Just walk inside and politely ask them to hand Vaso over?"],
		["anastasia", BOTTOM_RIGHT, false, "And when they say no we kick their ass!"],
		["ozias", BOTTOM_LEFT, true, "What if they capture us too?"],
		["anastasia", BOTTOM_RIGHT, false, "Don't worry, I've got a cyanide pill taped to the roof of my mouth."],
		["ozias", BOTTOM_LEFT, true, "I... Wait really?"],
		["anastasia", BOTTOM_RIGHT, false, "Yeah, you should have one too."],
		["ozias", BOTTOM_LEFT, true, "Ana, these are professional human traffickers. I'm sure they have someone with Cleanse Poison."],
		["anastasia", BOTTOM_RIGHT, false, "You don't get it, cyanide acts *really* fast."],
		["ozias", BOTTOM_LEFT, true, "Why can't we just pay the ransom and be on with it? You can afford it I'm sure."],
		["anastasia", BOTTOM_RIGHT, false, "It's about principle. Kidnapping people and selling them back to their family is wrong. I can't support that."],
		["anastasia", BOTTOM_RIGHT, false, "I'm going inside. You can follow me if you want."],
		["transition", "fade_out"],
		["move", "anastasia", Vector3(9, -1, -8)],
		["pause", 0.4],
		["hide", "anastasia"],
		["transition", "fade_in"],
		["ozias", BOTTOM_LEFT, true, "*sigh*"],
		["transition", "fade_out"],
		["move", "ozias", Vector3(9, -1, -8)],
		["hide", "ozias"],
		["pause", 0.4],
		["transition", "fade_out_screen"],
	]
	
func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/level_5/dialog_level_5_4.tscn")
