extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["move", "grace", PoolVector2Array([Vector2(69, 120), Vector2(92, 112), Vector2(115, 120), \
			Vector2(138, 112)])],
		["transition", "fade_in"],
		["grace", BOTTOM_LEFT, true, "Foo?"],
		["foo", BOTTOM_RIGHT, false, "Hi Grace."],
		["grace_smile", BOTTOM_LEFT, true, "Are you getting ready for bed?"],
		["foo", BOTTOM_RIGHT, false, "I was just reading."],
		["grace_hmm", BOTTOM_LEFT, true, "Oh. I didn't know Illiana kept many\nbooks around."],
		["foo", BOTTOM_RIGHT, false, "I don't really understand them,\nthey're complicated."],
		["grace_sweat_smile", BOTTOM_LEFT, true, "Well, Illiana's hundreds of years old."],
		["grace", BOTTOM_LEFT, true, "You'll be able to understand them\nsomeday."],
		["foo", BOTTOM_RIGHT, false, "Oh... She doesn't look very old."],
		["grace_smile", BOTTOM_LEFT, true, "Almost everyone uses enchantments\nto make themselves look younger."],
		["foo", BOTTOM_RIGHT, false, "Do you use enchantments?"],
		["grace_happy", BOTTOM_LEFT, true, "I use some to make my eyes pink, but\nthat's it."],
		["foo", BOTTOM_RIGHT, false, "Wow, could you change my eyes?"],
		["grace", BOTTOM_LEFT, true, "Oh, sure. What color do you want?"],
		["foo", BOTTOM_RIGHT, false, "Blue!"],
		["transition", "fade_out"],
		["move", "grace", PoolVector2Array([Vector2(138, 96), Vector2(161, 88), Vector2(161, 72)]), \
			"continue"],
		["move", "foo", PoolVector2Array([Vector2(184, 96), Vector2(184, 80)])],
		["pause", 0.6],
		["transition", "fade_in"],
		["foo", BOTTOM_RIGHT, true, "Woah..."],
		["grace_happy", BOTTOM_LEFT, true, "Haha... It's just a bit of cosmetic\nmagic."],
		["grace_hmm", BOTTOM_LEFT, true, "Although I guess you didn't learn\nmuch magic growing up?"],
		["foo", BOTTOM_RIGHT, true, "I don't really remember."],
		["grace_down_left", BOTTOM_LEFT, true, "Right..."],
		# animate grace sitting down
		["grace_lookaway", BOTTOM_LEFT, true, "I got your ancestry results back\ntonight."],
		["foo", BOTTOM_RIGHT, false, "My family thing? What did it say?"],
		["grace_furrow", BOTTOM_LEFT, true, "Nothing. There was definitely an\nerror because not even a single\nperson came up."],
		["foo", BOTTOM_RIGHT, false, "Oh..."],
		["grace_neutral", BOTTOM_LEFT, true, "We'll keep looking, but you'll be\nstaying here a bit longer."],
		["foo", BOTTOM_RIGHT, false, "That's okay, I like it here. Illiana is\nnice and I want to stay with you."],
		["grace_happy", BOTTOM_LEFT, true, "Good, I'm glad."],
		["grace_down", BOTTOM_LEFT, true, "..."],
		["grace_worried", BOTTOM_LEFT, true, "You really don't remember anything\nabout yourself?"],
		["foo", BOTTOM_RIGHT, false, "Not really, everything's kinda blurry."],
		["foo", BOTTOM_RIGHT, true, "..."],
		["foo", BOTTOM_RIGHT, true, "Bits and pieces have returned, but\nnothing makes sense."],
		["grace_neutral", BOTTOM_LEFT, true, "Can you give me an example?"],
		["foo", BOTTOM_RIGHT, true, "..."],
		["foo", BOTTOM_RIGHT, true, "I remember riding a bus to school\neveryday, but I was jealous because\nother kids had bikes."],
		["grace_frown", BOTTOM_LEFT, true, "What's a bike?"],
		["foo", BOTTOM_RIGHT, true, "It's like..."],
		["foo", BOTTOM_RIGHT, true, "A thing with two wheels. And you\npedal to make it go forward."],
		["foo", BOTTOM_RIGHT, true, "Mine was broken and my dad insisted\non trying to fix it instead of just\nbuying a new one."],
		["foo", BOTTOM_RIGHT, true, "He was always busy though..."],
		["grace_furrow", BOTTOM_LEFT, true, "You couldn't just teleport to school?"],
		["foo", BOTTOM_RIGHT, true, "I don't think we had teleportation."],
		["grace_hmm", BOTTOM_LEFT, true, "I see. Maybe you were in a smaller\ntown without the infrastructure."],
		["foo", BOTTOM_RIGHT, true, "..."],
		["grace_lookaway", BOTTOM_LEFT, true, "Well, it's late. You should go to\nsleep."],
		["foo", BOTTOM_RIGHT, false, "Are you leaving?"],
		["grace", BOTTOM_LEFT, true, "I'll be back in the morning. Have a\ngood night Foo."],
		["foo", BOTTOM_RIGHT, false, "Good night."],
		# move grace and shut door
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/UIMenus/Interims/interim_menu_6.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
