extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["move", "grace", PoolVector2Array([Vector2(-46, 32), Vector2(-23, 40), \
			Vector2(46, 64)]), "continue"],
		["pause", 0.4],
		["branch", "function_1"],
		[],
		["pause", 0.2],
		["move", "acacius", PoolVector2Array([Vector2(-46, 64), Vector2(-23, 72), \
			Vector2(23, 88)]), "continue"],
		["pause", 0.4],
		["move", "cherry", PoolVector2Array([Vector2(-46, 48), Vector2(-23, 56), \
			Vector2(23, 72)]), "continue"],
		["pause", 0.4],
		["move", "foo", PoolVector2Array([Vector2(-46, 32), Vector2(-23, 40), \
			Vector2(23, 56)]), "continue"],
		["move", "illiana", PoolVector2Array([Vector2(-46, 64), Vector2(-23, 72), \
			Vector2(0, 80)])],
		["pause", 0.6],
	]
	
func function_1() -> void:
	if Config.aux_team.has("avram"):
		dialog[4] = ["move", "avram", PoolVector2Array([Vector2(-46, 80), Vector2(-23, 88), \
			Vector2(0, 96)]), "continue"]
	else:
		dialog[4] = ["pause", 0.2]
		
func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_5.tscn")
