extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["show", "grace"],
		["move", "grace", PoolVector2Array([Vector2(207, 56), Vector2(207, 72), Vector2(184, 80)])],
		["transition", "fade_in"],
		["acacius", BOTTOM_LEFT, true, "Morning Grace."],
		["grace", TOP_RIGHT, false, "Hey Acacius."],
		["illiana_happy", BOTTOM_RIGHT, false, "Grace, good to see you in one piece."],
		["grace_lookaway", TOP_RIGHT, false, "Uh, thanks. So what's happening\ntoday?"],
		["illiana_open", BOTTOM_RIGHT, false, "Big case came in today, Seth will be\ngone for a while working on it. But\nthere's plenty of work for you."],
		["grace_neutral", TOP_RIGHT, false, "Oh, okay."],
		["transition", "fade_out"],
		["move", "acacius", PoolVector2Array([Vector2(138, 80), Vector2(184, 64)]), "continue"],
		["pause", 0.1],
		["move", "illiana", PoolVector2Array([Vector2(253, 88), Vector2(230, 80), Vector2(230, 64)]), "continue"],
		["pause", 0.1],
		["move", "grace", PoolVector2Array([Vector2(161, 88), Vector2(161, 88)])],
		["pause", 0.2],
		["transition", "fade_in"],
		["illiana_raise", BOTTOM_RIGHT, false, "And Grace?"],
		["grace", BOTTOM_LEFT, true, "Yeah?"],
		["illiana", BOTTOM_RIGHT, false, "Take Cherry with you."],
		["grace_hmm", BOTTOM_LEFT, true, "I thought she didn't like fighting."],
		["illiana_lookaway", BOTTOM_RIGHT, false, "Well, Acacius and I are busy and I\ndon't want you going alone again."],
		["grace_down_left", BOTTOM_LEFT, true, "Right..."],
		["transition", "fade_out"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/Interims/interim_menu_1.tscn")
