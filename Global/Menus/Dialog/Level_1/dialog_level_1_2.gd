extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["move", "cherry", PoolVector2Array([Vector2(92, 160), Vector2(115, 152), Vector2(138, 160), \
			Vector2(161, 152), Vector2(184, 144), Vector2(184, 128), Vector2(184, 96)])],
		["transition", "fade_in"],
		["grace_lookaway", BOTTOM_LEFT, true, "You don't have to come with me you\nknow."],
		["cherry", BOTTOM_RIGHT, false, "I don't mind."],
		["grace_neutral", BOTTOM_LEFT, true, "No really, I'll be fine on my own. I\nknow you don't like fighting."],
		["cherry_lookaway", BOTTOM_RIGHT, false, "No, you won't be fine. And seriously,\nI don't mind."],
		["grace_down_left", BOTTOM_LEFT, true, "Well, I'm just saying."],
		["cherry", BOTTOM_RIGHT, false, "I understand, thanks for your\nconcern. Now let's get going."],
		["transition", "fade_out"],
		["move", "grace", PoolVector2Array([Vector2(184, 80), Vector2(207, 72), Vector2(207, 56)]), "continue"],
		["pause", 0.1],
		["move", "cherry", PoolVector2Array([Vector2(207, 88), Vector2(230, 80), Vector2(230, 64)])],
		["pause", 0.2],
		["hide", "grace"],
		["hide", "cherry"],
		["pause", 0.2],
		["transition", "fade_out_screen"],
	]

func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_1/dialog_level_1_3.tscn")
