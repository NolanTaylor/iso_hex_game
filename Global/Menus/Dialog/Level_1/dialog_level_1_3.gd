extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	dialog = [
		["transition", "fade_in_screen"],
		["move", "grace", PoolVector2Array([Vector2(-23, 8), Vector2(0, 16), Vector2(115, 56), \
							Vector2(115, 72), Vector2(138, 80), Vector2(207, 104),]), "continue"],
		["pause", 0.4],
		["move", "cherry", PoolVector2Array([Vector2(-46, 16), Vector2(-23, 24), Vector2(115, 72), \
							Vector2(115, 88), Vector2(138, 96), Vector2(184, 112),])],
		["transition", "fade_in"],
		["cherry", BOTTOM_LEFT, true, "This is it?"],
		["grace", BOTTOM_RIGHT, false, "Yep. You ready?"],
		["cherry_lookaway", BOTTOM_LEFT, true, "How many are in there?"],
		["grace_hmm", BOTTOM_RIGHT, false, "At least a few, scrying isn't super\naccurate."],
		["cherry", BOTTOM_LEFT, true, "No use waiting I guess."],
		["transition", "fade_out_screen"], # move them down stairs
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_1.tscn")
