extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["show", "grace"],
		["show", "foo"],
		["move", "grace", PoolVector2Array([Vector2(184, 64), Vector2(138, 80)]), "continue"],
		["pause", 0.2],
		["move", "foo", PoolVector2Array([Vector2(207, 56), Vector2(207, 72), Vector2(184, 80), \
			Vector2(161, 88)])],
		["transition", "fade_in"],
		["grace_lookaway", BOTTOM_RIGHT, false, "So we tried to get a copy of city\nrecords."],
		["cherry", BOTTOM_LEFT, true, "Tried?"],
		["grace", BOTTOM_RIGHT, false, "Yeah, because we found nothing.\nThere's no information at all. The\nscanner wouldn't even recognize him."],
		["move", "foo", PoolVector2Array([Vector2(161, 72), Vector2(138, 64), Vector2(115, 56), \
			Vector2(92, 64)]), "continue"],
		["cherry", BOTTOM_LEFT, true, "Does the city even bother to keep\nup-to-date records?"],
		["grace_down_left", BOTTOM_RIGHT, false, "For as corrupt and dysfunctional as\nthis place is, they keep incredibly\ndetailed records of everyone."],
		["cherry_lookaway", BOTTOM_LEFT, true, "I guess that's not surprising."],
		["grace_neutral", BOTTOM_RIGHT, false, "So what do we do?"],
		["cherry_raise", BOTTOM_LEFT, true, "You're asking me? This was your\nidea."],
		["grace_frown", BOTTOM_RIGHT, false, "Well obviously I didn't expect this."],
		["cherry", BOTTOM_LEFT, true, "So what do you think should be done?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "There's a spare bed in the other\nroom. He can just stay here for now,\nuntil we find his parents."],
		["cherry_concern", BOTTOM_LEFT, true, "We?"],
		["grace_worried", BOTTOM_RIGHT, false, "You're not going to help?"],
		["cherry_lookaway", BOTTOM_LEFT, true, "I'm an archivist, I just organize\npapers. I don't want to be responsible\nfor an entire person."],
		["cherry", BOTTOM_LEFT, true, "Besides, we just found him earlier\ntoday. We don't even know his name."],
		["grace_smile", BOTTOM_RIGHT, false, "Hey, what's your name?"],
		["foo", TOP_LEFT, true, "Uhh..."], # animate foo turning around
		["cherry_roll", BOTTOM_LEFT, true, "He doesn't remember."],
		["grace_happy", BOTTOM_RIGHT, false, "We'll call you Foo, how does that\nsound?"],
		["foo", TOP_LEFT, true, "Foo?"],
		["grace", BOTTOM_RIGHT, false, "Yeah, just a temporary name, until\nwe find your real one."],
		["foo", TOP_LEFT, true, "Foo, I like that."],
		["grace_smug", BOTTOM_RIGHT, false, "See Cherry, now you know his name."],
		["cherry_narrow", BOTTOM_LEFT, true, "*sigh* I'll do what I can, but I have\nmy own priorities."],
		["grace_happy", BOTTOM_RIGHT, false, "Great!"],
		["grace", BOTTOM_RIGHT, false, "Don't worry Foo, you'll stay with us\njust a bit longer. We'll find your\nparents soon enough."],
		["foo", TOP_LEFT, true, "Okay."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_2/dialog_level_2_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
