extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in"],
		["cherry", TOP_LEFT, true, "We're not taking Foo with us, right?"],
		["grace", BOTTOM_RIGHT, false, "Yeah we are."],
		["cherry", TOP_LEFT, true, "That's dangerous."],
		["grace_frown", BOTTOM_RIGHT, false, "It'll be fine. Besides, what's the\nalternative? Leave him here?"],
		["cherry_roll", TOP_LEFT, true, "Yes. Yes Grace, leave him here."],
		["grace_pout", BOTTOM_RIGHT, false, "The others are leaving, he'll be\nalone. And he wants to come anyway."],
		["cherry_narrow", TOP_LEFT, true, "Whatever. You're responsible for\nkilling him if he gets caught."],
		["transition", "fade_out_screen"],
	]
	
func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_2.tscn")
