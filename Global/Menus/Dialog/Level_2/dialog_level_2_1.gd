extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["grace_strain_smile", BOTTOM_LEFT, true, "Thanks for letting him stay here\nIlliana."],
		["illiana_happy", BOTTOM_RIGHT, false, "Of course, we couldn't just turn him\naway now could we."],
		["grace_sweat_smile", BOTTOM_LEFT, true, "Yeah."],
		["grace_lookaway", BOTTOM_LEFT, true, "I'll get a scrying mage as soon as I\ncan. See if there's anyone in the city\nrelated to him."],
		["illiana_smile", BOTTOM_RIGHT, false, "Take your time, he's welcome to stay\nas long as he needs. And don't forget,\nyou have regular work as well."],
		["grace_neutral", BOTTOM_LEFT, true, "I know."],
		["illiana_lookaway", BOTTOM_RIGHT, false, "Speaking of, we have a few cases\nbacked up."],
		["illiana", BOTTOM_RIGHT, false, "Take any of them and feel free to\nbring along whomever you need, none\nof us are busy today."],
		["grace", BOTTOM_LEFT, true, "Thanks."],
		["transition", "fade_out"],
		["move", "grace", PoolVector2Array([Vector2(184, 112), Vector2(161, 120), Vector2(138, 112), \
			Vector2(115, 120)])],
		["transition", "fade_in"],
		["foo", BOTTOM_LEFT, true, "So what do you do here?"],
		["grace_smile", BOTTOM_RIGHT, false, "Well, this is a rescue service, and\nwe're called excavators."],
		["grace", BOTTOM_RIGHT, false, "People pay us to find their friends\nthat have been captured and kill\nthem."],
		["foo", BOTTOM_LEFT, true, "You kill them?"],
		["acacius", TOP_LEFT, true, "So they can be resurrected."],
		["foo", BOTTOM_LEFT, true, "Wow, cool."],
		["grace_sweat_smile", BOTTOM_RIGHT, false, "Haha..."],
		["foo", BOTTOM_LEFT, true, "Why do the bad guys take people?"],
		["acacius", TOP_LEFT, true, "For money. They'll either ransom them\nback to their families or force them\ninto labor."],
		["acacius", TOP_LEFT, true, "That's what would've happened to you,\nbut Grace stopped them."],
		["foo", BOTTOM_LEFT, true, "Wow..."],
		["foo", BOTTOM_LEFT, true, "So are you going to beat up more bad\nguys today?"],
		["grace_smile", BOTTOM_RIGHT, false, "In fact we are."],
		["foo", BOTTOM_LEFT, true, "Can I watch?"],
		["grace_happy", BOTTOM_RIGHT, false, "Of course!"],
		["transition", "fade_out"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/Interims/interim_menu_2.tscn")
