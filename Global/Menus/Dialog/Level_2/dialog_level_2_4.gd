extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		# mishka walking and sitting down
		["transition", "fade_in"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Jesus, there's a lot of people in\nhere."],
		["grace_frown", BOTTOM_RIGHT, false, "I think this is a pretty normal amount\nof people."],
		["mishka", BOTTOM_LEFT, true, "Guess this city's got a lot of\nproblems."],
		["grace_down_left", BOTTOM_RIGHT, false, "You're not wrong..."],
		["mishka_raise", BOTTOM_LEFT, true, "So what's got you fucked up on a\nWednesday night?"],
		# animate mishka drinking
		["grace_neutral", BOTTOM_RIGHT, false, "Huh?"],
		["mishka_disgust", BOTTOM_LEFT, true, "What the fuck? There's no alcohol in\nhere."],
		["grace_sweat_smile", BOTTOM_RIGHT, false, "Well of course, it's just a substitute.\nNobody uses real alcohol anymore."],
		["mishka", BOTTOM_LEFT, true, "What's the point of going to a bar if\nI can't get wasted? Does this place\nhave any real drinks?"],
		["grace", BOTTOM_RIGHT, false, "There's enchantments in the drinks to\nsimulate being inebriated."],
		["mishka_roll", BOTTOM_LEFT, true, "Well at that point just use real\nalcohol."],
		["grace_frown", BOTTOM_RIGHT, false, "Real alcohol makes you a danger to\nothers and is also really bad for\nyour liver."],
		["mishka_lookaway", BOTTOM_LEFT, true, "I know, that's why I want it."],
		["grace_happy", BOTTOM_RIGHT, false, "With the enchantments, there are no\nnegative side effects. Just the fun of\nbeing drunk."],
		["mishka", BOTTOM_LEFT, true, "No, you don't understand. I don't want\nto just get drunk, I want to get\nfucking wasted."],
		["mishka_angry", BOTTOM_LEFT, true, "This is my time to take revenge on\nmy body for all the shit it's made me\ndeal with in the past twenty-four hours."],
		["grace_furrow", BOTTOM_RIGHT, false, "That doesn't sound very fun."],
		["mishka_lookaway", BOTTOM_LEFT, true, "It's not about having fun, it's about\nthe experience."],
		["grace_neutral", BOTTOM_RIGHT, false, "..."],
		["mishka_raise", BOTTOM_LEFT, true, "But you didn't answer my question.\nWhat's got you drinking this early in\nthe week?"],
		["grace_down", BOTTOM_RIGHT, false, "I-I don't know. Just worried about\nsomeone I guess."],
		["grace_lookaway", BOTTOM_RIGHT, false, "They need so much from me but I\ndon't know how I'm going to give it to\nthem."],
		["mishka", BOTTOM_LEFT, true, "Why is it your responsibility to give\nit to them?"],
		["grace_down_left", BOTTOM_RIGHT, false, "Because if I don't, nobody else will."],
		["mishka", BOTTOM_LEFT, true, "Sounds like a slippery slope."],
		["transition", "fade_out"],
		# animate both drinking
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/UIMenus/Interims/interim_menu_3.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
