extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["move", "grace", PoolVector2Array([Vector2(161, 104), Vector2(161, 120), Vector2(184, 128), \
			Vector2(207, 120), Vector2(207, 104), Vector2(184, 96), Vector2(161, 104), Vector2(161, 120)])],
		["transition", "fade_in"],
		["illiana", BOTTOM_RIGHT, false, "What's wrong Grace?"],
		["grace_down", BOTTOM_LEFT, false, "..."],
		["grace_raise", BOTTOM_LEFT, true, "Oh?"],
		["grace_lookaway", BOTTOM_LEFT, true, "I paid an ancestry mage to find any\nof Foo's relatives, but I won't get\nresults for another week."],
		["illiana_raise", BOTTOM_RIGHT, false, "So what's there to worry about?\nJust wait a week."],
		["grace_hmm", BOTTOM_LEFT, true, "It's weird that there's no information\non him at all."],
		["grace_furrow", BOTTOM_LEFT, true, "Maybe he was smuggled into the city.\nBut then why?"],
		["illiana_worried", BOTTOM_RIGHT, false, "Grace, Grace."],
		["transition", "fade_out"],
		["show", "illiana"], # animate her standing up
		["move", "illiana", PoolVector2Array([Vector2(253, 120), Vector2(253, 136), Vector2(230, 144), \
			Vector2(207, 136), Vector2(207, 120)])],
		["transition", "fade_in"], # animate her leaning on desk
		["illiana_purse", BOTTOM_RIGHT, false, "You've been nothing but a ball of\nstress for the past few days."],
		["illiana_lookaway", BOTTOM_RIGHT, false, "You've done all you can for Foo, take\nsome time for yourself."],
		["illiana_smile", BOTTOM_RIGHT, false, "Relax, go somewhere, do something.\nTake your mind off all this."],
		["transition", "fade_out"],
		["move", "grace", PoolVector2Array([Vector2(161, 136)])],
		["transition", "fade_in"], # animate grace sitting down
		["grace_down_left", BOTTOM_LEFT, true, "I guess..."],
		["transition", "fade_out_screen"],
	]

	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_2/dialog_level_2_4.tscn")
