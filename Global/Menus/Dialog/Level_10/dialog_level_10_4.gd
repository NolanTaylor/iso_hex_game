extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["illiana", BOTTOM_LEFT, true, "Have you told your parents?"],
		["fia", BOTTOM_RIGHT, true, "They'd find out whether I wanted\nthem to or not."],
		["illiana_open", BOTTOM_LEFT, true, "But have you told them?"],
		["fia", BOTTOM_RIGHT, false, "I'm waiting 'till I find another job\nbefore I break the news. Make it a\nbit more palatable y'know?"],
		["illiana_smile", BOTTOM_LEFT, true, "In a hundred years I've never seen\nyou hold a job for longer than a few\nmonths."],
		["fia", BOTTOM_RIGHT, false, "You're exaggerating. But what can I\nsay? I get bored easily."],
		["fia", BOTTOM_RIGHT, true, "Why won't you let me work for you\nthough?"],
		["fia", BOTTOM_RIGHT, true, "My parents love you, they'd be\nthrilled to know I'm working here."],
		["illiana_lookaway", BOTTOM_LEFT, true, "This really isn't the type of work\nyou're suited to."],
		["fia", BOTTOM_RIGHT, false, "You've got an actual child working\nfor you, how am I not qualified?"],
		["illiana", BOTTOM_LEFT, true, "It's not all fighting and blowing\nthings up, there's a lot more\npaperwork than you'd think."],
		["illiana_purse", BOTTOM_LEFT, true, "Also, the child isn't working, Grace\njust insists on bringing him with her\non every job."],
		["fia", BOTTOM_RIGHT, false, "That doesn't sound very responsible."],
		["illiana_lookaway", BOTTOM_LEFT, true, "She's young, plus Foo seems to enjoy\nit."],
		["fia", BOTTOM_RIGHT, false, "Would you ever want children?"],
		["illiana_open", BOTTOM_LEFT, true, "Most people don't have children until\nseveral thousand. You're only seven\nhundred and I'm not much older."],
		["fia", BOTTOM_RIGHT, true, "I'm not talking about now. But like, in\nthe future."],
		["illiana_raise", BOTTOM_LEFT, true, "I don't know, it's hard to think about."],
		["fia", BOTTOM_RIGHT, true, "I think it'd be cool to have a little\nme running around."],
		["illiana_smile", BOTTOM_LEFT, true, "Maybe."],
		["fia", BOTTOM_RIGHT, true, "Maybe."],
		["fia", BOTTOM_RIGHT, false, "Anyways, I'll be heading out."],
		["illiana_worried", BOTTOM_LEFT, true, "You're not going to stay with me?\nYou're already down here."],
		["fia", BOTTOM_RIGHT, false, "I'd love to babe, but who's going to\nwater my plants?"],
		["illiana_smile", BOTTOM_LEFT, true, "Two hundred years of marriage and\nthe plants still take priority over me."],
		["fia", BOTTOM_RIGHT, false, "Know your place."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/UIMenus/Interims/interim_menu_11.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
