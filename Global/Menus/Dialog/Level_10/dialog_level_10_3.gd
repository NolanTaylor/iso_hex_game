extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in"],
		["seth", BOTTOM_LEFT, true, "Hey Grace."],
		["grace_lookaway", BOTTOM_RIGHT, true, "Yeah?"],
		["seth", BOTTOM_LEFT, true, "Are you planning on bringing Foo\ntoday?"],
		["grace_neutral", BOTTOM_RIGHT, false, "I am, yeah. Are you going to lecture\nme too?"],
		["seth", BOTTOM_LEFT, true, "No Grace. I trust you to make the\nbest judgement."],
		["seth", BOTTOM_LEFT, true, "I understand why you're hesitant to\nleave him behind when we're all gone."],
		["seth", BOTTOM_LEFT, true, "But it might be worth it to teach him\na bit of magic. At least enough to kill\nhimself if something were to happen."],
		["grace_furrow", BOTTOM_RIGHT, false, "That's the thing Seth. I'm worried he\nwon't be able to learn any magic."],
		["grace_frown", BOTTOM_RIGHT, false, "He's had a lot of complications with\nscanners and ancestry checks."],
		["grace_neutral", BOTTOM_RIGHT, false, "It's a miracle I was even able to scry\non him the other day."],
		["seth", BOTTOM_LEFT, true, "Well, scrying works a bit differently."],
		["seth", BOTTOM_LEFT, true, "But I suppose that is a bit of a\nproblem. Do you know why this is?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "No idea. Illiana thinks it could be\nSakatevo's, I think it might be a\ncurse."],
		["seth", BOTTOM_LEFT, true, "Have you considered killing him in\nthe morning when you leave, and just\nresurrecting him when you get back?"],
		["grace_down_left", BOTTOM_RIGHT, false, "I don't know..."],
		["grace_neutral", BOTTOM_RIGHT, false, "I wouldn't want him to spend the\nentire day in the ethereal, and it's\nexpensive to resurrect him every day."],
		["seth", BOTTOM_LEFT, true, "Well Grace, let's hope we can find\nsomething that works."],
		["seth", BOTTOM_LEFT, true, "Taking Foo with you every day isn't\nthe most dangerous thing. But as you\nsaw yesterday, anything can happen."],
		["transition", "fade_out_screen"],
	]

func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_10.tscn")
