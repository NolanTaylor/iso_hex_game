extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["choose", ["grace", "foo"], 4],
		["branch", "function_1"],
		["transition", "fade_out_screen"],
	]
	
func function_1() -> void:
	Globals.picked_units = picked_units
	
func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_10/dialog_level_10_3.tscn")
