extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["pause", 0.4],
		["show", "foo"], # play door audio maybe animate
		["transition", "fade_in"],
		["foo", TOP_RIGHT, false, "Grace!"],
		["transition", "fade_out"],
		["move", "foo", PoolVector2Array([Vector2(184, 64), Vector2(184, 128)])],
		["pause", 0.4], # animate hugging
		["transition", "fade_in"],
		["grace_happy", BOTTOM_LEFT, true, "Hi Foo."],
		["seth", TOP_RIGHT, false, "We're glad everyone's safe."],
		["cherry", TOP_LEFT, true, "So did we learn anything from last\nnight?"],
		["grace_down_left", BOTTOM_LEFT, true, "..."],
		["seth", TOP_RIGHT, false, "Cherry, not now please."],
		["cherry_roll", TOP_LEFT, true, "Whatever."],
		["transition", "fade_out"],
		["pause", 0.4],
		["show", "illiana"],
		["show", "fia"],
		["transition", "fade_in"],
		["illiana", BOTTOM_RIGHT, false, "Hey everyone."],
		["acacius", BOTTOM_LEFT, true, "Who's the new girl?"],
		["fia", TOP_RIGHT, false, "I'm her wife!"],
		["acacius", BOTTOM_LEFT, true, "Illy you're married!?"],
		["illiana_happy", BOTTOM_RIGHT, false, "Yeah, I guess I don't talk about my\npersonal life too much."],
		["illiana_smile", BOTTOM_RIGHT, false, "Anyway, this is Fiammetta, she got\nlaid off so I'm forcing her to work\nwith us for a bit."],
		["fia", TOP_RIGHT, false, "Pff. You have a child working here,\nit can't be that hard."],
		["acacius", BOTTOM_LEFT, true, "Don't be fooled, this kid can split\nyour body in half with a single spell."],
		["fia", TOP_RIGHT, false, "Really now? What a little monster you\nare."],
		["foo", TOP_LEFT, true, "He's joking, I can't use any magic."],
		["fia", TOP_RIGHT, false, "Ahh. I can teach you some if you'd\nlike."],
		["foo", TOP_LEFT, true, "Really??"],
		["fia", TOP_RIGHT, false, "Sure! You'll be blowing people up in\nno time."],
		["foo", TOP_LEFT, true, "Wow!"],
		["illiana", BOTTOM_RIGHT, false, "Anyway guys, it's time to start\ngetting ready for the day."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/Interims/interim_menu_10.tscn")
