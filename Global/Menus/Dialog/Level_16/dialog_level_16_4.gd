extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["cherry", BOTTOM_RIGHT, false, "I don't think we're getting out of this\none."],
		["transition", "fade_out"],
		["pause", 0.4], # anim and hide cherry
		["transition", "fade_in"],
		["seth", TOP_LEFT, true, "Cherry's right, we should kill\nourselves and regroup."],
		["grace_furrow", BOTTOM_RIGHT, false, "But we haven't found Foo!"],
		["acacius", TOP_RIGHT, false, "We can come back for him, but right\nnow we don't have any other options."],
		["transition", "fade_out"],
		["pause", 0.4], # anim and hide seth/acacius
		["transition", "fade_in"],
		["grace_pleading", BOTTOM_RIGHT, false, "But..."],
		["fia", TOP_LEFT, true, "It's not over Grace. This is just a\nsetback."],
		["transition", "fade_out"],
		["pause", 0.4], # anim and hide fia
		["transition", "fade_in"],
		["grace_angry_open", BOTTOM_RIGHT, false, "I'm not leaving him again!"],
		["illiana_frown", BOTTOM_LEFT, true, "Grace, look at where we are. There's\nnothing else to be done in this\nsituation."],
		["illiana_worried", BOTTOM_LEFT, true, "Make the responsible choice. I'll see\nyou on the other side."],
		["transition", "fade_out"],
		["pause", 0.4], # anim and hide illiana
		["transition", "fade_in"],
		["grace_down", BOTTOM_RIGHT, false, "..."],
		["transition", "fade_out"],
		["pause", 0.4], # anim and hide grace
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_17/dialog_level_17_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
