extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["pause", 1.0],
		["transition", "fade_in"],
		["phrank", TOP_RIGHT, false, "Hello."],
		["transition", "fade_out"],
		["pause", 1.0], # seth animation and teleport
		# maybe camera pan??
		["transition", "fade_in"],
		["grace_frown", TOP_LEFT, true, "Jesus Christ."],
		["acacius", TOP_RIGHT, false, "Huh?"],
		["grace_furrow", TOP_LEFT, true, "Where's Foo?"],
		["fia", BOTTOM_LEFT, true, "Well since we're together, we've\nlikely been separated in two."],
		["fia", BOTTOM_LEFT, true, "He's probably with the others."],
		["grace_angry_open", TOP_LEFT, true, "We need to get to him."],
		["acacius", TOP_RIGHT, false, "Fia, can you break this lock?"],
		["fia", BOTTOM_LEFT, true, "Yeah, give me a second."],
		["transition", "fade_out"],
		["pause", 1.0], # animation
		["transition", "fade_in"],
		["grace_neutral", BOTTOM_LEFT, true, "Hurry."],
		["transition", "fade_out"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_16.tscn")
