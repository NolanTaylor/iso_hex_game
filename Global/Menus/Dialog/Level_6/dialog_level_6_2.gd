extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["move", "grace", PoolVector2Array([Vector2(184, 112), Vector2(207, 120), Vector2(207, 136), \
			Vector2(184, 128), Vector2(161, 120), Vector2(161, 104), Vector2(184, 112)])],
		["transition", "fade_in"],
		["illiana", BOTTOM_RIGHT, false, "Grace."],
		["illiana_worried", BOTTOM_RIGHT, false, "Grace?"],
		["grace_raise", BOTTOM_LEFT, true, "Hm?"],
		["illiana_frown", BOTTOM_RIGHT, false, "You're pacing again."],
		["grace_lookaway", BOTTOM_LEFT, true, "Oh."],
		["illiana", BOTTOM_RIGHT, false, "What's on your mind?"],
		["grace_furrow", BOTTOM_LEFT, true, "It's uhh... I got Foo's ancestry results\nback the other day."],
		["illiana_open", BOTTOM_RIGHT, false, "And?"],
		["grace_neutral", BOTTOM_LEFT, true, "And there's nothing."],
		["grace_hmm", BOTTOM_LEFT, true, "Like nothing at all. Not a single\nperson related to him."],
		["grace_furrow", BOTTOM_LEFT, true, "The mage said it's almost as if Foo\ndoesn't have any magic in him."],
		["illiana", BOTTOM_RIGHT, false, "Do you think he might have\nSakatevo's?"],
		["grace_neutral", BOTTOM_LEFT, true, "That really old disease from like a\nbajillion years ago?"],
		["illiana_raise", BOTTOM_RIGHT, false, "Yeah, from that one wizard who got\nblackout drunk and unleashed a\nmassive spell plague."],
		["grace_lookaway", BOTTOM_LEFT, true, "Oh right, he's the reason alcohol is\nbanned. But what does his disease\nhave to do with Foo?"],
		["illiana", BOTTOM_RIGHT, false, "Well it cripples magic. So maybe\nthat's why nothing came up in the\nancestry search."],
		["grace_hmm", BOTTOM_LEFT, true, "I guess. But I thought the disease\nwas eradicated?"],
		["illiana_lookaway", BOTTOM_RIGHT, false, "It's partially genetic, a few cases\npop up every thousand years or so."],
		["grace_down_left", BOTTOM_LEFT, true, "Hmm..."],
		["grace_neutral", BOTTOM_LEFT, true, "Do you think Foo's cursed?"],
		["illiana_worried", BOTTOM_RIGHT, false, "Cursed?"],
		["grace_frown", BOTTOM_LEFT, true, "When we went to get his memory loss\nchecked out, they said it's possible\nFoo has a powerful memory curse."],
		["grace_hmm", BOTTOM_LEFT, true, "So maybe the curse also blocks\nmagic."],
		["illiana_raise", BOTTOM_RIGHT, false, "But why would anybody want to curse\nFoo?"],
		["grace_down", BOTTOM_LEFT, true, "I don't know."],
		["grace_raise", BOTTOM_LEFT, true, "Maybe he got in trouble with some\nuber-powerful archmage and they\nwiped his memory so he couldn't snitch."],
		["illiana_purse", BOTTOM_RIGHT, false, "It's seems unlikely. He's just a kid\nafter all."],
		["grace_down_left", BOTTOM_LEFT, true, "..."],
		["grace_annoyed", BOTTOM_LEFT, true, "Ugh. Nothing adds up. Nothing about\nFoo makes any sense."],
		["grace_frown", BOTTOM_LEFT, true, "He randomly appears one day with no\nmemory of who he is."],
		["grace_furrow", BOTTOM_LEFT, true, "No family, no registration, he's\nseemingly not even magical."],
		["illiana_frown", BOTTOM_RIGHT, false, "Well Grace, I have to get to bed. And\nI think you need some sleep as well."],
		["grace_neutral", BOTTOM_LEFT, true, "Oh, okay."],
		["grace_lookaway", BOTTOM_LEFT, true, "I'm just gonna put away a few things.\nYou can go sleep, I'll lock up."],
		["illiana", BOTTOM_RIGHT, false, "Goodnight Grace. Try not to stress\nyourself too much."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_7/dialog_level_7_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
