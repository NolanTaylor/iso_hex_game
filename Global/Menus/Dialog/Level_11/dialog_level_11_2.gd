extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in"],
		["foo", BOTTOM_RIGHT, false, "Bunny ears, bunny ears, jumped into\nthe hole, popped out the other side\nbeautiful and bold."],
		["foo", BOTTOM_RIGHT, false, "Did you know the end of the shoelace\nis called an aglet?"],
		["grace", BOTTOM_LEFT, true, "I had no idea, where did you learn\nthat?"],
		["foo", BOTTOM_RIGHT, false, "From Phineas and Ferb."],
		["grace_raise", BOTTOM_LEFT, true, "Oh, are those your friends?"],
		["foo", BOTTOM_RIGHT, false, "I don't think so. I... don't remember."],
		["grace_lookaway", BOTTOM_LEFT, true, "Ah..."],
		["foo", BOTTOM_RIGHT, false, "They make cool things. And they have\na pet platypus."],
		["transition", "fade_out"],
		["pause", 0.4], # animate
		["move", "foo", PoolVector2Array([Vector2(207, 88), Vector2(207, 56)])],
		["pause", 0.4], # door animate
		["hide", "foo"],
		["pause", 0.4],
		["transition", "fade_in"],
		["acacius", BOTTOM_LEFT, true, "What the heck is a platypus?"],
		["grace_hmm", BOTTOM_RIGHT, false, "I don't know. Some creature he made\nup I think."],
		["transition", "fade_out"],
		["move", "acacius", PoolVector2Array([Vector2(184, 48), Vector2(207, 56)])],
		["hide", "acacius"],
		["move", "grace", PoolVector2Array([Vector2(184, 80), Vector2(207, 72), Vector2(207, 56)])],
		["hide", "grace"],
		["transition", "fade_out_screen"],
	]

func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_11.tscn")
