extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["doctor", BOTTOM_LEFT, true, "You said your name was Foo?"],
		["foo", BOTTOM_RIGHT, false, "Yeah, but I don't remember my real\nname."],
		["doctor", BOTTOM_LEFT, true, "I see. Well Grace, there doesn't seem\nto be any magic at play here."],
		["grace_hmm", TOP_LEFT, true, "It's not a memory curse?"],
		["doctor", BOTTOM_LEFT, true, "I suppose it's possible that Foo has\na very powerful curse. One that\nevades standard detection."],
		["doctor", BOTTOM_LEFT, true, "But the more likely answer is just\nnormal medical amnesia."],
		["foo", BOTTOM_RIGHT, false, "So it's not magical?"],
		["doctor", BOTTOM_LEFT, true, "Correct. The term is retrograde\namnesia."],
		["doctor", BOTTOM_LEFT, true, "The cause is uncertain, but it means\nyou've lost some amount of memories\nacquired before the onset of amnesia."],
		["doctor", BOTTOM_LEFT, true, "Thankfully, it doesn't affect your\nability to create and retain new\nmemories."],
		["grace_frown", TOP_LEFT, true, "If we killed him and resurrected him,\nwould the amnesia go away?"],
		["doctor", BOTTOM_LEFT, true, "Memory and the brain in general\ndoesn't function like normal illness\nwhen it comes to resurrection."],
		["doctor", BOTTOM_LEFT, true, "Whether it's a curse or regular\namnesia, resurrecting Foo wouldn't\ncure it."],
		["grace_worried", TOP_LEFT, true, "So what can we do then?"],
		["doctor", BOTTOM_LEFT, true, "There isn't any specific treatment\nfor retrograde amnesia."],
		["doctor", BOTTOM_LEFT, true, "Strong sensations from his past, like\nsounds or smells, might spark some\nof his lost memories."],
		["doctor", BOTTOM_LEFT, true, "And sometimes memories slowly\nresurface on their own. But in most\ncases the memory loss is permanent."],
		["foo", BOTTOM_RIGHT, false, "So there's nothing I can do?"],
		["doctor", BOTTOM_LEFT, true, "Not exactly nothing. But I'm afraid\nthere's very little to be done in the\neffort of finding solid results."],
		["grace_down_left", TOP_LEFT, true, "I see. Well, thank you anyway."],
		["doctor", BOTTOM_LEFT, true, "Of course."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/UIMenus/Interims/interim_menu_4.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
