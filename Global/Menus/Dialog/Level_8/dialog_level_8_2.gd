extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in"],
		["cherry", BOTTOM_LEFT, true, "You should really stop taking Foo\nwith you on these jobs."],
		["grace_dismiss", BOTTOM_RIGHT, false, "He wants to, he has fun watching us."],
		["grace_lookaway", BOTTOM_RIGHT, false, "And I'm not leaving him alone."],
		["cherry_narrow", BOTTOM_LEFT, true, "This isn't about him having fun, this\nis about his safety."],
		["grace_furrow", BOTTOM_RIGHT, false, "I know Cherry, that's why I'm doing\nthis. I need to be near him in case\nsomething happens."],
		["cherry_raise", BOTTOM_LEFT, true, "So you bring him into the middle of\na chaotic fight?"],
		["grace_frown", BOTTOM_RIGHT, false, "Yes, so I can keep him in my sight.\nI'm not letting him stay in this\nbuilding alone."],
		["grace_angry", BOTTOM_RIGHT, false, "If he's with me and something\nhappens at least I'll have some\ncontrol over the situation."],
		["grace_furrow", BOTTOM_RIGHT, false, "Instead of being halfway across the\ncity."],
		["cherry_roll", BOTTOM_LEFT, true, "What does your amount of control\nmatter? Overall, he'd be safer staying\nbehind."],
		["grace_annoyed", BOTTOM_RIGHT, false, "He doesn't know any magic; He can't\nkill himself if something happens."],
		["grace_frown", BOTTOM_RIGHT, false, "I am fully capable of keeping Foo\nsafe."],
		["cherry", BOTTOM_LEFT, true, "Whatever Grace. As long as you\nunderstand the gravity of your\nresponsibility."],
		["grace_angry", BOTTOM_RIGHT, false, "..."],
		["grace_down_left", BOTTOM_RIGHT, false, "..."],
	]

func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_8.tscn")
