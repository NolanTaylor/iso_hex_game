extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["pause", 2.4], # pause and animate
		["transition", "fade_in"],
		["cherry", BOTTOM_RIGHT, false, "I told you so."],
		["grace_down", BOTTOM_LEFT, false, "Shut up."],
		["cherry_narrow", BOTTOM_RIGHT, false, "You knew this was going to happen."],
		["grace_angry", BOTTOM_LEFT, false, "Shut up!"],
		["cherry_roll", BOTTOM_RIGHT, false, "No Grace! This isn't just an \"I won an\nargument.\" This is a child's life."],
		["grace_angry_open", BOTTOM_LEFT, true, "I KNOW Cherry! I know."],
		["seth", TOP_LEFT, true, "Guys, calm down. This isn't anybody's\nfault. We'll get Foo back."],
		["grace_neutral", BOTTOM_LEFT, true, "I'm going after him."],
		["cherry", BOTTOM_RIGHT, false, "No you're not Grace."],
		["grace_lookaway", BOTTOM_LEFT, true, "He's not registered with anyone, they\ncan't ransom him. They'll put him into\nforced labor."],
		["cherry", BOTTOM_RIGHT, false, "Grace."],
		["grace_lookaway", BOTTOM_LEFT, true, "Teleportation has a limit, they're not\nfar."],
		["cherry_open", BOTTOM_RIGHT, false, "Grace!"],
		["grace_angry", BOTTOM_LEFT, true, "If I go now I can catch up to them."],
		["cherry_roll", BOTTOM_RIGHT, false, "No Grace! You're not going after him."],
		["cherry", BOTTOM_RIGHT, false, "We are going to go home."],
		["cherry_open", BOTTOM_RIGHT, false, "We are going to SLEEP!"],
		["cherry", BOTTOM_RIGHT, false, "And we can hire a tracking mage in\nthe morning."],
		["grace_down_left", BOTTOM_LEFT, true, "..."],
		["illiana", TOP_LEFT, true, "Cherry is right Grace. Teleportation\nmay have a limit, but it's still an\nunreasonable amount of ground to cover."],
		["illiana_open", TOP_LEFT, true, "You've been working all day. You'll\nhave a much better chance at finding\nhim tomorrow."],
		["acacius", TOP_LEFT, true, "It'll still be at least a day before\nthey can process him or move him\nanywhere."],
		["acacius", TOP_LEFT, true, "Taking a day to scry for him will be\nultimately more helpful for us."],
		["grace_angry", BOTTOM_LEFT, true, "..."],
		["grace_down", BOTTOM_LEFT, true, "..."],
		["grace_down_left", BOTTOM_LEFT, true, "..."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_9/dialog_level_9_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
