extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["illiana_open", BOTTOM_RIGHT, false, "There's a group operating out of the\nsouth side."],
		["illiana", BOTTOM_RIGHT, false, "They'll be trafficking in a lot of\nvictims today as part of an exchange,\nmany of which belong to our clients."],
		["acacius", BOTTOM_LEFT, true, "And what will we do with the victims\nwe weren't hired for?"],
		["seth", TOP_RIGHT, false, "We'll kill them anyway."],
		["seth", TOP_RIGHT, false, "We may not be paid for it, but\nchances are we're helping them and\nit's of little consequence to us."],
		["acacius", BOTTOM_LEFT, true, "Fun, I love indiscriminate killing."],
		["illiana", BOTTOM_RIGHT, false, "We'll leave in a bit, make sure you\nhave everything ready."],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/Interims/interim_menu_8.tscn")
