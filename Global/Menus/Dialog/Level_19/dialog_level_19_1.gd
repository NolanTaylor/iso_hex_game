extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["move", "grace", PoolVector2Array([Vector2(299, 136), Vector2(207, 104), Vector2(207, 88), \
			Vector2(184, 80)])],
		["transition", "fade_in"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Grace?"],
		["grace_down_left", BOTTOM_RIGHT, false, "I... I don't think we ever exchanged\nnames."],
		["mishka", BOTTOM_LEFT, true, "I'm Mishka."],
		["mishka_down", BOTTOM_LEFT, true, "My son Alexei and wife Katya."],
		["grace_down", BOTTOM_RIGHT, false, "I'm sorry..."],
		["mishka_down", BOTTOM_LEFT, true, "..."],
		["transition", "fade_out"],
		["pause", 0.4], # anim
		["transition", "fade_in"],
		["grace_down_left", BOTTOM_RIGHT, false, "..."],
		["grace_neutral", BOTTOM_RIGHT, false, "What are you going to do?"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Go home. I've spent long enough\nliving in fantasy."],
		["grace_lookaway", BOTTOM_RIGHT, false, "Will I see you again?"],
		["mishka", BOTTOM_LEFT, true, "Probably not. I'll be dead in fourty\nor so years anyway."],
		["grace_down", BOTTOM_RIGHT, false, "... I guess this is goodbye then."],
		["mishka_down", BOTTOM_LEFT, true, "..."],
		["mishka_narrow", BOTTOM_LEFT, true, "Eternal life is sometimes poetically\nhailed as a curse rather than a\nblessing."],
		["mishka", BOTTOM_LEFT, true, "For your sake I hope they're wrong.\nGoodbye Grace."],
		["grace_down", BOTTOM_RIGHT, false, "Goodbye Mishka."],
		["transition", "fade_out"],
		["pause", 0.4], # move
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/credits_menu.tscn")
