extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["grace_down", BOTTOM_LEFT, true, "It's been almost two weeks and I feel\nlike we haven't gotten anywhere in\nfinding out who you are."],
		["foo", BOTTOM_RIGHT, false, "What happens if we don't find my\nparents?"],
		["grace_lookaway", BOTTOM_LEFT, true, "I..."],
		["foo", BOTTOM_RIGHT, false, "Do I get to stay here forever?"],
		["grace_furrow", BOTTOM_LEFT, true, "I don't think Illiana would want that.\nShe's been nice enough to let you\nstay this long."],
		["foo", BOTTOM_RIGHT, false, "Could I stay with you?"],
		["grace_down_left", BOTTOM_LEFT, true, "..."],
		["grace_sweat_smile", BOTTOM_LEFT, true, "We'll find your parents Foo."],
		["foo", BOTTOM_RIGHT, false, "Oh..."],
		["grace", BOTTOM_LEFT, true, "Do you remember anything about\nthem?"],
		["foo", BOTTOM_RIGHT, false, "I had a mom and dad. My mom was a\ndentist. I forgot what my dad did."],
		["grace_hmm", BOTTOM_LEFT, true, "A dentist..."],
		["foo", BOTTOM_RIGHT, false, "We lived in a house with a big tree\nand my dad made a swing on one of\nthe branches."],
		["foo", BOTTOM_RIGHT, false, "I played out there a lot."],
		["grace_frown", BOTTOM_LEFT, true, "Was the tree blue like the ones here?"],
		["foo", BOTTOM_RIGHT, false, "No, it was mostly green. But\nsometimes it was orange."],
		["foo", BOTTOM_RIGHT, false, "And then the leaves would fall off\nand we would play in the leaves."],
		["grace_raise", BOTTOM_LEFT, true, "Do trees normally do that?"],
		["foo", BOTTOM_RIGHT, false, "I think so. At least where we were."],
		["grace_down_left", BOTTOM_LEFT, true, "It sounds like a nice place. I'm sorry\nyou lost all that."],
		["foo", BOTTOM_RIGHT, false, "It's okay. I like it here too."],
		["grace_happy", BOTTOM_LEFT, true, "We can find an orange tree for you."],
		["foo", BOTTOM_RIGHT, false, "Really?"],
		["grace", BOTTOM_LEFT, true, "Yeah, I'm not sure if it'll change\ncolors. But we can get it to be orange."],
		["foo", BOTTOM_RIGHT, false, "Okay!"],
		["grace_dismiss", BOTTOM_LEFT, true, "Well, I think Illiana might kick me out\nif I stay any longer, so I'll let you go\nto sleep now."],
		["foo", BOTTOM_RIGHT, false, "You can't stay longer?"],
		["grace_sweat_smile", BOTTOM_LEFT, true, "I'll be back in the morning. Sleep\nwell."],
		["transition", "fade_out"],
		["transition", "fade_out_screen"], # move grace
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_14/dialog_level_14_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
