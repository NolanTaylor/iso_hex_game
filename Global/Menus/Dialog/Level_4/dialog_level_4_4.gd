extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["grace", BOTTOM_RIGHT, false, "Oh it's you again."],
		["mishka_raise", BOTTOM_LEFT, true, "Disappointed?"],
		["grace_dismiss", BOTTOM_RIGHT, false, "Sorely. This used to be my favorite\nseat before you started sitting next\nto it."],
		["mishka_lookaway", BOTTOM_LEFT, true, "Well, fuck me I guess. You come here\noften?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "Not really."],
		["mishka", BOTTOM_LEFT, true, "Just twice in one week."],
		["grace_down", BOTTOM_RIGHT, false, "It's been a long week."],
		["mishka_lookaway", BOTTOM_LEFT, true, "Tell me about it. So does everyone\nhere have magic superpowers or\nwhat?"],
		["grace_neutral", BOTTOM_RIGHT, false, "What do you mean?"],
		["mishka", BOTTOM_LEFT, true, "I saw a man this morning get his\nhead imploded because he catcalled\na girl."],
		["grace_smug", BOTTOM_RIGHT, false, "He deserved it."],
		["mishka_line", BOTTOM_LEFT, true, "No doubt, it was hilarious to watch.\nBut are head implosions a common\nthing around here?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "Well, basically everyone knows a bit\nof magic these days. Although I doubt\nmost could make your head implode."],
		["mishka", BOTTOM_LEFT, true, "I see. So what kind of magic do you\ndo?"],
		["grace", BOTTOM_RIGHT, false, "Me? Oh, mostly optics."],
		["mishka_raise", BOTTOM_LEFT, true, "Like eyesight?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "More like light. Waves and particles."],
		["mishka", BOTTOM_LEFT, true, "Can you make big beams of light?"],
		["grace", BOTTOM_RIGHT, false, "Yeah, that falls in the same category."],
		["mishka_down", BOTTOM_LEFT, true, "Wild. My son and I used to watch\nthese Japanese cartoons."],
		["grace_hmm", BOTTOM_RIGHT, false, "Japanese?"],
		["mishka_wist", BOTTOM_LEFT, true, "Yeah, his favorite was this one\nwhere the characters would scream\nand make these huge..."],
		["mishka_down", BOTTOM_LEFT, true, "..."],
		["grace_frown", BOTTOM_RIGHT, false, "Huge what?"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Ah... Never mind."],
		["mishka_down", BOTTOM_LEFT, true, "I'm sure it's tame compared to what\nyou see here anyway."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/UIMenus/Interims/interim_menu_5.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
