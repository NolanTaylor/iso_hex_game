extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["grace", BOTTOM_RIGHT, false, "Remember, stay in the building. If\nyou need anything, Illiana's upstairs."],
		["foo", BOTTOM_LEFT, true, "Do you have to go?"],
		["grace_strain_smile", BOTTOM_RIGHT, false, "I do, but I'll be back in the morning.\nSleep well."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_4/dialog_level_4_4.tscn")
