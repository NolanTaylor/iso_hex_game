extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["camera", Vector2(69, -86), 0.75, 1.0],
		["pause", 1.4],
		["show", "hostage_1"],
		["move", "hostage_1", PoolVector2Array([Vector2(161, -40), Vector2(161, -24), \
			Vector2(138, -16), Vector2(115, -8), Vector2(92, -16), Vector2(69, -8), \
			Vector2(46, -16), Vector2(23, -24)]), "continue"],
		["pause", 0.2],
		["show", "hostage_2"],
		["move", "hostage_2", PoolVector2Array([Vector2(161, -40), Vector2(161, -24), \
			Vector2(138, -16), Vector2(115, -8), Vector2(92, -16), Vector2(69, -8), \
			Vector2(46, -16), Vector2(23, -8), Vector2(23, 8), Vector2(23, 24), \
			Vector2(0, 32)]), "continue"],
		["pause", 0.2],
		["show", "hostage_3"],
		["move", "hostage_3", PoolVector2Array([Vector2(161, -40), Vector2(161, -24), \
			Vector2(161, 40), Vector2(184, 48), Vector2(276, 80), Vector2(276, 96)]), "continue"],
		["pause", 0.2],
		["show", "hostage_4"],
		["move", "hostage_4", PoolVector2Array([Vector2(162, -40), Vector2(161, -24), \
			Vector2(184, -16), Vector2(207, -24), Vector2(253, -40)]), "continue"],
		["pause", 0.2],
		["show", "hostage_5"],
		["move", "hostage_5", PoolVector2Array([Vector2(161, -40), Vector2(161, -24), \
			Vector2(161, 72)]), "continue"],
		["pause", 0.2],
		["show", "hostage_6"],
		["move", "hostage_6", PoolVector2Array([Vector2(161, -40), Vector2(161, -24), \
			Vector2(161, 40), Vector2(138, 48), Vector2(138, 64), Vector2(138, 80), \
			Vector2(115, 88), Vector2(46, 112), Vector2(46, 128), Vector2(46, 160)]), "continue"],
		["pause", 1.6],
		["transition", "fade_in"],
		["trafficker", BOTTOM_LEFT, true, "Who the hell let the goods escape?"],
		["transition", "fade_out"],
		["show", "hostage_8"],
		["move", "hostage_8", PoolVector2Array([Vector2(161, -40), Vector2(161, -24), \
			Vector2(184, -16), Vector2(207, -8)]), "continue"],
		["pause", 0.4],
		["show", "avram_og"],
		["move", "avram_og", PoolVector2Array([Vector2(161, -40), Vector2(161, -24), \
			Vector2(184, -16)])],
		["transition", "fade_in"],
		["woman", BOTTOM_RIGHT, false, "Please..."],
		["transition", "fade_out"],
		["unit_action", "avram_og", "attack"],
		["hide", "hostage_8"],
		["pause", 0.4],
		["show", "celestial"],
		["move", "celestial", PoolVector2Array([Vector2(161, -40), Vector2(161, -24)])],
		["transition", "fade_in"],
		["trafficker", BOTTOM_LEFT, true, "What the hell are you doing? We need\nthem alive."],
		["avram", BOTTOM_RIGHT, false, "Right... My bad."],
		["transition", "fade_out"],
		["camera", Vector2(0, -86), 1.0],
		["pause", 0.2],
		["unit_action", "hostage_1", "bind", "continue"],
		["pause", 0.2],
		["unit_action", "hostage_2", "bind", "continue"],
		["pause", 0.2],
		["unit_action", "hostage_3", "bind", "continue"],
		["pause", 0.2],
		["unit_action", "hostage_4", "bind", "continue"],
		["pause", 0.2],
		["unit_action", "hostage_5", "bind", "continue"],
		["pause", 0.2],
		["unit_action", "hostage_6", "bind"],
		["pause", 0.8],
		["show", "trafficker_1"],
		["move", "trafficker_1", Vector2(0, 128), "continue"],
		["pause", 0.2],
		["show", "optics_2"],
		["move", "optics_2", Vector2(253, 40), "continue"],
		["pause", 0.2],
		["show", "jake"],
		["move", "jake", Vector2(138, 80), "continue"],
		["pause", 0.2],
		["show", "phrank"],
		["move", "phrank", Vector2(46, 64), "continue"],
		["pause", 0.2],
		["show", "fil"],
		["move", "fil", Vector2(230, 80), "continue"],
		["pause", 0.2],
		["show", "optics_1"],
		["move", "optics_1", Vector2(161, 56), "continue"],
		["pause", 0.2],
		["show", "kinematics"],
		["move", "kinematics", Vector2(115, -8), "continue"],
		["pause", 2.0],
		["camera", Vector2(69, -86), 0.75, 1.0],
		["transition", "fade_in"],
		["trafficker", BOTTOM_LEFT, true, "Round them up. Don't kill a single one."],
		["trafficker", BOTTOM_LEFT, true, "And you. Put your cosmetics on. Don't want anybody recognizing us."],
		["avram", BOTTOM_RIGHT, false, "yeah..."],
		["transition", "fade_out"],
		["pause", 0.2],
		["hide", "avram_og"],
		["play_animation", $avram_magical_girl, "transform"],
		["show", "avram"],
		["hide", $avram_magical_girl],
		["pause", 0.4],
		["camera", Vector2(0, 0), 1.0, 1.0],
		["pause", 0.4],
		["move", "illiana", PoolVector2Array([Vector2(115, 232), Vector2(115, 216), \
			Vector2(115, 168)]), "continue"],
		["pause", 0.4],
		["move", "cherry", PoolVector2Array([Vector2(161, 248), Vector2(161, 232), \
			Vector2(161, 168)]), "continue"],
		["pause", 0.4],
		["move", "acacius", PoolVector2Array([Vector2(161, 232), Vector2(161, 216), \
			Vector2(161, 184), Vector2(184, 176)]), "continue"],
		["pause", 0.4],
		["move", "grace", PoolVector2Array([Vector2(138, 256), Vector2(138, 240), \
			Vector2(138, 175)]), "continue"],
		["pause", 0.4],
		["move", "foo", PoolVector2Array([Vector2(138, 240), Vector2(138, 224), \
			Vector2(138, 192), Vector2(115, 184)])],
		["pause", 0.4],
		["transition", "fade_in"],
		["illiana_raise", TOP_LEFT, true, "Looks like somebody got here before us."],
		["illiana", TOP_LEFT, true, "Kill as many as you can, before the traffickers can take them back."],
		["transition", "fade_out"],
	]
	
func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_4.tscn")
