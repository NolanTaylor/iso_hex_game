extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"], # maybe have movement idk
		["transition", "fade_in"],
		["foo", BOTTOM_RIGHT, false, "So you can make light with your\nhands?"],
		["grace", BOTTOM_LEFT, true, "It takes more than just hands, but\nyes."],
		["foo", BOTTOM_RIGHT, false, "What else do you need?"],
		["grace_lookaway", BOTTOM_LEFT, true, "Well, a converter for one. I need to\ntransition energy."],
		["foo", BOTTOM_RIGHT, false, "You make energy?"],
		["grace_sweat_smile", BOTTOM_LEFT, true, "More like manipulate it."],
		["foo", BOTTOM_RIGHT, false, "Can you make an energy blast?"],
		["grace", BOTTOM_LEFT, true, "Yeah, I guess that's possible."],
		["foo", BOTTOM_RIGHT, false, "Wow..."],
		["foo", BOTTOM_RIGHT, false, "You're like Goku!"],
		["grace_frown", BOTTOM_LEFT, true, "Goku?"],
		["foo", BOTTOM_RIGHT, false, "He makes a big light blast with\nenergy!"],
		["foo", BOTTOM_RIGHT, false, "But it's different from yours. Can you\nuse ki?"],
		["grace", BOTTOM_LEFT, true, "What's ki?"],
		["foo", BOTTOM_RIGHT, false, "It's a type of energy."],
		["grace_hmm", BOTTOM_LEFT, true, "Like kinetic or potential energy?"],
		["foo", BOTTOM_RIGHT, false, "Uhh... I think so, yeah."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_4/dialog_level_4_3.tscn")
