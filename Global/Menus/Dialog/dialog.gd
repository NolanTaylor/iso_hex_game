extends Node2D

const TOP_LEFT : Vector2 = Vector2(16, 4)
const TOP_RIGHT : Vector2 = Vector2(240, 4)
const BOTTOM_LEFT : Vector2 = Vector2(16, 132)
const BOTTOM_RIGHT : Vector2 = Vector2(240, 132)

const GRACE_COLOR : Color = Color("ffd5ea")
const CHERRY_COLOR : Color = Color("ebdeff")
const CAPTOR_COLOR : Color = Color("cacaca")
const DEFAULT_COLOR: Color = Color("c2c6ca")
const PRISONER_COLOR : Color = Color("ffffff")
const MISHKA_COLOR : Color = Color("bbdbdb")
const FOO_COLOR : Color = Color("bbdbdb")

export var fade : bool = true

onready var grid = Globals.get_hex_grid()

var dialog_ended : bool = false
var auto_advance : bool = false
var waiting : bool = false
var waiting_for_pause_timer : bool = false
var line : int = 0
var hex_index : int = 0
var picked_units : Array = []

var rng = RandomNumberGenerator.new()

var characters : Dictionary = {
	
}

var all_objects : Array = [
	
]

var dialog : Array = [
	
]

signal terminated

func init_dialog() -> void:
	# abstract function
	pass

func _ready():
	init_dialog()
	dialog_ended = false
	auto_advance = true
	line = -1
	$canvas_layer/label.visible_characters = 0
	rng.randomize()
	$less_units_popup.get_ok().text = "Continue"
	$less_units_popup.get_cancel().text = "Go Back"
	if fade:
		$canvas_layer/fade/color_rect.show()
	if has_node("level"):
		get_node("level").z_index = -6
		all_objects.append_array($level/objects/player_units.get_children())
		all_objects.append_array($level/objects/enemy_units.get_children())
		all_objects.append_array($level/objects/neutral_units.get_children())
		all_objects.append_array($level/objects/obstacles.get_children())
		all_objects.append_array($level/objects/ysort_decor.get_children())
		all_objects.append_array($level/objects/decor.get_children())
		
func _process(delta):
	if not dialog_ended:
		if Input.is_action_just_pressed("advance_dialog") and line >= 0:
			match dialog[line][0]:
				"transition":
					pass
				"choose":
					pass
				"branch":
					pass
				"pause":
					pass
				"move":
					pass
				"unit_action":
					pass
				"play_animation":
					pass
				"show":
					pass
				"hide":
					pass
				_:
					$canvas_layer/label.visible_characters = 999
		return
					
	if Input.is_action_just_pressed("advance_dialog") or auto_advance:
		auto_advance = false
		
		if line >= dialog.size() - 1:
			terminate()
			return
		line += 1
		
		match dialog[line][0]:
			"transition":
				dialog_ended = false
				waiting = true
				
				match dialog[line][1]:
					"fade_in":
						$canvas_layer/label.visible_characters = 0
						$canvas_layer/label.text = dialog[line + 1][3]
						set_portrait(dialog[line + 1][0], dialog[line + 1][1], dialog[line + 1][2])
						
						$canvas_layer/portrait_frame.modulate.a = 0
						$canvas_layer/portrait.modulate.a = 0
						$canvas_layer/label.modulate.a = 0
						
						$canvas_layer/portrait_frame.show()
						$canvas_layer/portrait.show()
						$canvas_layer/label.show()
						
						$tween.interpolate_property($canvas_layer/portrait_frame, "modulate:a", \
							0.0, 1.0, 0.5, Tween.TRANS_LINEAR)
						$tween.interpolate_property($canvas_layer/portrait, "modulate:a", \
							0.0, 1.0, 0.5, Tween.TRANS_LINEAR)
						$tween.interpolate_property($canvas_layer/label, "modulate:a", \
							0.0, 1.0, 0.5, Tween.TRANS_LINEAR)
						$tween.connect("tween_completed", self, "advance_after_fade", [ ], \
							CONNECT_ONESHOT)
						$tween.start()
					"fade_out":
						$canvas_layer/portrait_frame.modulate.a = 1
						$canvas_layer/portrait.modulate.a = 1
						$canvas_layer/label.modulate.a = 1
						
						$canvas_layer/portrait_frame.show()
						$canvas_layer/portrait.show()
						$canvas_layer/label.show()
						
						$tween.interpolate_property($canvas_layer/portrait_frame, "modulate:a", \
							1.0, 0.0, 0.5, Tween.TRANS_LINEAR)
						$tween.interpolate_property($canvas_layer/portrait, "modulate:a", \
							1.0, 0.0, 0.5, Tween.TRANS_LINEAR)
						$tween.interpolate_property($canvas_layer/label, "modulate:a", \
							1.0, 0.0, 0.5, Tween.TRANS_LINEAR)
						$tween.connect("tween_completed", self, "advance_after_fade", [ ], \
							CONNECT_ONESHOT)
						$tween.start()
					"fade_in_screen":
						$canvas_layer/fade/color_rect.modulate = Color(1, 1, 1, 1)
						$canvas_layer/fade/color_rect.show()
						$tween.interpolate_property($canvas_layer/fade/color_rect, "modulate:a", \
							1.0, 0.0, 1.0, Tween.TRANS_LINEAR)
						$tween.start()
						$tween.connect("tween_completed", self, "advance_after_fade", [ ], \
							CONNECT_ONESHOT)
					"fade_out_screen":
						$canvas_layer/fade/color_rect.modulate = Color(1, 1, 1, 0)
						$canvas_layer/fade/color_rect.show()
						$tween.interpolate_property($canvas_layer/fade/color_rect, "modulate:a", \
							0.0, 1.0, 1.0, Tween.TRANS_LINEAR)
						$tween.start()
						$tween.connect("tween_completed", self, "advance_after_fade", [true], \
							CONNECT_ONESHOT)
							
			"camera":
				dialog_ended = false
				waiting = true
				
				var cam = $level/camera
				var time : float = 0.6
				if dialog[line].size() >= 4:
					time = dialog[line][3]
				
				$tween.interpolate_property(cam, "position", cam.position, dialog[line][1], \
					time, Tween.TRANS_LINEAR)
				$tween.interpolate_property(cam, "zoom", cam.zoom, \
					Vector2(dialog[line][2], dialog[line][2]), time, Tween.TRANS_LINEAR)
				$tween.connect("tween_completed", self, "advance_after_fade", [ ], \
					CONNECT_ONESHOT)
				$tween.start()
			"choose":
				dialog_ended = false
				waiting = true
				picked_units.resize(0)
				picked_units = dialog[line][1].duplicate()
				
				var highlight : PoolVector3Array = PoolVector3Array([])
				
				for unit in $level/objects/player_units.get_children():
					if not unit.name in dialog[line][1]:
						highlight.append(unit.coords)
					
				$choose_ui/num_label.text = "{0}/{1}".format([String(dialog[line][1].size()), \
					String(dialog[line][2])])
				$choose_ui.show()
				$confirm_node.show()
					
				$level.cutscene_mode = false
				$level/hex_grid.mouse_state = $level/hex_grid.State.STATE_CHOOSE
				$level/hex_grid.highlight_coords = highlight
				$level/cursor.show()
			"branch":
				match dialog[line][1]:
					"function_1":
						function_1()
					"function_2":
						function_2()
					"function_3":
						function_3()
				auto_advance = true
			"pause":
				dialog_ended = false
				waiting = true
				waiting_for_pause_timer = true
				$pause_timer.start(dialog[line][1])
			"move":
				var subject
				
				if typeof(dialog[line][1]) != TYPE_STRING:
					subject = dialog[line][1]
				else:
					for obj in all_objects:
						if obj.name == dialog[line][1]:
							subject = obj
							
				if dialog[line].size() >= 4:
					if dialog[line][3] == "continue":
						dialog_ended = true
						auto_advance = true
				else:
					dialog_ended = false
					waiting = true
					subject.connect("complete_movement", self, "advance_after_movement", [ ], \
						CONNECT_ONESHOT)
						
				if typeof(dialog[line][2]) == TYPE_VECTOR2_ARRAY:
					subject.move(dialog[line][2], 0)
				elif typeof(dialog[line][2]) == TYPE_VECTOR2:
					subject.move(grid.find_path(subject.coords, \
						Globals.pixel_to_grid(dialog[line][2] + \
						Vector2(10, 10)), "all_units"), 0)
				elif typeof(dialog[line][2]) == TYPE_VECTOR3:
					subject.move(grid.find_path(subject.coords, dialog[line][2], "all_units"), 0)
			"unit_action":
				var subject
				
				if typeof(dialog[line][1]) != TYPE_STRING:
					subject = dialog[line][1]
				else:
					for obj in all_objects:
						if obj.name == dialog[line][1]:
							subject = obj
							
				if dialog[line].size() >= 4:
					if dialog[line][3] == "continue":
						dialog_ended = true
						auto_advance = true
				else:
					dialog_ended = false
					waiting = true
					subject.get_node("sprite").connect("animation_finished", self, \
						"advance_after_movement", [subject], CONNECT_ONESHOT)
						
				match dialog[line][2]:
					"attack":
						subject.attack(Vector3(0, 0, 0), true)
					"cast":
						subject.cast(Vector3(0, 0, 0), true)
					"bind":
						subject.bound = true
						subject.get_node("sprite").play("bind")
			"play_animation":
				dialog_ended = false
				waiting = true
				dialog[line][1].show()
				dialog[line][1].play(dialog[line][2])
				dialog[line][1].connect("animation_finished", self, "advance_after_anim", \
					[dialog[line][1]], CONNECT_ONESHOT)
			"show":
				auto_advance = true
				
				if typeof(dialog[line][1]) != TYPE_STRING:
					dialog[line][1].show()
					return
					
				for obj in all_objects:
					if obj.name == dialog[line][1]:
						obj.show()
						return
			"hide":
				auto_advance = true
				
				if typeof(dialog[line][1]) != TYPE_STRING:
					dialog[line][1].hide()
					return
					
				for obj in all_objects:
					if obj.name == dialog[line][1]:
						obj.hide()
						return
			_:
				dialog_ended = false
				$canvas_layer/label.visible_characters = 0
				$canvas_layer/label.text = dialog[line][3]
				set_portrait(dialog[line][0], dialog[line][1], dialog[line][2])
				
func _input(event):
	if dialog.size() == 0:
		return
	if dialog[line][0] != "choose":
		return
	if has_node("level"):
		for character in get_node("level/objects/player_units").get_children():
			if character.has_node("character_menu"):
				if character.get_node("character_menu").visible:
					return
	if $less_units_popup.visible:
		return
		
	if event is InputEventMouseButton and event.pressed:
		if event.button_index == BUTTON_LEFT:
			var unit = null
			
			if $level/hex_grid.grid.has($level/hex_grid.current_coords):
				unit = $level/hex_grid.get_unit($level/hex_grid.current_coords)
			
			if Globals.is_valid(unit):
				if unit.name in dialog[line][1]:
					return
				if not unit.name in picked_units:
					picked_units.append(unit.name)
				else:
					picked_units.erase(unit.name)
					
				if picked_units.size() > dialog[line][2]:
					$confirm_node/confirm_button.disabled = true
				else:
					$confirm_node/confirm_button.disabled = false
			
			var denominator : int = dialog[line][2]
			var fraction : String = "{0}/{1}".format([String(picked_units.size()), \
				String(denominator)])
			
			$choose_ui/num_label.text = fraction
	
func set_portrait(character : String, side : Vector2, flip : bool) -> void:
	match side:
		TOP_LEFT:
			$canvas_layer/portrait_frame.position = Vector2(0, 0)
			$canvas_layer/label.rect_position = Vector2(84, 12)
		TOP_RIGHT:
			$canvas_layer/portrait_frame.position = Vector2(0, 0)
			$canvas_layer/label.rect_position = Vector2(36, 12)
		BOTTOM_LEFT:
			$canvas_layer/portrait_frame.position = Vector2(0, 128)
			$canvas_layer/label.rect_position = Vector2(84, 140)
		BOTTOM_RIGHT:
			$canvas_layer/portrait_frame.position = Vector2(0, 128)
			$canvas_layer/label.rect_position = Vector2(36, 140)
			
	$canvas_layer/portrait.flip_h = flip
		
	$canvas_layer/label.rect_size = Vector2(208, 45)
	$canvas_layer/portrait.position = side
	if not characters.has(character):
		characters[character] = load( \
			"res://Assets/Portraits/" + character + "_portrait.png")
	$canvas_layer/portrait.texture = characters[character]
	
func function_1() -> void:
	# abstract function
	pass
	
func function_2() -> void:
	# abstract function
	pass
	
func function_3() -> void:
	# abstract function
	pass
	
func choose_go_next() -> void:
	$choose_ui.hide()
	$confirm_node.hide()
	$level/hex_grid.de_gayify_the_map()
	$level.cutscene_mode = true
	waiting = false
	dialog_ended = true
	auto_advance = true
	
func terminate() -> void:
	emit_signal("terminated")
	
func advance_after_fade(object : Object, key : NodePath, show : bool = false) -> void:
	dialog_ended = true
	auto_advance = true
	waiting = false
	
	if show:
		$canvas_layer/fade/color_rect.show()
	else:
		$canvas_layer/fade/color_rect.hide()
	
func advance_after_movement(unit) -> void:
	dialog_ended = true
	auto_advance = true
	waiting = false
	
func advance_after_anim(sprite) -> void:
	sprite.stop()
	sprite.frame = sprite.frames.get_frame_count(sprite.animation)
	auto_advance = true
	waiting = false
	
func exit_char_menu(menu) -> void:
	menu.hide()
	$level/cursor.show()
	
func _on_timer_timeout():
	if waiting:
		return
	$canvas_layer/label.visible_characters += 1
	if $canvas_layer/label.percent_visible >= 1.0:
		dialog_ended = true
		
func _on_hex_timer_timeout():
	pass
		
func _on_pause_timer_timeout():
	if waiting_for_pause_timer:
		dialog_ended = true
		auto_advance = true
		waiting = false
		waiting_for_pause_timer = false
		
func _on_confirm_button_pressed():
	if picked_units.size() < dialog[line][2]:
		$less_units_popup.popup()
	elif picked_units.size() == dialog[line][2]:
		choose_go_next()
	else:
		pass
	
func _on_less_units_popup_confirmed():
	choose_go_next()
