extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["pause", 0.8],
		["transition", "fade_in"], # animate drinking and pause
		["mishka", BOTTOM_LEFT, true, "So what's the black stuff on your\nneck?"],
		["grace_down_left", BOTTOM_RIGHT, false, "Nothing, just a tattoo."],
		["mishka_raise", BOTTOM_LEFT, true, "Huh..."],
		["transition", "fade_out"],
		["pause", 1.2], # animate drinking
		["transition", "fade_in"],
		["mishka_lookaway", BOTTOM_LEFT, true, "I have a very similar tattoo on my\nchest. But it's funny, I don't recall\never going to a tattoo parlor."],
		["grace_lookaway", BOTTOM_RIGHT, false, "..."],
		["mishka_down", BOTTOM_LEFT, true, "..."],
		["mishka", BOTTOM_LEFT, true, "About five years ago."],
		["mishka_lookaway", BOTTOM_LEFT, true, "My family and I were hit by a white\ntruck and we woke up in this world."],
		["grace_furrow", BOTTOM_RIGHT, false, "Huh?"],
		["mishka", BOTTOM_LEFT, true, "We were wandering around, trying to\nget our bearings when we got\nattacked."],
		["mishka_down", BOTTOM_LEFT, true, "My wife was killed and son taken\naway."],
		["grace_down", BOTTOM_RIGHT, false, "I'm sorry to hear that."],
		["mishka", BOTTOM_LEFT, true, "A few days later I noticed the black\nstuff spreading across my chest."],
		["grace_neutral", BOTTOM_RIGHT, false, "I'm guessing you couldn't afford the\nransom for your son?"],
		["mishka_lookaway", BOTTOM_LEFT, true, "We weren't registered with the city\nso they wouldn't've known who to\nransom him to."],
		["mishka_down", BOTTOM_LEFT, true, "And he was too young to be put to\nlabor, so they would have just killed\nhim."],
		["grace_raise", BOTTOM_RIGHT, false, "That's great then. You could just\nresurrect him."],
		["mishka_line", BOTTOM_LEFT, true, "We uhh..."],
		["mishka_down", BOTTOM_LEFT, true, "We don't work like that."],
		["mishka", BOTTOM_LEFT, true, "Where I come from you can't\nresurrect the dead."],
		["grace_neutral", BOTTOM_RIGHT, false, "What do you mean? Like you don't\nhave clinics there?"],
		["mishka_lookaway", BOTTOM_LEFT, true, "I mean you physically cannot bring\npeople back from the dead."],
		["grace_frown", BOTTOM_RIGHT, false, "So they're like stuck in the ethereal\nrealm forever?"],
		["mishka_line", BOTTOM_LEFT, true, "Not really, no."],
		["mishka", BOTTOM_LEFT, true, "We just die and our bodies stay\nthere. We become like any other\ninanimate object."],
		["grace_furrow", BOTTOM_RIGHT, false, "I don't get it."],
		["mishka_lookaway", BOTTOM_LEFT, true, "It'd be like..."],
		["mishka", BOTTOM_LEFT, true, "If you fell asleep and never woke up."],
		["mishka_down", BOTTOM_LEFT, true, "Except you weren't breathing and your\nheart wasn't pumping."],
		["mishka", BOTTOM_LEFT, true, "There's no dreams or REM cycles or\nanything."],
		["mishka_down", BOTTOM_LEFT, true, "It's just... Nothing."],
		["grace_lookaway", BOTTOM_RIGHT, false, "So what happens to your spirit then?"],
		["mishka_line", BOTTOM_LEFT, true, "We don't really have..."],
		["mishka_lookaway", BOTTOM_LEFT, true, "I mean there's no tangible..."],
		["mishka", BOTTOM_LEFT, true, "I guess it just doesn't really exist."],
		["grace_neutral", BOTTOM_RIGHT, false, "You're just lying there forever then?"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Well your body decomposes and\neventually returns to whatever carbon\natoms it was made from."],
		["grace_furrow", BOTTOM_RIGHT, false, "..."],
		["grace_down", BOTTOM_RIGHT, false, "I'm still having trouble wrapping my\nhead around the whole \"dying\npermanently\" thing."],
		["mishka", BOTTOM_LEFT, true, "Your mother gave birth to you, right?"],
		["grace_neutral", BOTTOM_RIGHT, false, "Well, yeah, of course."],
		["mishka", BOTTOM_LEFT, true, "What do you remember from before\nyou were born?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "Nothing, I mean I didn't exist before\nbeing born."],
		["mishka_lookaway", BOTTOM_LEFT, true, "Yeah, it's kind of like that. After\ndying you just... Cease to exist."],
		["grace_down", BOTTOM_RIGHT, false, "That's... Really weird to think about."],
		["grace_down_left", BOTTOM_RIGHT, false, "So your wife and son are..."],
		["mishka_down", BOTTOM_LEFT, true, "Gone..."],
		["grace_down", BOTTOM_RIGHT, false, "I'm sorry."],
		["transition", "fade_out"],
		["pause", 1.2], # animate drinking
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_8/dialog_level_8_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
