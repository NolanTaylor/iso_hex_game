extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["show", "seth"],
		["transition", "fade_in"], # play door sound and animate
		["acacius", BOTTOM_LEFT, true, "SETH!"],
		["illiana_happy", BOTTOM_RIGHT, false, "Welcome back."],
		["seth", TOP_RIGHT, false, "Thanks guys."],
		["grace", BOTTOM_LEFT, true, "Hey Seth."],
		["seth", TOP_RIGHT, false, "Hey Grace. Who's the kid?"],
		["grace_smile", BOTTOM_LEFT, true, "This is Foo. Say hi Foo."],
		["foo", TOP_LEFT, true, "He has black stuff on his face like\nyours Grace."],
		["grace_frown", BOTTOM_LEFT, true, "Shh, it's rude to point that out."],
		["grace_strain_smile", BOTTOM_LEFT, true, "Sorry Seth, he doesn't know any\nbetter."],
		["seth", TOP_RIGHT, false, "It's alright."],
		# move seth
		["seth", TOP_RIGHT, false, "The black stuff is called corruption,\neverybody has a little bit."],
		["seth", TOP_RIGHT, false, "It starts in your heart, above your\nchest, and grows outward as you go\nthrough pain or cause pain to others."],
		["seth", TOP_RIGHT, false, "Eventually it grows past what normal\nclothes can hide."],
		["seth", TOP_RIGHT, false, "But it's wrong to assume somebody's\na bad person because they have a lot\nof corruption."],
		["foo", TOP_LEFT, true, "So what happened to you?"],
		["grace_lookaway", BOTTOM_LEFT, true, "That's enough Foo."],
		["foo", TOP_LEFT, true, "Did you do something bad Grace?"],
		["grace_angry", BOTTOM_LEFT, true, "I said that's enough. Let's go, we\nhave work to do."],
		["transition", "fade_out_screen"], # maybe move idk
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/Interims/interim_menu_7.tscn")
