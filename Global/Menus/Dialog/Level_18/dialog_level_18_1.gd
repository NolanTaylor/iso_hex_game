extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["cherry", TOP_LEFT, true, "Everyone's finally accounted for."],
		["cherry_lookaway", TOP_LEFT, true, "This place is massive, almost a\nthousand victims were being held."],
		["cherry_narrow", TOP_LEFT, true, "Some having been trafficked for\nseveral decades."],
		["grace_neutral", TOP_RIGHT, false, "You have a list?"],
		["cherry", TOP_LEFT, true, "Just finished it, yeah."],
		["grace", TOP_RIGHT, false, "Can I see?"],
		["transition", "fade_out"],
		["pause", 1.0], # move and anim
		["transition", "fade_in"],
		["cherry_lookaway", TOP_LEFT, true, "Not many were our clients, but it's\ngood they're free anyways."],
		["grace_down", TOP_RIGHT, false, "..."],
		["cherry", TOP_LEFT, true, "If you're looking for Foo, he's with\nFiammetta."],
		["grace_down", TOP_RIGHT, false, "..."],
		["cherry_raise", TOP_LEFT, true, "Grace?"],
		["cherry_concern", TOP_LEFT, true, "Are you looking for someone else?"],
		["grace_down_left", TOP_RIGHT, false, "No..."],
		["transition", "fade_out"],
		["pause", 1.0], # anim and move foo/fia
		["transition", "fade_in"],
		["foo", BOTTOM_RIGHT, false, "Grace!"],
		["transition", "fade_out"],
		["pause", 0.4], # move foo/grace
		["transition", "fade_in"],
		["grace_pleading", BOTTOM_LEFT, true, "Foo, are you okay?"],
		["seth", TOP_RIGHT, false, "Sorry to interrupt but there are more\ncoming, we need to prepare."],
		["cherry", TOP_LEFT, true, "More coming? From where?"],
		["seth", TOP_RIGHT, false, "We tripped some sort of alarm."],
		["seth", TOP_RIGHT, false, "The closest open spaces to a bridge\nare the cells below. They'll be\nteleporting in from there."],
		["illiana", TOP_LEFT, true, "We don't have time to prepare our\nown teleports, we'll just have to fend\nthem off."],
		["foo", BOTTOM_RIGHT, false, "Are we fighting?"],
		["grace_neutral", BOTTOM_LEFT, true, "Yes, we'll have to fight."],
		["cherry", TOP_LEFT, true, "Not Foo."],
		["grace_frown", BOTTOM_LEFT, true, "We're not just going to leave him."],
		["cherry_roll", TOP_LEFT, true, "Grace, this is the second time he's\nbeen captured, you can't keep track\nof him in a chaotic battle."],
		["grace_angry", BOTTOM_LEFT, true, "It's better than leaving him alone."],
		["cherry_narrow", TOP_LEFT, true, "And what happened last time you\nbrought him into a fight?"],
		["grace_down_left", BOTTOM_LEFT, true, "..."],
		["grace_pout", BOTTOM_LEFT, true, "Where would you have me leave him?\nIn a cell?"],
		["cherry", TOP_LEFT, true, "We've got a professional locksmith\nwith us, she can ward the cell door."],
		["cherry_narrow", TOP_LEFT, true, "He'll be safer there than the middle\nof a fight."],
		["grace_furrow", BOTTOM_LEFT, true, "But then I can't see him."],
		["cherry_open", TOP_LEFT, true, "You can leave him this once. He's not\ngoing to disappear if you so much as\nblink."],
		["grace_down", BOTTOM_LEFT, true, "..."],
		["acacius", TOP_RIGHT, false, "I hear them on the stairs. We need to\nleave now."],
		["cherry_concern", TOP_LEFT, true, "Grace, please. We need your full\nattention for this fight."],
		["transition", "fade_out"],
		["pause", 1.0], # move all
		["transition", "fade_in"],
		["foo", BOTTOM_RIGHT, false, "You're going to leave me again?"],
		["grace_down_left", BOTTOM_LEFT, true, "..."],
		["grace_furrow", BOTTOM_LEFT, true, "I'm sorry. It's too dangerous for you\nto come. You'll be safe in the cell."],
		["foo", BOTTOM_RIGHT, false, "How long will you be gone?"],
		["grace_worried", BOTTOM_LEFT, true, "I don't know. But I'll come back I\npromise."],
		["grace_neutral", BOTTOM_LEFT, true, "Take this."],
		["transition", "fade_out"],
		["pause", 1.0], # anim and move grace (maybe zoom)
		["transition", "fade_in"],
		["grace_frown", BOTTOM_LEFT, true, "If anybody other than me opens the\ncell, point this between your eyes\nand pull the trigger."],
		["grace_neutral", BOTTOM_LEFT, true, "I can resurrect you later."],
		["foo", BOTTOM_RIGHT, false, "..."],
		["grace_pleading", BOTTOM_LEFT, true, "Foo, do you understand? If it's\nanybody but me you need to kill\nyourself. I can't lose you again."],
		["foo", BOTTOM_RIGHT, false, "A gun?"],
		["grace_hmm", BOTTOM_LEFT, true, "Wait, how do you know-"],
		["cherry_open", BOTTOM_RIGHT, false, "GRACE!"],
		["grace_lookaway", BOTTOM_LEFT, true, "Coming!"],
		["transition", "fade_out"],
		["pause", 1.0], # move grace
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_18.tscn")
