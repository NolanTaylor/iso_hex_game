extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["pause", 1.0], # move mishka
		["transition", "fade_in"],
		["mishka", BOTTOM_RIGHT, false, "Alexei?"],
		["foo", BOTTOM_LEFT, true, "..."],
		["mishka_open", BOTTOM_RIGHT, false, "Holy shit. Alexei, open the door."],
		["foo", BOTTOM_LEFT, true, "You're not Grace."],
		["mishka_lookaway", BOTTOM_RIGHT, false, "Who the fuck is Grace? Help me open\nthis."],
		["transition", "fade_out"],
		["pause", 1.0], # anim
		["transition", "fade_in"],
		["foo", BOTTOM_LEFT, true, "Who are you?"],
		["mishka_shock", BOTTOM_RIGHT, false, "You don't recognize me?"],
		["foo", BOTTOM_LEFT, true, "I-I can't remember. She told me to\nshoot if anyone except her came."],
		["transition", "fade_out"],
		["pause", 1.0], # anim
		["transition", "fade_in"],
		["mishka_narrow", BOTTOM_RIGHT, false, "Where did you get that?"],
		["foo", BOTTOM_LEFT, true, "You're not Grace."],
		["mishka_shock", BOTTOM_RIGHT, false, "Alexei, please."],
		["foo", BOTTOM_LEFT, true, "She'll resurrect me."],
		["mishka_open", BOTTOM_RIGHT, false, "Alexei-!"],
		["transition", "fade_out"],
		["pause", 1.0], # anim :eyes:
		["transition", "fade_in"],
		["cherry", BOTTOM_LEFT, true, "I'm sure he's fine."],
		["mishka_lookaway", BOTTOM_RIGHT, false, "..."],
		["transition", "fade_out"],
		["pause", 0.4], # anim
		["transition", "fade_out_screen"],
		["pause", 0.4],
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["grace_furrow", BOTTOM_RIGHT, false, "I heard a gun shot."],
		["cherry_lookaway", BOTTOM_LEFT, true, "Oh, he's still alive."],
		["transition", "fade_out"],
		["pause", 0.4], # anim
		["transition", "fade_in"],
		["cherry_concern", BOTTOM_LEFT, true, "Umm... He's not breathing!?"],
		["grace_furrow", BOTTOM_RIGHT, false, "..."],
		["cherry", BOTTOM_LEFT, true, "I don't get it, why is his body still\nhere?"],
		["grace_set_1", BOTTOM_RIGHT, false, "..."],
		["transition", "fade_out"],
		["pause", 0.4], # anim
		["transition", "fade_in"],
		["cherry_raise", BOTTOM_LEFT, true, "Grace?"],
		["grace_set_2", BOTTOM_RIGHT, false, "..."],
		["transition", "fade_out"],
		["pause", 0.4], # anim
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_19/dialog_level_19_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
