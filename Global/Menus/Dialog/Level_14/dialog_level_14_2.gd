extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["show", "grace"],
		["show", "acacius"],
		["move", "acacius", PoolVector2Array([Vector2(207, 72), Vector2(184, 80), Vector2(161, 88), \
			Vector2(161, 104), Vector2(161, 120), Vector2(138, 128), Vector2(115, 120), \
				Vector2(92, 112), Vector2(69, 120)]), "continue"],
		["pause", 0.4],
		["move", "grace", PoolVector2Array([Vector2(230, 64), Vector2(207, 72), Vector2(184, 80), \
			Vector2(184, 96), Vector2(184, 112), Vector2(161, 120), Vector2(138, 128), \
				Vector2(115, 120), Vector2(92, 112)]), "continue"],
		["pause", 0.6],
		["show", "foo"],
		["move", "foo", PoolVector2Array([Vector2(230, 64), Vector2(207, 72), Vector2(184, 80), \
			Vector2(184, 96), Vector2(184, 112), Vector2(161, 120), Vector2(138, 128), \
				Vector2(115, 120)])],
		["pause", 0.8],
		["transition", "fade_in"],
		["acacius", BOTTOM_LEFT, true, "There's a warehouse in the upper\ncity. I found it the other night\nwalking through that area."],
		["acacius", BOTTOM_LEFT, true, "There were people keeping watch\nwith generic cosmetic enchantments."],
		["grace_hmm", BOTTOM_RIGHT, false, "The one that makes them all look like\nthe same person with slightly\ndifferent haircuts?"],
		["acacius", BOTTOM_LEFT, true, "They very same. I believe the place\nis connected to the group that was\ntrafficking Foo."],
		["grace_smile", BOTTOM_RIGHT, false, "Let's go check it out then."],
		["acacius", BOTTOM_LEFT, true, "..."],
		["acacius", BOTTOM_LEFT, true, "Illy, you good with this?"],
		["illiana", BOTTOM_RIGHT, false, "..."],
		["illiana_raise", BOTTOM_RIGHT, false, "Ah."],
		["illiana_lookaway", BOTTOM_RIGHT, false, "We don't really get paid for\ntrafficking victims around upper city."],
		["illiana_open", BOTTOM_RIGHT, false, "But if you think it will help Foo, then\nyou can take the day off to search it."],
		["grace_frown", BOTTOM_RIGHT, false, "I'm going to go. How about you?"],
		["acacius", BOTTOM_LEFT, true, "Well, I'm not letting you go alone, So\nI guess I'm in."],
		["grace_smile", BOTTOM_RIGHT, false, "Great. Illiana, we'll be going then."],
		["illiana", BOTTOM_RIGHT, false, "Be safe."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/Interims/interim_menu_14.tscn")
