extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"], # move grace/foo
		["grace_down", BOTTOM_LEFT, true, "I'm sorry we couldn't find anything\ntoday."],
		["foo", BOTTOM_RIGHT, false, "It's okay, it was fun breaking into the\nwarehouse."],
		["grace_down_left", BOTTOM_LEFT, true, "Yeah."],
		["grace_happy", BOTTOM_LEFT, true, "Goodnight Foo."],
		["transition", "fade_out"], # move foo
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_15/dialog_level_15_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
