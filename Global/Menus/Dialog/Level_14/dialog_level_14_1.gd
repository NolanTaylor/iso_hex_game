extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"], # move acacius
		["acacius", BOTTOM_RIGHT, false, "Why is the tree orange?"],
		["grace", BOTTOM_LEFT, true, "I turned it orange."],
		["foo", TOP_LEFT, true, "..."],
		["acacius", BOTTOM_RIGHT, false, "Cool."],
		["acacius", BOTTOM_RIGHT, false, "By the way Grace, I found something\nyou might want to see."],
		["grace_hmm", BOTTOM_LEFT, true, "What is it?"],
		["acacius", BOTTOM_RIGHT, false, "It's inside."],
		["transition", "fade_out"], # move characters inside
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_14/dialog_level_14_2.tscn")
