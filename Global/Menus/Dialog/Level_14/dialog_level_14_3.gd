extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["acacius", BOTTOM_LEFT, true, "I've searched every drawer, cabinet,\nand shelf."],
		["acacius", BOTTOM_LEFT, true, "There are files for every victim\ntrafficked, transaction made, and\nransom paid."],
		["acacius", BOTTOM_LEFT, true, "Everything except for Foo."],
		["grace_hmm", BOTTOM_RIGHT, false, "Well he'd be under a different name\nright?"],
		["acacius", BOTTOM_LEFT, true, "Most likely. But there are\ndescriptions and dates for every\nvictim and still nothing matches Foo."],
		["grace_neutral", BOTTOM_RIGHT, false, "..."], # move grace
		["grace_furrow", BOTTOM_RIGHT, false, "There's a secret door here."],
		["acacius", BOTTOM_LEFT, true, "What?"],
		["grace_frown", BOTTOM_RIGHT, true, "There's a light source behind this\nwall. I can tell."],
		["acacius", BOTTOM_LEFT, true, "The construction crew probably just\nleft their light orbs between the\ndrywall."],
		["grace_hmm", BOTTOM_RIGHT, true, "It doesn't seem like that."],
		["acacius", BOTTOM_LEFT, true, "Grace, we've been searching for\nseven hours and found nothing."],
		["acacius", BOTTOM_LEFT, true, "Even if there is a secret room you\nhave no idea how to get it open."],
		["acacius", BOTTOM_LEFT, true, "We can come back tomorrow with a\nlocksmith and eight hours of sleep."],
		["grace_neutral", BOTTOM_RIGHT, true, "All the guards will be resurrected\ntomorrow and there'll be higher\nsecurity now that we've broken in."],
		["acacius", BOTTOM_LEFT, true, "I'm going home Grace. And I think you\nshould too."],
		["grace_lookaway", BOTTOM_RIGHT, false, "..."],
		["foo", BOTTOM_LEFT, true, "I'm tired."],
		["grace_down_left", BOTTOM_RIGHT, false, "Okay, we can go."],
		["transition", "fade_out"], # move party
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_14/dialog_level_14_4.tscn")
