extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"], # move
		["transition", "fade_in"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Are you going to drug me and sell my\norgans?"],
		["grace_frown", BOTTOM_RIGHT, true, "What?"],
		["mishka_raise", BOTTOM_LEFT, true, "Why are we at an old warehouse in\nthe middle of the night?"],
		["grace_hmm", BOTTOM_RIGHT, false, "I just want to check something, and\nyou're here to keep watch."],
		["mishka_narrow", BOTTOM_LEFT, true, "What am I watching for?"],
		["grace_neutral", BOTTOM_RIGHT, false, "A young guy with fair skin, wearing a\ndress shirt and slacks."],
		["mishka_line", BOTTOM_LEFT, true, "That's the vaguest description of a\nperson I've ever heard."],
		["grace_lookaway", BOTTOM_RIGHT, false, "You'll know exactly what I'm talking\nabout if you see them."],
		["mishka", BOTTOM_LEFT, true, "And what do I do if I see them?"],
		["grace_dismiss", BOTTOM_RIGHT, false, "Just scream, before he tries to kill\nyou."],
		["mishka_raise", BOTTOM_LEFT, true, "..."],
		["mishka_narrow", BOTTOM_LEFT, true, "What exactly are you doing here?"],
		["transition", "fade_out"], # move and pan camera
		["pause", 1.0],
		["transition", "fade_in"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Jesus Christ, learn to use Excel."],
		["grace_frown", BOTTOM_RIGHT, true, "Who is that?"],
		["mishka", BOTTOM_LEFT, true, "Who is who?"],
		["grace_lookaway", BOTTOM_RIGHT, true, "Jesus Christ. You've said their name\na few times before."],
		["mishka_lookaway", BOTTOM_LEFT, true, "Oh, it's not really a person. Just an\nexpression."],
		["mishka_line", BOTTOM_LEFT, true, "I mean, technically he is a person,\nbut I don't really know him."],
		["grace", BOTTOM_RIGHT, true, "Is he like a celebrity?"],
		["mishka", BOTTOM_LEFT, true, "Yeah, he's pretty popular you could\nsay."],
		["grace_hmm", BOTTOM_RIGHT, true, "What does he do?"],
		["mishka_raise", BOTTOM_LEFT, true, "What do you mean?"],
		["grace_dismiss", BOTTOM_RIGHT, true, "Well, like, why is he popular?"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Oh. I'm not really sure, I've never\nbeen a big follower."],
		["mishka_narrow", BOTTOM_LEFT, true, "His fan base is kinda weird."],
		["mishka", BOTTOM_LEFT, true, "But I think he said some stuff about\nlove thy neighbor and he could turn\nwater into wine."],
		["grace_frown", BOTTOM_RIGHT, true, "I thought you didn't have magic where\nyou're from."],
		["mishka_lookaway", BOTTOM_LEFT, true, "Uhh... It's complicated."],
		["grace_hmm", BOTTOM_RIGHT, true, "Hm. It's interesting."],
		["grace_neutral", BOTTOM_RIGHT, true, "Magic has undergone a lot of\nsemantic change."],
		["grace_frown", BOTTOM_RIGHT, true, "It used to be a word we used to\ndescribe things we didn't understand."],
		["grace_neutral", BOTTOM_RIGHT, true, "Phenomena that couldn't be explained\nby any other reasoning."],
		["grace", BOTTOM_RIGHT, true, "But as we learned and grew to\nunderstand them we continued to use\nthe word magic to describe them."],
		["grace_sweat_smile", BOTTOM_RIGHT, true, "It's at a point now where magic has\nan entirely opposite meaning to its\noriginal definition."],
		["grace", BOTTOM_RIGHT, true, "It's a rigid system of rules and\ninteraction that we can control and\npredict."],
		["grace_dismiss", BOTTOM_RIGHT, true, "I can cast a spell and be completely\nconfident in what it will do."],
		["grace_hmm", BOTTOM_RIGHT, true, "People say magic has lost that sense\nof, well magic, that came from not\nknowing how it worked."],
		["mishka_smile", BOTTOM_LEFT, true, "Where I'm from, we have a word for\nthat system of rules and predictability.\nWe call it science."],
		["grace_lookaway", BOTTOM_RIGHT, true, "Science?"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Yeah. When I say magic doesn't exist\nwhere I'm from, I don't mean we've\nlost it as a concept."],
		["mishka", BOTTOM_LEFT, true, "There are things I see here that we\nwould call magic because we don't\nhave an explanation for them."],
		["mishka_lookaway", BOTTOM_LEFT, true, "And wouldn't be able to replicate at\nhome."],
		["mishka_line", BOTTOM_LEFT, true, "Of course, we have things that you\ndon't have, like cell phones and\nautomobiles."],
		["mishka", BOTTOM_LEFT, true, "But we don't call that magic because\nwe understand how they work."],
		["grace_neutral", BOTTOM_RIGHT, true, "So instead you call it science?"],
		["mishka_line", BOTTOM_LEFT, true, "Pretty much, yeah."],
		["grace", BOTTOM_RIGHT, true, "That makes a lot more sense than\nwhat we do."],
		["grace_smile", BOTTOM_RIGHT, true, "Anyway, I got the lock open."],
		["transition", "fade_out"],
		["pause", 1.0], # animate door
		["transition", "fade_in"],
		["grace_smug", BOTTOM_RIGHT, true, "I knew there was a secret room here."],
		["mishka_lookaway", BOTTOM_LEFT, true, "I think I hear footsteps."],
		["transition", "fade_out"],
		["pause", 0.8], # show trafficker and move
		["transition", "fade_in"],
		["phrank", TOP_LEFT, true, "Jake is that you?"],
		["mishka", BOTTOM_LEFT, true, "Oh shit."],
		["transition", "fade_out"],
		["pause", 0.8], # animate gun
		["transition", "fade_in"],
		["grace_surprise", BOTTOM_RIGHT, false, "..."],
		["grace_raise", BOTTOM_RIGHT, false, "What kind of spell was that?"],
		["mishka_lookaway", BOTTOM_LEFT, true, "Uhh... Bullet?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "There will be more coming. Go into\nthe room and grab everything you\ncan."],
		["transition", "fade_out"], # move
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_15.tscn")
