extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["mishka_raise", BOTTOM_LEFT, true, "What's so important about these\npapers?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "It's a long story, but I think they hold\nsome answers to a mystery I've had\nfor a while."],
		["mishka", BOTTOM_LEFT, true, "Well, I hope you find whatever you're\nlooking for. It was certainly a lot of\ntrouble to get them."],
		["grace_down_left", BOTTOM_RIGHT, false, "Yeah. Sorry again for dragging you\ninto this."],
		["grace", BOTTOM_RIGHT, false, "Although, you were more than capable\nof holding your own out there."],
		["mishka_lookaway", BOTTOM_LEFT, true, "I still don't think I ever want to do\nthat again."],
		["mishka_down", BOTTOM_LEFT, true, "The whole dying permanently thing is\nstill something I'm not quite ready for."],
		["grace_down", BOTTOM_RIGHT, false, "Right."],
		["transition", "fade_out"],
		["pause", 0.4], # move mishka
		["transition", "fade_in"],
		["mishka", BOTTOM_LEFT, true, "Take it."],
		["grace_raise", BOTTOM_RIGHT, false, "Your spell thing? Don't you need this?"],
		["mishka_lookaway", BOTTOM_LEFT, true, "It's called a gun, and I've got more."],
		["mishka_down", BOTTOM_LEFT, true, "You seem like you could make better\nuse of it anyways."],
		["grace_raise", BOTTOM_RIGHT, false, "I... Thank you."],
		["grace_frown", BOTTOM_RIGHT, false, "By the way, have you ever heard of\ndivination?"],
		["mishka_raise", BOTTOM_LEFT, true, "Like fortune telling?"],
		["grace", BOTTOM_RIGHT, false, "Yeah, predicting the future."],
		["mishka", BOTTOM_LEFT, true, "Where I'm from those are pretty much\nall scams."],
		["grace_lookaway", BOTTOM_RIGHT, false, "Well here they're not always scams."],
		["grace_frown", BOTTOM_RIGHT, false, "We don't fully understand why, but\nsometimes they work."],
		["grace", BOTTOM_RIGHT, false, "In that way I guess it's the one type\nof magic that's still sort of magical."],
		["grace_down_left", BOTTOM_RIGHT, false, "Anyway, it's never really worked for\nme; but maybe it would be different\nfor you."],
		["mishka_down", BOTTOM_LEFT, true, "Maybe."],
		["mishka", BOTTOM_LEFT, true, "I'm not sure how I feel about knowing\nthe future. But thank you for letting\nme know."],
		["grace_lookaway", BOTTOM_RIGHT, false, "Yeah..."],
		["grace_neutral", BOTTOM_RIGHT, false, "I'll see you then."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_16/dialog_level_16_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
