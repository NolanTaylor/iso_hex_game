extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["grace_neutral", BOTTOM_RIGHT, false, "He's being held by the Skyview\nPromenade."],
		["grace_neutral", BOTTOM_RIGHT, false, "From what the scrying mage told me\nit looks like an abandoned bell tower."],
		["illiana_worried", BOTTOM_LEFT, true, "That's a wealthy district, why would\nthey traffic victims through there?"],
		["seth", TOP_LEFT, true, "Especially since law enforcement\nactually bothers to patrol the\nneighborhoods higher up."],
		["grace_furrow", BOTTOM_RIGHT, false, "No time to question it, we need to\nleave as soon as possible."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/Interims/interim_menu_9.tscn")
