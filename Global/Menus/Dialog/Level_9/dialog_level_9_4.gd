extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["grace_lookaway", BOTTOM_RIGHT, false, "So if you've never died, how do you\nknow what happens?"],
		["mishka_line", BOTTOM_LEFT, true, "I suppose technically I don't."],
		["mishka", BOTTOM_LEFT, true, "But there's evidence enough to make\na reasonable assumption."],
		["mishka_lookaway", BOTTOM_LEFT, true, "Scans that show no brain activity\nand stuff like that."],
		["grace_neutral", BOTTOM_RIGHT, false, "Do you think your family might still\nbe out there? Just in a different way\nor something?"],
		["mishka_down", BOTTOM_LEFT, true, "Some people believe in an afterlife\nor place after death, and maybe it's\ntrue."],
		["mishka", BOTTOM_LEFT, true, "But the only thing we know for\ncertain is we don't come back after\ndying."],
		["grace_down_left", BOTTOM_RIGHT, false, "..."],
		["grace_lookaway", BOTTOM_RIGHT, false, "When I was young my brother got\nkidnapped."],
		["grace_neutral", BOTTOM_RIGHT, false, "We lived in a small town and there\nwas a forest nearby that we played\nin."],
		["grace_neutral", BOTTOM_RIGHT, false, "We knew how to kill ourselves\nin case of emergency, but nothing\never happened."],
		["grace_down", BOTTOM_RIGHT, false, "Until of course it did."],
		["grace_lookaway", BOTTOM_RIGHT, false, "We were out there practicing magic,\njust making rocks float and stuff."],
		["grace_neutral", BOTTOM_RIGHT, false, "I had turned around for a moment to\npick up the rocks we were using and\nthey came out of nowhere."],
		["grace_down_left", BOTTOM_RIGHT, false, "They grabbed my brother and tried to\ngrab me."],
		["grace_down", BOTTOM_RIGHT, false, "I was able to kill myself, but he\nwasn't so lucky."],
		["grace_furrow", BOTTOM_RIGHT, false, "They have these special binds that\nsuppress magic."],
		["mishka_raise", BOTTOM_LEFT, true, "They keep him alive so you can't\nresurrect him?"],
		["grace_neutral", BOTTOM_RIGHT, false, "Pretty much, yeah."],
		["grace_lookaway", BOTTOM_RIGHT, false, "My family couldn't afford the ransom,\nso they'd've trafficked him into labor."],
		["grace_down", BOTTOM_RIGHT, false, "That night after losing him I found\nthe first signs of corruption\nspreading over my chest."],
		["grace_neutral", BOTTOM_RIGHT, false, "My parents told me it was normal,\nthat everybody has some and it would\nfade with time."],
		["grace_down_left", BOTTOM_RIGHT, false, "And they're right, but it didn't feel\nany better."],
		["mishka", BOTTOM_LEFT, true, "You get the corruption just by being\nhere?"],
		["grace_neutral", BOTTOM_RIGHT, false, "I don't know anything about the magic\nbehind it, but I guess so if you found\nit on yourself."],
		["mishka_raise", BOTTOM_LEFT, true, "Is there any way to get rid of it?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "It's supposed to fade with time, but\nmine only seems to grow."],
		["grace_neutral", BOTTOM_RIGHT, false, "I've tried several times to locate my\nbrother with scrying or tracking, but\nevery result has been a red herring."],
		["grace_down_left", BOTTOM_RIGHT, false, "And every time the corruption\nspreads further."],
		["grace_sweat_smile", BOTTOM_RIGHT, false, "Most people I know hide their visible\ncorruption with enchantment magic or\nphysical makeup and concealer."],
		["mishka", BOTTOM_LEFT, true, "You don't hide yours?"],
		["grace_lookaway", BOTTOM_RIGHT, false, "I did for a while, but kind of gave\nup."],
		["grace_furrow", BOTTOM_RIGHT, false, "Needing to reapply the enchantments\nevery few weeks just reminded me of\neverything that had happened."],
		["mishka_down", BOTTOM_LEFT, true, "I'm sorry."],
		["grace_down", BOTTOM_RIGHT, false, "I feel like one day I just blinked and\nhe was gone."],
		["grace_down_left", BOTTOM_RIGHT, false, "..."],
		["grace_lookaway", BOTTOM_RIGHT, false, "I know it's not the same as your\nfamily, but maybe that's what it's like\nto lose somebody permanently."],
		["mishka", BOTTOM_LEFT, true, "Loss is still loss. You may find him\nagain someday, but that won't make\nyour time apart any less meaningful."],
		["grace_down", BOTTOM_RIGHT, false, "Thanks..."],
		["transition", "fade_out_screen"], # animate
	]
	
func terminate() -> void:
	Globals.buffered_scene = "res://Global/Menus/Dialog/Level_10/dialog_level_10_1.tscn"
	get_tree().change_scene("res://Global/Menus/UIMenus/save_menu.tscn")
