extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in"],
		["acacius", BOTTOM_LEFT, true, "Hey Grace. How're you holding up?"],
		["grace_lookaway", BOTTOM_RIGHT, true, "I'm fine. Why?"],
		["acacius", BOTTOM_LEFT, true, "It's not your fault we lost Foo.\nAnything could have happened."],
		["grace_down", BOTTOM_RIGHT, true, "It is my fault, I wasn't watching him\nclose enough."],
		["grace_angry", BOTTOM_RIGHT, true, "..."],
		["grace_lookaway", BOTTOM_RIGHT, true, "But it's fine. I'm not going to wallow\nin self pity if that's what you're\nworried about."],
		["acacius", BOTTOM_LEFT, true, "Uhh, yeah. Well, we'll get him back."],
		["grace_angry", BOTTOM_RIGHT, true, "Yes, we will."],
	]

func _ready():
	pass
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_9.tscn")
