extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["grace_strain_smile", BOTTOM_LEFT, true, "Can I sleep here tonight?"],
		["illiana_raise", BOTTOM_RIGHT, false, "I'm sorry?"],
		["grace_lookaway", BOTTOM_LEFT, true, "Just on the couch, for one night?"],
		["illiana_worried", BOTTOM_RIGHT, false, "What's wrong, did your apartment\nflood?"],
		["grace_down_left", BOTTOM_LEFT, true, "No, but I just..."],
		["illiana", BOTTOM_RIGHT, false, "Grace. I know a lot just happened,\nbut he's safe here. You don't need to\nbe constantly watching him."],
		["grace_down", BOTTOM_LEFT, true, "I'd just feel more comfortable if I\nwere here."],
		["illiana_purse", BOTTOM_RIGHT, false, "Do you not trust me?"],
		["grace_furrow", BOTTOM_LEFT, true, "No, I trust you Illiana. But..."],
		["illiana_worried", BOTTOM_RIGHT, false, "Then go home, Grace. You'll see him\nin the morning."],
		["grace_down", BOTTOM_LEFT, true, "..."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/Dialog/Level_9/dialog_level_9_4.tscn")
