extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["transition", "fade_in"],
		["illiana", BOTTOM_LEFT, true, "Alright, we're aiming for the cells on\nthe lowest floor."],
		["illiana_open", BOTTOM_LEFT, true, "Make sure you're in range and\nteleport when you're ready."],
		["fia", TOP_LEFT, true, "You got it chief."],
		["transition", "fade_out"],
		["pause", 0.4], # anim fia
		["transition", "fade_in"],
		["acacius", TOP_LEFT, true, "This should be fun."],
		["transition", "fade_out"],
		["pause", 0.4], # anim acacius
		["transition", "fade_in"],
		["cherry", TOP_LEFT, true, "I'm going to regret this."],
		["transition", "fade_out"],
		["pause", 0.4], # anim cherry
		["transition", "fade_in"],
		["seth", TOP_LEFT, true, "See you there."],
		["transition", "fade_out"],
		["pause", 0.4], # anim seth
		["transition", "fade_in"],
		["grace_down", BOTTOM_RIGHT, true, "..."],
		["illiana_worried", BOTTOM_LEFT, true, "Grace?"],
		["grace_down", BOTTOM_RIGHT, true, "Why did it work for him?"],
		["illiana", BOTTOM_LEFT, true, "I'm sorry?"],
		["grace_neutral", BOTTOM_RIGHT, false, "Have you ever used divination\nIlliana?"],
		["illiana_purse", BOTTOM_LEFT, true, "Once or twice. Something about\nknowing the future gives me anxiety."],
		["grace_furrow", BOTTOM_RIGHT, false, "I've tried it so many times and never\ngotten a solid result."],
		["grace_frown", BOTTOM_RIGHT, false, "Then he tries it once and finds\nexactly what he's looking for."],
		["grace_down_left", BOTTOM_RIGHT, false, "I know I should be happy for him, but\nsome part of me feels wronged by\nthe universe."],
		["illiana_down", BOTTOM_LEFT, true, "The universe seldom does right by\nanyone."],
		["illiana_frown", BOTTOM_LEFT, true, "The best we can do is keep moving\nforward."],
		["illiana", BOTTOM_LEFT, true, "And right now Foo needs your help."],
		["grace_neutral", BOTTOM_RIGHT, false, "You're right. I can't lose him too."],
		["transition", "fade_out"],
		["pause", 0.4], # anim grace
		["transition", "fade_in"],
		["illiana_worried", BOTTOM_LEFT, true, "Hmm..."],
		["illiana_down", BOTTOM_LEFT, true, "..."],
		["transition", "fade_out"],
		["pause", 0.4], # anim illiana
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Levels/level_17.tscn")
