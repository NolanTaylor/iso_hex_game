extends "res://Global/Menus/Dialog/dialog.gd"

func init_dialog() -> void:
	$level.toggle_cutscene_mode()
	
	dialog = [
		["transition", "fade_in_screen"],
		["pause", 0.4], # anim door
		["show", "grace"],
		["pause", 0.4],
		["move", "grace", PoolVector2Array([Vector2(207, 72), Vector2(207, 88), Vector2(184, 96), \
			Vector2(184, 112), Vector2(161, 120)])],
		["transition", "fade_in"],
		["illiana_purse", TOP_RIGHT, false, "By the way Grace, some creepy old\nguy came by this morning and left a\nnote for you."],
		["grace_lookaway", BOTTOM_RIGHT, false, "Oh, thanks."],
		["illiana_lookaway", TOP_RIGHT, false, "Weirdly he didn't know your name.\nJust said it's for the girl with pink\neyes."],
		["acacius", BOTTOM_LEFT, true, "I thought you were ace, why are you\nseeing old men?"],
		["grace_down_left", BOTTOM_RIGHT, false, "I am. It's... not like that."],
		["transition", "fade_out"],
		["pause", 0.4], # open note
		["transition", "fade_in"],
		["note", BOTTOM_LEFT, false, "You should really invent cellphones\nor something, this shit is tedious."],
		["note", BOTTOM_LEFT, false, "Anyway, I thought about what you\nsaid and I saw a divination mage last\nnight."],
		["note", BOTTOM_LEFT, false, "(or maybe it was scrying, I couldn't\ntell)."],
		["note", BOTTOM_LEFT, false, "The visions were messy and cryptic,\nbut the vibe is I think my son might\nbe alive."],
		["note", BOTTOM_LEFT, false, "I'm going to look for him today, but I\nwanted to let you know because\nhowever this works out I'll be leaving."],
		["note", BOTTOM_LEFT, false, "I've been feeling sorry for myself\nlong enough and it's time I returned\nto my reality."],
		["note", BOTTOM_LEFT, false, "Thank you for being a good friend."],
		["grace_down", BOTTOM_RIGHT, false, "..."],
		["illiana", TOP_RIGHT, false, "Anyways, we should start getting\nready."],
		["cherry_raise", TOP_LEFT, true, "How can we expect to take on this\norganization when we failed so\nmiserably last time."],
		["illiana_smile", TOP_RIGHT, false, "That's what preparation is for. We\nknow where they are now, so we can\nteleport directly in."],
		["seth", TOP_LEFT, true, "Surely they'll have wards against\nteleportation."],
		["fia", BOTTOM_LEFT, true, "Actually, we saw them use\nteleportation on us so there's nothing\nstopping that."],
		["illiana_open", TOP_RIGHT, false, "Grace, what do you think?"],
		["grace_down", BOTTOM_RIGHT, false, "..."],
		["illiana_frown", TOP_RIGHT, false, "Grace?"],
		["grace_raise", BOTTOM_RIGHT, false, "Huh?"],
		["illiana_worried", TOP_RIGHT, false, "Please pay attention, we're doing this\nfor you after all."],
		["grace_down_left", BOTTOM_RIGHT, false, "Right... Thanks guys."],
		["transition", "fade_out_screen"],
	]
	
func terminate() -> void:
	get_tree().change_scene("res://Global/Menus/UIMenus/Interims/interim_menu_17.tscn")
