extends Node2D

var starting_modulate : Color = modulate
var disabled_modulate : Color = Color(starting_modulate.r / 2, starting_modulate.g / 2, \
	starting_modulate.b / 2)
var toss_mode : bool = false

signal button_pressed(node)
signal button_entered(node)
signal button_left(node)

func _ready():
	if toss_mode:
		$button.show()
	else:
		$button.hide()
		
	$button.connect("gui_input", self, "mouse_input")
	$button.connect("mouse_entered", self, "mouse_entered")
	$button.connect("mouse_exited", self, "mouse_exited")
	
func mouse_input(event):
	if event is InputEventMouseButton and event.pressed and not $button.disabled:
		if event.button_index == BUTTON_LEFT:
			emit_signal("button_pressed", self)
			if $action_subfolder.get_child_count() > 0:
				for sibling in get_parent().get_children():
					sibling.undisplay()
				for i in $action_subfolder.get_child_count():
					$action_subfolder.get_child(i).display_to(Globals.ACTION_COORDS[i])
		elif event.button_index == BUTTON_RIGHT:
			pass
			
func mouse_entered() -> void:
	if not $button.disabled:
		emit_signal("button_entered", self)
		
func mouse_exited() -> void:
	if not $button.disabled:
		emit_signal("button_left", self)
		
func enable() -> void:
	$button.disabled = false
	
func disable() -> void:
	$button.disabled = true
	
func display_to(pos : Vector2, flip : bool = false) -> void:
	if $button.disabled:
		self.modulate = disabled_modulate
	else:
		self.modulate = starting_modulate
		
	if flip:
		$button.rect_position = Vector2(16, 0)
	else:
		$button.rect_position = Vector2(0, 0)
	$button.show()
	$tween.interpolate_property($button, "rect_position", $button.rect_position, \
		pos, 0.6, Tween.TRANS_ELASTIC)
	$tween_opacity.interpolate_property($button, "modulate:a", 0.0, 1.0, 0.4, Tween.TRANS_LINEAR)
	$tween.start()
	$tween_opacity.start()
	
func undisplay() -> void:
	$button.hide()
	$button.disabled = false
	$button.rect_position = Vector2(0, 0)
	
func undisplay_all() -> void:
	undisplay()
	for child in $action_subfolder.get_children():
		child.undisplay_all()
