extends Node2D

signal animation_finished

func _ready():
	pass
	
func turn_animation(color : Color = Color.white) -> void:
	$hexes.change_color(color)
	$hexes.show()
	$hexes.animate_hexes_in($tween)
	
func _on_tween_tween_all_completed():
	if $hexes.finished:
		$hexes.hide()
		emit_signal("animation_finished")
	else:
		$hexes.animate_hexes_out($tween)
