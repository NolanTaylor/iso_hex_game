extends "res://Conditions/condition.gd"

var triggers : Dictionary = {} # <unit, int>

func _ready():
	pass
	
func remove_trigger(unit) -> void:
	triggers.erase(unit)
	
	if triggers.empty():
		remove_condition()
