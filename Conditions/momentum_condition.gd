extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func initial_apply(intensity : int) -> void:
	this_unit.add_mod("damage", "stab", intensity)
	this_unit.add_mod("damage", "spell", intensity)
	
	.initial_apply(intensity)
	
func reapply(intensity : int) -> void:
	return
	
func remove_condition() -> void:
	this_unit.damage_modifications["stab"] -= intensity
	this_unit.damage_modifications["spell"] -= intensity
	
	.remove_condition()
	
func endstep() -> void:
	remove_condition()
