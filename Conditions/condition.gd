extends Node2D

const EXPIRE : PoolStringArray = PoolStringArray([
	"Blind",
	"Charge",
	"Spur",
])

export var proper_name : String = "name"
export var condition_type : String = "public_condition"
export var condition_icon : Texture

onready var this_unit = get_node("../../")

var intensity : int = 0
var extended : bool = false

func _ready():
	pass
	
func initial_apply(intensity : int) -> void:
	self.intensity = intensity
	if Globals.is_valid(this_unit):
		this_unit.apply_ui()
	
func reapply(intensity : int) -> void:
	self.intensity += intensity
	if Globals.is_valid(this_unit):
		this_unit.apply_ui()
	
func remove_condition() -> void:
	get_parent().remove_child(self)
	queue_free()
	
	this_unit.apply_ui()
	
func expire() -> bool:
	if not extended and this_unit.has_condition("epicycle") and proper_name in EXPIRE:
		this_unit.expiring_conditions.append(self)
		return false
	else:
		return true
	
func upkeep() -> void:
	# abstract function
	return
	
func endstep() -> void:
	# abstract function
	return
	
func runSBAs() -> void:
	# abstract function
	return
