extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func upkeep() -> void:
	var extra_entropy_flag : bool = false
	
	if this_unit.entropy == 0:
		extra_entropy_flag = true
	
	this_unit.entropy += 1
	
	match intensity:
		1:
			if this_unit.entropy > 3:
				this_unit.entropy = 3
		2:
			this_unit.entropy += 1
			if this_unit.entropy > 5:
				this_unit.entropy = 5
		3:
			this_unit.entropy += 1
			if extra_entropy_flag:
				this_unit.entropy += 1
			
	this_unit.apply_ui()
