extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func runSBAs() -> void:
	if not this_unit.is_in_group("PlayerUnits"):
		return
		
	for unit in get_tree().get_nodes_in_group("PlayerUnits"):
		if not unit.has_condition("guardian_dynamic"):
			var condition = unit.apply_condition("guardian_dynamic")
			condition.triggers[this_unit] = 99
