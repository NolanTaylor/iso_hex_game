extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func initial_apply(intensity : int) -> void:
	.initial_apply(intensity)
	
	this_unit.movement_left = 0
	
func reapply(intensity : int) -> void:
	.reapply(intensity)
	
	this_unit.movement_left = 0
	
func upkeep() -> void:
	this_unit.movement_left = 0
	
func endstep() -> void:
	intensity -= 1
	
	if intensity == 0:
		remove_condition()
