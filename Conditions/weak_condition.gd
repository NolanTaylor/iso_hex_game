extends "res://Conditions/condition.gd"


func _ready():
	pass

func initial_apply(intensity : int) -> void:
	.initial_apply(intensity)
	
func reapply(intensity : int) -> void:
	.reapply(intensity)
	
func remove_condition() -> void:
	.remove_condition()
	
func endstep() -> void:
	remove_condition()
