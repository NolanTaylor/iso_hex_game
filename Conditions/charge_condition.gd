extends "res://Conditions/condition.gd"

func _ready():
	pass

func initial_apply(intensity : int) -> void:
	this_unit.add_mod("damage", "stab", intensity)
	
	.initial_apply(intensity)
	
func reapply(intensity : int) -> void:
	this_unit.add_mod("damage", "stab", intensity)
	
	.reapply(intensity)
	
func remove_condition() -> void:
	this_unit.damage_modifications["stab"] -= intensity
	
	.remove_condition()
	
func endstep() -> void:
	if expire():
		remove_condition()
