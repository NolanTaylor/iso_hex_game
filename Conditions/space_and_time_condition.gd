extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func upkeep() -> void:
	this_unit.rapidity += 1
	
	match intensity:
		1:
			if this_unit.rapidity > 3:
				this_unit.rapidity = 3
		2:
			this_unit.rapidity += 1
			if this_unit.rapidity > 5:
				this_unit.rapidity = 5
			
	this_unit.apply_ui()
