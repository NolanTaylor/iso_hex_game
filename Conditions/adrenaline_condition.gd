extends "res://Conditions/condition.gd"


func _ready():
	pass

func upkeep() -> void:
	if this_unit.health_left <= round(this_unit.max_health / 3):
		this_unit.heal(1)
		this_unit.toss_skill("adrenaline")
		
	this_unit.apply_ui()
