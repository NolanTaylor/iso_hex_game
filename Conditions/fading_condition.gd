extends "res://Conditions/condition.gd"

func _ready():
	pass

func endstep() -> void:
	intensity -= 1
	if intensity <= 0:
		remove_condition()
		this_unit.vanish()
