extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func upkeep() -> void:
	this_unit.flux += 1
	
	match intensity:
		1:
			if this_unit.flux > 3:
				this_unit.flux = 3
		2:
			this_unit.flux += 1
			if this_unit.flux > 5:
				this_unit.flux = 5
			
	this_unit.apply_ui()
