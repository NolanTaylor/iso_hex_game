extends "res://Conditions/condition.gd"

var radius : int = 2

func _ready():
	radius += this_unit.get_modification("range", ["spell", "light", "reaction", \
		"spectrum_shatter"])
	
func runSBAs() -> void:
	if not this_unit.is_in_group("PlayerUnits"):
		return
		
	for unit in get_tree().get_nodes_in_group("EnemyUnits"):
		if Globals.hex_distance(this_unit.coords, unit.coords) <= radius:
			var condition = unit.apply_condition("spectrum_shatter_dynamic")
			condition.triggers[this_unit] = radius
		elif unit.has_condition("spectrum_shatter_dynamic"):
			unit.get_condition("spectrum_shatter_dynamic").remove_trigger(this_unit)
