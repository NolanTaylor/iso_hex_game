extends "res://Conditions/condition.gd"

var radius : int = 2

func _ready():
	radius += this_unit.get_modification("range", ["spell", "thermo", "reaction", "deposition"])
	
func runSBAs() -> void:
	if not this_unit.is_in_group("PlayerUnits"):
		return
		
	for unit in get_tree().get_nodes_in_group("EnemyUnits"):
		if Globals.hex_distance(this_unit.coords, unit.coords) <= radius:
			var condition = unit.apply_condition("deposition_dynamic")
			condition.triggers[this_unit] = radius
		elif unit.has_condition("deposition_dynamic"):
			unit.get_condition("deposition_dynamic").remove_trigger(this_unit)
