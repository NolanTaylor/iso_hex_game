extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func upkeep() -> void:
	this_unit.movement_left -= self.intensity
	if this_unit.movement_left <= 0:
		this_unit.movement_left = 1
	remove_condition()
