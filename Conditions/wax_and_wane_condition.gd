extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func upkeep() -> void:
	this_unit.lune += 1
	
	match intensity:
		1:
			if this_unit.lune > 3:
				this_unit.lune = 3
		2:
			this_unit.lune += 1
			if this_unit.lune > 5:
				this_unit.lune = 5
			
	this_unit.apply_ui()
