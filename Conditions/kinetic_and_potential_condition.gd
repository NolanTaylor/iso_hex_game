extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func upkeep() -> void:
	this_unit.work += 1
	
	match intensity:
		1:
			if this_unit.work > 3:
				this_unit.work = 3
		2:
			this_unit.work += 1
			if this_unit.work > 5:
				this_unit.work = 5
		3:
			this_unit.work += 1
			
	this_unit.apply_ui()
	
func endstep() -> void:
	if intensity == 3 and this_unit.work > 5:
		this_unit.work -= 1
