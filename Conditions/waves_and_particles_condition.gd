extends "res://Conditions/condition.gd"

func _ready():
	pass
	
func upkeep() -> void:
	this_unit.light += 1
	
	match intensity:
		1:
			if this_unit.light > 3:
				this_unit.light = 3
		2:
			this_unit.light += 1
			if this_unit.light > 5:
				this_unit.light = 5
			
	this_unit.apply_ui()
